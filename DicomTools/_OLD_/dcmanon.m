function [] = dcmanon(inpath, outpath, whiteTable)

    init;
      
    dcm_info = dicominfo(inpath);
    
    tagTable = getDicomTags(inpath);
    allProps = tagTable.Name;
    
    for i = 1:1:numel(allProps)
        propName = allProps{i};
       if ~any(strcmp(whiteTable.Name,  propName))
           dcm_info = setfield(dcm_info, propName, '');
       end
    end
    
    dcm_data = dicomread(inpath);
    
    [outdir, outname, outext] = fileparts(outpath);
    if ~exist(outdir, 'dir')
        mkdir(outdir);
    end
    
    dicomwrite(dcm_data, ...
               outpath, ...
               dcm_info, ...
               'CreateMode', 'copy',...
               'WritePrivate', true);
    
end

