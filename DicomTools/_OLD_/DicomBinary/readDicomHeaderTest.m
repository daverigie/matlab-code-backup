

function [Elems, TransferSyntaxUID] =  readDicomHeaderTest(filepath)
    
    global mapVR;
    mapVR = load('mapVR.mat');
    mapVR = mapVR.mapVR;
    
    

    %//////////////////////////////////////////////////
    %// BASED ON                                     // 
    %// Riddle, William R., and David R. Pickens.    //
    %// �Extracting Data from a DICOM File.�          //
    %// Medical physics 32.6 (2005): 1537�1541. Web. // 
    %//////////////////////////////////////////////////

    % ====== SPECIFY FILE, DISPLAY CONTENTS WITH MATLAB'S DICOMDISP ======= %
    clc;
    
    if nargin == 0
        [filename, filedir] = uigetfile('*.*');
        filepath = fullfile(filedir, filename);
    end
    
    [filedir, filename, ext] = fileparts(filepath);

    %dicomdisp(filepath)
    % ========================================================================


    % ====================== Read contents to BYTEARRAY ==================== %
    M         = memmapfile(filepath);
    BYTEARRAY = M.Data;
    % ====================================================================== %

    H = BYTEARRAY(1:128);

    % CHECK IF FILE IS DICOM FILE
    if strcmp(char(BYTEARRAY(129:132))', 'DICM')
        disp('FILE IS DICOM');
    else
        disp('NOT DICOM!!!');
        return
    end
    
    GL0002 = uint32(BYTEARRAY(141));
    ind = 145;

    Elems = [];
    
    fid = fopen([fullfile(filedir,filename) '.csv'],'w+');
    s = 'Position$GroupNumber$ElementNumber$VR$VL$Name$Value';
        fprintf(fid, '%s\n', s);
    
    while ind < GL0002 + 145
        
        [Elem, ind] = readElement(BYTEARRAY, ind);
                        
        displayElement(Elem);
        writeElement(fid, Elem);
        
        if strcmp(Elem.ElementNumber, '0010')
           TransferSyntaxUID = deblank(char(Elem.ValueField)')
           disp(decodeTransferSyntaxUID(TransferSyntaxUID));
        end
        
        Elems = [Elems, Elem];
        
    end
    
    while ind <= length(BYTEARRAY)
        [Elem, ind] = readElement(BYTEARRAY, ind);
                        
        displayElement(Elem);
        writeElement(fid, Elem);
        Elems = [Elems, Elem];
    end
    %Elems = struct2table(Elems);
    fclose(fid);
end

function [Elem, ind] = readElement(BYTEARRAY, ind)

    global mapVR;
    pos = ind-1;
        GroupNumber   = dec2hex(typecast(BYTEARRAY(ind:ind+1), 'uint16'), 4);
        ind           = ind + 2;
        ElementNumber = dec2hex(typecast(BYTEARRAY(ind:ind+1), 'uint16'), 4);
        ind           = ind + 2;
        VR            = BYTEARRAY(ind:ind+1);
        VR            = char(VR)';
        ind           = ind + 2;

        if any(strcmp(VR, {'OB', 'OW', 'SQ', 'UN'}))
            Reserved = BYTEARRAY(ind:ind+1);
            ind      = ind + 2;
            VL       = typecast(BYTEARRAY(ind:ind+3), 'uint32');
            ind      = ind + 4;
        else
            VL       = typecast(BYTEARRAY(ind:ind+1), 'uint16');
            ind      = ind + 2;
        end

        VL = uint32(VL);

        ValueField   = BYTEARRAY(ind:(ind+VL-1));
        ind          = ind + VL;
        
        valRep = mapVR(VR);
        
        if strcmp(valRep.fmt, 'char')
            ValueField = strtrim(deblank(char(ValueField)'));
        else
            ValueField = typecast(ValueField, valRep.fmt);
        end
        
        Elem.pos = pos;
        Elem.GroupNumber   = GroupNumber;
        Elem.ElementNumber = ElementNumber;
        Elem.VR            = VR;
        Elem.VL            = VL;
        Elem.ValueField    = ValueField;
        Elem.Name = dicomlookup(GroupNumber, ElementNumber);
        

end

function writeElement(fid, Elem)

               

        if Elem.VL < 200
            value = num2str(Elem.ValueField);
        else
            value = '';
        end
        
        s = sprintf('%i$%s$%s$%s$%i$%s$%s', ...
            Elem.pos, Elem.GroupNumber, Elem.ElementNumber, ...
            Elem.VR, Elem.VL, Elem.Name, value);
        
        fprintf(fid, '%s\n', s);
   
end

function displayElement(Elem)

        if Elem.VL < 200
            fprintf('\npos: %i  (%s, %s)  %s  VL = %i Bytes  %s [%s]\n', ...
                Elem.pos, Elem.GroupNumber, Elem.ElementNumber, ...
                Elem.VR, Elem.VL, Elem.Name, num2str(Elem.ValueField));
        else
            
            fprintf('\npos: %i  (%s, %s)  %s  VL = %i Bytes  %s\n', ...
                    Elem.pos, Elem.GroupNumber, Elem.ElementNumber, ...
                    Elem.VR, Elem.VL, Elem.Name);
            
        end
end

function name = decodeTransferSyntaxUID(tsUID)

    mapUID = containers.Map;
    
    mapUID('1.2.840.10008.1.2') = 'Implicit VR little endian';
    mapUID('1.2.840.10008.1.2.1') = 'Explicit VR little endian';
    mapUID('1.2.840.10008.1.2.1.99') = 'Deflated explicit VR little endian';
    mapUID('1.2.840.10008.1.2.2') = 'Explicit VR big endian';
    mapUID('1.2.840.10008.1.2.4.50') = 'JPEG baselines (process 1)';
    mapUID('1.2.840.10008.1.2.4.51') = 'JPEG extended (processes 2 and 4)';
    mapUID('1.2.840.10008.1.2.4.57') = 'JPEG lossless, nonhierarchical (process 14)';
    mapUID('1.2.840.10008.1.2.4.70') = 'JPEG lossless, nonhierarchical\n[process 14 (selection value 1)]';
    
    mapUID('1.2.840.10008.1.2.4.80') = 'JPEG-LS lossless';
    mapUID('1.2.840.10008.1.2.4.81') = 'JPEG-LS lossy (near lossless)';
    mapUID('1.2.840.10008.1.2.4.90') = 'JPEG 2000 (lossless only)';
    mapUID('1.2.840.10008.1.2.4.91') = 'JPEG 2000';
    mapUID('1.2.840.10008.1.2.5')    = 'RLE lossless';
    
    try
        name = mapUID(tsUID);
    catch ME
        name = ME.message;
    end
    
end






