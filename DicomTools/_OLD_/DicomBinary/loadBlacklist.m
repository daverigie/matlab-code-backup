function [blacklist_out] = loadBlacklist(csvpath)

    fid = fopen(csvpath);
    data = textscan(fid, '%s','delimiter','\n');
    
    split = @(s) strsplit(s,'\t');
    D = cellfun(split, data{1}, 'UniformOutput',0);
    blacklist = cat(1,D{:});
    
    fclose(fid);
    
    badRows = logical(arrayfun(@str2num, [blacklist{:,3}]'));
    
    blacklist = blacklist(badRows,1:2);
    
    for i=1:1:length(blacklist)
       blacklist_out{i,1} = sprintf('(%s,%s)', blacklist{i,1}, blacklist{i,2}); 
    end
   
    
    
    
    
    

    

end

