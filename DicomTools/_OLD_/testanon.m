function [] = testanon(whiteTable)

    indir = fullfile(pwd, 'TestData', 'brain7_test');
    filelist = recursiveFileList(indir);
    
    n_files = numel(filelist);
        
    for i = 1:1:n_files
       fprintf('\nProcessing file %i of %i', i,n_files);
       filename = filelist{i};
       filename_out = strrep(filename, 'brain7_test',... 
                             'brain7_test_anon');
                         
       
       try
            dcmanon(filename, filename_out, whiteTable);
       catch ME
           disp(ME);disp(ME.message);
       end
       
    end
    
    
end

function fileList =  recursiveFileList(dirpath)

    listing = dir(dirpath);
    listing = listing(3:end);
    
    dirs = {listing([listing.isdir]).name}'
    files = {listing(~[listing.isdir]).name}';
    
    fileList = fullfile(dirpath, files);
    
    for d = dirs'
       dirname = d{1}
       fileList = [fileList; recursiveFileList(fullfile(dirpath,dirname))];
    end
    
    
    
end




