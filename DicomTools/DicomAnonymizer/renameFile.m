function [] = renameFile(inpath, outpath)

    if ispc
        system( sprintf('rename %s %s', inpath, outpath) );
    else
        system( sprintf('mv %s %s', inpath, outpath) );   
    end
    
end