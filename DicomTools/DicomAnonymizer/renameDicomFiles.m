function [] = renameDicomFiles(rootdir, basename, makeCopy)
% RENAMEDICOMFILES renames all dicom files in rootdir
%
% renameDicomFiles(rootdir, basename) renames all of the files inside of
% rootdir using basename and some appended number to ensure uniqueness. For
% example if basename = 'anon' then the files may be renamed anon_0001.dcm
% anon_0002.dcm ... etc. 
%
% if makeCopy is set to true, it will make copies of all of the files
% instead of just renaming them in place.
%
    
    if nargin < 1
        rootdir = uigetdir;
    end
    
    if nargin < 2
        basename = 'ANON';
    end
    
    if nargin < 3
        makeCopy = false;
    end
    
    allpaths = recursiveListFiles(rootdir);

    counter = 1;
    
    for i = 1:1:numel(allpaths)
        
        fprintf('\nRenaming file %i of %i',i,numel(allpaths));
        
        path = allpaths{i};
        [dirpath, filename, ext] = fileparts(path);

        newpath = strrep(path, filename, [basename, '_', sprintf('%04i', counter)]);
        
        while exist(newpath, 'file');
           counter = counter + 1;
           newpath = strrep(path, filename, [basename, '_', sprintf('%04i', counter)]);
        end
        
        if makeCopy
            copyfile(path, newpath)
        else
            movefile(path, newpath);
        end
        
        counter = 1; 
              
        
    end
    
    fprintf('\n');

end

