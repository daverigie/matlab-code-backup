To use the dicom anonymizer with the already existing PETMR_master_blacklist.mat
you don't need to do anything but call "anonymizeDicomDir" with the path
to the directory containing your dicoms. See the comments/help for that
function for more details.

NOTE the new directory of anonymized dicom files will still have the ORIGINAL
file names. These will likely contain PHI, such as patient name. You need to 
separately call 'renameDicomFiles' on this directory. This will automatically
rename all of the anonymized dicom files in your directory structure. See
the help/comments on this function for usage details.

If you want to create your own blacklist, you should:

1) call dicoms2masterCSV (see help) passing in all of the different dicom types
   that you expect to encounter in your data.

   This will create a spreadsheet that you can open in excel with a detailed
   summary of all of the tags. You need to manually look through them and decide
   which ones you want to blacklist or keep.

2) Create a new column in this .CSV spreadsheet called "blacklist" and put 
   a 0 or a 1 next to each tag. '1' indicates it should be blacklisted, and
   0 indicates that it should be kept.

   Save this spreadsheet as a tab delimited CSV file.

3) Now call blacklistcsv2mat passing in the path to the newly created CSV file.
   
   This will create a .MAT file that contains a cell array of the tags that you
   have decided to blacklist. This is the file that will be used by the 
   anonymizer

4) Now you can use the newly created .MAT filepath to call anonymizeDicomDir

