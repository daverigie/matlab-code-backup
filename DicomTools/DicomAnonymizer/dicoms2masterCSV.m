function [] = dicoms2masterCSV(dicomPaths, outpath)
% DICOMS2MASTERCSV Create CSV of DICOM tags.
%
% dicom2masterCSV(dicomPaths, outpath) takes a cell array of paths to dicom
% files in dicomPaths and writes a single csv file at outpath containing
% all of the information about the header tags. Multiple dicom files can be
% specified. Duplicated tags will only be represented once in the CSV file.
% The purpose is to that the output CSV file can be quickly scanned by the
% user to determine which elements need to be blacklisted. 
%
% For example for the PET-MR, one can provide the paths to a umap and
% multiple kinds of PET raw data files. This will create a single CSV file
% of tags, which can then be scanned  by the user to look for possible 
% sensitive patient information. 

    if nargin < 1
       [filenames, pathname] = uigetfile('*.*','Multiselect','on'); 
       dicomPaths = fullfile(pathname, filenames);
    end

    if nargin < 2
        [filename, pathname] = uiputfile('*.*');
        outpath = fullfile(pathname,filename);
    end

    for i = 1:1:length(dicomPaths)
       T{i} = getDicomTags(dicomPaths{i}); 
    end
    
    Tmerged = mergeTagTables(T{:});
    
    table2csv(Tmerged, outpath);


end


function [T] = mergeTagTables(varargin)

    % Return a new table of tags that has no duplicate (Group#,Elem#)
    TT = cat(1, varargin{:});
    [U,I] = unique(TT{:,3});
    T = TT(I,:);
        
end

function [] = table2csv(T, filepath)

    headers = T.Properties.VariableNames;
    
    C = table2cell(T);
    
    fid = fopen(filepath,'w+');
    
    fprintf(fid,'%s','sep=$');
    fprintf(fid,'\n');
    
    rowString = strjoin(headers,'$');
    
    fprintf(fid,'%s', rowString);
    fprintf(fid,'\n');
    
    for i = 1:1:length(C)
       
        rowString = strjoin(C(i,:),'\t$');
        fprintf(fid,'%s', rowString);
        if i < length(C)
            fprintf(fid, '\n');
        end
        %keyboard;
    end

    
    fclose(fid);
    
end


