function [] = anonymizeDicomDir(varargin)
% ANONYMIZEDICOMDIR  batch-anonymize a directory of dicom files
%   
%   anonymizeDicomDir(inputDir, outputDir, blacklistPath) anonymizes all of the dicom files inside
%   of the directory inputDir. The entire directory structure is
%   maintained, while the output is written to another directory specified.
%   If outputDir is not specified, one will be created with the
%   same name, appending '_anon' to the original directory. 
%   blacklistPath gives the path to a .mat file with the blacklist
%   information. This can be created using blacklistcsv2mat.m.
%
%   OPTIONAL PARAMETERS
%   Some additional arguments can be provided using name value pairs as
%   follows:
%
%   
%   anonymizeDicomDir(... , 'CopyNonDicom', true, ...  
%                           'AllowedExtensions', {'.IMA', '.DCM'}, ...
%                           'MaxCopySize', 5e9)
%
%   will copy non-dicom files to the new anonymized directory structure so
%   that all of the files from the original directory are present.
%   Non-dicom files will be identified based on the 'AllowedExtensions'
%   parameter. However, files exceeding 5GB will not be copied, as
%   specified by 'MaxCopySize' In the case of these large files, a dummy
%   file with the same name will be written in order to keep the file list
%   the same as the original directory. The reason for this is explained
%   below.
%
%   NOTE running this anonymization routine on a folder takes all of the
%   blacklisted tags out of the dicom headers, but it does NOT change the
%   filenames, which usually contain PHI, such as patient name. A separate
%   routine called 'renameDicomFiles' can automatically obfuscate the
%   filenames. The previously mentioned dummy file is written so that the
%   PET raw data files can be properly named. These usually
%   contain a binary file and an associated dicom file. They must have the
%   same base name in order for e7 tools to work properly. 

% /////////////////////////////////////////////////////////////////////////
% // ------------------ Parse Input Arguments ---------------------------//
% /////////////////////////////////////////////////////////////////////////

p = inputParser;

addOptional(p, 'inputDir', '', @ischar)
addOptional(p, 'outputDir', '', @ischar);
addOptional(p, 'blacklistPath', '', @ischar);
addParameter(p, 'AllowedExtensions', {'.DCM', '.IMA'});
addParameter(p, 'CopyNonDicom', false);
addParameter(p, 'MaxCopySize', 5e9); % max file size in bytes

parse(p, varargin{:});

inputDir = p.Results.inputDir;
outputDir = p.Results.outputDir;
blacklistPath = p.Results.blacklistPath;
allowedExtensions = p.Results.AllowedExtensions;
copyNonDicom = p.Results.CopyNonDicom;
maxCopySize = p.Results.MaxCopySize;

if isempty(inputDir)
   inputDir = uigetdir; 
end

if isempty(outputDir)
   outputDir = [inputDir, '_anon']; 
end

if isempty(blacklistPath)
   thisdir = fileparts(mfilename('fullpath'));
   listing = dir(fullfile(thisdir, '*blacklist*.mat'));
   if length(listing) == 1
        blacklistPath = listing.name;
        fprintf('\nFound %s, using as blacklist...\n\n', blacklistPath);
   end
end


MF = load(blacklistPath);
blacklist = MF.blacklist;

finishup = onCleanup(@cleanupFunction);

% /////////////////////////////////////////////////////////////////////////
% // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
% /////////////////////////////////////////////////////////////////////////

   % CREATE LOG FILE AND ANONYMOUS FUNCTION FOR WRITING TO IT
   mkdir(outputDir);
   global log_fid;
   logpath = fullfile(outputDir, 'log.txt');
   log_fid = fopen(logpath, 'a+');
   

   filelist = sort(recursiveListFiles(inputDir));
   
   numDicoms = 0;
   for i = 1:1:length(filelist)
       if isDicom(filelist{i}, allowedExtensions)
           numDicoms = numDicoms + 1;
       end
   end
   
   counter = 1;
   
   for i = 1:1:length(filelist)
       
       path_in = filelist{i};
       path_out = strrep(path_in, inputDir, outputDir);
       
       if ~isDicom(path_in, allowedExtensions)
           if copyNonDicom 
               logmsg(path_in);
               if ~exist(fileparts(path_out),'dir')
                   mkdir(fileparts(path_out));
               end 
               
               info = dir(path_in);
               
               if info.bytes > maxCopySize
                  logmsg('File too large, writing dummy-file ...'); 
                  fid = fopen(path_out, 'w+'); 
                  fclose(fid);
               else
                   logmsg('\nCopying non-dicom file to destination\n');
                   copyfile(path_in, path_out);  
               end
           end
           continue;           
       end
     
       %fprintf('\nProcessing file %i of %i', counter,numel(filelist));
       %anonymizeDicom(path_in, path_out, blacklist);
       %counter = counter + 1;
       
       try
            fprintf('\nProcessing file %i of %i', counter, numDicoms);
            anonymizeDicom(path_in, path_out, blacklist);
            counter = counter + 1;
       catch ME
           logmsg(sprintf('Problem with file %s', filelist{i}));
           logmsg(ME.message);
       end
       
       
   end

   fprintf('\n');
   
   fclose(log_fid);
   
end

function [] = anonymizeDicom(dicompath, outputPath, blacklist)
% anonymizes a single dicom file

    if nargin < 1
       [filename, dirpath] = uigetfile('*.*');
       dicompath = fullfile(dirpath, filename);
    end

    [dirpath, filename, ext] = fileparts(dicompath);
    
    if nargin < 2
        outputPath = fullfile(dirpath, [filename '_anon' ext]);
    end
    
    M = memmapfile(dicompath);
    BYTEARRAY = M.Data;
    
    TagTable = getDicomTags(dicompath);
    numRows = height(TagTable);
    
    numel(blacklist);
    
    BYTEARRAY2 = BYTEARRAY;
    
    for i = 1:1:numel(blacklist)
       Tag = blacklist{i};
       rowIndList = find(strcmp(TagTable{:,3}, Tag));
        
       if isempty(rowIndList)
            continue; 
       end
    
       for j = 1:1:numel(rowIndList)
            rowInd = rowIndList(j);
            VL_str = TagTable{rowInd, 5}{:};
            VL = str2num(strrep(VL_str, ' bytes',''));

            if rowInd == numRows
                right = length(BYTEARRAY);
                left = right - VL + 1;
            else
                right = str2num(TagTable{rowInd+1,1}{:});       
                left = right - VL + 1;
            end

            oldVal = BYTEARRAY2(left:right);
            newVal = makeBlankValue(oldVal);

            char(oldVal)';
            char(newVal)';

            BYTEARRAY2(left:right) = newVal;
        end
    end
    
    [outdir, ~, ~] = fileparts(outputPath);
    if ~exist(outdir, 'dir')
        mkdir(outdir);
    end
    
    
    fid = fopen(outputPath, 'w+');
    
    fwrite(fid, BYTEARRAY2);
    
    fclose(fid);
    
end



function newVal =  makeBlankValue(val)

    newVal = char(val)';
    % Replace all lowercase letters with x
    newVal = regexprep(newVal, '[a-z]', 'x');
    % Replace all uppercase letters with X
    newVal = regexprep(newVal, '[A-Z]', 'X');
    % Replace all numbers with 0
    newVal = regexprep(newVal, '\d', '0');
    
    newVal = uint8(newVal)'; 
    
end


function flag = isDicom(filepath, allowedExtensions)

    [~,~,ext] = fileparts(filepath);
    
    flag = false;
    
    if any(strcmpi(ext, allowedExtensions))
       flag = true; 
    end
    

end

function flag = isValidDirPath(dirpath)

    try 
        mkdir(dirpath);
        delete(dirpath);
        flag = true;
    catch ME
        flag = false;
    end


end

function [] =  logmsg(msg)
    global log_fid;
    fprintf(log_fid, '\n%s', msg);
    fprintf('\n%s', msg);
    

end

function [] = cleanupFunction()
    fprintf('\nCleaning up...\n');
    fclose('all');
end

