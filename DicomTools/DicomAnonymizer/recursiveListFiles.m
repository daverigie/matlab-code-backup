function allpaths = recursiveListFiles(basedir)
% RECURSIVELISTFILES  list all filepaths in basedir recursively.
%   
%   allpaths = recursiveListFiles(basedir) creates a struct array of all 
%              filepaths in inside the directory specified by the string
%              basedir.
%

    % List files/dirs directly beneath basedir, ignore '.' and '..'
    listing = dir(basedir);
    listing = listing(3:end); 
    
    % Separate files from directories
    dirInd = [listing.isdir]; 
    dirs   = listing(dirInd);
    files  = listing(~dirInd);
    
    dirpaths = fullfile(basedir, {dirs.name});
    allpaths = fullfile(basedir, {files.name})';
    
    % Add all filepaths to allpaths, passing each subdir into this
    % function
    for i = 1:1:length(dirs)
       allpaths = [allpaths; recursiveListFiles(dirpaths{i})]; 
    end
      
end