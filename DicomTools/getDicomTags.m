function [T,meta, S] = getDicomTags(filepath)

if nargin == 0
    [filename, dirpath] = uigetfile('*.*');
    filepath = fullfile(dirpath,filename);
end

S = evalc('dicomdisp(filepath)');

rows = strsplit(strtrim(S),'\n');

column_names = strsplit(strtrim(rows{7}));

n_columns = numel(column_names);
n_data_rows = numel(rows) - 8;

Data = cell(n_data_rows, n_columns);

for i = 9:1:numel(rows)
    
    r = rows{i};
    matches = regexp(r, '(\d{7})\s*(\d)\s*(\([\w,]*\))\s*([A-Z]{2})\s*(\d*\s*bytes) - (\w*)(\s.*)','tokens');
    matches = cellfun(@strtrim, matches, 'UniformOutput',false);
    Data(i-8,:) = matches{:};
    
end

T = cell2table(Data, 'VariableNames', column_names);

meta = strtrim(strjoin(rows(1:6),'\n'));



end