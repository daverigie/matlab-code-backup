#!/usr/bin/env bash

rsync -aPr --include-from=rsync-include.lst --files-from=rsync-folders.lst --max-size=1m /media/q/labspace/ ./
