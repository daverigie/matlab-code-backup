function [output] = findScanStartStop(filepath)

    [dirpath, basename, ext] = fileparts(filepath);

    if ~strcmpi(ext,'.TXT')
        fprintf('\nThis function is only for parsing .TXT files produced by dumptags\n\n\n');
        output = [];
        return;
    end
    
    % START = [ON OFF OFF OFF]
    % STOP  = [ON OFF ON OFF]

    RESP_ON  = 16;
    RESP_OFF = 32;
    
    SCAN_START_PATTERN = [RESP_ON, RESP_ON, RESP_OFF, RESP_OFF];
    SCAN_STOP_PATTERN  = [RESP_ON, RESP_OFF, RESP_ON, RESP_OFF];
        
    S = parse_tag_dump(filepath);
    
    idx = or(S.TagType==RESP_OFF, S.TagType==RESP_ON);
    
    tt   = S.LastTimeTag(idx);
    tags = S.TagType(idx);
    
    % Find possible scan start patterns
    
    idx       = findpattern(tags, SCAN_START_PATTERN);
    durations = tt(idx+3) - tt(idx);
    idx = idx(durations <= 100);
    startTimes = tt(idx);
        
    % Find possible scan start patterns
    idx       = findpattern(tags, SCAN_STOP_PATTERN);
    durations = tt(idx+3) - tt(idx);
    idx = idx(durations <= 100);
    stopTimes = tt(idx);
    
    numScans = max(numel(startTimes), numel(stopTimes));
    
    % Format output into struct array. One element for each scan
    output = [];
    for i = 1:1:numScans
        scan_i.startTime = []; 
        scan_i.stopTime  = [];
        try
            scan_i.startTime = startTimes(i);
            scan_i.stopTime  = stopTimes(i);
        catch ME
           warning(ME.message); 
        end
        output = cat(1,output,scan_i);
    end
    
end

function [idx] = findpattern(tags, pattern)

    col = @(x) x(:);
    row = @(x) x(:)';
    
    idx = strfind(row(tags), row(pattern));

end

