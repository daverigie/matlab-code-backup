function [success,messageid] = dcm2niix(src, dest)

    if nargin < 2
        dest = pwd;
    end
    
    if iscell(src)
       for iFile = 1:numel(src)
          [success, messageid] = dcm2niix(src{iFile}, dest); 
       end
       return;
    end
    
    if exist(dest,'dir') % dest is a directory
       [parent, dirname] = fileparts(src);
       foldername = dest;
       filename   = dirname;
    else
       [foldername, filename, ext] = fileparts(dest);
    end
        
    command = sprintf('dcm2niix -f %s -o %s %s', filename, foldername, src);
    
    [success, messageid] = system(command, '-echo');
    
    % Remove JSON
    jsonpath = fullfile(foldername, [filename '.json']);
    delete(jsonpath);

end