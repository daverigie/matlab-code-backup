#include <iostream>
#include <cstdlib>

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define VERSION 0.3

// - This temporary tools is used to extract event information from mMR data.
// - For each time point (in ms) two values are stored according to prompt and
//   delayed coincidences.
// - 0 ms entry is skipped, all values represents counts up to the corresponding
//   timemark
// - Version 0.2: in order to speed up the analysis the data is read in 256MB blocks
//   into memory and processed from there on
// - Version 0.3: support for invalid starttimes added

int main(int argc, char* argv[])   {

    // show version
    printf("\nLM_ExtractEvents\nVersion %1.1f\n\n",VERSION);

    // check whether all required parameters are used
    if (argc != 3) {
        printf("usage: \t LM_ExtractEvents <lm-filename> <eventfile>\n\n");
        exit(EXIT_FAILURE);
    }

    // - read number of tags in file
    // - is later used to loop over all tags (and events)
    FILE *listmodefile;
    errno_t err;

    err = fopen_s(&listmodefile,argv[1],"rb");
    if (err != 0) {
        printf("reading listmode [%s] failed.\n",argv[1]);
        exit(EXIT_FAILURE);
    }

    _fseeki64(listmodefile,0,SEEK_END);
    __int64 filesize = _ftelli64(listmodefile);
    printf("filesize:\t %I64d\n",filesize);
    __int64 tags = filesize/4;
    printf("tags in file:\t %I64d\n",tags);

    // - try to obtain first and last timetag in file
    // - in case of success calloc precise amount memory for data
    int time_offset = 0;
    int last_timemark = -1;
    int first_timemark = -1;
    unsigned int n;
    __int64 tag;
    __int64 starttag = max(0,tags-100000);

    // check for last_timemark
    _fseeki64(listmodefile,starttag*4,SEEK_SET);
    for (tag=starttag-1;tag<tags;tag++) {
        fread(&n,4,1,listmodefile);

        // time marker --> PETLINK manual
        if (n >= 2147483648 && n < 2684354560) {
            last_timemark = (int)(n-2147483648);
        }
    }

    if (last_timemark > -1) {
        printf("\nlast timemark:\t %d ms\n",last_timemark);
        printf("last timemark:\t %d s\n",last_timemark/1000);
        printf("last timemark:\t %d m\n",last_timemark/60000);

        // - check for first_timemark
        // - only in case of a valid last_timemark
        //   (otherwise precise calloc not possible anyway)
        _fseeki64(listmodefile,0,SEEK_SET);
        for (tag=0;tag<tags;tag++) {
            fread(&n,4,1,listmodefile);

            // time marker --> PETLINK manual
            if (n >= 2147483648 && n < 2684354560) {
                first_timemark = (int)(n-2147483648);
                tag = tags;
            }
        }

        // only first_timemark = 0 valid; otherwise correct start and stoptime
        if (first_timemark != 0) {
            printf("\ninvalid first timemark:\t %d ms -> adjust starttime stoptime\n",first_timemark);

            time_offset = first_timemark;
            last_timemark = last_timemark - time_offset;
            first_timemark = 0;

            printf("\nnew last timemark:\t %d ms\n",last_timemark);
            printf("new last timemark:\t %d s\n",last_timemark/1000);
            printf("new last timemark:\t %d m\n",last_timemark/60000);
        }

        printf("\nfirst timemark:\t %d ms\n",first_timemark);
        printf("first timemark:\t %d s\n",first_timemark/1000);
        printf("first timemark:\t %d m\n",first_timemark/60000);
    }
    else {
        printf("no timemarks found\n");
        exit(EXIT_FAILURE);
    }

    // back to bof and loop until end while counting events
    int current_timemark = -1;
    short *counter = (short*)calloc(2*last_timemark,sizeof(short));
    _fseeki64(listmodefile,0,SEEK_SET);


    // use 256MB chunks
    __int64 gb_limit = 1024*1024*256;
    unsigned int *tagvector = (unsigned int*)calloc(gb_limit,sizeof(unsigned int));

    int loop,local_tags;
    int loops = tags / gb_limit;
    if (tags % gb_limit != 0) {
        loops = loops + 1;
    }

    printf("\nloops [%d]\n",loops);

    for (loop=0;loop<loops;loop++) {

        if (loop == loops-1) {
            local_tags = tags - loop*gb_limit;
        }
        else {
            local_tags = gb_limit;
        }

        printf("loop: [%d] [%d]\n",loop,local_tags);
        fread(tagvector,sizeof(unsigned int),gb_limit,listmodefile);

        for (tag=0;tag<local_tags;tag++) {
            n = tagvector[tag];

            // time marker --> PETLINK manual
            if (n >= 2147483648 && n < 2684354560) {
                current_timemark = (int)(n-2147483648);

                // events after last timemark are rejected
                if ((current_timemark-time_offset) == last_timemark) {
                    tag = tags;
                }
            }

            if (current_timemark > -1) {
                // prompt event --> PETLINK manual
                if (n < 2147483648 && n >= 1073741824) {
                    counter[2*(current_timemark-time_offset)] += 1;
                }
                // delayed event --> PETLINK manual
                if (n < 1073741824) {
                    counter[2*(current_timemark-time_offset)+1] += 1;
                }
            }
        }
    }

    // close listmodefile finally
    fclose(listmodefile);

    // write counter to disk
    FILE *counterfile;
    err = fopen_s(&counterfile,argv[2],"wb");
    if (err != 0) {
        printf("\nwriting of [%s] failed.\n",argv[2]);
        exit(EXIT_FAILURE);
    }
    fwrite(&last_timemark,sizeof(int),1,counterfile);
    fwrite(counter,sizeof(short),2*last_timemark,counterfile);
    fclose(counterfile);
    free(counter);

    return 0;
}
