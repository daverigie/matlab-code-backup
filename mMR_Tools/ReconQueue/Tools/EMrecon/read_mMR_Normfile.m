function [normdata, normdata_scatter] = read_mMR_Normfile(filename)


%% Siemens Biograph mMR
nsize = zeros(8,2);
nsize(1,:) = [344 127];
nsize(2,:) = [9 344];
nsize(3,:) = [504 64];
nsize(4,:) = [837 1];
nsize(5,:) = [64 1];
nsize(6,:) = [64 1];
nsize(7,:) = [9 1];
nsize(8,:) = [837 1];

fid = fopen(filename,'rb');
offset = sum(nsize(:,1).*nsize(:,2))*4;
fseek(fid,-offset,'eof');


%%
figure('Name',['Siemens Biograph mMR, Filename: ' filename]);


%% geometry effects
norm_title_1 = sprintf('1) geometry effects [%dx%d]',nsize(1,1),nsize(1,2));
norm_1 = reshape(fread(fid,nsize(1,1)*nsize(1,2),'float32'),nsize(1,1),nsize(1,2));

subplot(4,2,1);
imagesc(norm_1);
title(norm_title_1); colorbar;

%% crystal interference
norm_title_2 = sprintf('2) crystal interference [%dx%d]',nsize(2,1),nsize(2,2));
norm_2 = reshape(fread(fid,nsize(2,1)*nsize(2,2),'float32'),nsize(2,1),nsize(2,2));

subplot(4,2,2);
imagesc(norm_2);
title(norm_title_2); colorbar;

%% crystal efficiencies
norm_title_3 = sprintf('3) crystal efficiencies [%dx%d]',nsize(3,1),nsize(3,2));
norm_3 = reshape(fread(fid,nsize(3,1)*nsize(3,2),'float32'),nsize(3,1),nsize(3,2));

subplot(4,2,3);
imagesc(norm_3);
title(norm_title_3); colorbar;

%% axial effects
norm_title_4 = sprintf('4) axial effects [%dx%d]',nsize(4,1),nsize(4,2));
norm_4 = reshape(fread(fid,nsize(4,1)*nsize(4,2),'float32'),nsize(4,1),nsize(4,2));

subplot(4,2,4);
plot(norm_4); axis([1 nsize(4,1) 0 2.0*max(norm_4(:))]);
title(norm_title_4); colorbar;

%% paralyzing ring DT parameters
norm_title_5 = sprintf('5) paralyzing ring DT parameters [%dx%d]',nsize(5,1),nsize(5,2));
norm_5 = reshape(fread(fid,nsize(5,1)*nsize(5,2),'float32'),nsize(5,1),nsize(5,2));

subplot(4,2,5);
plot(norm_5); axis([1 nsize(5,1) min(norm_5) eps+max(norm_5)]);
title(norm_title_5); colorbar;


%% non-paralyzing ring DT parameters
norm_title_6 = sprintf('6) non-paralyzing ring DT parameters [%dx%d]',nsize(6,1),nsize(6,2));
norm_6 = reshape(fread(fid,nsize(6,1)*nsize(6,2),'float32'),nsize(6,1),nsize(6,2));

subplot(4,2,6);
plot(norm_6); axis([1 nsize(6,1) min(norm_6) eps+max(norm_6)]);
title(norm_title_6); colorbar;


%% TX crystal DT parameters
norm_title_7 = sprintf('7) TX crystal DT parameters [%dx%d]',nsize(7,1),nsize(7,2));
norm_7 = reshape(fread(fid,nsize(7,1)*nsize(7,2),'float32'),nsize(7,1),nsize(7,2));

subplot(4,2,7);
plot(norm_7); axis([1 nsize(7,1) min(norm_7) eps+max(norm_7)]);
title(norm_title_7); colorbar;


%% additional scatter scaling
norm_title_8 = sprintf('8) additional scatter scaling [%dx%d]',nsize(8,1),nsize(8,2));
norm_8 = reshape(fread(fid,nsize(8,1)*nsize(8,2),'float32'),nsize(8,1),nsize(8,2));

subplot(4,2,8);
plot(norm_8); axis([1 nsize(8,1) 0 2.0*max(norm_8(:))]);
title(norm_title_8); colorbar;

fclose(fid);

normdata_scatter = norm_8(:);
normdata = zeros(sum(nsize(1:4,1).*nsize(1:4,2)),1);
stop = 0;
for i=1:4
    start = stop + 1;
    stop = stop + nsize(i,1).*nsize(i,2);
    normdata(start:stop) = eval(sprintf('norm_%d(:)',i));
end

end
