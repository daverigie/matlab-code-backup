function [image] = readimage_int16(filename,x,y,z,t)

image = 1;

if (nargin ~= 5)
    fprintf('[image] = readimage_int16(filename,x,y,z,t)\n');
else
    fid = fopen(filename);
    if (fid > 0)
        [image, count] = fread(fid,x*y*z*t,'int16');
        if (count == x*y*z*t)
            image = reshape(image,x,y,z,t);
        else
            image = 1;
            fprintf('could only read %d of %d values\n',count,x*y*z*t);
        end
        fclose(fid);
    else
        fprintf('invalid filename: %s\n',filename);
    end
end
end