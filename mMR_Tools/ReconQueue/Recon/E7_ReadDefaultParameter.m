function [E7_parm] = E7_ReadDefaultParameter(RQ_parm)

% set default
E7_parm.algo = 'op-osem';

fid = fopen([RQ_parm.base_path '\Parameter.txt'],'r');
while (~feof(fid))
    v = fgetl(fid);
    
    % E7 parameter
    % reconstruction algorithm
    pos = strfind(v,'algo=');
    if (pos > 0)
        E7_parm.algo = v(pos+5:end);
    end
    
    % scatter scaling
    pos = strfind(v,'scatterscaling=');
    if (pos > 0)
        E7_parm.scatterscaling = v(pos+15:end);
    end
    
    % iteration and subsets
    pos = strfind(v,'is=');
    if (pos > 0)
        pos2 = strfind(v,',');
        E7_parm.is_1 = v(pos+3:pos2-1);
        E7_parm.is_2 = v(pos2+1:end);
    end
    
    % matrix size
    pos = strfind(v,'w=');
    if (pos > 0)
        E7_parm.w = v(pos+2:end);
    end
    
    % filter setup
    pos = strfind(v,'fltr=');
    if (pos > 0)
        pos2 = strfind(v,',');
        E7_parm.fltr_1 = v(pos+5:pos2(1)-1);
        E7_parm.fltr_2 = v(pos2(1)+1:pos2(2)-1);
        E7_parm.fltr_3 = v(pos2(2)+1:end);
    end
    
    % resampling parameter
    pos = strfind(v,'R=');
    if (pos > 0)
        pos2 = strfind(v,',');
        E7_parm.r_1 = v(pos+2:pos2-1);
        E7_parm.r_2 = v(pos2+1:end);
    end
    
    % zoom factor
    pos = strfind(v,'izoom=');
    if (pos > 0)
        E7_parm.izoom = v(pos+6:end);
    end
    
    % gpu
    pos = strfind(v,'gpu=');
    if (pos > 0)
        E7_parm.gpu = v(pos+4:end);
    end
    
    % binning
    pos = strfind(v,'binning=');
    if (pos > 0)
        E7_parm.binning = v(pos+8:end);
    end
    
    % ext (for mlaa in case of WB)
    pos = strfind(v,'ext=');
    if (pos > 0)
        pos2 = strfind(v,',');
        number_of_beds = numel(pos2)+1;
        E7_parm.ext = zeros(number_of_beds,1);
        if (number_of_beds == 1)
            E7_parm.ext(1) = str2double(v(pos+4:end));
        else
            E7_parm.ext(1) = str2double(v(pos+4:pos2(1)-1));
            E7_parm.ext(number_of_beds) = str2double(v(pos2(end)+1:end));
        end
        for i=2:number_of_beds-1
            E7_parm.ext(i) = str2double(v(pos2(i-1)+1:pos2(i)-1));
        end
    end
    
    %% JSRecon parameter
    % usemlaa
    pos = strfind(v,'usemlaa=');
    if (pos > 0)
        E7_parm.usemlaa = v(pos+8:end);
    end
    
    % mapv18tov20
    pos = strfind(v,'mapv18tov20=');
    if (pos > 0)
        E7_parm.mapv18tov20 = v(pos+12:end);
    end
    
    %% RQ parameter
    % cps
    pos = strfind(v,'cps=');
    if (pos > 0)
        E7_parm.cps = v(pos+4:end);
    end
    
    % recontype
    pos = strfind(v,'recontype=');
    if (pos > 0)
        E7_parm.recontype = v(pos+10:end);
    end
    
    % anonymized
    pos = strfind(v,'anonymized=');
    if (pos > 0)
        E7_parm.anonymized = v(pos+11:end);
    end
    
    % anonymized_id
    pos = strfind(v,'anonymized_id=');
    if (pos > 0)
        E7_parm.anonymized_id = v(pos+14:end);
    end
    
    % log
    pos = strfind(v,'log=');
    if (pos > 0)
        E7_parm.log = v(pos+4:end);
    end
    
    % user
    pos = strfind(v,'user=');
    if (pos > 0)
        E7_parm.user = v(pos+5:end);
    end
    
    %% EMrecon parameter
    % EMreconData
    pos = strfind(v,'EMreconData=');
    if (pos > 0)
        E7_parm.emrecondata = v(pos+12:end);
    end
    
    % EMreconVersion
    pos = strfind(v,'EMreconVersion=');
    if (pos > 0)
        E7_parm.emreconversion = v(pos+15:end);
    end
end
fclose(fid);

end
