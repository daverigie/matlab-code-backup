function [injectiontime] = E7_Listmode_GetInjectionTime(RQ_parm,E7_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_GetInjectionTime ["%s"]',folder),7);

% get listmode filename
file_list = dir([RQ_parm.recon_path '\' folder]);
for i=3:numel(file_list)
    if (numel(strfind(lower(file_list(i).name),'.dcm')) == 1)
        dinfo = dicominfo([RQ_parm.recon_path '\' folder '\' file_list(i).name]);
        if (strcmp(dinfo.ImageComments,'Listmode') == 1)
            filename = [RQ_parm.recon_path '\' folder '\' file_list(i).name];
            filename = [filename(1:end-4) '.bf'];
        end
    end
end

% calculate number of tags in file
fid = fopen(filename,'rb');
fseek(fid,0,1);
filesize = ftell(fid);
tags = filesize/4;
fclose(fid);

% check for first timetag (all count before are dropped) [deprecated]
fid = fopen(filename,'rb');
t = 1;
while (t <= tags)
    tag = fread(fid,1,'uint32');
    if (tag >= 2147483648 && tag < 2684354560)
        t = tags + 1;
    end
    t = t + 1;
end
fclose(fid);

% get injection time, ie loop until cps is higher than E7_parm.cps
fid = fopen(filename,'rb');
current_timemark = -1;
t = 1;
msec = 0;
counter = 0;
injectiontime = 0;
while (t <= tags)
    tag = fread(fid,1,'uint32');
    
    % all tags for 0 msec timemark are skipped
    if (tag >= 2147483648 && tag < 2684354560)
        current_timemark = tag-2147483648;
        
        if (current_timemark > 0)
            msec = msec + 1;
        end
    end
    
    % use only valid events
    if (current_timemark > -1)
        
        % use only 1sec bins
        if (msec == 1000)
            if (counter >= str2double(E7_parm.cps))
                t = tags + 1;
            else
                msec = 0;
                counter = 0;
                injectiontime = injectiontime + 1;
            end
        else
            if (tag < 2147483648)
                counter = counter + 1;
            end
        end
    end
    
    t = t + 1;
end
fclose(fid);

% correct injection time by "-1", ie the second before the limit was hit
injectiontime = num2str(max(0,injectiontime-1));

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_GetInjectionTime ["%s" (%s)]',folder,injectiontime),7);

end
