function [new_binning] = E7_Listmode_AnalyzeBinning(RQ_parm,E7_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_AnalyzeBinning ["%s"]',folder),6);

binning = E7_parm.binning;

% determine injection time / starttime
skip_first_frame = 1;
pos = strfind(binning,':');
if (pos == 2)
    if (strcmp(binning(1),'X') == 1)
        starttime = str2double(E7_Listmode_GetInjectionTime(RQ_parm,E7_parm,folder));
        skip_first_frame = 0;
    else
        starttime = str2double(binning(1:pos-1));
    end
else
    starttime = str2double(binning(1:pos-1));
end

% extract binning
tmp_binning = 0;
tmp_binning_index = 1;
pos_x = strfind(binning,'x');
pos_comma = strfind(binning,',');
for i=1:numel(pos_x)
    if (i == 1)
        startindex_A = strfind(binning,':')+1;
        stopindex_A = pos_x(i) - 1;
    else
        startindex_A = pos_comma(i-1)+1;
        stopindex_A = pos_x(i) - 1;
    end
    
    startindex_B = pos_x(i) + 1;
    if (numel(strfind(binning(startindex_B:end),',')) > 0)
        stopindex_B = pos_comma(i) - 1;
    else
        stopindex_B = length(binning);
    end
    
    if (strcmp(binning(startindex_A:stopindex_A),'Y') == 0)
        for p=1:str2double(binning(startindex_A:stopindex_A))
            tmp_binning(tmp_binning_index) = str2double(binning(startindex_B:stopindex_B));
            tmp_binning_index = tmp_binning_index + 1;
        end
    else
        tmp_binning(tmp_binning_index) = str2double(binning(startindex_B:stopindex_B)); %#ok<*AGROW>
    end
end

% check for Z in the last index
% undefined result if no Z in last index
fill_end = 0;
if (numel(pos_x) == numel(pos_comma))
    if (numel(pos_x) > 0)
        if (strcmp(binning(pos_comma(end)+1:end),'Z') == 1)
            fill_end = 1;
        end
    else
        if (strcmp(binning(end),'Z') == 1)
            fill_end = 1;
        end
    end
end

% determine last timetag
stoptime = E7_Listmode_GetLastTimetag(RQ_parm,folder);

% calculate new binning using given injection time, last timetag and old binning
bin_count = numel(tmp_binning);
bin_index_old = 1;
bin_index_new = 1;

% start new_binning
new_binning(bin_index_new) = starttime;
bin_index_new = bin_index_new + 1;

% sum up all times to catch end of file
bin_total_time = new_binning(1);


if (numel(pos_x) > 0)
    % loop over old binning
    while(bin_index_old <= bin_count && bin_total_time + tmp_binning(bin_index_old) < stoptime)
        new_binning(bin_index_new) = tmp_binning(bin_index_old);
        bin_total_time = bin_total_time + new_binning(bin_index_new);
        bin_index_new = bin_index_new + 1;
        bin_index_old = bin_index_old + 1;
    end
    
    % add some additional frames using last bin of the old binning
    if (strcmp(binning(startindex_A:stopindex_A),'Y') == 1)
        while(bin_total_time + tmp_binning(bin_count) <= stoptime)
            new_binning(bin_index_new) = tmp_binning(bin_count);
            bin_total_time = bin_total_time + new_binning(bin_index_new);
            bin_index_new = bin_index_new + 1;
        end
    end
end

% only if Z is set
% create a last frame using the remaining time
if (fill_end == 1)
    if (bin_total_time < stoptime)
        new_binning(bin_index_new) = stoptime - bin_total_time;
    end
end

% negative sign for first bin if to be skipped
if (skip_first_frame == 1)
    new_binning(1) = -new_binning(1);
end
clear('tmp_binning');

RQ_WriteLog(RQ_parm,sprintf('E7_Listmode_AnalyzeBinning ["%s"] END',folder),6);
