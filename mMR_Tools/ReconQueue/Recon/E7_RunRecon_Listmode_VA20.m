function [status] = E7_RunRecon_Listmode_VA20(RQ_parm,E7_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_Listmode_VA20 ["%s"]',folder),5);

% set default status
status = 0;

% get path for e7 installation
path_to_e7_bin = E7_SetupRecon_VA20(RQ_parm);

% Histogramming
% evaluate binning and create corresponding batch file
number_of_frames = 0;
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\Run-00-' folder '-LM-00-Histogramming.bat'],'r');
new_fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\RQ_Run-00-' folder '-LM-00-Histogramming.bat'],'w');
while (~feof(fid))
    % read single line and replace filenames
    v = fgetl(fid);
    
    % if special case occurs, skip_line is set to 1
    skip_line = 0;
    
    % - find line with 'bin.win64-VA20' and replace path to e7 tools (NO
    %   Histogramming in scanner version, has to be modified!)
    %     if (numel(strfind(v,'bin.win64-VA20')) > 0)
    %         pos = strfind(v,'bin.win64-VA20');
    %         fprintf(new_fid,'%s%s%s\r\n',v(1:pos-1),path_to_e7_bin,v(pos+14:end));
    %         skip_line = 1;
    %     end
    
    % cmd_all (reversed; is required for MLAA)
    if (numel(strfind(v,'cmd_all')) > 0)
        % skip_line = 1;
    end
    
    % count number of frames
    if (numel(strfind(v,'--frame')) > 0 && skip_line == 0)
        number_of_frames = numel(strfind(v,','))+1;
    end
    
    % save line if not skipped
    if (skip_line == 0)
        fprintf(new_fid,'%s\r\n',v);
    end
end
fclose(new_fid);
fclose(fid);

% DoMLAA
if (strcmp(E7_parm.usemlaa,'1') == 1)
    fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\RQ_Run-01-' folder '-LM-00-DoMLAA.bat'],'w');
    fprintf(fid,'@echo off\r\n');
    fprintf(fid,'set cmd2= C:\\Siemens\\PET\\%s\\e7_mlaa\r\n',path_to_e7_bin);
    fprintf(fid,'set cmd2= %%cmd2%% -e %s-LM-00-sino-all.mhdr\r\n',folder);
    fprintf(fid,'set cmd2= %%cmd2%% -n ..\\%s-norm.n.hdr\r\n',folder);
    fprintf(fid,'set cmd2= %%cmd2%% -u "%s-LM-00-umap.mhdr","%s-LM-00-umap-hardware.mhdr"\r\n',folder,folder);
    fprintf(fid,'set cmd2= %%cmd2%% --ou "%s-LM-00-umap-mlaa.mhdr"\r\n',folder);
    fprintf(fid,'set cmd2= %%cmd2%% -R 2,4 --pthr 0.2,0.2 --fov 0,0,0,215,230,272');
    fprintf(fid,' --beta 0.1,0.001,0.1 --gamma 0.001,0.001 --is 1,1 --isaa 20,9 --msw 0,1,1,0.0885,1,1,0.0963,1,1,0.03,1,1');
    fprintf(fid,' --prior GEMAN_3D -w 344 --gf --rs -l 73,.\r\n');
    fprintf(fid,'set cmd2= %%cmd2%% --ext 0 --mat (0,0,0)\r\n');
    if (str2double(E7_parm.gpu) >=0 )
        fprintf(fid,'set cmd2= %%cmd2%% --gpu %s\r\n',E7_parm.gpu);
    end
    fprintf(fid,'\r\n');
    fprintf(fid,'pushd "%s\\%s-Converted\\%s-LM-00"\r\n',RQ_parm.recon_path,folder,folder);
    fprintf(fid,'%%cmd2%%\r\n');
    fprintf(fid,'popd\r\n');
    fclose(fid);
end

% OP or PSF
if (strcmp(E7_parm.algo,'op-osem') == 1)
    fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\Run-04-' folder '-LM-00-OP.bat'],'r');
    new_fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\RQ_Run-04-' folder '-LM-00-OP.bat'],'w');
end
if (strcmp(E7_parm.algo,'psf') == 1)
    fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\Run-04-' folder '-LM-00-PSF.bat'],'r');
    new_fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\RQ_Run-04-' folder '-LM-00-PSF.bat'],'w');
end
while (~feof(fid))
    % read single line and replace filenames
    v = fgetl(fid);
    
    % if special case occurs, skip_line is set to 1
    skip_line = 0;
    
    % find line with 'bin.win64-VA20' and replace path to e7 tools
    if (numel(strfind(v,'bin.win64-VA20')) > 0)
        pos = strfind(v,'bin.win64-VA20');
        fprintf(new_fid,'%s%s%s\r\n',v(1:pos-1),path_to_e7_bin,v(pos+14:end));
        skip_line = 1;
    end
    
    % --algo
    if (numel(strfind(v,'set cmd= %cmd% --algo')) > 0)
        skip_line = 1;
    end
    
    % --psf
    if (numel(strfind(v,'set cmd= %cmd% --psf')) > 0)
        skip_line = 1;
    end
    
    % --is
    if (numel(strfind(v,'set cmd= %cmd% --is')) > 0)
        skip_line = 1;
    end
    
    % -w
    if (numel(strfind(v,'set cmd= %cmd% -w')) > 0)
        skip_line = 1;
    end
    
    % --fltr
    if (numel(strfind(v,'set cmd= %cmd% --fltr')) > 0)
        skip_line = 1;
    end
    
    % --izoom
    if (numel(strfind(v,'set cmd= %cmd% --izoom')) > 0)
        skip_line = 1;
    end
    
    % --gpu
    if (numel(strfind(v,'set cmd= %cmd% --gpu')) > 0)
        skip_line = 1;
    end
    
    % pushd is always at the end of the file
    if (numel(strfind(v,'pushd "')) > 0)
        
        % add debug if EMreconData should be created
        if (strcmp(E7_parm.emrecondata,'1') == 1)
            fprintf(new_fid,'set cmd= %%cmd%% -d %s\\%s-Converted\\debug\r\n',RQ_parm.recon_path,folder);
            if (exist(sprintf('%s\\%s-Converted\\debug',RQ_parm.recon_path,folder),'dir') ~= 7)
                mkdir(sprintf('%s\\%s-Converted\\debug',RQ_parm.recon_path,folder));
            end
        end
        
        if (strcmp(E7_parm.scatterscaling,'abs') == 1)
            fprintf(new_fid,'set cmd= %%cmd%% --abs\r\n');
        end
        if (strcmp(E7_parm.algo,'op-osem') == 1)
            fprintf(new_fid,'set cmd= %%cmd%% --algo %s\r\n',E7_parm.algo);
        end
        if (strcmp(E7_parm.algo,'psf') == 1)
            fprintf(new_fid,'set cmd= %%cmd%% --algo op-osem\r\n');
            fprintf(new_fid,'set cmd= %%cmd%% --psf\r\n');
        end
        fprintf(new_fid,'set cmd= %%cmd%% --is %s,%s\r\n',E7_parm.is_1,E7_parm.is_2);
        fprintf(new_fid,'set cmd= %%cmd%% -w %s\r\n',E7_parm.w);
        % from VA10 to VA20 the parameter changed from GAUSS to GAUSSIAN
        if (strcmp(E7_parm.fltr_1,'GAUSS') == 1)
            fprintf(new_fid,'set cmd= %%cmd%% --fltr GAUSSIAN,%s,%s\r\n',E7_parm.fltr_2,E7_parm.fltr_3);
        else
            fprintf(new_fid,'set cmd= %%cmd%% --fltr %s,%s,%s\r\n',E7_parm.fltr_1,E7_parm.fltr_2,E7_parm.fltr_3);
        end
        fprintf(new_fid,'set cmd= %%cmd%% --izoom %s\r\n',E7_parm.izoom);
        if (str2double(E7_parm.gpu) >=0 )
            fprintf(new_fid,'set cmd= %%cmd%% --gpu %s\r\n',E7_parm.gpu);
        end
        fprintf(new_fid,'\r\n');
    end
    
    % save line if not skipped
    if (skip_line == 0)
        fprintf(new_fid,'%s\r\n',v);
    end
end
fclose(new_fid);
fclose(fid);

% IF2Dicom
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\RQ_Run-05-' folder '-LM-00-IF2Dicom.bat'],'w');
fprintf(fid,'@echo off\r\n');
fprintf(fid,'pushd "%s\\%s-Converted\\%s-LM-00"\r\n',RQ_parm.recon_path,folder,folder);
for n=0:number_of_frames-1
    if (strcmp(E7_parm.algo,'op-osem') == 1)
        fprintf(fid,'cscript C:\\JSRecon12\\IF2Dicom.js %s-LM-00-OP_%.3d_000.v.hdr %s\\%s-Converted\\%s-LM-00\\Run-05-%s-LM-00-IF2Dicom.txt\r\n',folder,n,RQ_parm.recon_path,folder,folder,folder);
    end
    if (strcmp(E7_parm.algo,'psf') == 1)
        fprintf(fid,'cscript C:\\JSRecon12\\IF2Dicom.js %s-LM-00-PSF_%.3d_000.v.hdr %s\\%s-Converted\\%s-LM-00\\Run-05-%s-LM-00-IF2Dicom.txt\r\n',folder,n,RQ_parm.recon_path,folder,folder,folder);
    end
end
if (strcmp(E7_parm.usemlaa,'1') == 1)
    fprintf(fid,'cscript C:\\JSRecon12\\IF2Dicom.js %s-LM-00-umap-mlaa_000_000_00.v.hdr %s\\%s-Converted\\%s-LM-00\\Run-05-%s-LM-00-IF2Dicom.txt\r\n',folder,RQ_parm.recon_path,folder,folder,folder);
end
fprintf(fid,'popd\r\n');
fclose(fid);

% ALL
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\RQ_Run-99-' folder '-LM-00-All.bat'],'w');
fprintf(fid,'@echo off\r\n');
fprintf(fid,'pushd "%s\\%s-Converted\\%s-LM-00"\r\n',RQ_parm.recon_path,folder,folder);
fprintf(fid,'call RQ_Run-00-%s-LM-00-Histogramming.bat\r\n',folder);
if (strcmp(E7_parm.usemlaa,'1') == 1)
    fprintf(fid,'call RQ_Run-01-%s-LM-00-DoMLAA.bat\r\n',folder);
end
if (strcmp(E7_parm.algo,'op-osem') == 1)
    fprintf(fid,'call RQ_Run-04-%s-LM-00-OP.bat\r\n',folder);
end
if (strcmp(E7_parm.algo,'psf') == 1)
    fprintf(fid,'call RQ_Run-04-%s-LM-00-PSF.bat\r\n',folder);
end
fprintf(fid,'call RQ_Run-05-%s-LM-00-IF2Dicom.bat\r\n',folder);
fprintf(fid,'popd\r\n');
fclose(fid);

% run reconstruction using the new batch files
dos([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\RQ_Run-99-' folder '-LM-00-All.bat']);

% check if OP or PSF folder exists --> assumption the reconstruction was successful
if (strcmp(E7_parm.algo,'op-osem') == 1)
    if (exist([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\' folder '-LM-00-OP_000_000.v-DICOM'],'dir') == 7)
        status = 1;
    end
end
if (strcmp(E7_parm.algo,'psf') == 1)
    if (exist([RQ_parm.recon_path '\' folder '-Converted\' folder '-LM-00\' folder '-LM-00-PSF_000_000.v-DICOM'],'dir') == 7)
        status = 1;
    end
end

if (status == 1)
    RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_Listmode_VA20 ["%s" finished] END',folder),5);
else
    RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_Listmode_VA20 ["%s" failed] END',folder),5);
end

end
