function [path_to_e7_bin] = E7_SetupRecon_VA20(RQ_parm)

% set the installation folder for VA20 reconstruction

RQ_WriteLog(RQ_parm,'E7_SetupRecon_VA20',4);

path_to_e7_bin = 'bin.win64_VB20_SP4_20141226';

RQ_WriteLog(RQ_parm,'E7_SetupRecon_VA20 END',4);

end