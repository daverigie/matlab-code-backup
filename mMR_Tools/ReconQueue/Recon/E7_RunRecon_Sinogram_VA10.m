function [status] = E7_RunRecon_Sinogram_VA10(RQ_parm,E7_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_Sinogram_VA10 ["%s"]',folder),5);

% set default status
status = 0;

% DoMLAA
if (strcmp(E7_parm.usemlaa,'1') == 1)
    fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\RQ_Run-01-' folder '-00-DoMLAA.bat'],'w');
    fprintf(fid,'@echo off\r\n');
    fprintf(fid,'set cmd1= C:\\Siemens\\PET\\bin.win64-mMR\\e7_sino\r\n');
    fprintf(fid,'set cmd1= %%cmd1%% -e %s-00-sino.mhdr\r\n',folder);
    fprintf(fid,'set cmd1= %%cmd1%% -n ..\\%s-norm.n.hdr\r\n',folder);
    fprintf(fid,'set cmd1= %%cmd1%% --oearc %s-00-sino-arccornormalized.mhdr\r\n',folder);
    fprintf(fid,'set cmd1= %%cmd1%% --gf --rs -l 73,.\r\n');
    fprintf(fid,'\r\n');
    fprintf(fid,'set cmd2= C:\\Siemens\\PET\\bin.win64-mMR\\e7_mlaa\r\n');
    fprintf(fid,'set cmd2= %%cmd2%% -e %s-00-sino-arccornormalized.mhdr\r\n',folder);
    fprintf(fid,'set cmd2= %%cmd2%% -u "%s-00-umap.mhdr","%s-00-umap-hardware.mhdr"\r\n',folder,folder);
    fprintf(fid,'set cmd2= %%cmd2%% --ou "%s-00-umap-mlaa.mhdr"\r\n',folder);
    fprintf(fid,'set cmd2= %%cmd2%% -R 2,4 --pthr 0.2,0.2 --fov 0.336476,0.72428,2.7943,220,220,190');
    fprintf(fid,' --beta 0.1,0.001,0.1 --gamma 0.001,0.001 --is 3,21 --isaa 20,9 --msw 0,1,1,0.0885,1,1,0.113,1,1,0.0277,1,1');
    fprintf(fid,' --prior GEMAN_3D -w 344 --gf -l 73,.\r\n');
    fprintf(fid,'set cmd2= %%cmd2%% --ext 0 --mat (0,0,0)\r\n');
    if (str2double(E7_parm.gpu) >=0 )
        fprintf(fid,'set cmd2= %%cmd2%% --gpu %s\r\n',E7_parm.gpu);
    end
    fprintf(fid,'\r\n');
    fprintf(fid,'pushd "%s\\%s-Converted\\%s-00"\r\n',RQ_parm.recon_path,folder,folder);
    fprintf(fid,'%%cmd1%%\r\n');
    fprintf(fid,'%%cmd2%%\r\n');
    fprintf(fid,'popd\r\n');
    fclose(fid);
end

% MakeACF
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\Run-02-' folder '-00-MakeACF.bat'],'r');
new_fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\RQ_Run-02-' folder '-00-MakeACF.bat'],'w');
while (~feof(fid))
    % read single line and replace filenames
    v = fgetl(fid);
    
    % if special case occurs, skip_line is set to 1
    skip_line = 0;
    
    % -R
    if (numel(strfind(v,'set cmd= %cmd% -R')) > 0)
        skip_line = 1;
    end
    
    % --gpu
    if (numel(strfind(v,'set cmd= %cmd% --gpu')) > 0)
        skip_line = 1;
    end
    
    % pushd is always at the end of the file
    if (numel(strfind(v,'pushd "')) > 0)
        fprintf(new_fid,'set cmd= %%cmd%% -R %s,%s\r\n',E7_parm.r_1,E7_parm.r_2);
        if (str2double(E7_parm.gpu) >=0 )
            fprintf(new_fid,'set cmd= %%cmd%% --gpu %s\r\n',E7_parm.gpu);
        end
        fprintf(new_fid,'\r\n');
    end
    
    % save line if not skipped
    if (skip_line == 0)
        fprintf(new_fid,'%s\r\n',v);
    end
end
fclose(new_fid);
fclose(fid);

% MakeSinograms
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\Run-03-' folder '-00-MakeSinograms.bat'],'r');
new_fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\RQ_Run-03-' folder '-00-MakeSinograms.bat'],'w');
while (~feof(fid))
    % read single line and replace filenames
    v = fgetl(fid);
    
    % if special case occurs, skip_line is set to 1
    skip_line = 0;
    
    % -R
    if (numel(strfind(v,'set cmd= %cmd% -R')) > 0)
        skip_line = 1;
    end
    
    % --gpu
    if (numel(strfind(v,'set cmd= %cmd% --gpu')) > 0)
        skip_line = 1;
    end
    
    % pushd is always at the end of the file
    if (numel(strfind(v,'pushd "')) > 0)
        if (strcmp(E7_parm.scatterscaling,'abs') == 1)
            fprintf(new_fid,'set cmd= %%cmd%% --abs\r\n');
        end
        fprintf(new_fid,'set cmd= %%cmd%% -R %s,%s\r\n',E7_parm.r_1,E7_parm.r_2);
        if (str2double(E7_parm.gpu) >=0 )
            fprintf(new_fid,'set cmd= %%cmd%% --gpu %s\r\n',E7_parm.gpu);
        end
        fprintf(new_fid,'\r\n');
    end
    
    % save line if not skipped
    if (skip_line == 0)
        fprintf(new_fid,'%s\r\n',v);
    end
end
fclose(new_fid);
fclose(fid);

% OP
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\Run-04-' folder '-00-OP.bat'],'r');
new_fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\RQ_Run-04-' folder '-00-OP.bat'],'w');
while (~feof(fid))
    % read single line and replace filenames
    v = fgetl(fid);
    
    % if special case occurs, skip_line is set to 1
    skip_line = 0;
    
    % --algo
    if (numel(strfind(v,'set cmd= %cmd% --algo')) > 0)
        skip_line = 1;
    end
    
    % --is
    if (numel(strfind(v,'set cmd= %cmd% --is')) > 0)
        skip_line = 1;
    end
    
    % -w
    if (numel(strfind(v,'set cmd= %cmd% -w')) > 0)
        skip_line = 1;
    end
    
    % --fltr
    if (numel(strfind(v,'set cmd= %cmd% --fltr')) > 0)
        skip_line = 1;
    end
    
    % -R
    if (numel(strfind(v,'set cmd= %cmd% -R')) > 0)
        skip_line = 1;
    end
    
    % --izoom
    if (numel(strfind(v,'set cmd= %cmd% --izoom')) > 0)
        skip_line = 1;
    end
    
    % --gpu
    if (numel(strfind(v,'set cmd= %cmd% --gpu')) > 0)
        skip_line = 1;
    end
    
    % pushd is always at the end of the file
    if (numel(strfind(v,'pushd "')) > 0)
        fprintf(new_fid,'set cmd= %%cmd%% --algo op-osem\r\n');
        fprintf(new_fid,'set cmd= %%cmd%% --is %s,%s\r\n',E7_parm.is_1,E7_parm.is_2);
        fprintf(new_fid,'set cmd= %%cmd%% -w %s\r\n',E7_parm.w);
        % from VA10 to VA20 the parameter changed from GAUSS to GAUSSIAN
        if (strcmp(E7_parm.fltr_1,'GAUSSIAN') == 1)
            fprintf(new_fid,'set cmd= %%cmd%% --fltr GAUSS,%s,%s\r\n',E7_parm.fltr_2,E7_parm.fltr_3);
        else
            fprintf(new_fid,'set cmd= %%cmd%% --fltr %s,%s,%s\r\n',E7_parm.fltr_1,E7_parm.fltr_2,E7_parm.fltr_3);
        end
        fprintf(new_fid,'set cmd= %%cmd%% -R %s,%s\r\n',E7_parm.r_1,E7_parm.r_2);
        fprintf(new_fid,'set cmd= %%cmd%% --izoom %s\r\n',E7_parm.izoom);
        if (str2double(E7_parm.gpu) >=0 )
            fprintf(new_fid,'set cmd= %%cmd%% --gpu %s\r\n',E7_parm.gpu);
        end
        fprintf(new_fid,'\r\n');
    end
    
    % save line if not skipped
    if (skip_line == 0)
        fprintf(new_fid,'%s\r\n',v);
    end
end
fclose(new_fid);
fclose(fid);

% IF2Dicom
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\RQ_Run-05-' folder '-00-IF2Dicom.bat'],'w');
fprintf(fid,'@echo off\r\n');
fprintf(fid,'pushd "%s\\%s-Converted\\%s-00"\r\n',RQ_parm.recon_path,folder,folder);
fprintf(fid,'cscript C:\\JSRecon12\\IF2Dicom.js %s-00-OP_000.v.hdr "%s\\%s-Converted\\%s-00\\Run-05-%s-00-IF2Dicom.txt"\r\n',folder,RQ_parm.recon_path,folder,folder,folder);
if (strcmp(E7_parm.usemlaa,'1') == 1)
    fprintf(fid,'cscript C:\\JSRecon12\\IF2Dicom.js %s-00-umap-mlaa_000_000.v.hdr "%s\\%s-Converted\\%s-00\\Run-05-%s-00-IF2Dicom.txt"\r\n',folder,RQ_parm.recon_path,folder,folder,folder);
end
fprintf(fid,'popd\r\n');
fclose(fid);

% ALL
fid = fopen([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\RQ_Run-99-' folder '-00-All.bat'],'w');
fprintf(fid,'@echo off\r\n');
fprintf(fid,'pushd "%s\\%s-Converted\\%s-00"\r\n',RQ_parm.recon_path,folder,folder);
if (strcmp(E7_parm.usemlaa,'1') == 1)
    fprintf(fid,'call RQ_Run-01-%s-00-DoMLAA.bat\r\n',folder);
end
fprintf(fid,'call RQ_Run-02-%s-00-MakeACF.bat\r\n',folder);
fprintf(fid,'call RQ_Run-03-%s-00-MakeSinograms.bat\r\n',folder);
fprintf(fid,'call RQ_Run-04-%s-00-OP.bat\r\n',folder);
fprintf(fid,'call RQ_Run-05-%s-00-IF2Dicom.bat\r\n',folder);
fprintf(fid,'popd\r\n');
fclose(fid);

% run reconstruction using the new batch files
dos([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\RQ_Run-99-' folder '-00-All.bat']);

% check if OP folder exists --> assumption the reconstruction was successful
if (exist([RQ_parm.recon_path '\' folder '-Converted\' folder '-00\' folder '-00-OP_000.v-DICOM'],'dir') == 7)
    status = 1;
end

if (status == 1)
    RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_Sinogram_VA10 ["%s" finished] END',folder),5);
else
    RQ_WriteLog(RQ_parm,sprintf('E7_RunRecon_Sinogram_VA10 ["%s" failed] END',folder),5);
end

end
