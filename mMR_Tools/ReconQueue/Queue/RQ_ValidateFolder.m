function [status] = RQ_ValidateFolder(RQ_parm,folder)

RQ_WriteLog(RQ_parm,sprintf('RQ_ValidateFolder [validate "%s"]',folder),2);

% set default status
status = 0;

% get folder content
file_list = dir([RQ_parm.data_path '\' folder]);

% only 3 files are allowed in the data folder
% 1) folder with pet raw data, folder name must contain "PET_RAW_DATA" (not case sensitive)
% 2) folder with umap, folder name must contain "UMAP" (not case sensitive)
% 3) parm.txt, includes all required parameter for the reconstruction
if (numel(file_list) == 2+3)
    
    b_pet_raw_data = 0;
    b_umap = 0;
    b_parm = 0;
    
    for i=3:numel(file_list)
        if (exist([RQ_parm.data_path '\' folder '\' file_list(i).name],'dir') == 7)
            % add search for independent appearance of pet, raw and data
            if (numel(strfind(lower(file_list(i).name),'pet')) > 0 && numel(strfind(lower(file_list(i).name),'raw')) > 0 && numel(strfind(lower(file_list(i).name),'data')) > 0)
                b_pet_raw_data = b_pet_raw_data + 1;
            end
            if (numel(strfind(lower(file_list(i).name),'umap')) > 0)
                b_umap = b_umap + 1;
            end
        end
        if (exist([RQ_parm.data_path '\' folder '\' file_list(i).name],'file') == 2)
            if (length(file_list(i).name) == 8 && numel(strfind(lower(file_list(i).name),'parm.txt')) > 0)
                b_parm = b_parm + 1;
            end
        end
    end
    
    % check if all required files are available
    if (b_pet_raw_data == 1 && b_umap == 1 && b_parm == 1)
        status = 1;
    end
end

if (status == 0)
    RQ_WriteLog(RQ_parm,sprintf('RQ_ValidateFolder [invalid folder "%s"] END',folder),2);
else
    RQ_WriteLog(RQ_parm,sprintf('RQ_ValidateFolder [valid folder "%s"] END',folder),2);
end

end
