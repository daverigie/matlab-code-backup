function [status,queue] = RQ_CheckForNewFolder(RQ_parm)

RQ_WriteLog(RQ_parm,'RQ_CheckForNewFolder');

% set default status
status = 1;
queue = 0;

% - due to the new CBI network structure it was required to check / create
%   all folders before reconstruction
if (exist(RQ_parm.data_path,'dir') ~= 7)
    mkdir(RQ_parm.data_path);
end
if (exist(RQ_parm.image_path,'dir') ~= 7)
    mkdir(RQ_parm.image_path);
end

% check for new datasets only if data_path is valid
if (status == 1)
    % search folder for new datasets
    folder_list = dir(RQ_parm.data_path);
    
    % loop only if data exists
    if (numel(folder_list) > 2)
        % remove .,.., order by date
        folder_list = folder_list(3:end);
        [~,idx] = sort([folder_list.datenum]);
        
        % prealloc
        candidates(numel(folder_list)).status = 0;
        candidates(numel(folder_list)).folder_name = '';
        
        % loop over all subfolder
        valid_candidates = 0;
        for i=1:numel(folder_list)
            candidates(i).folder_name = folder_list(idx(i)).name;
            candidates(i).status = RQ_ValidateFolder(RQ_parm,folder_list(idx(i)).name);
            if (candidates(i).status == 1)
                valid_candidates = valid_candidates + 1;
            end
        end
        
        % read list from queue.txt
        fid = fopen([RQ_parm.base_path '\Queue.txt'],'r');
        v = fgetl(fid);
        pos = strfind(v,'Datasets=');
        datasets = str2double(v(pos+9:end));
        fclose(fid);
        
        % prealloc
        if (datasets > 0)
            queue_member(datasets).status = 0;
            queue_member(datasets).folder_name = '';
        else
            queue_member(1).status = 0;
            queue_member(1).folder_name = '';
        end
        
        % loop over all queue member
        fid = fopen([RQ_parm.base_path '\Queue.txt'],'r');
        for i=1:datasets
            queue_member(i).status = 0;
            queue_member(i).folder_name = fgetl(fid);
        end
        fclose(fid);
        
        % check whether folders of queue member still exist
        valid_queue_member = 0;
        for i=1:datasets
            for j=1:size(candidates,2)
                if (candidates(j).status == 1)
                    if (strcmp(queue_member(i).folder_name,candidates(j).folder_name) == 1)
                        queue_member(i).status = 1;
                        candidates(j).status = 0;
                        valid_candidates = valid_candidates - 1;
                    end
                end
            end
            if (queue_member(i).status == 1)
                valid_queue_member = valid_queue_member + 1;
            end
        end
        
        % prealloc if valid candidates or queue member exist
        if (valid_candidates+valid_queue_member > 0)
            clear('queue');
            queue(valid_candidates+valid_queue_member).folder_name = '';
            
            % store current list in queue
            valid_data_id = 1;
            for i=1:datasets
                if (queue_member(i).status == 1)
                    queue(valid_data_id).folder_name = queue_member(i).folder_name;
                    valid_data_id = valid_data_id + 1;
                end
            end
            for j=1:size(candidates,2)
                if (candidates(j).status == 1)
                    queue(valid_data_id).folder_name = candidates(j).folder_name;
                    valid_data_id = valid_data_id + 1;
                end
            end
            
            % store new queue in Queue.txt
            fid = fopen([RQ_parm.base_path '\Queue.txt'],'w');
            fprintf(fid,'Datasets=%d\r\n',valid_candidates+valid_queue_member);
            for i=1:valid_candidates+valid_queue_member
                fprintf(fid,'%s\r\n',queue(i).folder_name);
            end
            fclose(fid);
        end
        
        clear('candidates');
        clear('queue_member');
        clear('folder_list');
    else
        % needs to be disabled for now
        % status = 0;
    end
end

if (status == 0)
    RQ_WriteLog(RQ_parm,sprintf('RQ_CheckForNewFolder [invalid folder %s] END',RQ_parm.data_path));
else
    RQ_WriteLog(RQ_parm,'RQ_CheckForNewFolder END');
end

end
