function [recon_path] = RQ_GetInitReconPath(RQ_parm)

RQ_WriteLog(RQ_parm,'RQ_GetInitReconPath');

recon_path = '';
fid = fopen([RQ_parm.base_path '\Init.txt'],'r');
while (~feof(fid))
    v = fgetl(fid);
    pos = strfind(v,'ReconPath=');
    if (pos > 0)
        recon_path = v(pos+10:end);
    end
end
fclose(fid);

if (isempty(recon_path))
    RQ_WriteLog(RQ_parm,'RQ_GetInitReconPath [no valid path] END');
else
    RQ_WriteLog(RQ_parm,sprintf('RQ_GetInitReconPath ["%s"] END',recon_path));
end

end
