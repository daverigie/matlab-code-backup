function [] = RQ_WriteLog(RQ_parm,message,offset)

% check for number of arguments
if (nargin == 2)
    offset = 1;
end

fid_private = fopen([RQ_parm.base_path '\Log\Log_' date '.txt'],'a');
fid_public = fopen([RQ_parm.public_log_path '\Log_' date '.txt'],'a');

for fid = [fid_private, fid_public]

    if fid == -1
        continue;
    end
        
    fprintf(fid,'[%s] ',datestr(now));

    % add offset to highlight different logs
    for i=1:offset
        fprintf(fid,' ');
    end

    fprintf(fid,'%s\r\n',message);
    fclose(fid);

end