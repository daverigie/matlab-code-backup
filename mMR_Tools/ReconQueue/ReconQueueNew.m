%% clean up etc before start
close all; clear; clc;

% extend path
addpath('D:\ReconQueue\Tools');
addpath('D:\ReconQueue\Tools\EMrecon');
addpath(genpath('D:\ReconQueue\Queue'));
addpath(genpath('D:\ReconQueue\Recon'));


%% set parameter
% set base path
RQ_parm.base_path = 'D:\ReconQueue';
RQ_parm.public_log_path = '';

%% run queue
RQ_WriteLog(RQ_parm,'RQ_ReconQueue [queue started]',0);

% get init parameter once before entering the loop
RQ_parm.data_path = RQ_GetInitDataPath(RQ_parm);
RQ_parm.recon_path = RQ_GetInitReconPath(RQ_parm);
RQ_parm.public_log_path = RQ_GetInitPublicLogPath(RQ_parm);
RQ_parm.image_path = RQ_GetInitImagePath(RQ_parm);
RQ_parm.pause_time = RQ_GetInitPauseTime(RQ_parm);
RQ_parm.status = RQ_GetInitStatus(RQ_parm);

% loop as long as RQ_parm.status = 1
while (RQ_parm.status == 1)
    
    % check for new folder with data
    % whole queue is returned; so far only first dataset used
    [RQ_parm.status,queue] = RQ_CheckForNewFolder(RQ_parm);
    
    % check whether a valid folder is available for reconstruction
    if (RQ_parm.status == 1 && isstruct(queue) == 1)
            RQ_parm.status = RQ_SubmitReconJob(RQ_parm,queue(1).folder_name);
    end
    
    % only check for init status if not yet 0
    if (RQ_parm.status == 1)
        RQ_parm.status = RQ_GetInitStatus(RQ_parm);
    end
    
    % clean up memory
    % workspace is stored, memory is released, workspace is reloaded
    if (RQ_parm.status == 1)
        save('RQ_Workspace.mat','RQ_parm','queue');
        close all; clear; clc;
        load('RQ_Workspace.mat');
    end
    
    % debug modus
    % RQ_parm.status = 0;
    
    % go to sleep before starting next loop
    if (RQ_parm.status == 1)
        fprintf('[%s] going to sleep for %d sec...\r\n',datestr(now),RQ_parm.pause_time);
        pause(RQ_parm.pause_time);
    else
        RQ_WriteLog(RQ_parm,'RQ_ReconQueue [queue stopped]',0);
    end
end
