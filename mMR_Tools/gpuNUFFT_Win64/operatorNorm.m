addpath(genpath(pwd));

OVERSAMPLING_FACTOR = 2;
KERNEL_WIDTH = 3;
SECTOR_WIDTH = 8;

IMAGE_DIM = [64,64,48];

%% Data parameters
col = @(x) x(:);
k = repmat((-0.5:1/64:0.5-1/64), [1 64*64]);
k(2,:) = repmat(col(repmat((-0.5:1/64:0.5-1/64), [64 1])), [64 1]);
k(3,:) = col(repmat((-0.5:1/64:0.5-1/64), [64*64 1]));

A = gpuNUFFT(k,ones(size(k,2),1),OVERSAMPLING_FACTOR, ... 
                                 KERNEL_WIDTH, ...
                                 SECTOR_WIDTH, ...
                                 IMAGE_DIM, ...
                                 [],true);
K = @(y) A*y; Kt = @(x) A'*x;

disp('----------------');
disp('Adjoint Test');
disp('----------------');
x = rand(IMAGE_DIM) + i*rand(IMAGE_DIM);


for i = 1:1:200
   
    x = Kt(K(x));
    x = x/norm(col(x));
    s = norm(col(K(x)));
    s*s
end