function [X] = col(x)

    mat2vec = @(m) m(:);

    if iscell(x)
        x = cell2mat(cellfun(mat2vec, x(:), 'UniformOutput', false));
    end

    X = x(:);
    
end
