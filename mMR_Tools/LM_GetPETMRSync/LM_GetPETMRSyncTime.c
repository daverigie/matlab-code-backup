#include "LM_GetPETMRSyncTime.h"

// requirement: 2 resp_on flags and 2 resp_off flags have to occur within 100 msec
// 100 msec after first resp_on flag both counters are set to zero

long LM_GetPETMRSyncTime(char *filename, int skip_events) {
    
    long i,tag;
    int bit[32];
    unsigned int n,m;
    long last_timetag, sync_timetag, current_timetag;
    int skipped_events;
    int resp_on,resp_off;
    __int64 filesize,tags;
    int count_ms;
    
    // - read number of tags in file
    // - is later used to loop over all tags (and events)
    FILE *listmodefile;
    errno_t err;
    
    err = fopen_s(&listmodefile,filename,"rb");
    if (err != 0) {
        printf("reading listmode [%s] failed.\n",filename);
        exit(EXIT_FAILURE);
    }
    
    _fseeki64(listmodefile,0,SEEK_END);
    filesize = _ftelli64(listmodefile);
    tags = filesize/4;
    _fseeki64(listmodefile,0,SEEK_SET);
    
    // read tags from file and evaluate
    resp_on = 0;
    resp_off = 0;
    last_timetag = 0;
    sync_timetag = -1;
    current_timetag = 0;
    count_ms = 0;
    skipped_events = 0;
    for (tag=0;tag<tags;tag++)
    {
        fread(&n,4,1,listmodefile);
        m = n;
        
        // extract bits
        for(i=0;i<32;i++)
        {
            bit[i] = n%2;
            n /= 2;
        }
        
        // time tag
        if (m >= 2147483648 && m < 2684354560)
        {
            current_timetag = m-2147483648;
            
            if (resp_on < 2)
            {
                last_timetag = m-2147483648;
            }
            
            if (resp_on > 0)
            {
                count_ms++;
                // mexPrintf("%d | resp_on %d\n",count_ms,resp_on);
            }
        }
        else
        {
            // TAG 3: Expanded Format #1 (Gating)
            if (bit[31] == 1 && bit[30] == 1 && bit[29] == 1 && bit[28] == 0 && bit[27] == 0 && bit[26] == 0 && bit[25] == 0 && bit[24] == 1)
            {
                // P = 1
                if (bit[15] == 0 && bit[14] == 0 && bit[13] == 0 && bit[12] == 1)
                {
                    // Gate on
                    if (bit[0] == 0)
                    {
                        resp_on++;
                    }
                    // Gate off
                    if (bit[0] == 1)
                    {
                        if (resp_on == 2)
                        {
                            resp_off++;
                        }
                        else
                        {
                            resp_on = 0;
                        }
                    }
                    //mexPrintf("%d | %u | %d | %d | resp_on %d | resp_off %d\n",tag,m,current_timetag,count_ms,resp_on,resp_off);
                }
            }
            
            if (count_ms > 100)
            {
                count_ms = 0;
                resp_on = 0;
                resp_off = 0;
            }
            
            if (resp_on == 2 && resp_off == 2)
            {
                if (skipped_events == skip_events)
                {
                    tag = tags;
                    sync_timetag = last_timetag;
                }
                else
                {
                    skipped_events++;
                    resp_on = 0;
                    resp_off = 0;
                }
            }
        }
    }
    
    fclose(listmodefile);
    return sync_timetag;
}
