% Compile MEX version of LM_GetPETMRSyncTime_MEX
clear mex; clear; clc;

disp('Compiling LM_GetPETMRSyncTime_MEX...');
filenames = 'LM_GetPETMRSyncTime_MEX.c LM_GetPETMRSyncTime.c';
cmdline = sprintf('mex %s',filenames);
tic; eval(cmdline);
fprintf('Compiling LM_GetPETMRSyncTime_MEX...done. (%.4f secs)\n',toc);

disp('Compiling LM_GetPETMRSyncTimeStop_MEX...');
filenames = 'LM_GetPETMRSyncTimeStop_MEX.c LM_GetPETMRSyncTimeStop.c';
cmdline = sprintf('mex %s',filenames);
tic; eval(cmdline);
fprintf('Compiling LM_GetPETMRSyncTimeStop_MEX...done. (%.4f secs)\n',toc);
