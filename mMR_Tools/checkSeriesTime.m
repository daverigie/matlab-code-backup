function [T] = checkSeriesTime(basedir, outputpath)
    % basedir should point to folder of dicom series. The subfolders should
    % contain .IMA files. This will make a text file report indicating the
    % seriesTime for each
    
    listing = dir(fullfile(basedir,'*'));
    
    T = table;
    
    for i = 1:1:numel(listing)
        disp(sprintf('Processing series %i of %i', i,numel(listing)));
        
        item = listing(i);
        
        if ~isValidDir(item)
            continue
        end
        
 
        seriesName = item.name;
        seriesTime = getSeriesTime(fullfile(basedir, item.name));        
        [pixel_bw, image_dims, bw] = getBandwidth(fullfile(basedir, item.name));

        T = [T; table({seriesName}, {seriesTime}, {pixel_bw}, {image_dims}, {bw})];

        
        
    end
    
    T = sortrows(T, 2);
    T.Properties.VariableNames = {'SeriesName', 'SeriesTime', 'Pixel_Bandwidth', 'Dims', 'Total_Bandwidth'};
    %keyboard;
    writeFormattedTable(T, outputpath);
    
end

function [condition] = isValidDir(fileobj)
    condition =  fileobj.isdir && ~strcmp(fileobj.name,'.') && ~strcmp(fileobj.name, '..');
end


function seriesTime = getSeriesTime(dirpath)

        sublisting = dir(fullfile(dirpath, '*.IMA'));
        sublisting = [sublisting, dir(fullfile(dirpath, '*.ima'))];
        
        seriesTime = '';
        
        for j = 1:1:numel(sublisting)
            path_of_dicom = fullfile(dirpath, sublisting(j).name);
            try
                info = dicominfo(path_of_dicom);
                seriesTime = info.SeriesTime;        
                hour = seriesTime(1:2);
                minute = seriesTime(3:4);
                seconds = seriesTime(5:end);
                seriesTime = sprintf('%s:%s:%s', hour, minute, seconds);
            catch ME
                seriesTime = '';
            end
            
            break
        end
end

function [pixel_bw, image_dims, bw] = getBandwidth(dirpath)

    sublisting = dir(fullfile(dirpath, '*.IMA'));
    sublisting = [sublisting, dir(fullfile(dirpath, '*.ima'))];

    seriesTime = '';
    
    pixel_bw = ' ';
    image_dims = ' ';
    bw = ' ';

    for j = 1:1:numel(sublisting)
        path_of_dicom = fullfile(dirpath, sublisting(j).name);
        try
            info = dicominfo(path_of_dicom);
            pixel_bw = uint32(info.PixelBandwidth);
            dims.rows = uint32(info.Rows);
            dims.cols = uint32(info.Columns);
            total_bw = pixel_bw*max(dims.rows,dims.cols);
            
            pixel_bw = sprintf('%4i', pixel_bw);
            image_dims = sprintf('(%3i,%3i)', dims.rows, dims.cols);
            bw = sprintf('%.03f', double(total_bw)/1000.0);
            
            return;
        catch ME
            fprintf('\n%s\n',dirpath);
        end

        break
    end

end

    
function writeFormattedTable(T, outputpath)

    Tcell = table2cell(T);
    colNames = {'Series Name', 'Series Time', 'Pixel Bandwidth (Hz)', 'Image Dims', 'Total Bandwidth (kHz)'};
    Tcell = [colNames; Tcell];
    [n_rows, n_cols] = size(Tcell);
    
    fid = fopen(outputpath, 'w+');
    
    [~,~,ext] = fileparts(outputpath);
    csvpath = strrep(outputpath, ext, '.csv');
    fidcsv = fopen(csvpath, 'w+');
    fprintf(fidcsv,'%s','sep=$');
    fprintf(fidcsv,'\n');
    
    for i = 1:1:n_cols
        maxWidth(i) = max(cellfun(@numel, Tcell(:,i)));
    end
        
    for i = 1:1:n_rows

        for j = 1:1:n_cols
            item = Tcell{i,j};
            w = length(item);
            space = maxWidth(j) - w + 2;
            fprintf(fid, '%s%s', item, repmat(' ', [1,space]));
            fprintf(fidcsv,'%s\t$', item);
        end
        fprintf(fid, '\n');
        fprintf(fidcsv,'\n');
    end
    
    
    fclose(fid);
    fclose(fidcsv);

end

    
    
    
    
   
    

