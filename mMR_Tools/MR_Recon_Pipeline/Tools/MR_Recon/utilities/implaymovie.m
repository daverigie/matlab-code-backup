function [fig] = implaymovie(imstack, nLoop, imviewfun)


    imstack = squeeze(mat2gray(imstack));

    if nargin < 2
        nLoop = 10;
    end
    
    if nargin < 3
       imviewfun = @(x) imshow(x);
    end

    nGates = size(imstack,3);
    
    fig = figure();
    h = imviewfun(imstack(:,:,1)); hold on;
    axis square;

    frameOrder = [(1:1:nGates), (nGates-1):-1:2];
    
    for i = 1:1:nLoop
        for j = frameOrder 
            title(sprintf('Frame %i', j));
            set(h,'CData',imstack(:,:,j));
            drawnow;
            pause(1.0/nGates);
        end
    end


end

