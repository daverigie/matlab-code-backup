function y = grad1d(x, dim)

    % Shift dimensions to operate on first one
    N = size(x,dim);
    [x,perm,nshifts] = shiftdata(x, dim);
    y = zeros(size(x));

    % Perform finite differencing
    y(:,:) = x([2:N,N], :) - x(1:N, :);

    % shift back
    y = unshiftdata(y, perm, nshifts);

end
