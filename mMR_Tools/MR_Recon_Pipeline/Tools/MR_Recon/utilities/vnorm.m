function varargout = vnorm(x, varargin)
% Behaves like norm function but treats x as 1d vector regardless of shape

    varargout{:} = norm(x(:), varargin{:});

end