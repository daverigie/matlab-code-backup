function gatingSignal2d = gatingSignal1Dto2D(gatingSignal1d, nResp, nCard)

    idx = (gatingSignal1d < 1);
    gatingSignal1d(idx) = nan;

    % If 2d gating signal is supplied, convert it to 1d gating signal
    [g1,g2] = ind2sub([nResp, nCard], gatingSignal1d);
    gatingSignal2d = [g1,g2];

    gatingSignal2d(idx) = -1;

end
