classdef KSpaceData
    %KSPACEDATA This object is a container for k-space data that includes
    %trajectory information, density compensation, and dimensionality
    
    properties
        data
        dims
        traj
        densityComp
    end
    
    methods
        function obj = KSpaceData(data, dims, traj, densityComp)
           obj.data = data;
           obj.dims = dims;
           obj.traj = traj;
           obj.densityComp = densityComp;
        end
    end
    
end

