classdef ReconCSL1NCG < handle
   
    properties 
       
       saveFrequency = 10;
       maxlsiter = 6;
       gradTol   = 1e-8;
       alpha     = 0.01;
       beta0     = 0.6;
       beta      = 0.6;
       t0        = 1;
       l1smooth  = 1.0e-15;
       t;
        
       logfilepath;
       
       x;
       param;
       iter;
       g0;
       g1;
       dx;
       objectiveValue;
       gradNorm;
       logfiledir;
       screenshotdir;
       
      
    end
    
    methods
        function obj = ReconCSL1NCG(logfilepath)
            if (nargin < 1) || isempty(logfilepath)
                logfilepath = ReconCSL1NCG.uniquelogpath;
            end
            
            obj.logfilepath = logfilepath;
            
            obj.logfiledir = fileparts(obj.logfilepath);
            obj.screenshotdir = fullfile(obj.logfiledir, 'screenshots');
            if ~exist(obj.screenshotdir, 'dir')
                mkdir(obj.screenshotdir);
            end
                
            
        end
        
       %___________________________________________________________________
       %                             Recon Loop
       %-------------------------------------------------------------------
       function [] = init(obj, x0, param)
            obj.param = param;
            obj.iter  = 0;
            obj.x     = x0;
            obj.g0    = obj.grad(x0);
            obj.dx    = -obj.g0;
       end
       
       function [success] = minimize(obj)
          
          stopflag = false;
          
          
                    
          while (~stopflag) && (obj.iter < obj.param.nIter)
             
             obj.nextIter(); 
             obj.iter = obj.iter + 1;
             
             % Save 2d image file for checking progress every iteration
             obj.saveScreenshot(fullfile(obj.screenshotdir, 'screenshot.jpg'));
             
             % Every obj.saveFrequency iterations, save the actual image
             if mod(obj.iter, obj.saveFrequency) == 0
                 obj.saveIter(fullfile(obj.logfiledir, 'intermediateResult.mat'));
             end
          end
       end
       
       function [stopflag] = nextIter(obj)
           % backtracking line-search
            f0 = obj.objective(obj.x, obj.dx, 0);
            t  = obj.t0;
            f1 = obj.objective(obj.x, obj.dx, t);
            lsiter = 0;
            while (f1 > f0 - obj.alpha*t*abs(obj.g0(:)'*obj.dx(:)))^2 & (lsiter<obj.maxlsiter)
                lsiter = lsiter + 1;
                t = t * obj.beta;
                f1 = obj.objective(obj.x, obj.dx, t);
            end

            obj.objectiveValue = f1;
            obj.gradNorm       = norm(obj.dx(:));
            
            % control the number of line searches by adapting the initial step search
            if lsiter > 2, obj.t0 = obj.t0 * obj.beta;end 
            if lsiter<1, obj.t0 = obj.t0 / obj.beta; end
        %     test(:,:,k+1)=x(:,:,103,9);
            obj.x = (obj.x + t*obj.dx);

            % print some numbers for debug purposes	
            obj.log('%d   , obj: %f, L-S: %d', obj.iter, obj.objectiveValue, lsiter);
            
            % stopping criteria (to be improved)
            if (obj.gradNorm < obj.gradTol)
                stopflag = true
                return;
            end
            
            stopflag = false;

            %conjugate gradient calculation
            g1 = obj.grad(obj.x);
            bk = g1(:)'*g1(:)/(obj.g0(:)'*obj.g0(:)+eps);
            obj.g0 = g1;
            obj.dx =  - g1 + bk*obj.dx;

            if mod(obj.iter, obj.param.restartInterval) == 0
                %Restart so CG doesn't lose conjugacy
                obj.g0 = obj.grad(obj.x);
                obj.dx = -obj.g0;
                obj.beta   = obj.beta0;
            end
       end
       
       function res = objective(obj,x,dx,t) %**********************************

            param = obj.param;

            % L2-norm part
            w = param.A(x+t*dx)-param.y;
            L2Obj=0.5*w(:)'*w(:);

            % TV part along time
            if param.TVWeight
                w = param.Grad(x+t*dx); 
                TVObj = sum((w(:).*conj(w(:))+obj.l1smooth).^(1/2));
            else
                TVObj = 0;
            end

            res = L2Obj + param.TVWeight*TVObj;
       end

        function g = grad(obj,x)%***********************************************

            param = obj.param;
            
            % L2-norm part
            L2Grad = (param.AT(param.A(x)-param.y));

            % TV part along time
            if param.TVWeight
                w = param.Grad(x);
                TVGrad = param.GradT(w.*(w.*conj(w)+obj.l1smooth).^(-0.5));
            else
                TVGrad=0;
            end

            g = L2Grad+param.TVWeight*TVGrad;

        end
       %__________________________________________________________________%
           
       % _________________________________________________________________
       %                          Log/Display methods
       % -----------------------------------------------------------------
        function [] = log(obj, varargin)
           msg = sprintf(varargin{:});
           fid = fopen(obj.logfilepath, 'a+');
           S = sprintf('\n[%s] - %s', datestr(datetime), msg);
           fprintf(fid, S);
           fprintf(S);
           fclose(fid); 
        end
        
        function [] = clearLog(obj)
           fclose(fopen(obj.logfilepath,'w+')); 
        end
        
        function im = impreview(obj)
           x = obj.param.restore(obj.x);
           im = abs(x(:,:,ceil(end/2),ceil(end/2)));
           im = mat2gray(im);
        end
        
        function [] = saveIter(obj, filepath)
            [dirpath, basename, ext] = fileparts(filepath);
            filepath = fullfile(dirpath, sprintf('%s.mat', basename));
            im = obj.x;
            iter = obj.iter;
            warning(filepath); % DEBUG
            save(filepath, 'im', 'iter', '-v7.3');
        end
        
        function [] = saveScreenshot(obj, filepath);
            [dirpath, basename, ext] = fileparts(filepath);
            filepath = fullfile(dirpath, sprintf('%s_iter%06i.jpg', basename, obj.iter));
            im = obj.impreview();
           
            imwrite(im, filepath);
        end
       %_________________________________________________________________%
    end
    
    methods(Static)
        function fpath = uniquelogpath(dirpath)
            if nargin < 1
                dirpath = pwd;
            end
            
            alphabet = ['a':'z','A':'Z','0':'9'];
            uniqueID = alphabet(randperm(numel(alphabet), 10));
            
            filename = strcat('reconcsl1ncg.', uniqueID, '.log');
            fpath = fullfile(dirpath, filename);
  
        end
    end
    
    
end
