%% Load Data File
load('Q:/labspace/MR_Recon_Dave_v2/sampleData/meas_MID03068_FID282426_Abd_RadVibeNyuNavi_082016_kdata_clean.mat');

%% Perform reconstructions with different numbers of views
imarr = cell(0);
viewArr = [50, 100, 200, 400, 800];
for numViews = viewArr
    
    NVIEWS = numViews
    kdata = kdata_clean(:,:,1:NVIEWS,:);

    %% Gridded recon
    im = gridrecon(kdata,[256,256,48]);
    im = permute(abs(im), [2,1,3]);
    imarr = [imarr, im];
    
end

%% Save the results

save('viewNumberTest.mat', 'imarr', 'viewArr');


%% Display Results

weights = 800./viewArr;

clear imarr_weighted;
for i = 1:1:numel(imarr)
   imarr_weighted{i} = imarr{i}*weights(i); 
end

montage = cat(2, imarr_weighted{:});
imagesc(montage(:,:,23));


%% difference image

for i = 1:1:numel(viewArr)
    figure;
    
    im1 = imarr_weighted{i}(:,:,23);
    im2 = imarr_weighted{5}(:,:,23);

    imagesc(abs(im1-im2));
    title(sprintf('Difference Image, %i views, %i views', viewArr(i), viewArr(end)));

end