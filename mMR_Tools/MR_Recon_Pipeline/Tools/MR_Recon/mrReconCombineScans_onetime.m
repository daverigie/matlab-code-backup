function [im, gatingSignal] = mrReconMain(kdata, gatingSignal, varargin)

%_______________________________________________________________________________
%                          Parse Input Arguments                               
%-------------------------------------------------------------------------------

CG_RESTART_INTERVAL = 5; %This has not been tested, but it's the value Li used, so for now we just hard code it.

pass = @(x) true;

p = inputParser;
addRequired(p, 'kdata', pass);
addRequired(p, 'gatingSignal', @isnumeric);
addParameter(p, 'createGatingSignal', false, @islogical);
addParameter(p, 'nGates', []);
addParameter(p, 'tvWeight', 0.01);
addParameter(p, 'imageDim', [256, 256, 48]);
addParameter(p, 'nIter', 15);
addParameter(p, 'gradType', 'time');
addParameter(p, 'logfilepath', []);

parse(p, kdata, gatingSignal, varargin{:});

kdata 	        = p.Results.kdata;
gatingSignal    = p.Results.gatingSignal;
tvWeight        = p.Results.tvWeight;
imageDim        = p.Results.imageDim;
nIter           = p.Results.nIter;
logfilepath     = p.Results.logfilepath;

% Perform Amplitude based gating on-the-fly in case user has supplied the
% raw gating signal
if p.Results.createGatingSignal
    nGates = p.Results.nGates;
    switch numel(nGates)
       case 1
           gatingSignal = createGatingSignal1d(gatingSignal, nGates);
       case 2
           gatingSignal = createGatingSignal2d(gatingSignal, nGates);
    end
end

%===============================================================================


% kdata dimensions:
% -----------------
% nFE  - Number of sample points along readout direction
% nPar  - Number of partitions per view (phase encoding direction)
% nView - Number of angular samples
% nCoil - Number of coil elements, i.e. raw data channels
    

[nFE, nPar, nView, nCoil]  =   size(kdata);
nSlice = imageDim(3);

%Generate the sampling trajectory
[traj3d_1, densityCompen3d_1] = goldenAngleStackOfStars(nFE, nSlice, 800, nPar);
traj3d = cat(1,traj3d_1, traj3d_1);
densityCompen3d = cat(1, densityCompen3d_1, densityCompen3d_1);

% estimate coil sensitivies
b1 = estimateCoilSensitivityMaps(kdata, traj3d, densityCompen3d, imageDim);

% Form gated data, trajectory, etc.
[kdataArr, trajArr, densArr] = sortGatedKdata(kdata, ...
                                                    traj3d, ...
                                                    densityCompen3d, ...
                                                    gatingSignal);

% Create 1D or 2D Gradient Operator Depending on gating signal

if size(gatingSignal,2) > size(gatingSignal,1)
    warning('gating signal oriented wrong, transposing');
    gatingSignal = gatingSignal';
end

nGates = max(gatingSignal, [], 1);

if isvector(gatingSignal)
   % 1D gating, use 1D gradient
   GradOperatorTime = Grad([imageDim, nGates], [4]);
   GradOperatorSpace = Grad([imageDim, nGates], [1,2,3]);
else
   % 2D gating signal, use 2D gradient
   GradOperatorTime = Grad([imageDim, nGates], [4,5]);
   GradOperatorSpace = Grad([imageDim, nGates, 2], [1,2,3]);
end


%% Temp DEBUG
param.y                    = col(kdataArr);  
FT                         = gpuNUFFTND(trajArr, densArr, b1); 
param.y                    = FT.scaleData(param.y); % Scale data by sqrt of density compensation for use with symmetrized NUFFT
FT.densityCompensationMode = 'symmetric'; % applies sqrt of density compensation in forward and adjoint operator
param.A                    = @(x) FT*x;
param.AT                   = @(x) FT'*x;
param.restore              = @(x) FT.restore(x);

recon_cs                   = param.AT(param.y);

switch p.Results.gradType
    case 'time'
        param.Grad  = @(x) GradOperatorTime*x;
        param.GradT = @(x) GradOperatorTime'*x;
    case 'spaceTime'
        param.Grad  = @(x) GradOperatorSpace*(GradOperatorTime*x);
        param.GradT = @(x) GradOperatorTime'*(GradOperatorSpace'*x);
    otherwise
        error('Invalid Gradient Type. Must be either "time" or "spaceTime"');
end

param.TVWeight        = max(abs(recon_cs(:)))*tvWeight; %%This is the parameter you want to test
param.nIter           = nIter;
param.restartInterval = CG_RESTART_INTERVAL;


tic;

ReconSolver = ReconCSL1NCG(logfilepath);
ReconSolver.init(recon_cs, param);
ReconSolver.minimize();

time=toc/60;
recon_cs = ReconSolver.x;
recon_cs = abs(recon_cs);
im = recon_cs/max(abs(recon_cs(:)));
im = FT.restore(im);

fprintf('\r\nDone in %f minutes.\r', time);



end


