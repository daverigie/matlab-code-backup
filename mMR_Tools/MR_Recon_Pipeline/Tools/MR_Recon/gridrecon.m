function [im, b1] = gridrecon(kdata, imageDim)

    % kdata dimensions:
    % -----------------
    % nCol  - Number of sample points along readout direction
    % nPar  - Number of partitions per view (phase encoding direction)
    % nView - Number of angular samples
    % nCoil - Number of coil elements, i.e. raw data channels
    
    [nCol, nPar, nView, nCoil]  =   size(kdata);
                         
    nSlice = imageDim(3);
    
    %Generate the sampling trajectory
    [traj3d, densityCompen3d] = goldenAngleStackOfStars(nCol, nSlice, nView, nPar);
    
    if nCoil > 1
        % estimate coil sensitivies
        fprintf('\nEstimating Coil Sensitivities...'); tic;
        b1 = estimateCoilSensitivityMaps(kdata, traj3d, densityCompen3d, imageDim);
        toc;
    else
        b1 = [];
    end
    
    fprintf('\nConstructing GPU NUFFT Operator...'); tic;
    FT    = gpuNUFFTND({traj3d.'}, {densityCompen3d}, b1, imageDim);
    FT.densityCompensationMode = 'asymmetric';
    toc;
    
    % Apply adjoint NUFFT to filtered/densitycomp data
    kdata = {reshape(kdata, [], nCoil)};
    
    fprintf('\nPerforming NUFFT adjoint...'); tic;
    im = FT'*kdata;
    toc;
    
    fprintf('\nRearranging Image Dimensions...'); tic;
    im = FT.restore(im);
    
    im = swapaxes(im, 1, 2);
    im = flip(im, 3);
    toc;
   
        
end

