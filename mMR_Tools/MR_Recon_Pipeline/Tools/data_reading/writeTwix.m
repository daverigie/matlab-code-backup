function [] = writeTwix(twix_obj, filepath)

    tic;
    
    [dirpath, filename, ext] = fileparts(filepath);
    ext = '.twix'; % make filename twix
    filepath = fullfile(dirpath, [filename ext]);

    % twix_obj should be a structs
    bs = getByteStreamFromArray(twix_obj);
    
    % save bytestream
    fid = fopen(filepath,'wb');
    fwrite(fid, bs, 'uint8', 'ieee-le');
    fclose(fid);
        
    toc;

end