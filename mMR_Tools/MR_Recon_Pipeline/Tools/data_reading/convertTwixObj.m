function [twix_obj_new] = convertTwixObj(twix_obj, dataType)

    % This function saves all of the header and MDH information from the twix file
    % and converts it to a simple MATLAB struct.
    %
    % The actual data (nav or image) will be stored in a new field called
    % 'data'. 
    %
    % twix_obj should look like:
    %
    % twix_obj = 
    %
    %   struct with fields:
    % 
    %      hdr: [1�1 struct]
    %    image: [1�1 twix_map_obj_Nav]
    % 
    
    tic;
    fprintf('\nExtracting %s data...  ', dataType);
    
    switch upper(dataType)
        case 'IMG'
            data = squeeze(twix_obj.image(:,:,:,:,:,:,:,1));
        case 'NAV'
            data = squeeze(twix_obj.image(:,:,:,:,:,:,:,2));
    end
    
    data = permute(data,[1 4 3 2]);
    fprintf('Done.\n\n');
    toc;
    
    tic;
    fprintf('\nConverting from twix_map_obj to MATLAB struct...  ');
    twix_obj_new.data = data;
    twix_obj_new.hdr = twix_obj.hdr;
    twix_obj_new.image = copyAllFields(twix_obj.image);
    fprintf('Done.\n\n');
    toc;
    
end


function S2 = copyAllFields(S1)

    S2 = struct();
    fieldNames = fields(S1);
    
    for iField = 1:numel(fieldNames)
        f = fieldNames{iField};
        S2 = setfield( S2, f, getfield(S1, f) );
    end
    
end