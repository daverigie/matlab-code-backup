function [twix_obj] = getknav(twixpath)

    twix_obj = mapVBVD_Nav(twixpath, 'NAV');
    twix_obj = convertTwixObj(twix_obj, 'NAV');

end