function [head, numInvalid] = createMiniHeaderFromTwixVE11p(twix_obj)


    numInvalid = 0;

    %keyboard;
    function val = search(varargin)
       
        [val, successFlag] = getNestedField(twix_obj, varargin{:}); 
       
       if ~successFlag
           queryField = strcat(varargin{:});
           %warning(sprintf('Invalid field: %s', queryField));
           numInvalid = numInvalid + 1;
       end
       
    end
    
    head.osfactor              = search('hdr','Dicom','flReadoutOSFactor');
    head.centerFreq            = search('hdr','Dicom','lFrequency');
    
    head.fov.read              = search('hdr','MeasYaps','sAdjData','sAdjVolume','dReadoutFOV');
    head.fov.phase             = search('hdr','MeasYaps','sAdjData','sAdjVolume','dPhaseFOV');
    head.fov.slice             = search('hdr','MeasYaps','sAdjData','sAdjVolume','dThickness');
    
    head.kdata.dwellTime       = search('hdr','MeasYaps','sRXSPEC','alDwellTime'); %nanoseconds
    head.kdata.dwellTime       = head.kdata.dwellTime(1);
    
    head.kdata.dims.nCol       = search('hdr','Config','RawCol');
    head.kdata.dims.nLin       = search('hdr','MeasYaps','sKSpace','lRadialViews'); 
    head.kdata.dims.nPar       = search('hdr','Config','NParMeas'); 
    head.kdata.dims.nCha       = []; % Gets assigned later
    
    head.scanTime              = search('hdr','MeasYaps','lTotalScanTimeSec'); 
    head.TE                    = cell2mat(search('hdr','MeasYaps','alTE'));
    head.TR                    = cell2mat(search('hdr','MeasYaps','alTR'));
    head.lPartitions           = search('hdr','MeasYaps','sKSpace','lPartitions');
    head.nImagesPerSlab        = search('hdr','MeasYaps','sKSpace','lImagesPerSlab'); 
    head.linesPerShot          = search('hdr','MeasYaps','sKSpace','lLinesPerShot'); 
    
    head.offset.dSag           = search('hdr','MeasYaps','sAdjData','sAdjVolume','sPosition','dSag');
    head.offset.dCor           = search('hdr','MeasYaps','sAdjData','sAdjVolume','sPosition','dCor');
    head.offset.dTra           = search('hdr','MeasYaps','sAdjData','sAdjVolume','sPosition','dTra');
    
    head.normalVector.dSag     = search('hdr','MeasYaps','sAdjData','sAdjVolume','sNormal','dSag');
    head.normalVector.dCor     = search('hdr','MeasYaps','sAdjData','sAdjVolume','sNormal','dCor');
    head.normalVector.dTra     = search('hdr','MeasYaps','sAdjData','sAdjVolume','sNormal','dTra');
  
    head.globalTablePos.dSag     = search('hdr', 'Config','GlobalTablePosSag');
    head.globalTablePos.dCor     = search('hdr', 'Config','GlobalTablePosCor');
    head.globalTablePos.dTra     = search('hdr', 'Config','GlobalTablePosTra');
    
    head.ImageSize.nRow           = search('hdr', 'Config','NImageLins');
    head.ImageSize.nCol           = search('hdr', 'Config','NImageCols');
    head.ImageSize.nSlice         = search('hdr', 'Config','NImagePar');
        
    head.navigator.dwellTime   = 2500; % This is valid for Rosette as of 9/29/2016
    warning('Need to validate navigator dwell time with Tiejun for new WIP991');
    
    head.coilInfo              = search('hdr','MeasYaps','sCoilSelectMeas','aRxCoilSelectData');
    head.coilInfo              = head.coilInfo{1}.asList;
    head.coilInfo              = [head.coilInfo{:}];
    head.kdata.dims.nCha       = length(head.coilInfo);
    
end

 





