function [twix_obj] = readTwix(filepath)

    tic;

    [dirpath, filename, ext] = fileparts(filepath);
    
    if ~strcmpi(ext,'.TWIX')
        error('extension must be TWIX');
    end

    fid = fopen(filepath,'r','ieee-le');
    
    bs = fread(fid,inf,'uint8=>uint8',0,'ieee-le');
    
    fclose(fid);
    
    try
        twix_obj = getArrayFromByteStream(bs);
    catch ME
        keyboard;
    end
    
    toc;
    
end

