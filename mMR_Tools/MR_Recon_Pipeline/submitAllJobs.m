inputlist = readFilePaths(CONFIG.paths.queue_path);

for i=1:1:numel(inputlist)

    inputpath = inputlist{i};
    [parentpath, dirname, ext] = fileparts(inputpath);
    
    workingpath = fullfile(CONFIG.paths.workingdir_basepath, ['job', '.', uniquestring(10)]); 
    outputpath  = fullfile(CONFIG.paths.output_basepath, dirname);
    
    RS = ReconSeries(inputpath, workingpath, outputpath);
   
    submitReconSeries(RS);	
    
    writeFilePaths(inputlist((i+1):end), CONFIG.paths.queue_path);
    
end


