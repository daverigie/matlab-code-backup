init;

directory_list = dir(CONFIG.paths.input_basepath);

ind = ~cellfun(@isempty, regexpi({directory_list.name}, ... 
               CONFIG.regex_match_patterns.patient_folder, 'match'));
           
ind = logical(ind.*[directory_list.isdir]);

directory_list = directory_list(ind);
{directory_list.name}'

for i=1:1:numel(directory_list)

    D = directory_list(i);
    inputpath   = fullfile(CONFIG.paths.input_basepath, D.name);    
    inputlist{i} = inputpath;

end

writeFilePaths(inputlist, 'queue.txt');


