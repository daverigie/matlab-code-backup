function check_regex()

CONFIG = YAML.read('../config.yml');

% PATIENT FOLDER

    pattern = CONFIG.regex_match_patterns.patient_folder
    tests = {{'Patient1', true}, ...
           {'phantomsomething', true}, ... 
           {'patient.txt', false}};
       
    check_tests(tests, pattern);

% MR Data

  pattern = CONFIG.regex_match_patterns.mr_data;
  tests = {{'somekdata.mat', true}, ...
           {'meas349234_patjljksf.dat', true}, ... 
           {'kmeas.dat', false}};
       
  check_tests(tests, pattern);

% GATING FILE

  pattern = CONFIG.regex_match_patterns.gating_file;
  tests = {{'somethinggatingelsesignal.mat', true}, ...
           {'gatingfile.mat', false}, ... 
           {'gating_signal.txt', false}};
       
  check_tests(tests, pattern);


% PARAMETER FILE

  pattern = CONFIG.regex_match_patterns.parameter_file;
  tests = {{'parm.yml', true}, ...
           {'parameterfileforrecon.yml', true}, ... 
           {'params.yml', true}, ...
           {'parameter.txt', false},...
           {'para.yml', false}};
       
  check_tests(tests, pattern);

% PET_DICOM

  pattern = CONFIG.regex_match_patterns.pet_dicom
  tests = {{'petdicomfolder', true}, ...
           {'folderwithpetdata', true}, ... 
           {'somePETdata', true}, ...
           {'PET.dicom', false}};
       
  check_tests(tests, pattern);

end


function check_tests(tests, pattern)

  num_passed = 0;
  for i = 1:numel(tests)
     filename = tests{i}{1};
     outcome = ~isempty(regexp(filename, pattern));
     expected_outcome = tests{i}{2};
     fprintf('\n%s  expected: %i  outcome: %i', filename, expected_outcome, outcome);
     if outcome == expected_outcome
         num_passed = num_passed + 1;
     end
  end
  
  fprintf('\n%i of %i tests passed\n\n', num_passed, numel(tests));



end