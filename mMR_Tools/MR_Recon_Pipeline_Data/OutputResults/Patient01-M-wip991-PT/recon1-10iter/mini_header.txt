osfactor = 2
centerFreq = 123215665
fov.read = 400
fov.phase = 400
fov.slice = 396
kdata.dwellTime =     [4000]


kdata.dims.nCol = 512
kdata.dims.nLin = 1000
kdata.dims.nPar = 38
kdata.dims.nCha = 18
scanTime = 342
TE = [5290 15000 25000 35000 45000 55000 65000 75000 85000 95000 105000 115000]
TR = 9000
lPartitions = 48
nImagesPerSlab = 88
linesPerShot = 38
offset.dSag = 2.77108433735
offset.dCor = 25.3012048192
offset.dTra = 0.0722891566265
normalVector.dSag = 1
normalVector.dCor = 0
normalVector.dTra = 0
globalTablePos.dSag = 0
globalTablePos.dCor = 0
globalTablePos.dTra = -2
ImageSize.nRow = 256
ImageSize.nCol = 256
ImageSize.nSlice = 88
navigator.dwellTime = 2500
coilInfo.lElementSelected = 1
coilInfo.lMuxChannelConnected = 54
coilInfo.lRxChannelConnected = 7
coilInfo.lADCChannelConnected = 7
coilInfo.uiInsertionTime = 1386
coilInfo.sCoilElementID.tCoilID = '"SpineMatrix_MRPET"'
coilInfo.sCoilElementID.lCoilCopy = 1
coilInfo.sCoilElementID.tElement = '"SP1"'
coilInfo.sCoilElementID.tCoilClass = '"Spine"'
coilInfo.sCoilElementID.ulUniqueKey = 380925302
coilInfo.sCoilProperties.eCoilType = 4