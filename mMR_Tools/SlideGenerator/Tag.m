classdef Tag < handle

    properties
        Name
        Value
    end
    
    methods
        function this = Tag(tagString)
           
            pattern = '\\TAG{<([^>]+)>}(.*)\\TAG{</\1>}';
            match   = regexp(tagString, pattern, 'tokens');
            this.Name  = match{1}{1};
            this.Value = match{1}{2};
        end
        
        function s = print(this)
           
            left  = sprintf('\\TAG{<%s>}', this.Name);
            right = sprintf('\\TAG{</%s>}', this.Name);
            
            s = sprintf('%s%s%s', left, this.Value, right);
            
            sprintf('%s', s);
            
        end
        
    end
    
end

