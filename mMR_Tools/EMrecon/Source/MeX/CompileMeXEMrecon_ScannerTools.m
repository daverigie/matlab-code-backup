%% mexEMrecon_Version
if (compile_mexEMrecon_Version == 1)
    fprintf('compiling mexEMrecon_Version...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' Main/C/mexEMrecon_Version.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'Main/Matlab/EMrecon_Version';
    
    fprintf('compiling mexEMrecon_Version...done in %2.5f secs.\n',toc);
end
