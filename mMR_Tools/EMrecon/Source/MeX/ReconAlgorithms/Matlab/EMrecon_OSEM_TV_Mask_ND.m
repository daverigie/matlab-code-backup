function [image] = EMrecon_OSEM_TV_Mask_ND(parm,data,startimage,mask)

% EMrecon_OSEM_TV_Mask(parm,data,startimage,mask)

if (nargin ~= 4 || nargout ~= 1)
    fprintf('\nEMrecon_OSEM_TV_Mask_ND\n');
    fprintf('Usage: image = EMrecon_OSEM_TV_Mask_ND(parm,data,startimage,mask);\n\n');
    mexEMrecon_OSEM_TV_Mask;
    fprintf('\n');
    image = -1;
else
    vfprintf(1,parm,'EMrecon_OSEM_TV_Mask_ND...\n');
    tic;
    [image,dims] = mexEMrecon_OSEM_TV_Mask(parm,data,startimage(:),mask);
    if (numel(dims) == 3)
        image = reshape(image,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_OSEM_TV_Mask_ND...done. [%f sec]\n',toc);
end

end