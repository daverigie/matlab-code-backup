function [image] = EMrecon_EM_Mask_ND(parm,data,data_fp,sensitivity_map,startimage,mask)

% EMrecon_EM_Mask_ND(parm,data,data_fp,sensitivity_map,startimage,mask)

if (nargin ~= 6 || nargout ~= 1)
    fprintf('\nEMrecon_EM_Mask_ND\n');
    fprintf('Usage: image = EMrecon_EM_Mask_ND(parm,data,data_fp,sensitivity_map,startimage,mask);\n\n');
    mexEMrecon_EM_Mask;
    fprintf('\n');
    image = -1;
else
    vfprintf(1,parm,'EMrecon_EM_Mask_ND...\n');
    tic;
    [image,dims] = mexEMrecon_EM_Mask(parm,data,data_fp,sensitivity_map(:),startimage(:),mask);
    if (numel(dims) == 3)
        image = reshape(image,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_EM_Mask_ND...done. [%f sec]\n',toc);
end

end