function [image] = EMrecon_OSEM_TV_Mask_1D(parm,data,startimage,mask)

% EMrecon_OSEM_TV_Mask_1D(parm,data,startimage,mask)

if (nargin ~= 4 || nargout ~= 1)
    fprintf('\nEMrecon_OSEM_TV_Mask_1D\n');
    fprintf('Usage: image = EMrecon_OSEM_TV_Mask_1D(parm,data,startimage,mask);\n\n');
    mexEMrecon_OSEM_TV_Mask;
    fprintf('\n');
    image = -1;
else
    vfprintf(1,parm,'EMrecon_OSEM_TV_Mask_1D...\n');
    tic;
    image = mexEMrecon_OSEM_TV_Mask(parm,data,startimage,mask);
    vfprintf(1,parm,'EMrecon_OSEM_TV_Mask_1D...done. [%f sec]\n',toc);
end

end