function [bp_image] = EMrecon_BackProj_ND(parm,data)

% EMrecon_BackProj_ND(parm,data)

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_BackProj_ND\n');
    fprintf('Usage: bp_image = EMrecon_BackProj_ND(parm,data);\n\n');
    mexEMrecon_BackProj;
    fprintf('\n');
    bp_image = -1;
else
    vfprintf(1,parm,'EMrecon_BackProj_ND...\n');
    tic;
    [bp_image,dims] = mexEMrecon_BackProj(parm,data);
    if (numel(dims) == 3)
        bp_image = reshape(bp_image,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_BackProj_ND...done. [%f sec]\n',toc);
end

end