function [image] = EMrecon_EM_Mask_1D(parm,data,data_fp,sensitivity_map,startimage,mask)

% EMrecon_EM_Mask_1D(parm,data,data_fp,sensitivity_map,startimage,mask)

if (nargin ~= 6 || nargout ~= 1)
    fprintf('\nEMrecon_EM_Mask_1D\n');
    fprintf('Usage: image = EMrecon_EM_Mask_1D(parm,data,data_fp,sensitivity_map,startimage,mask);\n\n');
    mexEMrecon_EM_Mask;
    fprintf('\n');
    image = -1;
else
    vfprintf(1,parm,'EMrecon_EM_Mask_1D...\n');
    tic;
    image = mexEMrecon_EM_Mask(parm,data,data_fp,sensitivity_map,startimage,mask);
    vfprintf(1,parm,'EMrecon_EM_Mask_1D...done. [%f sec]\n',toc);
end

end