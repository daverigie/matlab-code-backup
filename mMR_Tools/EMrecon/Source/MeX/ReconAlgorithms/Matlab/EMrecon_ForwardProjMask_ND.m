function [fp_data] = EMrecon_ForwardProjMask_ND(parm,image,mask)

% EMrecon_ForwardProjMask_ND(parm,image,mask)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_ForwardProjMask_ND\n');
    fprintf('Usage: fp_data = EMrecon_ForwardProjMask_ND(parm,image,mask);\n\n');
    mexEMrecon_ForwardProjMask;
    fprintf('\n');
    fp_data = -1;
else
    vfprintf(1,parm,'EMrecon_ForwardProjMask_ND...\n');
    tic;
    fp_data = mexEMrecon_ForwardProjMask(parm,image(:),mask);
    vfprintf(1,parm,'EMrecon_ForwardProjMask_ND...done. [%f sec]\n',toc);
end

end