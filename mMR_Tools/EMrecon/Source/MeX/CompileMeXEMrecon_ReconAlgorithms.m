%% mexEMrecon_EM
if (compile_mexEMrecon_EM == 1)
    fprintf('compiling mexEMrecon_EM...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_EM.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/EM.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_ND';
    
    fprintf('compiling mexEMrecon_EM...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_OSEM_TV_Mask
if (compile_mexEMrecon_OSEM_TV_Mask == 1)
    fprintf('compiling mexEMrecon_OSEM_TV_Mask...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_OSEM_TV_Mask.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/EM.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_OSEM_TV_Mask_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_OSEM_TV_Mask_ND';
    
    fprintf('compiling mexEMrecon_OSEM_TV_Mask...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_EM_Mask
if (compile_mexEMrecon_EM_Mask == 1)
    fprintf('compiling mexEMrecon_EM_Mask...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_EM_Mask.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/EM.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_Mask_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_EM_Mask_ND';
    
    fprintf('compiling mexEMrecon_EM_Mask...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_BackProj
if (compile_mexEMrecon_BackProj == 1)
    fprintf('compiling mexEMrecon_BackProj...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_BackProj.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/BackProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProj_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProj_ND';
    
    fprintf('compiling mexEMrecon_BackProj...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_BackProjMask
if (compile_mexEMrecon_BackProjMask == 1)
    fprintf('compiling mexEMrecon_BackProjMask...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_BackProjMask.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/BackProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProjMask_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_BackProjMask_ND';
    
    fprintf('compiling mexEMrecon_BackProjMask...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_ForwardProj
if (compile_mexEMrecon_ForwardProj == 1)
    fprintf('compiling mexEMrecon_ForwardProj...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_ForwardProj.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/ForwardProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProj_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProj_ND';
    
    fprintf('compiling mexEMrecon_ForwardProj...done in %2.5f secs.\n',toc);
end

%% mexEMrecon_ForwardProjMask
if (compile_mexEMrecon_ForwardProjMask == 1)
    fprintf('compiling mexEMrecon_ForwardProjMask...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' ReconAlgorithms/C/mexEMrecon_ForwardProjMask.c'];
    mex_cmd = [mex_cmd ' ../ReconAlgorithms/ForwardProjection.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProjMask_1D';
    mex_id = mex_id + 1;
    MexEMreconTools(mex_id).name = 'ReconAlgorithms/Matlab/EMrecon_ForwardProjMask_ND';
    
    fprintf('compiling mexEMrecon_ForwardProjMask...done in %2.5f secs.\n',toc);
end
