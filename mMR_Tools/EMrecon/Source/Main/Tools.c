/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "Tools.h"

double tv_2d_u(double *in, int x, int y, double eps)
{
    double fdx,fdy,dx,dy,result;

    dx = Parameter.fov_x/Parameter.size_x;
    dy = Parameter.fov_y/Parameter.size_y;

    fdx = (in[(x+1)+y*Parameter.size_x]-in[x+y*Parameter.size_x])/dx;
    fdy = (in[x+(y+1)*Parameter.size_x]-in[x+y*Parameter.size_x])/dy;

    result = sqrt(fdx*fdx+fdy*fdy+eps);

    return result;
}

double tv_3d_u(double *in, int x, int y, int z, double eps)
{
    double fdx,fdy,fdz,dx,dy,dz,result;

    dx = Parameter.fov_x/Parameter.size_x;
    dy = Parameter.fov_y/Parameter.size_y;
    dz = Parameter.fov_z/Parameter.size_z;

    fdx = (in[(x+1)+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y]-in[x+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y])/dx;
    fdy = (in[x+(y+1)*Parameter.size_x+z*Parameter.size_x*Parameter.size_y]-in[x+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y])/dy;
    fdz = (in[x+y*Parameter.size_x+(z+1)*Parameter.size_x*Parameter.size_y]-in[x+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y])/dz;

    result = sqrt(fdx*fdx+fdy*fdy+fdz*fdz+eps);

    return result;
}

void tv_2d(double *in, double *out, double eps)
{
    double dx,dy;
    int y;

    dx = Parameter.fov_x/Parameter.size_x;
    dy = Parameter.fov_y/Parameter.size_y;

    for (y=1;y<Parameter.imagesize;y++)
    {
        out[y] = 0.0;
    }

#pragma omp parallel for
    for (y=1;y<Parameter.size_y-1;y++)
    {
        int i1,i2,i3,i5,i6,x;

        for (x=1;x<Parameter.size_x-1;x++)
        {
            i1 = x+y*Parameter.size_x;
            i2 = (x+1)+y*Parameter.size_x;
            i3 = x+(y+1)*Parameter.size_x;
            i5 = (x-1)+y*Parameter.size_x;
            i6 = x+(y-1)*Parameter.size_x;

            out[i1] = ((in[i1]-in[i2])/dx+(in[i1]-in[i3])/dy)/tv_2d_u(in,x,y,eps);
            out[i1] += ((in[i1]-in[i5])/dx)/tv_2d_u(in,x-1,y,eps);
            out[i1] += ((in[i1]-in[i6])/dy)/tv_2d_u(in,x,y-1,eps);
        }
    }
}

void tv_3d(double *in, double *out, double eps)
{
    double dx,dy,dz;
    int z;

    dx = Parameter.fov_x/Parameter.size_x;
    dy = Parameter.fov_y/Parameter.size_y;
    dz = Parameter.fov_z/Parameter.size_z;

#pragma omp parallel for
    for (z=1;z<Parameter.imagesize;z++)
    {
        out[z] = 0.0;
    }

#pragma omp parallel for
    for (z=1;z<Parameter.size_z-1;z++)
    {
        int i1,i2,i3,i4,i5,i6,i7,x,y;

        for (y=1;y<Parameter.size_y-1;y++)
        {
            for (x=1;x<Parameter.size_x-1;x++)
            {
                i1 = x+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y;
                i2 = (x+1)+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y;
                i3 = x+(y+1)*Parameter.size_x+z*Parameter.size_x*Parameter.size_y;
                i4 = x+y*Parameter.size_x+(z+1)*Parameter.size_x*Parameter.size_y;
                i5 = (x-1)+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y;
                i6 = x+(y-1)*Parameter.size_x+z*Parameter.size_x*Parameter.size_y;
                i7 = x+y*Parameter.size_x+(z-1)*Parameter.size_x*Parameter.size_y;

                out[i1] = ((in[i1]-in[i2])/dx+(in[i1]-in[i3])/dy+(in[i1]-in[i4])/dz)/tv_3d_u(in,x,y,z,eps);
                out[i1] += ((in[i1]-in[i5])/dx)/tv_3d_u(in,x-1,y,z,eps);
                out[i1] += ((in[i1]-in[i6])/dy)/tv_3d_u(in,x,y-1,z,eps);
                out[i1] += ((in[i1]-in[i7])/dz)/tv_3d_u(in,x,y,z-1,eps);
            }
        }
    }
}

void tv(double *in, double *out, double eps)
{
    if (Parameter.size_z == 1)
    {
        tv_2d(in,out,eps);
    }
    else
    {
        tv_3d(in,out,eps);
    }
}

/* ############################################################################################# */
/* ### convolve ################################################################################ */
/* ############################################################################################# */
/**
 * @brief convolve
 * @param in
 * @param out
 */
void convolve(double *in, double *out)
{
    int i;
    double *local_copy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        local_copy[i] = in[i];
    }

    /* check for valid fwhm */
    if (Parameter.fwhm > 0.0 || (Parameter.fwhm_x > 0.0 && Parameter.fwhm_y > 0.0 && Parameter.fwhm_z > 0.0))
    {
        int x,y,z,k;
        int ksizex,ksizey,ksizez;

        double px,py;
        double *image1, *image2;
        double *kernelx,*kernely,*kernelz;
        double sigma_x,sigma_y,sigma_z,sum,norm;

        double dx = Parameter.fov_x/Parameter.size_x;
        double dy = Parameter.fov_y/Parameter.size_y;
        double dz = Parameter.fov_z/Parameter.size_z;

        double middlex = Parameter.fov_x/2.0;
        double middley = Parameter.fov_y/2.0;

        if (Parameter.fwhm_x < 0.0)
        {
            Parameter.fwhm_x = Parameter.fwhm;
        }
        if (Parameter.fwhm_y < 0.0)
        {
            Parameter.fwhm_y = Parameter.fwhm;
        }
        if (Parameter.fwhm_z < 0.0)
        {
            Parameter.fwhm_z = Parameter.fwhm;
        }

        if (Parameter.fov_x > 0.0)
        {
            sigma_x = (Parameter.fwhm_x/2.3548);
        }
        else
        {
            sigma_x = 0.0;
        }

        if (Parameter.fov_y > 0.0)
        {
            sigma_y = (Parameter.fwhm_y/2.3548);
        }
        else
        {
            sigma_y = 0.0;
        }

        if (Parameter.fov_z > 0.0)
        {
            sigma_z = (Parameter.fwhm_z/2.3548);
        }
        else
        {
            sigma_z = 0.0;
        }

        image1 = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
        image2 = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

        ksizex = (int)(6.0*sigma_x/dx)+1;
        ksizey = (int)(6.0*sigma_y/dy)+1;
        ksizez = (int)(6.0*sigma_z/dz)+1;

        kernelx = (double*)(safe_calloc(ksizex, sizeof(double)));
        kernely = (double*)(safe_calloc(ksizey, sizeof(double)));
        kernelz = (double*)(safe_calloc(ksizez, sizeof(double)));

        for (k=-(int)(ksizex/2);k<=(int)(ksizex/2);k++)
        {
            kernelx[k+(int)(ksizex/2)] = exp(-(k*k*dx*dx)/(2*sigma_x*sigma_x));
        }

        for (k=-(int)(ksizey/2);k<=(int)(ksizey/2);k++)
        {
            kernely[k+(int)(ksizey/2)] = exp(-(k*k*dy*dy)/(2*sigma_y*sigma_y));
        }

        for (k=-(int)(ksizez/2);k<=(int)(ksizez/2);k++)
        {
            kernelz[k+(int)(ksizez/2)] = exp(-(k*k*dz*dz)/(2*sigma_z*sigma_z));
        }

        /* convolution in x-direction */
#pragma omp parallel for private(x,y,k,norm,sum)
        for (z=0;z<Parameter.size_z;z++)
        {
            for (y=0;y<Parameter.size_y;y++)
            {
                for (x=0;x<Parameter.size_x;x++)
                {
                    sum = 0.0;
                    norm = 0.0;
                    for (k=-(int)(ksizex/2);k<=(int)(ksizex/2);k++)
                    {
                        if ((k+x)>=0 && (k+x)<Parameter.size_x)
                        {
                            sum += kernelx[k+(int)(ksizex/2)]*in[z*Parameter.size_x*Parameter.size_y+y*Parameter.size_x+x+k];
                            norm += kernelx[k+(int)(ksizex/2)];
                        }
                    }
                    if (norm > 0.0)
                    {
                        image1[z*Parameter.size_x*Parameter.size_y+y*Parameter.size_x+x] = sum/norm;
                    }
                }
            }
        }

        /* convolution in y-direction */
#pragma omp parallel for private(x,y,k,norm,sum)
        for (z=0;z<Parameter.size_z;z++)
        {
            for (y=0;y<Parameter.size_y;y++)
            {
                for (x=0;x<Parameter.size_x;x++)
                {
                    sum = 0.0;
                    norm = 0.0;
                    for (k=-(int)(ksizey/2);k<=(int)(ksizey/2);k++)
                    {
                        if ((k+y)>=0 && (k+y)<Parameter.size_y)
                        {
                            sum += kernely[k+(int)(ksizey/2)]*image1[z*Parameter.size_x*Parameter.size_y+(y+k)*Parameter.size_x+x];
                            norm += kernely[k+(int)(ksizey/2)];
                        }
                    }
                    if (norm > 0.0)
                    {
                        image2[z*Parameter.size_x*Parameter.size_y+y*Parameter.size_x+x] = sum/norm;
                    }
                }
            }
        }

        /* convolution in z-direction */
#pragma omp parallel for private(x,y,k,norm,sum)
        for (z=0;z<Parameter.size_z;z++)
        {
            for (y=0;y<Parameter.size_y;y++)
            {
                for (x=0;x<Parameter.size_x;x++)
                {
                    sum = 0.0;
                    norm = 0.0;
                    for (k=-(int)(ksizez/2);k<=(int)(ksizez/2);k++)
                    {
                        if ((k+z)>=0 && (k+z)<Parameter.size_z)
                        {
                            sum += kernelz[k+(int)(ksizez/2)]*image2[(z+k)*Parameter.size_x*Parameter.size_y+y*Parameter.size_x+x];
                            norm += kernelz[k+(int)(ksizez/2)];
                        }
                    }
                    if (norm > 0.0) {
                        out[z*Parameter.size_x*Parameter.size_y+y*Parameter.size_x+x] = sum/norm;
                    }
                }
            }
        }

        /* replace all voxel outside cylindrical fov with original values to suppress artifacts */
        if (Parameter.offset_c > 0.0) {
#pragma omp parallel for private(x,y,px,py)
            for (z=0;z<Parameter.size_z;z++)
            {
                for (y=0;y<Parameter.size_y;y++)
                {
                    for (x=0;x<Parameter.size_x;x++)
                    {
                        px = (double)x * dx + dx/2.0;
                        py = (double)y * dy + dy/2.0;

                        if (sqrt((px-middlex)*(px-middlex) + (py-middley)*(py-middley)) >= Parameter.offset_c - 5.0 * Parameter.fwhm)
                        {
                            out[x+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y] = local_copy[x+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y];
                        }
                    }
                }
            }
        }

        free(image1);
        free(image2);
        free(kernelx);
        free(kernely);
        free(kernelz);
    }
    else
    {
#pragma omp parallel for
        for (i=0;i<Parameter.imagesize;i++)
        {
            out[i] = local_copy[i];
        }
    }
    free(local_copy);
}


/* ############################################################################################# */
/* ### cut_image ############################################################################### */
/* ############################################################################################# */
/**
 * @brief cut_image
 * @param f
 */
void cut_image(double *f)
{
    if (Parameter.offset_c > 0.0)
    {
        int x,y,z;
        double px,py;

        double middlex = Parameter.fov_x/2.0;
        double middley = Parameter.fov_y/2.0;

        double dx = Parameter.fov_x / (double)Parameter.size_x;
        double dy = Parameter.fov_y / (double)Parameter.size_y;

        for (z=0;z<Parameter.size_z;z++)
        {
            for (y=0;y<Parameter.size_y;y++)
            {
                for (x=0;x<Parameter.size_x;x++)
                {
                    px = (double)x * dx + dx/2.0;
                    py = (double)y * dy + dy/2.0;
                    if (sqrt((px-middlex)*(px-middlex) + (py-middley)*(py-middley)) > Parameter.offset_c)
                    {
                        f[x+y*Parameter.size_x+z*Parameter.size_x*Parameter.size_y] = 0.0;
                    }
                }
            }
        }
    }
}


/* ############################################################################################# */
/* ### safe_calloc ############################################################################# */
/* ############################################################################################# */
/**
 * @brief safe_calloc
 * @param num
 * @param size
 * @return
 */
void *safe_calloc(long num, int size)
{
    void *ptr = calloc(num,size);
    if (ptr==0)
    {
        message(0,"Could not malloc (%d) x (%d).",num,size);
        exit(EXIT_FAILURE);
    }
    return ptr;
}


/* ############################################################################################# */
/* ### safe_calloc_path ######################################################################## */
/* ############################################################################################# */
/**
 * @brief safe_calloc_path
 * @return
 */
path_element *safe_calloc_path()
{
#ifndef WINDOWS_MATLAB
    path_element *ptr = safe_calloc((Parameter.size_x+Parameter.size_y+Parameter.size_z)*safe_omp_get_num_threads(),sizeof(path_element));
#else
    path_element *ptr = safe_calloc(Parameter.size_x+Parameter.size_y+Parameter.size_z,sizeof(path_element));
#endif

    return ptr;
}


/* ############################################################################################# */
/* ### safe_omp_get_num_threads ################################################################ */
/* ############################################################################################# */
/**
 * @brief safe_omp_get_num_threads
 * @return
 */
int safe_omp_get_num_threads()
{
    /* omp_get_num_threads does not seem to be stable on all platforms */
    int num_threads = 0;

#pragma omp parallel reduction(+:num_threads)
    num_threads += 1;

    return num_threads;
}


/* ############################################################################################# */
/* ### safe_omp_get_thread_num ################################################################# */
/* ############################################################################################# */
/**
 * @brief safe_omp_get_thread_num
 * @return
 */
int safe_omp_get_thread_num()
{
    int thread_num = 0;

#ifndef WINDOWS_MATLAB
    thread_num = omp_get_thread_num();
#endif

    return thread_num;
}
