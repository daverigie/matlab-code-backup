/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "Siddon.h"


/* ############################################################################################# */
/* ### compute_path ############################################################################ */
/* ############################################################################################# */
/**
 * @brief compute_path
 * @param X1
 * @param Y1
 * @param Z1
 * @param X2
 * @param Y2
 * @param Z2
 * @param Path
 */
void compute_path(double X1, double Y1, double Z1, double X2, double Y2, double Z2, path_element *Path)
{
    int i,in_plane;

    double tmp_1,tmp_n;
    double dx,dy,dz,d12;
    double alpha_min,alpha_max;

    double diffx,diffy,diffz;

    double alpha_min_x,alpha_min_y,alpha_min_z;
    double alpha_max_x,alpha_max_y,alpha_max_z;
    double new_alpha,new_alpha_x,new_alpha_y,new_alpha_z;

    int x_direction,y_direction,z_direction;

    diffx = X2-X1;
    diffy = Y2-Y1;
    diffz = Z2-Z1;

    x_direction = -1;
    y_direction = -1;
    z_direction = -1;

    alpha_min_x = 0.0;
    alpha_min_y = 0.0;
    alpha_min_z = 0.0;
    alpha_max_x = 1.0;
    alpha_max_y = 1.0;
    alpha_max_z = 1.0;

    new_alpha = 0.0;
    new_alpha_x = 1.0;
    new_alpha_y = 1.0;
    new_alpha_z = 1.0;

    dx = (siddon_x_plane_Nx-siddon_x_plane_1)/siddon_size_x;
    dy = (siddon_y_plane_Ny-siddon_y_plane_1)/siddon_size_y;
    dz = (siddon_z_plane_Nz-siddon_z_plane_1)/siddon_size_z;
    d12 = sqrt(diffx*diffx+diffy*diffy+diffz*diffz);

    i = 0;
    in_plane = 1;
    Path[0].coord = -1;

    if (fabs(diffx) > Parameter.epsilon)
    {
        new_alpha_x = 0.0;
        tmp_1 = (siddon_x_plane_1-X1)/diffx;
        tmp_n = (siddon_x_plane_Nx-X1)/diffx;
        if (tmp_1 > tmp_n)
        {
            alpha_min_x = tmp_n;
            alpha_max_x = tmp_1;
        }
        else
        {
            alpha_max_x = tmp_n;
            alpha_min_x = tmp_1;
            x_direction = 1;
        }
    }
    else
    {
        diffx = Parameter.epsilon;
        if (X1 < siddon_x_plane_1 || X1 > siddon_x_plane_Nx)
        {
            in_plane = 0;
        }
    }

    if (fabs(diffy) > Parameter.epsilon)
    {
        new_alpha_y = 0.0;
        tmp_1 = (siddon_y_plane_1-Y1)/diffy;
        tmp_n = (siddon_y_plane_Ny-Y1)/diffy;
        if (tmp_1 > tmp_n)
        {
            alpha_min_y = tmp_n;
            alpha_max_y = tmp_1;
        }
        else
        {
            alpha_max_y = tmp_n;
            alpha_min_y = tmp_1;
            y_direction = 1;
        }
    }
    else {
        diffy = Parameter.epsilon;
        if (Y1 < siddon_y_plane_1 || Y1 > siddon_y_plane_Ny)
        {
            in_plane = 0;
        }
    }

    if (fabs(diffz) > Parameter.epsilon)
    {
        new_alpha_z = 0.0;
        tmp_1 = (siddon_z_plane_1-Z1)/diffz;
        tmp_n = (siddon_z_plane_Nz-Z1)/diffz;
        if (tmp_1 > tmp_n)
        {
            alpha_min_z = tmp_n;
            alpha_max_z = tmp_1;
        }
        else
        {
            alpha_max_z = tmp_n;
            alpha_min_z = tmp_1;
            z_direction = 1;
        }
    }
    else {
        diffz = Parameter.epsilon;
        if (Z1 < siddon_z_plane_1 || Z1 > siddon_z_plane_Nz)
        {
            in_plane = 0;
        }
    }

    alpha_min = max(alpha_min_x,max(alpha_min_y,max(alpha_min_z,0.0)));
    alpha_max = min(alpha_max_x,min(alpha_max_y,min(alpha_max_z,1.0)));

    if (alpha_min < alpha_max && in_plane == 1)
    {
        float i_start = (X1+alpha_min*diffx-siddon_x_plane_1)/dx;
        float j_start = (Y1+alpha_min*diffy-siddon_y_plane_1)/dy;
        float k_start = (Z1+alpha_min*diffz-siddon_z_plane_1)/dz;

        int i_m = max(0,min((int)i_start,siddon_size_x-1));
        int j_m = max(0,min((int)j_start,siddon_size_y-1));
        int k_m = max(0,min((int)k_start,siddon_size_z-1));

        if (new_alpha_x < 1.0) new_alpha_x = (siddon_x_plane_1+i_m*dx-X1)/diffx;
        if (new_alpha_y < 1.0) new_alpha_y = (siddon_y_plane_1+j_m*dy-Y1)/diffy;
        if (new_alpha_z < 1.0) new_alpha_z = (siddon_z_plane_1+k_m*dz-Z1)/diffz;

        if (x_direction > 0) new_alpha_x += fabs(dx/diffx);
        if (y_direction > 0) new_alpha_y += fabs(dy/diffy);
        if (z_direction > 0) new_alpha_z += fabs(dz/diffz);

        while (alpha_min < alpha_max)
        {
            new_alpha = min(alpha_max,min(new_alpha_x,min(new_alpha_y,new_alpha_z)));

            if (i_m >= 0 && i_m < siddon_size_x && j_m >= 0 && j_m < siddon_size_y && k_m >= 0 && k_m < siddon_size_z)
            {
                Path[i].coord = i_m+j_m*siddon_size_x+k_m*siddon_size_x*siddon_size_y;
                Path[i].length = (new_alpha-alpha_min)*d12;
                i++;
            }

            if (fabs(new_alpha_x-new_alpha) < Parameter.epsilon)
            {
                new_alpha_x += fabs(dx/diffx);
                i_m += x_direction;
            }

            if (fabs(new_alpha_y-new_alpha) < Parameter.epsilon)
            {
                new_alpha_y += fabs(dy/diffy);
                j_m += y_direction;
            }

            if (fabs(new_alpha_z-new_alpha) < Parameter.epsilon)
            {
                new_alpha_z += fabs(dz/diffz);
                k_m += z_direction;
            }

            alpha_min = new_alpha;
        }
        Path[i].coord = -1;
    }
}


/* ############################################################################################# */
/* ### setup_siddon ############################################################################ */
/* ############################################################################################# */
void setup_siddon()
{
    siddon_size_x = Parameter.size_x;
    siddon_size_y = Parameter.size_y;
    siddon_size_z = Parameter.size_z;
    siddon_x_plane_1 = Parameter.offset_x;
    siddon_y_plane_1 = Parameter.offset_y;
    siddon_z_plane_1 = Parameter.offset_z;
    siddon_x_plane_Nx = Parameter.offset_x+Parameter.fov_x;
    siddon_y_plane_Ny = Parameter.offset_y+Parameter.fov_y;
    siddon_z_plane_Nz = Parameter.offset_z+Parameter.fov_z;
}
