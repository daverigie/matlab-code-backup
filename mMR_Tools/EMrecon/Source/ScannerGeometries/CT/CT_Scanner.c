/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "CT_Scanner.h"


/* ############################################################################################# */
/* ### ct_scanner_get_number_of_events_in_file ################################################# */
/* ############################################################################################# */
/**
 * @brief ct_scanner_get_number_of_events_in_file Calculates the number of events in the CT datafile.
 * @return
 */
long ct_scanner_get_number_of_events_in_file()
{
    long events_in_file;
    events_in_file = CT_Scanner.detector_channels*CT_Scanner.detector_rows*CT_Scanner.readings;
    return events_in_file;
}


/* ############################################################################################# */
/* ### ct_scanner_setup ######################################################################## */
/* ############################################################################################# */
/**
 * @brief ct_scanner_setup
 */
void ct_scanner_setup()
{
    CT_Scanner.angles = 1;

    CT_Scanner.fan_angle_increment = 0.06;

    CT_Scanner.slice_width = 1.0;

    CT_Scanner.detector_channels = 1;
    CT_Scanner.detector_rows = 1;

    CT_Scanner.distance_source_rcenter = 500.0;
    CT_Scanner.distance_rcenter_detector = 500.0;

    CT_Scanner.ffs_mode = 0;

    CT_Scanner.readings = 1;

    CT_Scanner.rotation_offset = 0.0;

    CT_Scanner.source_z0 = 0.0;


    /* image related parameter */
    Parameter.size_x = 256;
    Parameter.size_y = 256;
    Parameter.size_z = 20;
    Parameter.fov_x = 512.0;
    Parameter.fov_y = 512.0;
    Parameter.fov_z = 80.0;
    Parameter.offset_x = -Parameter.fov_x/2.0;
    Parameter.offset_y = -Parameter.fov_y/2.0;
    Parameter.offset_z = -Parameter.fov_z/2.0;
    Parameter.offset_c = 400.0;

    /* data related parameter */
    Parameter.datatype = 1;

    /* scanner geometry */
    CT_Scanner.angles = 2100;
    CT_Scanner.detector_rows = 96;
    CT_Scanner.slice_width = 0.6;
}


/* ############################################################################################# */
/* ### ct_scanner_update_parameter ############################################################# */
/* ############################################################################################# */
/**
 * @brief ct_scanner_update_parameter
 */
void ct_scanner_update_parameter()
{
    CT_Scanner.angles = UpdateParameterInt("ANGLES",CT_Scanner.angles);

    CT_Scanner.fan_angle_increment = UpdateParameterDouble("FAN_ANGLE_INCREMENT",CT_Scanner.fan_angle_increment);

    CT_Scanner.ffs_mode = UpdateParameterInt("FFS_MODE",CT_Scanner.ffs_mode);

    CT_Scanner.slice_width = UpdateParameterDouble("SLICE_WIDTH",CT_Scanner.slice_width);

    CT_Scanner.readings = UpdateParameterInt("READINGS",CT_Scanner.readings);

    CT_Scanner.rotation_offset = UpdateParameterDouble("ROTATION_OFFSET",CT_Scanner.rotation_offset);

    CT_Scanner.source_z0 = UpdateParameterDouble("SOURCE_Z0",CT_Scanner.source_z0);

    CT_Scanner.detector_alignment = UpdateParameterDouble("DETECTOR_ALIGNMENT",CT_Scanner.detector_alignment);
    CT_Scanner.detector_channels = UpdateParameterInt("DETECTOR_CHANNELS",CT_Scanner.detector_channels);
    CT_Scanner.detector_rows = UpdateParameterInt("DETECTOR_ROWS",CT_Scanner.detector_rows);

    CT_Scanner.distance_source_rcenter = UpdateParameterDouble("DISTANCE_SOURCE_RCENTER",CT_Scanner.distance_source_rcenter);
    CT_Scanner.distance_rcenter_detector = UpdateParameterDouble("DISTANCE_RCENTER_DETECTOR",CT_Scanner.distance_rcenter_detector);
}


/* ############################################################################################# */
/* ### ct_scanner_update_subset_index ########################################################## */
/* ############################################################################################# */
/**
 * @brief ct_scanner_update_subset_index
 * @param subsetindex
 * @param subset
 */
void ct_scanner_update_subset_index(short *subsetindex, int subset)
{
    int phi,current_row,current_channel;
    int i,rotations,valid_phi;
    int *subsetvector;
    long events_in_file = get_number_of_events_in_file();

    if (Parameter.subsets > 1)
    {
        for (i=0;i<events_in_file;i++)
        {
            subsetindex[i] = 0;
        }

        /* choose subsets per full rotation (angles) */
        subsetvector = (int*)safe_calloc(CT_Scanner.angles/Parameter.subsets,sizeof(int));
        derive_subset_vector(CT_Scanner.angles,subset-1,subsetvector);

        /* expand valid phi to full datasets (in case of more than one rotation) */
        rotations = CT_Scanner.readings/CT_Scanner.angles;
        if (CT_Scanner.readings % CT_Scanner.angles > 0)
        {
            rotations++;
        }

        for (i=0;i<rotations;i++)
        {
            for (phi=0;phi<CT_Scanner.angles/Parameter.subsets;phi++)
            {
                valid_phi = subsetvector[phi] + i*CT_Scanner.angles;
                if (valid_phi < CT_Scanner.readings)
                {
                    for (current_row=0;current_row<CT_Scanner.detector_rows;current_row++)
                    {
                        for (current_channel=0;current_channel<CT_Scanner.detector_channels;current_channel++)
                        {
                            subsetindex[current_channel+current_row*CT_Scanner.detector_channels+valid_phi*CT_Scanner.detector_channels*CT_Scanner.detector_rows] = 1;
                        }
                    }
                }
            }
        }

        free(subsetvector);
    }
    else
    {
        for (i=0;i<events_in_file;i++)
        {
            subsetindex[i] = 1;
        }
    }
}
