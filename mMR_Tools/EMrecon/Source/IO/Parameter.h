/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#ifndef PARAMETER_H
#define PARAMETER_H


/* includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/* emrecon includes */
#include "IO.h"


/* define constants */
#define MAX_EXTERNAL_PARAMETER 255


/* struct definitions */
typedef struct {
    char var_name[255];
    char var_value[255];
    double *var_value_double;
    int type;
} parm_char;

typedef struct {
    parm_char var_parm[MAX_EXTERNAL_PARAMETER];

    int convolve,
    convolutioninterval,
    datatype,
    fixed_subset,
    imagesize,
    iterations,
    samplingfactor_phi,
    samplingfactor_z,
    scannertype,
    size_x,size_y,size_z,
    subsets,
    subset_order,
    verbose;

    double epsilon,
    fwhm,fwhm_x,fwhm_y,fwhm_z,
    fov_x,fov_y,fov_z,
    offset_c,offset_x,offset_y,offset_z;

} parameter;


/* global variables */
parameter Parameter;


/* function declarations */
void GetParameterChar(char *ParameterName, char *retVal);
double GetParameterDouble(char *ParameterName);
int GetParameterInt(char *ParameterName);
void GetParameterVectorDouble(char *ParameterName, double *ParameterValue, int ParameterSize);
void InitializeParameter();
void ParameterReleaseMemory();
void SetParameter(char *ParameterName, char *ParameterValue);
void SetParameterVector(char *ParameterName, double *ParameterValue, int ParameterSize);
double UpdateParameterDouble(char *ParameterName, double ParameterValue);
int UpdateParameterInt(char *ParameterName, int ParameterValue);
void UpdateParameterList();


#endif
