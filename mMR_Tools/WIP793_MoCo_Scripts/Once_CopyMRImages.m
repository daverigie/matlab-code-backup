% Once_CopyMRImages.m
close all; clear; clc;

setupid = 4;
gates = 10;
slices = 88;

i = 0;
if (setupid == 1)
    i = i + 1; id(i).name = '1';
    i = i + 1; id(i).name = '6';
    i = i + 1; id(i).name = '7';
    % i = i + 1; id(i).name = '8'; % error
    i = i + 1; id(i).name = '9';
    % i = i + 1; id(i).name = '10'; % error
    i = i + 1; id(i).name = '12';
    % i = i + 1; id(i).name = '13'; % error
    % i = i + 1; id(i).name = '14'; % error
    % i = i + 1; id(i).name = '18'; % error
    % i = i + 1; id(i).name = '19'; % error
end

if (setupid == 2)
    i = i + 1; id(i).name = 'B';
    i = i + 1; id(i).name = 'D';
    i = i + 1; id(i).name = 'F';
    i = i + 1; id(i).name = 'I';
    i = i + 1; id(i).name = 'K';
    i = i + 1; id(i).name = 'P';
    i = i + 1; id(i).name = 'Q';
    i = i + 1; id(i).name = 'S';
    % i = i + 1; id(i).name = 'V';
end

if (setupid == 3)
    i = i + 1; id(i).name = 'V';
end

if (setupid == 4)
    i = i + 1; id(i).name = '8';
    i = i + 1; id(i).name = '10';
    i = i + 1; id(i).name = '13';
    i = i + 1; id(i).name = '14';
    i = i + 1; id(i).name = '18';
    i = i + 1; id(i).name = '19';
end

if (setupid == 1)
    base_path_source = 'R:\koestt01labspace\Collaboration\Siemens\received\Erlangen\2015-12-16\NYU10Bins\NYU10Bins';
end

if (setupid == 2)
    base_path_source = 'R:\koestt01labspace\Collaboration\Siemens\received\Erlangen\2015-12-23\NYU10Bins';
end

if (setupid == 3)
    base_path_source = 'R:\koestt01labspace\Collaboration\Siemens\received\Erlangen\2016-01-07\NYU10Bins_Missing';
end

if (setupid == 4)
    base_path_source = 'R:\koestt01labspace\Collaboration\Siemens\received\Erlangen\2016-01-07\NYU10Bins_Missing';
end

base_path_destination = 'R:\koestt01labspace\Data\PET\MotionCorrection\WIP793';

for i=1:numel(id)
    if (setupid ~= 3)
        fprintf('%s\\PATIENT%s_10BINS\n',base_path_source,id(i).name);
        dirinfo = dir(sprintf('%s\\PATIENT%s_10BINS',base_path_source,id(i).name));
    else
        fprintf('%s\\PATIENT_%s_10BINS\n',base_path_source,id(i).name);
        dirinfo = dir(sprintf('%s\\PATIENT_%s_10BINS',base_path_source,id(i).name));
    end
    
    % check if data is complete
    cgate = zeros(gates,1);
    gatedid(gates).name = ''; %#ok<SAGROW>
    for j=3:numel(dirinfo)
        if (setupid ~= 3)
            dinfo = dicominfo(sprintf('%s\\PATIENT%s_10BINS\\%s',base_path_source,id(i).name,dirinfo(j).name));
        else
            dinfo = dicominfo(sprintf('%s\\PATIENT_%s_10BINS\\%s',base_path_source,id(i).name,dirinfo(j).name));
        end
        
        for g=1:gates
            pos = strfind(dinfo.SeriesDescription,sprintf('RR_Uniform-Bin%d',g));
            if (numel(pos) > 0)
                if (cgate(g) == 0)
                    gatedid(g).name = dirinfo(j).name(1:29);
                    fprintf('%d %s\n',g,gatedid(g).name);
                    cgate(g) = 1;
                end
            end
        end
    end
    
    if (sum(cgate) ~= gates)
        fprintf('invalid number of gates identified: %d/%d\n',cgate,gates);
        return;
    end
    
    for k=1:gates
        destination = sprintf('%s\\Patient_%s\\Patient_%s_MR_bin_%d_%d',base_path_destination,id(i).name,id(i).name,k,gates);
        if (exist(destination,'dir') ~= 7)
            mkdir(destination);
        end
        disp(destination);
        
        cslice = 0;
        for j=3:numel(dirinfo)
            pos = strfind(dirinfo(j).name,gatedid(k).name);
            if (numel(pos) > 0)
                cslice = cslice + 1;
                if (setupid ~= 3)
                    source = sprintf('%s\\PATIENT%s_10BINS\\%s',base_path_source,id(i).name,dirinfo(j).name);
                else
                    source = sprintf('%s\\PATIENT_%s_10BINS\\%s',base_path_source,id(i).name,dirinfo(j).name);
                end
                dos(sprintf('copy %s %s',source,destination));
            end
        end
        
        if (cslice ~= slices)
            fprintf('invalid number of slices identified: %d/%d\n',cslice,slices);
            return;
        end
    end
end
