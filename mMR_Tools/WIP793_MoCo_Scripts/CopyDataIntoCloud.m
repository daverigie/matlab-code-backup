close all; clear; clc;

i = 0;
i = i+1; patient_list(i).name = 'Patient_1';
i = i+1; patient_list(i).name = 'Patient_6';
i = i+1; patient_list(i).name = 'Patient_7';
i = i+1; patient_list(i).name = 'Patient_8';
i = i+1; patient_list(i).name = 'Patient_9';
i = i+1; patient_list(i).name = 'Patient_10';
i = i+1; patient_list(i).name = 'Patient_12';
i = i+1; patient_list(i).name = 'Patient_13';
i = i+1; patient_list(i).name = 'Patient_14';
i = i+1; patient_list(i).name = 'Patient_18';
i = i+1; patient_list(i).name = 'Patient_19';
i = i+1; patient_list(i).name = 'Patient_B';
i = i+1; patient_list(i).name = 'Patient_D';
i = i+1; patient_list(i).name = 'Patient_F';
i = i+1; patient_list(i).name = 'Patient_I';
i = i+1; patient_list(i).name = 'Patient_K';
i = i+1; patient_list(i).name = 'Patient_P';
i = i+1; patient_list(i).name = 'Patient_Q';
i = i+1; patient_list(i).name = 'Patient_S';
i = i+1; patient_list(i).name = 'Patient_V';

base_path_cloud = 'D:\Shared\Box Sync\Michael\WIP793';
base_path_results = 'Q:\labspace\mMR_MotionCorrection\WIP793';

for i=1:numel(patient_list)
    dst_dir = sprintf('%s\\%s',base_path_cloud,patient_list(i).name);
    if (exist(dst_dir,'dir') ~= 7)
        mkdir(dst_dir);
    end
    
    gates = [5 10];
    
    dst_path = sprintf('%s\\%s',base_path_cloud,patient_list(i).name);
    src_path = sprintf('%s\\%s\\results\\',base_path_results,patient_list(i).name);
    
    for g=1:numel(gates)
        dst = sprintf('%s\\%s_PET_Recon_Gate_AC_1_%d_172.mat',dst_path,patient_list(i).name,gates(g));
        src = sprintf('%s\\%s_PET_Recon_Gate_AC_1_%d_172.mat',src_path,patient_list(i).name,gates(g));
        if (exist(dst,'file') ~= 2 && exist(src,'file'))
            dos(sprintf('cp ''%s'' ''%s''',src,dst));
        end
        
        dst = sprintf('%s\\%s_PET_AllGates_MC_IW_AC_%d_172.mat',dst_path,patient_list(i).name,gates(g));
        src = sprintf('%s\\%s_PET_AllGates_MC_IW_AC_%d_172.mat',src_path,patient_list(i).name,gates(g));
        if (exist(dst,'file') ~= 2 && exist(src,'file'))
            dos(sprintf('cp ''%s'' ''%s''',src,dst));
        end
        
        dst = sprintf('%s\\%s_PET_AllGates_MC_PM_AC_%d_172.mat',dst_path,patient_list(i).name,gates(g));
        src = sprintf('%s\\%s_PET_AllGates_MC_PM_AC_%d_172.mat',src_path,patient_list(i).name,gates(g));
        if (exist(dst,'file') ~= 2 && exist(src,'file'))
            dos(sprintf('cp ''%s'' ''%s''',src,dst));
        end
        
        dst = sprintf('%s\\%s_PET_AllGates_MC_PM_INV_AC_%d_172.mat',dst_path,patient_list(i).name,gates(g));
        src = sprintf('%s\\%s_PET_AllGates_MC_PM_INV_AC_%d_172.mat',src_path,patient_list(i).name,gates(g));
        if (exist(dst,'file') ~= 2 && exist(src,'file'))
            dos(sprintf('cp ''%s'' ''%s''',src,dst));
        end
        
        dst = sprintf('%s\\%s_PET_AllGates_MC_PM_V2_AC_%d_172.mat',dst_path,patient_list(i).name,gates(g));
        src = sprintf('%s\\%s_PET_AllGates_MC_PM_V2_AC_%d_172.mat',src_path,patient_list(i).name,gates(g));
        if (exist(dst,'file') ~= 2 && exist(src,'file'))
            dos(sprintf('cp ''%s'' ''%s''',src,dst));
        end
    end
    
    dst = sprintf('%s\\%s_PET_AllGates_AC_%d_172.mat',dst_path,patient_list(i).name,10);
    src = sprintf('%s\\%s_PET_AllGates_AC_%d_172.mat',src_path,patient_list(i).name,10);
    if (exist(dst,'file') ~= 2 && exist(src,'file'))
        dos(sprintf('cp ''%s'' ''%s''',src,dst));
    end
end
