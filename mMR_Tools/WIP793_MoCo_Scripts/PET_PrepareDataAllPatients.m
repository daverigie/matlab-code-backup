% clean up
close all; clear mex; clear; clc;

% extend path
addpath(genpath('H:\Projects\Matlab'));
addpath(genpath('R:\koestt01labspace\Projects\MotionCorrection\WIP793'));

% set parameter
preview_only = 1;
base_path_win_data = 'R:\koestt01labspace\Data\PET\MotionCorrection\WIP793';
base_path_win_results = 'Q:\labspace\mMR_MotionCorrection\WIP793';
base_path_linux_data = '/srv/mriscan/Working-Temp/koestt01/MoCo_WIP';
matrix_size = 172;
gates = 5;
only_missing_scripts = 1;

% debug
% fixed_id = [1 2 4 5 6 9 10 14 15 16 17 19 21 22 25 26 29 30 31 33];
fixed_id = [14 17 4 10 19 21 30 33];

% get list of patients to process
p = 1;
dirinfo = dir(base_path_win_data);
for i=3:numel(dirinfo)
    % get patient folder in base_path_data
    pos = strfind(sprintf('%s\\%s',base_path_win_data,dirinfo(i).name),'Patient_');
    if (numel(pos) > 0 && exist(sprintf('%s\\%s',base_path_win_data,dirinfo(i).name),'dir') == 7)
        % search for .mat file in patient folder
        pdirinfo = dir(sprintf('%s\\%s',base_path_win_data,dirinfo(i).name));
        for j=3:numel(pdirinfo)
            pos2 = strfind(sprintf('%s\\%s\\%s',base_path_win_data,dirinfo(i).name,pdirinfo(j).name),'.mat');
            % - if .mat exists, patient data is available. check if already
            %   processed
            if (numel(pos2) > 0)
                patient_list(p).name = dirinfo(i).name;
                p = p + 1;
            end
        end
    end
end

% process data sets
if (exist('patient_list','var') == 1)
    for i=1:numel(patient_list)
        if (max(fixed_id) < 0 || (min(fixed_id) > 0 && ismember(i,fixed_id) == 1))
            fprintf('%d - %s ...\n',i,patient_list(i).name);
            prepare_tic = tic;
            if (preview_only == 0)
                PET_PrepareDataSinglePatient(base_path_win_data,base_path_win_results,base_path_linux_data,matrix_size,gates,patient_list(i).name,only_missing_scripts);
            end
            fprintf('%d - %s ... done. [%f sec]\n',i,patient_list(i).name,toc(prepare_tic));
        end
    end
end
