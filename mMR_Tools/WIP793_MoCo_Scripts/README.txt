1) PET_PrepareDataAllPatients
- creates PET sinograms and reconstruction scripts


2) MR_ResampleDataAllPatients
- resamples MR images to PET resolution + location


3a) MR_ReformatVectorsAllPatients_PM
- reformats MR based vectors from Siemens and applies it to MR (and if available PET)
- updates only recon scripts if image based correction was found
- only file that uses reference_gate

3b) MR_ReformatVectorsAllPatients_PM_Inv
- reformats MR based vectors from Siemens and applies it to MR (and if available PET)
- updates only recon scripts if image based correction was found
- only file that uses reference_gate

3c) MR_ReformatVectorsAllPatients_PM_v2
- reformats MR based vectors from Siemens and applies it to MR (and if available PET)
- updates only recon scripts if image based correction was found
- only file that uses reference_gate

3d) MR_ReformatVectorsAllPatients_IW
- reformats MR based vectors from Siemens and applies it to MR (and if available PET)
- updates only recon scripts if image based correction was found
- only file that uses reference_gate


4a) CopyDataAndScripts_Recon_MoCo_PM
4b) CopyDataAndScripts_Recon_MoCo_PM_Inv
4c) CopyDataAndScripts_Recon_MoCo_PM_v2
4d) CopyDataAndScripts_Recon_MoCo_IW


5) PET_RunMoCoRecons


6a) CopyResults_Recon_MoCo_PM
6b) CopyResults_Recon_MoCo_PM_Inv
6c) CopyResults_Recon_MoCo_PM_v2
6d) CopyResults_Recon_MoCo_IW

