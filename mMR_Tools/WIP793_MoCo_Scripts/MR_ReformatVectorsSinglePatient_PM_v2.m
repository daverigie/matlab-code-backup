function [] = MR_ReformatVectorsSinglePatient_PM_v2(base_path_win_data,base_path_win_results,base_path_linux_data,gates,reference_gate,patient,show_results)

% convolutioninterval
convolutioninterval = 63;

% matrix size
matrix_size = 172;

% initialize vectors
vecs = zeros(matrix_size,matrix_size,127,3,gates);

% check whether vectors have been reformated already
load_data = gates;
for g=1:gates
    if (exist(sprintf('%s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected_v2.mat',base_path_win_results,patient,patient,g,gates,matrix_size),'file') == 2)
        load_data = load_data - 1;
    end
end

% load vectors
if (load_data > 0)
    for g=1:gates
        if (g ~= reference_gate)
            vecs(:,:,:,1,g) = readimage(sprintf('%s\\%s\\Vectors\\%d_%d_%d\\invDefFieldX.v',base_path_win_data,patient,gates,reference_gate,g),matrix_size,matrix_size,127,1);
            vecs(:,:,:,2,g) = readimage(sprintf('%s\\%s\\Vectors\\%d_%d_%d\\invDefFieldY.v',base_path_win_data,patient,gates,reference_gate,g),matrix_size,matrix_size,127,1);
            vecs(:,:,:,3,g) = readimage(sprintf('%s\\%s\\Vectors\\%d_%d_%d\\invDefFieldZ.v',base_path_win_data,patient,gates,reference_gate,g),matrix_size,matrix_size,127,1);
        end
    end
end

for g=1:gates
    % convert motion vectors if not yet done
    if (exist(sprintf('%s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected_v2.mat',base_path_win_results,patient,patient,g,gates,matrix_size),'file') ~= 2)
        fprintf('converting motion vectors for gate %d of %d...\n',g,gates);
        
        % extract motion for current gate
        u = vecs(:,:,:,1,g);
        v = vecs(:,:,:,2,g);
        w = vecs(:,:,:,3,g);
        
        % save corrected MR gate and motion vectors
        save(sprintf('%s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected_v2.mat',base_path_win_results,patient,patient,g,gates,matrix_size),'u','v','w');
        
        fprintf('converting motion vectors for gate %d of %d...done.\n',g,gates);
    end
end

%% create script to reconstruct data using motion correction (pm)
fid = fopen(sprintf('%s\\%s\\%s_recon_mc_pm_v2.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path etc.\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.EPSILON = 0.0001;\n');
fprintf(fid,'parm.CONVOLVE = 0;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = %d;\n',convolutioninterval);
fprintf(fid,'parm.MC_SCALING = 0;\n');
fprintf(fid,'parm.FIXED_SUBSET = -1;\n');

fprintf(fid,'\n%% process motion vectors\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected_v2.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertTemplate2Reference(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w'');\n');
end

fprintf(fid,'\n%% load data\n');
for g=1:gates
    fprintf(fid,'load(''data/%s_PET_Gate_%d_%d_%d.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
    fprintf(fid,'clear(''sensitivity_map_%d'');\n',matrix_size);
    fprintf(fid,'data_%d = data;\n',g);
    fprintf(fid,'data_fp_%d = data_fp;\n',g);
    fprintf(fid,'clear(''data'');\n');
    fprintf(fid,'clear(''data_fp'');\n');
end

fprintf(fid,'\n%% create sensitivity_map\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'sensitivity_map_mc_pm_v2_%d_%d = EMrecon_MC_BackProj_ND(parm,ones(344*252*837,1),motion_data_%d);\n',gates,matrix_size,g);
    else
        fprintf(fid,'sensitivity_map_mc_pm_v2_%d_%d = sensitivity_map_mc_pm_v2_%d_%d + EMrecon_MC_BackProj_ND(parm,ones(344*252*837,1),motion_data_%d);\n',gates,matrix_size,gates,matrix_size,g);
    end
end

fprintf(fid,'\n%% reconstruct PET data for all gates MC\n');
fprintf(fid,sprintf('em_all_gates_mc_pm_v2_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));

fprintf(fid,'for i=1:parm.ITERATIONS\n');
fprintf(fid,'\tfor s=1:parm.SUBSETS\n');
fprintf(fid,'\t\tparm.FIXED_SUBSET = s;\n\n');
fprintf(fid,'\t\tfprintf(''i%%d s%%d\\n'',i,s)\n');
fprintf(fid,'\t\tsubset_tic = tic;\n\n');

fprintf(fid,'\t\t%% em mc step for each gate\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'\t\tback = EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d,em_all_gates_mc_pm_v2_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    else
        fprintf(fid,'\t\tback = back + EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d,em_all_gates_mc_pm_v2_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    end
end

fprintf(fid,'\n\t\t%% image update\n');
fprintf(fid,'\t\tindex_image_update = (em_all_gates_mc_pm_v2_%d_%d > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map_mc_pm_v2_%d_%d > parm.EPSILON);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_%d_%d(index_image_update) = em_all_gates_mc_pm_v2_%d_%d(index_image_update) .* back(index_image_update) ./ sensitivity_map_mc_pm_v2_%d_%d(index_image_update);\n',gates,matrix_size,gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_%d_%d(~index_image_update) = 0.0;\n\n',gates,matrix_size);

fprintf(fid,'\t\t%% smooth image\n');
fprintf(fid,'\t\tif (parm.CONVOLUTIONINTERVAL > 0 && mod(i*s,parm.CONVOLUTIONINTERVAL) == 0)\n');
fprintf(fid,'\t\t\t em_all_gates_mc_pm_v2_%d_%d = EMrecon_IP_Convolve_ND(parm,em_all_gates_mc_pm_v2_%d_%d);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tend\n\n');

fprintf(fid,'\t\t%% reduce image to cylindrical fov\n');
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_%d_%d = EMrecon_IP_CutImage_ND(parm,em_all_gates_mc_pm_v2_%d_%d);\n',gates,matrix_size,gates,matrix_size);

if (show_results == 1)
    fprintf(fid,'\n\t\t%% show current result\n');
    fprintf(fid,'\t\timagesc(squeeze(em_all_gates_mc_pm_v2_%d_%d(:,80,:))''); title(sprintf(''i%%d s%%d'',i,s)); axis xy; axis image; drawnow;\n\n',matrix_size);
end

fprintf(fid,'\t\tfprintf(''i%%d s%%d done [%%f]\\n'',i,s,toc(subset_tic))\n');
fprintf(fid,'\tend\n');
fprintf(fid,'end\n');

fprintf(fid,'\n%% save results\n');
fprintf(fid,sprintf('save(''results/%s_PET_AllGates_MC_PM_V2_%d_%d'',''em_all_gates_mc_pm_v2_%d_%d'',''sensitivity_map_mc_pm_v2_%d_%d'',''parm'');\n\n',patient,gates,matrix_size,gates,matrix_size,gates,matrix_size));

fprintf(fid,'%% free memory \n');
for g=1:gates
    fprintf(fid,'clear(''motion_data_%d''); ',g);
    fprintf(fid,'clear(''data_%d''); ',g);
    fprintf(fid,'clear(''data_fp_%d'');\n',g);
end

fprintf(fid,'clear(''sensitivity_map_mc_pm_v2_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''em_all_gates_mc_pm_v2_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''back'');\n');
fprintf(fid,'clear(''parm'');\n');
fclose(fid);

%% create script to reconstruct data using motion correction (pm) and zero motion vectors
fid = fopen(sprintf('%s\\%s\\%s_recon_mc_pm_v2_0.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path etc.\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.EPSILON = 0.0001;\n');
fprintf(fid,'parm.CONVOLVE = 0;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = %d;\n',convolutioninterval);
fprintf(fid,'parm.MC_SCALING = 0;\n');
fprintf(fid,'parm.FIXED_SUBSET = -1;\n');

fprintf(fid,'\n%% process motion vectors\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected_v2.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'u = zeros(size(u));\n');
    fprintf(fid,'v = zeros(size(v));\n');
    fprintf(fid,'w = zeros(size(w));\n');
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertTemplate2Reference(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w'');\n');
end

fprintf(fid,'\n%% load data\n');
for g=1:gates
    fprintf(fid,'load(''data/%s_PET_Gate_%d_%d_%d.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
    fprintf(fid,'clear(''sensitivity_map_%d'');\n',matrix_size);
    fprintf(fid,'data_%d = data;\n',g);
    fprintf(fid,'data_fp_%d = data_fp;\n',g);
    fprintf(fid,'clear(''data'');\n');
    fprintf(fid,'clear(''data_fp'');\n');
end

fprintf(fid,'\n%% create sensitivity_map\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'sensitivity_map_mc_pm_v2_0_%d_%d = EMrecon_MC_BackProj_ND(parm,ones(344*252*837,1),motion_data_%d);\n',gates,matrix_size,g);
    else
        fprintf(fid,'sensitivity_map_mc_pm_v2_0_%d_%d = sensitivity_map_mc_pm_v2_0_%d_%d + EMrecon_MC_BackProj_ND(parm,ones(344*252*837,1),motion_data_%d);\n',gates,matrix_size,gates,matrix_size,g);
    end
end

fprintf(fid,'\n%% reconstruct PET data for all gates MC\n');
fprintf(fid,sprintf('em_all_gates_mc_pm_v2_0_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));

fprintf(fid,'for i=1:parm.ITERATIONS\n');
fprintf(fid,'\tfor s=1:parm.SUBSETS\n');
fprintf(fid,'\t\tparm.FIXED_SUBSET = s;\n\n');
fprintf(fid,'\t\tfprintf(''i%%d s%%d\\n'',i,s)\n');
fprintf(fid,'\t\tsubset_tic = tic;\n\n');

fprintf(fid,'\t\t%% em mc step for each gate\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'\t\tback = EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d,em_all_gates_mc_pm_v2_0_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    else
        fprintf(fid,'\t\tback = back + EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d,em_all_gates_mc_pm_v2_0_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    end
end

fprintf(fid,'\n\t\t%% image update\n');
fprintf(fid,'\t\tindex_image_update = (em_all_gates_mc_pm_v2_0_%d_%d > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map_mc_pm_v2_0_%d_%d > parm.EPSILON);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_0_%d_%d(index_image_update) = em_all_gates_mc_pm_v2_0_%d_%d(index_image_update) .* back(index_image_update) ./ sensitivity_map_mc_pm_v2_0_%d_%d(index_image_update);\n',gates,matrix_size,gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_0_%d_%d(~index_image_update) = 0.0;\n\n',gates,matrix_size);

fprintf(fid,'\t\t%% smooth image\n');
fprintf(fid,'\t\tif (parm.CONVOLUTIONINTERVAL > 0 && mod(i*s,parm.CONVOLUTIONINTERVAL) == 0)\n');
fprintf(fid,'\t\t\t em_all_gates_mc_pm_v2_0_%d_%d = EMrecon_IP_Convolve_ND(parm,em_all_gates_mc_pm_v2_0_%d_%d);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tend\n\n');

fprintf(fid,'\t\t%% reduce image to cylindrical fov\n');
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_0_%d_%d = EMrecon_IP_CutImage_ND(parm,em_all_gates_mc_pm_v2_0_%d_%d);\n',gates,matrix_size,gates,matrix_size);

if (show_results == 1)
    fprintf(fid,'\n\t\t%% show current result\n');
    fprintf(fid,'\t\timagesc(squeeze(em_all_gates_mc_pm_v2_0_%d_%d(:,80,:))''); title(sprintf(''i%%d s%%d'',i,s)); axis xy; axis image; drawnow;\n\n',gates,matrix_size);
end

fprintf(fid,'\t\tfprintf(''i%%d s%%d done [%%f]\\n'',i,s,toc(subset_tic))\n');
fprintf(fid,'\tend\n');
fprintf(fid,'end\n');

fprintf(fid,'\n%% save results\n');
fprintf(fid,sprintf('save(''results/%s_PET_AllGates_MC_PM_V2_0_%d_%d'',''em_all_gates_mc_pm_v2_0_%d_%d'',''sensitivity_map_mc_pm_v2_0_%d_%d'',''parm'');\n\n',patient,gates,matrix_size,gates,matrix_size,gates,matrix_size));

fprintf(fid,'%% free memory \n');
for g=1:gates
    fprintf(fid,'clear(''motion_data_%d''); ',g);
    fprintf(fid,'clear(''data_%d''); ',g);
    fprintf(fid,'clear(''data_fp_%d'');\n',g);
end

fprintf(fid,'clear(''sensitivity_map_mc_pm_v2_0_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''em_all_gates_mc_pm_v2_0_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''back'');\n');
fprintf(fid,'clear(''parm'');\n');
fclose(fid);

%% create script to reconstruct data using motion correction and attenuation correction (pm)
fid = fopen(sprintf('%s\\%s\\%s_recon_mc_pm_v2_ac.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path etc.\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.EPSILON = 0.0001;\n');
fprintf(fid,'parm.CONVOLVE = 0;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = %d;\n',convolutioninterval);
fprintf(fid,'parm.MC_SCALING = 0;\n');
fprintf(fid,'parm.FIXED_SUBSET = -1;\n');

fprintf(fid,'\n%% process motion vectors for attenuation correction\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected_v2.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertTemplate2Reference(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w'');\n');
end

fprintf(fid,'\n%% load data and perform attenuation correction\n');
fprintf(fid,'load(''data/%s_PET_AllGates_AC_%d_%d.mat'');\n',patient,gates,matrix_size);
fprintf(fid,'clear(''allgates_data''); ');
fprintf(fid,'clear(''allgates_data_fp_ac''); ');
fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
fprintf(fid,'clear(''sensitivity_map_ac_%d'');\n',matrix_size);
for g=1:gates
    fprintf(fid,'load(''data/%s_PET_Gate_%d_%d_%d.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
    fprintf(fid,'clear(''sensitivity_map_%d'');\n',matrix_size);
    fprintf(fid,'data_%d = data;\n',g);
    fprintf(fid,'clear(''data'');\n');
    fprintf(fid,'data_fp_%d_ac = EMrecon_MC_ForwardProjAC_ND(parm,umap_%d,data_fp,motion_data_%d);\n',g,matrix_size,g);
    fprintf(fid,'clear(''data_fp'');\n');
end

fprintf(fid,'\n%% create sensitivity_map\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'sensitivity_map_mc_pm_v2_ac_%d_%d = EMrecon_MC_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d,motion_data_%d);\n',gates,matrix_size,matrix_size,g);
    else
        fprintf(fid,'sensitivity_map_mc_pm_v2_ac_%d_%d = sensitivity_map_mc_pm_v2_ac_%d_%d + EMrecon_MC_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d,motion_data_%d);\n',gates,matrix_size,gates,matrix_size,matrix_size,g);
    end
end

fprintf(fid,'\n%% process motion vectors for reconstruction\n');
fprintf(fid,'parm.MC_SCALING = 0;\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected_v2.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertTemplate2Reference(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w'');\n');
end

fprintf(fid,'\n%% reconstruct PET data for all gates MC AC\n');
fprintf(fid,sprintf('em_all_gates_mc_pm_v2_ac_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));

fprintf(fid,'for i=1:parm.ITERATIONS\n');
fprintf(fid,'\tfor s=1:parm.SUBSETS\n');
fprintf(fid,'\t\tparm.FIXED_SUBSET = s;\n\n');
fprintf(fid,'\t\tfprintf(''i%%d s%%d\\n'',i,s)\n');
fprintf(fid,'\t\tsubset_tic = tic;\n\n');

fprintf(fid,'\t\t%% em mc step for each gate\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'\t\tback = EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d_ac,em_all_gates_mc_pm_v2_ac_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    else
        fprintf(fid,'\t\tback = back + EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d_ac,em_all_gates_mc_pm_v2_ac_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    end
end

fprintf(fid,'\n\t\t%% image update\n');
fprintf(fid,'\t\tindex_image_update = (em_all_gates_mc_pm_v2_ac_%d_%d > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map_mc_pm_v2_ac_%d_%d > parm.EPSILON);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_ac_%d_%d(index_image_update) = em_all_gates_mc_pm_v2_ac_%d_%d(index_image_update) .* back(index_image_update) ./ sensitivity_map_mc_pm_v2_ac_%d_%d(index_image_update);\n',gates,matrix_size,gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_ac_%d_%d(~index_image_update) = 0.0;\n\n',gates,matrix_size);

fprintf(fid,'\t\t%% smooth image\n');
fprintf(fid,'\t\tif (parm.CONVOLUTIONINTERVAL > 0 && mod(i*s,parm.CONVOLUTIONINTERVAL) == 0)\n');
fprintf(fid,'\t\t\t em_all_gates_mc_pm_v2_ac_%d_%d = EMrecon_IP_Convolve_ND(parm,em_all_gates_mc_pm_v2_ac_%d_%d);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tend\n\n');

fprintf(fid,'\t\t%% reduce image to cylindrical fov\n');
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_ac_%d_%d = EMrecon_IP_CutImage_ND(parm,em_all_gates_mc_pm_v2_ac_%d_%d);\n',gates,matrix_size,gates,matrix_size);

if (show_results == 1)
    fprintf(fid,'\n\t\t%% show current result\n');
    fprintf(fid,'\t\timagesc(squeeze(em_all_gates_mc_pm_v2_ac_%d_%d(:,80,:))''); title(sprintf(''i%%d s%%d'',i,s)); axis xy; axis image; drawnow;\n\n',gates,matrix_size);
end

fprintf(fid,'\t\tfprintf(''i%%d s%%d done [%%f]\\n'',i,s,toc(subset_tic))\n');
fprintf(fid,'\tend\n');
fprintf(fid,'end\n');

fprintf(fid,'\n%% save results\n');
fprintf(fid,sprintf('save(''results/%s_PET_AllGates_MC_PM_V2_AC_%d_%d'',''em_all_gates_mc_pm_v2_ac_%d_%d'',''sensitivity_map_mc_pm_v2_ac_%d_%d'',''parm'');\n\n',patient,gates,matrix_size,gates,matrix_size,gates,matrix_size));

fprintf(fid,'%% free memory \n');
for g=1:gates
    fprintf(fid,'clear(''motion_data_%d''); ',g);
    fprintf(fid,'clear(''data_%d''); ',g);
    fprintf(fid,'clear(''data_fp_%d_ac'');\n',g);
end

fprintf(fid,'clear(''sensitivity_map_mc_pm_v2_ac_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''em_all_gates_mc_pm_v2_ac_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''back'');\n');
fprintf(fid,'clear(''parm'');\n');
fclose(fid);

%% create script to reconstruct data using motion correction and attenuation correction (pm) and zero motion vectors
fid = fopen(sprintf('%s\\%s\\%s_recon_mc_pm_v2_ac_0.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path etc.\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.EPSILON = 0.0001;\n');
fprintf(fid,'parm.CONVOLVE = 0;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = %d;\n',convolutioninterval);
fprintf(fid,'parm.MC_SCALING = 0;\n');
fprintf(fid,'parm.FIXED_SUBSET = -1;\n');

fprintf(fid,'\n%% process motion vectors for attenuation correction\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected_v2.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'u = zeros(size(u));\n');
    fprintf(fid,'v = zeros(size(v));\n');
    fprintf(fid,'w = zeros(size(w));\n');
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertTemplate2Reference(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w'');\n');
end

fprintf(fid,'\n%% load data and perform attenuation correction\n');
fprintf(fid,'load(''data/%s_PET_AllGates_AC_%d_%d.mat'');\n',patient,gates,matrix_size);
fprintf(fid,'clear(''allgates_data''); ');
fprintf(fid,'clear(''allgates_data_fp_ac''); ');
fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
fprintf(fid,'clear(''sensitivity_map_ac_%d'');\n',matrix_size);
for g=1:gates
    fprintf(fid,'load(''data/%s_PET_Gate_%d_%d_%d.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
    fprintf(fid,'clear(''sensitivity_map_%d'');\n',matrix_size);
    fprintf(fid,'data_%d = data;\n',g);
    fprintf(fid,'clear(''data'');\n');
    fprintf(fid,'data_fp_%d_ac = EMrecon_MC_ForwardProjAC_ND(parm,umap_%d,data_fp,motion_data_%d);\n',g,matrix_size,g);
    fprintf(fid,'clear(''data_fp'');\n');
end

fprintf(fid,'\n%% create sensitivity_map\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'sensitivity_map_mc_pm_v2_ac_0_%d_%d = EMrecon_MC_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d,motion_data_%d);\n',gates,matrix_size,matrix_size,g);
    else
        fprintf(fid,'sensitivity_map_mc_pm_v2_ac_0_%d_%d = sensitivity_map_mc_pm_v2_ac_0_%d_%d + EMrecon_MC_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d,motion_data_%d);\n',gates,matrix_size,gates,matrix_size,matrix_size,g);
    end
end

fprintf(fid,'\n%% process motion vectors for reconstruction\n');
fprintf(fid,'parm.MC_SCALING = 0;\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected_v2.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'u = zeros(size(u));\n');
    fprintf(fid,'v = zeros(size(v));\n');
    fprintf(fid,'w = zeros(size(w));\n');
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertTemplate2Reference(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w'');\n');
end

fprintf(fid,'\n%% reconstruct PET data for all gates MC AC\n');
fprintf(fid,sprintf('em_all_gates_mc_pm_v2_ac_0_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));

fprintf(fid,'for i=1:parm.ITERATIONS\n');
fprintf(fid,'\tfor s=1:parm.SUBSETS\n');
fprintf(fid,'\t\tparm.FIXED_SUBSET = s;\n\n');
fprintf(fid,'\t\tfprintf(''i%%d s%%d\\n'',i,s)\n');
fprintf(fid,'\t\tsubset_tic = tic;\n\n');

fprintf(fid,'\t\t%% em mc step for each gate\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'\t\tback = EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d_ac,em_all_gates_mc_pm_v2_ac_0_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    else
        fprintf(fid,'\t\tback = back + EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d_ac,em_all_gates_mc_pm_v2_ac_0_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    end
end

fprintf(fid,'\n\t\t%% image update\n');
fprintf(fid,'\t\tindex_image_update = (em_all_gates_mc_pm_v2_ac_0_%d_%d > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map_mc_pm_v2_ac_0_%d_%d > parm.EPSILON);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_ac_0_%d_%d(index_image_update) = em_all_gates_mc_pm_v2_ac_0_%d_%d(index_image_update) .* back(index_image_update) ./ sensitivity_map_mc_pm_v2_ac_0_%d_%d(index_image_update);\n',gates,matrix_size,gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_ac_0_%d_%d(~index_image_update) = 0.0;\n\n',gates,matrix_size);

fprintf(fid,'\t\t%% smooth image\n');
fprintf(fid,'\t\tif (parm.CONVOLUTIONINTERVAL > 0 && mod(i*s,parm.CONVOLUTIONINTERVAL) == 0)\n');
fprintf(fid,'\t\t\t em_all_gates_mc_pm_v2_ac_0_%d_%d = EMrecon_IP_Convolve_ND(parm,em_all_gates_mc_pm_v2_ac_0_%d_%d);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tend\n\n');

fprintf(fid,'\t\t%% reduce image to cylindrical fov\n');
fprintf(fid,'\t\tem_all_gates_mc_pm_v2_ac_0_%d_%d = EMrecon_IP_CutImage_ND(parm,em_all_gates_mc_pm_v2_ac_0_%d_%d);\n',gates,matrix_size,gates,matrix_size);

if (show_results == 1)
    fprintf(fid,'\n\t\t%% show current result\n');
    fprintf(fid,'\t\timagesc(squeeze(em_all_gates_mc_pm_v2_ac_0_%d_%d(:,80,:))''); title(sprintf(''i%%d s%%d'',i,s)); axis xy; axis image; drawnow;\n\n',gates,matrix_size);
end

fprintf(fid,'\t\tfprintf(''i%%d s%%d done [%%f]\\n'',i,s,toc(subset_tic))\n');
fprintf(fid,'\tend\n');
fprintf(fid,'end\n');

fprintf(fid,'\n%% save results\n');
fprintf(fid,sprintf('save(''results/%s_PET_AllGates_MC_PM_V2_AC_0_%d_%d'',''em_all_gates_mc_pm_v2_ac_0_%d_%d'',''sensitivity_map_mc_pm_v2_ac_0_%d_%d'',''parm'');\n\n',patient,gates,matrix_size,gates,matrix_size,gates,matrix_size));

fprintf(fid,'%% free memory \n');
for g=1:gates
    fprintf(fid,'clear(''motion_data_%d''); ',g);
    fprintf(fid,'clear(''data_%d''); ',g);
    fprintf(fid,'clear(''data_fp_%d_ac'');\n',g);
end

fprintf(fid,'clear(''sensitivity_map_mc_pm_v2_ac_0_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''em_all_gates_mc_pm_v2_ac_0_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''back'');\n');
fprintf(fid,'clear(''parm'');\n');
fclose(fid);

end
