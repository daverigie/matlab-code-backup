function [] = CopyResults_Recon_MoCo_SinglePatient_PM_inv(base_path_win_results,base_path_win_transfer,gates,patient,matrix_size)

fprintf('Gates: %d\n',gates);

if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_0_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_0_%d_%d.mat',base_path_win_transfer,patient,patient,gates,matrix_size),'file') == 2)
        dos(sprintf('copy %s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_0_%d_%d.mat %s\\%s\\results',base_path_win_transfer,patient,patient,gates,matrix_size,base_path_win_results,patient));
        load(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_0_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size));
        writeimage(eval(sprintf('em_all_gates_mc_pm_inv_0_%d_%d',gates,matrix_size)),sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_0_%d_%d.raw',base_path_win_results,patient,patient,gates,matrix_size));
    else
        fprintf('SOURCE [%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_0_%d_%d.mat] does not exist\n',base_path_win_transfer,patient,patient,gates,matrix_size);
    end
else
    fprintf('TARGET [%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_0_%d_%d.mat] already exists\n',base_path_win_results,patient,patient,gates,matrix_size);
end

if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_%d_%d.mat',base_path_win_transfer,patient,patient,gates,matrix_size),'file') == 2)
        dos(sprintf('copy %s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_%d_%d.mat %s\\%s\\results',base_path_win_transfer,patient,patient,gates,matrix_size,base_path_win_results,patient));
        load(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size));
        writeimage(eval(sprintf('em_all_gates_mc_pm_inv_%d_%d',gates,matrix_size)),sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_%d_%d.raw',base_path_win_results,patient,patient,gates,matrix_size));
    else
        fprintf('SOURCE [%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_%d_%d.mat] does not exist\n',base_path_win_transfer,patient,patient,gates,matrix_size);
    end
else
    fprintf('TARGET [%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_%d_%d.mat] already exists\n',base_path_win_results,patient,patient,gates,matrix_size);
end

if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_0_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_0_%d_%d.mat',base_path_win_transfer,patient,patient,gates,matrix_size),'file') == 2)
        dos(sprintf('copy %s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_0_%d_%d.mat %s\\%s\\results',base_path_win_transfer,patient,patient,gates,matrix_size,base_path_win_results,patient));
        load(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_0_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size));
        writeimage(eval(sprintf('em_all_gates_mc_pm_inv_ac_0_%d_%d',gates,matrix_size)),sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_0_%d_%d.raw',base_path_win_results,patient,patient,gates,matrix_size));
    else
        fprintf('SOURCE [%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_0_%d_%d.mat] does not exist\n',base_path_win_transfer,patient,patient,gates,matrix_size);
    end
else
    fprintf('TARGET [%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_0_%d_%d.mat] already exists\n',base_path_win_results,patient,patient,gates,matrix_size);
end

if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_%d_%d.mat',base_path_win_transfer,patient,patient,gates,matrix_size),'file') == 2)
        dos(sprintf('copy %s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_%d_%d.mat %s\\%s\\results',base_path_win_transfer,patient,patient,gates,matrix_size,base_path_win_results,patient));
        load(sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size));
        writeimage(eval(sprintf('em_all_gates_mc_pm_inv_ac_%d_%d',gates,matrix_size)),sprintf('%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_%d_%d.raw',base_path_win_results,patient,patient,gates,matrix_size));
    else
        fprintf('SOURCE [%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_%d_%d.mat] does not exist\n',base_path_win_transfer,patient,patient,gates,matrix_size);
    end
else
    fprintf('TARGET [%s\\%s\\results\\%s_PET_AllGates_MC_PM_INV_AC_%d_%d.mat] already exists\n',base_path_win_results,patient,patient,gates,matrix_size);
end

%% copy independent gates and not motion corrected reconstructions
if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_AC_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_AC_%d_%d.mat',base_path_win_transfer,patient,patient,gates,matrix_size),'file') == 2)
        dos(sprintf('copy %s\\%s\\results\\%s_PET_AllGates_AC_%d_%d.mat %s\\%s\\results',base_path_win_transfer,patient,patient,gates,matrix_size,base_path_win_results,patient));
        load(sprintf('%s\\%s\\results\\%s_PET_AllGates_AC_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size));
        writeimage(eval(sprintf('em_all_gates_ac_%d_%d',gates,matrix_size)),sprintf('%s\\%s\\results\\%s_PET_AllGates_AC_%d_%d.raw',base_path_win_results,patient,patient,gates,matrix_size));
    else
        fprintf('SOURCE [%s\\%s\\results\\%s_PET_AllGates_AC_%d_%d.mat] does not exist\n',base_path_win_transfer,patient,patient,gates,matrix_size);
    end
else
    fprintf('TARGET [%s\\%s\\results\\%s_PET_AllGates_AC_%d_%d.mat] already exists\n',base_path_win_results,patient,patient,gates,matrix_size);
end

if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    if (exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_%d_%d.mat',base_path_win_transfer,patient,patient,gates,matrix_size),'file') == 2)
        dos(sprintf('copy %s\\%s\\results\\%s_PET_AllGates_%d_%d.mat %s\\%s\\results',base_path_win_transfer,patient,patient,gates,matrix_size,base_path_win_results,patient));
        load(sprintf('%s\\%s\\results\\%s_PET_AllGates_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size));
        writeimage(eval(sprintf('em_all_gates_%d_%d',gates,matrix_size)),sprintf('%s\\%s\\results\\%s_PET_AllGates_%d_%d.raw',base_path_win_results,patient,patient,gates,matrix_size));
    else
        fprintf('SOURCE [%s\\%s\\results\\%s_PET_AllGates_%d_%d.mat] does not exist\n',base_path_win_transfer,patient,patient,gates,matrix_size);
    end
else
    fprintf('TARGET [%s\\%s\\results\\%s_PET_AllGates_%d_%d.mat] already exists\n',base_path_win_results,patient,patient,gates,matrix_size);
end

for i=1:gates
    if (exist(sprintf('%s\\%s\\results\\%s_PET_Recon_Gate_%d_%d_%d.mat',base_path_win_results,patient,patient,i,gates,matrix_size),'file') ~= 2)
        if (exist(sprintf('%s\\%s\\results\\%s_PET_Recon_Gate_%d_%d_%d.mat',base_path_win_transfer,patient,patient,i,gates,matrix_size),'file') == 2)
            dos(sprintf('copy %s\\%s\\results\\%s_PET_Recon_Gate_%d_%d_%d.mat %s\\%s\\results',base_path_win_transfer,patient,patient,i,gates,matrix_size,base_path_win_results,patient));
            load(sprintf('%s\\%s\\results\\%s_PET_Recon_Gate_%d_%d_%d.mat',base_path_win_results,patient,patient,i,gates,matrix_size));
            writeimage(eval(sprintf('em_gate_%d_%d_%d',i,gates,matrix_size)),sprintf('%s\\%s\\results\\%s_PET_Recon_Gate_%d_%d_%d.raw',base_path_win_results,patient,patient,i,gates,matrix_size));
        else
            fprintf('SOURCE [%s\\%s\\results\\%s_PET_Recon_Gate_%d_%d_%d.mat] does not exist\n',base_path_win_transfer,patient,patient,i,gates,matrix_size);
        end
    else
        fprintf('TARGET [%s\\%s\\results\\%s_PET_Recon_Gate_%d_%d_%d.mat] already exists\n',base_path_win_results,patient,patient,i,gates,matrix_size);
    end
end

end
