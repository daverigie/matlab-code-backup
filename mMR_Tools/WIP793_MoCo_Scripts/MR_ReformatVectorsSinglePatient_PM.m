function [] = MR_ReformatVectorsSinglePatient_PM(base_path_win_data,base_path_win_results,base_path_linux_data,gates,reference_gate,patient,show_results,write_raw)

if (nargin == 7)
    write_raw = 0;
end

% convolutioninterval
convolutioninterval = 63;

% matrix size
matrix_size = 172;

% initialize vectors
vecs = zeros(matrix_size,matrix_size,127,3,gates);

% check whether vectors have been reformated already
load_data = gates;
for g=1:gates
    if (exist(sprintf('%s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected.mat',base_path_win_results,patient,patient,g,gates,matrix_size),'file') == 2)
        load_data = load_data - 1;
    end
end

% load vectors
if (load_data > 0)
    for g=1:gates
        if (g ~= reference_gate)
            vecs(:,:,:,1,g) = readimage(sprintf('%s\\%s\\Vectors\\%d_%d_%d\\DefFieldX.v',base_path_win_data,patient,gates,reference_gate,g),matrix_size,matrix_size,127,1);
            vecs(:,:,:,2,g) = readimage(sprintf('%s\\%s\\Vectors\\%d_%d_%d\\DefFieldY.v',base_path_win_data,patient,gates,reference_gate,g),matrix_size,matrix_size,127,1);
            vecs(:,:,:,3,g) = readimage(sprintf('%s\\%s\\Vectors\\%d_%d_%d\\DefFieldZ.v',base_path_win_data,patient,gates,reference_gate,g),matrix_size,matrix_size,127,1);
        end
    end
end

%% set parameter
% image parameter
parm_MC.SCANNERTYPE = 8;
parm_MC.SIZE_X = matrix_size;
parm_MC.SIZE_Y = matrix_size;
parm_MC.SIZE_Z = 127;
parm_MC.FOV_X = 344 * 2.08626;
parm_MC.FOV_Y = 344 * 2.08626;
parm_MC.FOV_Z = 127 * 2.03125;

for g=1:gates
    % convert motion vectors if not yet done
    if (exist(sprintf('%s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected.mat',base_path_win_results,patient,patient,g,gates,matrix_size),'file') ~= 2)
        fprintf('converting motion vectors for gate %d of %d...\n',g,gates);
        
        % load MR gate
        load(sprintf('%s\\%s\\data\\%s_MR_Gate_%d_%d_%d.mat',base_path_win_results,patient,patient,g,gates,matrix_size));
        
        % extract motion for current gate
        u = vecs(:,:,:,1,g);
        v = vecs(:,:,:,2,g);
        w = vecs(:,:,:,3,g);
        
        % convert motion vectors
        motion_data = EMrecon_MC_ConvertReference2Template(parm_MC,u,v,w);
        
        % apply motion correction to MR image
        eval(sprintf('MR_gate_%d_%d_%d_corrected = EMrecon_MC_CorrectSingleGate_ND(parm_MC,MR_gate_%d_%d_%d,motion_data);',g,gates,matrix_size,g,gates,matrix_size));
        
        % save corrected MR gate and motion vectors
        save(sprintf('%s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected.mat',base_path_win_results,patient,patient,g,gates,matrix_size),sprintf('MR_gate_%d_%d_%d_corrected',g,gates,matrix_size),'u','v','w','parm_MC');
        
        % write raw images
        if (write_raw == 1)
            eval(sprintf('writeimage(MR_gate_%d_%d_%d,''%s\\%s\\results\\%s_MR_gate_%d_%d_%d.raw'');',g,gates,matrix_size,base_path_win_results,patient,patient,g,gates,matrix_size));
            eval(sprintf('writeimage(MR_gate_%d_%d_%d_corrected,''%s\\%s\\results\\%s_MR_gate_%d_%d_%d_corrected.raw'');',g,gates,matrix_size,base_path_win_results,patient,patient,g,gates,matrix_size));
        end
        
        fprintf('converting motion vectors for gate %d of %d...done.\n',g,gates);
    end
    
    % if PET reconstruction for single gate exists, apply motion to PET (if not yet done)
    if (exist(sprintf('%s\\%s\\results\\%s_PET_Gate_%d_%d_Recon_%d.mat',base_path_win_results,patient,patient,g,gates,matrix_size),'file') == 2)
        if (exist(sprintf('%s\\%s\\results\\%s_PET_Gate_%d_%d_Recon_%d_corrected.mat',base_path_win_results,patient,patient,g,gates,matrix_size),'file') ~= 2)
            if (exist(sprintf('%s\\%s\\results\\%s_PET_Gate_%d_%d_Recon_%d.mat',base_path_win_results,patient,patient,g,gates,matrix_size),'file') == 2)
                fprintf('applying motion estimate to PET gate %d of %d...\n',g,gates);
                
                % load PET gate
                load(sprintf('%s\\%s\\results\\%s_PET_Gate_%d_%d_Recon_%d.mat',base_path_win_results,patient,patient,g,gates,matrix_size));
                
                fprintf('loading motion estimate for gate %d of %d...\n',g,gates);
                load(sprintf('%s\\%s\\results\\%s_MR_Gate_%d_%d_%d_corrected.mat',base_path_win_results,patient,patient,g,gates,matrix_size));
                fprintf('loading motion estimate for gate %d of %d...done.\n',g,gates);
                
                % extract motion for current gate
                u = vecs(:,:,:,1,g);
                v = vecs(:,:,:,2,g);
                w = vecs(:,:,:,3,g);
                
                % convert motion vectors
                motion_data = EMrecon_MC_ConvertReference2Template(parm_MC,u,v,w);
                
                % apply motion correction to PET image
                eval(sprintf('em_gate_%d_%d_%d_corrected = EMrecon_MC_CorrectSingleGate_ND(parm_MC,em_gate_%d_%d_%d,motion_data);',g,gates,matrix_size,g,gates,matrix_size));
                
                % write raw images
                if (write_raw == 1)
                    eval(sprintf('writeimage(em_gate_%d_%d_%d,''%s\\%s\\results\\%s_em_gate_%d_%d_%d.raw'');',g,gates,matrix_size,base_path_win_results,patient,patient,g,gates,matrix_size));
                    eval(sprintf('writeimage(em_gate_%d_%d_%d_corrected,''%s\\%s\\results\\%s_em_gate_%d_%d_%d_corrected.raw'');',g,gates,matrix_size,base_path_win_results,patient,patient,g,gates,matrix_size));
                end
                
                % save corrected PET gate
                save(sprintf('%s\\%s\\results\\%s_PET_Gate_%d_%d_Recon_%d_corrected.mat',base_path_win_results,patient,patient,g,gates,matrix_size),sprintf('em_gate_%d_%d_%d_corrected',g,gates,matrix_size),'parm_MC');
                
                fprintf('applying motion estimate to PET gate %d of %d...done.\n',g,gates);
            end
        end
    end
end

if (exist(sprintf('%s\\%s\\results\\%s_PET_Average_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    if (exist(sprintf('%s\\%s\\results\\%s_PET_Average_%d_%d_corrected.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
        if (exist(sprintf('%s\\%s\\results\\%s_PET_Gate_%d_%d_Recon_%d.mat',base_path_win_results,patient,patient,gates,gates,matrix_size),'file') == 2)
            % create average image
            eval(sprintf('PET_Average_%d_%d = zeros(%d,%d,%d);',gates,matrix_size,parm_MC.SIZE_X,parm_MC.SIZE_Y,parm_MC.SIZE_Z));
            eval(sprintf('PET_Average_%d_%d_corrected = zeros(%d,%d,%d);',gates,matrix_size,parm_MC.SIZE_X,parm_MC.SIZE_Y,parm_MC.SIZE_Z));
            
            for g=1:gates
                % load PET gate and corrected PET gate
                load(sprintf('%s\\%s\\results\\%s_PET_Gate_%d_%d_Recon_%d.mat',base_path_win_results,patient,patient,g,gates,matrix_size));
                load(sprintf('%s\\%s\\results\\%s_PET_Gate_%d_%d_Recon_%d_corrected.mat',base_path_win_results,patient,patient,g,gates,matrix_size));
                eval(sprintf('PET_Average_%d_%d = PET_Average_%d_%d + em_gate_%d_%d_%d;',gates,matrix_size,gates,matrix_size,g,gates,matrix_size));
                eval(sprintf('PET_Average_%d_%d_corrected = PET_Average_%d_%d_corrected + em_gate_%d_%d_%d_corrected;',gates,matrix_size,gates,matrix_size,g,gates,matrix_size));
            end
            
            % scale according to number of gates
            eval(sprintf('PET_Average_%d_%d = PET_Average_%d_%d ./ %d;',gates,matrix_size,gates,matrix_size,gates));
            eval(sprintf('PET_Average_%d_%d_corrected = PET_Average_%d_%d_corrected ./ %d;',gates,matrix_size,gates,matrix_size,gates));
            
            % save average PET
            save(sprintf('%s\\%s\\results\\%s_PET_Average_%d_%d.mat',base_path_win_results,patient,patient,matrix_size,gates),sprintf('PET_Average_%d_%d',gates,matrix_size));
            
            % save corrected average PET
            save(sprintf('%s\\%s\\results\\%s_PET_Average_%d_%d_corrected.mat',base_path_win_results,patient,patient,gates,matrix_size),sprintf('PET_Average_%d_%d_corrected',gates,matrix_size));
            
            % write raw images
            if (write_raw == 1)
                eval(sprintf('writeimage(PET_Average_%d_%d,''%s\\%s\\results\\%s_PET_Average_%d_%d.raw'');',gates,matrix_size,base_path_win_results,patient,patient,gates,matrix_size));
                eval(sprintf('writeimage(PET_Average_%d_%d_corrected,''%s\\%s\\results\\%s_PET_Average_%d_%d_corrected.raw'');',gates,matrix_size,base_path_win_results,patient,patient,gates,matrix_size));
            end
        end
    end
end

%% create script to reconstruct data using motion correction (pm)
fid = fopen(sprintf('%s\\%s\\%s_recon_mc_pm.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path etc.\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.EPSILON = 0.0001;\n');
fprintf(fid,'parm.CONVOLVE = 0;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = %d;\n',convolutioninterval);
fprintf(fid,'parm.MC_SCALING = 1;\n');
fprintf(fid,'parm.FIXED_SUBSET = -1;\n');

fprintf(fid,'\n%% process motion vectors\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertReference2Template(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''parm_MC''); ');
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w''); ');
    fprintf(fid,'clear(''MR_gate_%d_%d_%d_corrected'');\n',g,gates,matrix_size);
end

fprintf(fid,'\n%% load data\n');
for g=1:gates
    fprintf(fid,'load(''data/%s_PET_Gate_%d_%d_%d.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
    fprintf(fid,'clear(''sensitivity_map_%d'');\n',matrix_size);
    fprintf(fid,'data_%d = data;\n',g);
    fprintf(fid,'data_fp_%d = data_fp;\n',g);
    fprintf(fid,'clear(''data'');\n');
    fprintf(fid,'clear(''data_fp'');\n');
end

fprintf(fid,'\n%% create sensitivity_map\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'sensitivity_map_mc_pm_%d_%d = EMrecon_MC_BackProj_ND(parm,ones(344*252*837,1),motion_data_%d);\n',gates,matrix_size,g);
    else
        fprintf(fid,'sensitivity_map_mc_pm_%d_%d = sensitivity_map_mc_pm_%d_%d + EMrecon_MC_BackProj_ND(parm,ones(344*252*837,1),motion_data_%d);\n',gates,matrix_size,gates,matrix_size,g);
    end
end

fprintf(fid,'\n%% reconstruct PET data for all gates MC\n');
fprintf(fid,sprintf('em_all_gates_mc_pm_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));

fprintf(fid,'for i=1:parm.ITERATIONS\n');
fprintf(fid,'\tfor s=1:parm.SUBSETS\n');
fprintf(fid,'\t\tparm.FIXED_SUBSET = s;\n\n');
fprintf(fid,'\t\tfprintf(''i%%d s%%d\\n'',i,s)\n');
fprintf(fid,'\t\tsubset_tic = tic;\n\n');

fprintf(fid,'\t\t%% em mc step for each gate\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'\t\tback = EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d,em_all_gates_mc_pm_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    else
        fprintf(fid,'\t\tback = back + EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d,em_all_gates_mc_pm_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    end
end

fprintf(fid,'\n\t\t%% image update\n');
fprintf(fid,'\t\tindex_image_update = (em_all_gates_mc_pm_%d_%d > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map_mc_pm_%d_%d > parm.EPSILON);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_%d_%d(index_image_update) = em_all_gates_mc_pm_%d_%d(index_image_update) .* back(index_image_update) ./ sensitivity_map_mc_pm_%d_%d(index_image_update);\n',gates,matrix_size,gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_%d_%d(~index_image_update) = 0.0;\n\n',gates,matrix_size);

fprintf(fid,'\t\t%% smooth image\n');
fprintf(fid,'\t\tif (parm.CONVOLUTIONINTERVAL > 0 && mod(i*s,parm.CONVOLUTIONINTERVAL) == 0)\n');
fprintf(fid,'\t\t\t em_all_gates_mc_pm_%d_%d = EMrecon_IP_Convolve_ND(parm,em_all_gates_mc_pm_%d_%d);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tend\n\n');

fprintf(fid,'\t\t%% reduce image to cylindrical fov\n');
fprintf(fid,'\t\tem_all_gates_mc_pm_%d_%d = EMrecon_IP_CutImage_ND(parm,em_all_gates_mc_pm_%d_%d);\n',gates,matrix_size,gates,matrix_size);

if (show_results == 1)
    fprintf(fid,'\n\t\t%% show current result\n');
    fprintf(fid,'\t\timagesc(squeeze(em_all_gates_mc_pm_%d_%d(:,80,:))''); title(sprintf(''i%%d s%%d'',i,s)); axis xy; axis image; drawnow;\n\n',matrix_size);
end

fprintf(fid,'\t\tfprintf(''i%%d s%%d done [%%f]\\n'',i,s,toc(subset_tic))\n');
fprintf(fid,'\tend\n');
fprintf(fid,'end\n');

fprintf(fid,'\n%% save results\n');
fprintf(fid,sprintf('save(''results/%s_PET_AllGates_MC_PM_%d_%d'',''em_all_gates_mc_pm_%d_%d'',''sensitivity_map_mc_pm_%d_%d'',''parm'');\n\n',patient,gates,matrix_size,gates,matrix_size,gates,matrix_size));

fprintf(fid,'%% free memory \n');
for g=1:gates
    fprintf(fid,'clear(''motion_data_%d''); ',g);
    fprintf(fid,'clear(''data_%d''); ',g);
    fprintf(fid,'clear(''data_fp_%d'');\n',g);
end

fprintf(fid,'clear(''sensitivity_map_mc_pm_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''em_all_gates_mc_pm_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''back'');\n');
fprintf(fid,'clear(''parm'');\n');
fclose(fid);

%% create script to reconstruct data using motion correction (pm) and zero motion vectors
fid = fopen(sprintf('%s\\%s\\%s_recon_mc_pm_0.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path etc.\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.EPSILON = 0.0001;\n');
fprintf(fid,'parm.CONVOLVE = 0;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = %d;\n',convolutioninterval);
fprintf(fid,'parm.MC_SCALING = 1;\n');
fprintf(fid,'parm.FIXED_SUBSET = -1;\n');

fprintf(fid,'\n%% process motion vectors\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'u = zeros(size(u));\n');
    fprintf(fid,'v = zeros(size(v));\n');
    fprintf(fid,'w = zeros(size(w));\n');
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertReference2Template(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''parm_MC''); ');
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w''); ');
    fprintf(fid,'clear(''MR_gate_%d_%d_%d_corrected'');\n',g,gates,matrix_size);
end

fprintf(fid,'\n%% load data\n');
for g=1:gates
    fprintf(fid,'load(''data/%s_PET_Gate_%d_%d_%d.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
    fprintf(fid,'clear(''sensitivity_map_%d'');\n',matrix_size);
    fprintf(fid,'data_%d = data;\n',g);
    fprintf(fid,'data_fp_%d = data_fp;\n',g);
    fprintf(fid,'clear(''data'');\n');
    fprintf(fid,'clear(''data_fp'');\n');
end

fprintf(fid,'\n%% create sensitivity_map\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'sensitivity_map_mc_pm_0_%d_%d = EMrecon_MC_BackProj_ND(parm,ones(344*252*837,1),motion_data_%d);\n',gates,matrix_size,g);
    else
        fprintf(fid,'sensitivity_map_mc_pm_0_%d_%d = sensitivity_map_mc_pm_0_%d_%d + EMrecon_MC_BackProj_ND(parm,ones(344*252*837,1),motion_data_%d);\n',gates,matrix_size,gates,matrix_size,g);
    end
end

fprintf(fid,'\n%% reconstruct PET data for all gates MC\n');
fprintf(fid,sprintf('em_all_gates_mc_pm_0_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));

fprintf(fid,'for i=1:parm.ITERATIONS\n');
fprintf(fid,'\tfor s=1:parm.SUBSETS\n');
fprintf(fid,'\t\tparm.FIXED_SUBSET = s;\n\n');
fprintf(fid,'\t\tfprintf(''i%%d s%%d\\n'',i,s)\n');
fprintf(fid,'\t\tsubset_tic = tic;\n\n');

fprintf(fid,'\t\t%% em mc step for each gate\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'\t\tback = EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d,em_all_gates_mc_pm_0_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    else
        fprintf(fid,'\t\tback = back + EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d,em_all_gates_mc_pm_0_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    end
end

fprintf(fid,'\n\t\t%% image update\n');
fprintf(fid,'\t\tindex_image_update = (em_all_gates_mc_pm_0_%d_%d > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map_mc_pm_0_%d_%d > parm.EPSILON);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_0_%d_%d(index_image_update) = em_all_gates_mc_pm_0_%d_%d(index_image_update) .* back(index_image_update) ./ sensitivity_map_mc_pm_0_%d_%d(index_image_update);\n',gates,matrix_size,gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_0_%d_%d(~index_image_update) = 0.0;\n\n',gates,matrix_size);

fprintf(fid,'\t\t%% smooth image\n');
fprintf(fid,'\t\tif (parm.CONVOLUTIONINTERVAL > 0 && mod(i*s,parm.CONVOLUTIONINTERVAL) == 0)\n');
fprintf(fid,'\t\t\t em_all_gates_mc_pm_0_%d_%d = EMrecon_IP_Convolve_ND(parm,em_all_gates_mc_pm_0_%d_%d);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tend\n\n');

fprintf(fid,'\t\t%% reduce image to cylindrical fov\n');
fprintf(fid,'\t\tem_all_gates_mc_pm_0_%d_%d = EMrecon_IP_CutImage_ND(parm,em_all_gates_mc_pm_0_%d_%d);\n',gates,matrix_size,gates,matrix_size);

if (show_results == 1)
    fprintf(fid,'\n\t\t%% show current result\n');
    fprintf(fid,'\t\timagesc(squeeze(em_all_gates_mc_pm_0_%d_%d(:,80,:))''); title(sprintf(''i%%d s%%d'',i,s)); axis xy; axis image; drawnow;\n\n',gates,matrix_size);
end

fprintf(fid,'\t\tfprintf(''i%%d s%%d done [%%f]\\n'',i,s,toc(subset_tic))\n');
fprintf(fid,'\tend\n');
fprintf(fid,'end\n');

fprintf(fid,'\n%% save results\n');
fprintf(fid,sprintf('save(''results/%s_PET_AllGates_MC_PM_0_%d_%d'',''em_all_gates_mc_pm_0_%d_%d'',''sensitivity_map_mc_pm_0_%d_%d'',''parm'');\n\n',patient,gates,matrix_size,gates,matrix_size,gates,matrix_size));

fprintf(fid,'%% free memory \n');
for g=1:gates
    fprintf(fid,'clear(''motion_data_%d''); ',g);
    fprintf(fid,'clear(''data_%d''); ',g);
    fprintf(fid,'clear(''data_fp_%d'');\n',g);
end

fprintf(fid,'clear(''sensitivity_map_mc_pm_0_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''em_all_gates_mc_pm_0_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''back'');\n');
fprintf(fid,'clear(''parm'');\n');
fclose(fid);

%% create script to reconstruct data using motion correction and attenuation correction (pm)
fid = fopen(sprintf('%s\\%s\\%s_recon_mc_pm_ac.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path etc.\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.EPSILON = 0.0001;\n');
fprintf(fid,'parm.CONVOLVE = 0;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = %d;\n',convolutioninterval);
fprintf(fid,'parm.MC_SCALING = 1;\n');
fprintf(fid,'parm.FIXED_SUBSET = -1;\n');

fprintf(fid,'\n%% process motion vectors\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertReference2Template(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''parm_MC''); ');
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w''); ');
    fprintf(fid,'clear(''MR_gate_%d_%d_%d_corrected'');\n',g,gates,matrix_size);
end

fprintf(fid,'\n%% load data and perform attenuation correction\n');
fprintf(fid,'load(''data/%s_PET_AllGates_AC_%d_%d.mat'');\n',patient,gates,matrix_size);
fprintf(fid,'clear(''allgates_data''); ');
fprintf(fid,'clear(''allgates_data_fp_ac''); ');
fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
fprintf(fid,'clear(''sensitivity_map_ac_%d'');\n',matrix_size);
for g=1:gates
    fprintf(fid,'load(''data/%s_PET_Gate_%d_%d_%d.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
    fprintf(fid,'clear(''sensitivity_map_%d'');\n',matrix_size);
    fprintf(fid,'data_%d = data;\n',g);
    fprintf(fid,'clear(''data'');\n');
    fprintf(fid,'data_fp_%d_ac = EMrecon_MC_ForwardProjAC_ND(parm,umap_%d,data_fp,motion_data_%d);\n',g,matrix_size,g);
    fprintf(fid,'clear(''data_fp'');\n');
end

fprintf(fid,'\n%% create sensitivity_map\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'sensitivity_map_mc_pm_ac_%d_%d = EMrecon_MC_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d,motion_data_%d);\n',gates,matrix_size,matrix_size,g);
    else
        fprintf(fid,'sensitivity_map_mc_pm_ac_%d_%d = sensitivity_map_mc_pm_ac_%d_%d + EMrecon_MC_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d,motion_data_%d);\n',gates,matrix_size,gates,matrix_size,matrix_size,g);
    end
end

fprintf(fid,'\n%% reconstruct PET data for all gates MC AC\n');
fprintf(fid,sprintf('em_all_gates_mc_pm_ac_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));

fprintf(fid,'for i=1:parm.ITERATIONS\n');
fprintf(fid,'\tfor s=1:parm.SUBSETS\n');
fprintf(fid,'\t\tparm.FIXED_SUBSET = s;\n\n');
fprintf(fid,'\t\tfprintf(''i%%d s%%d\\n'',i,s)\n');
fprintf(fid,'\t\tsubset_tic = tic;\n\n');

fprintf(fid,'\t\t%% em mc step for each gate\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'\t\tback = EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d_ac,em_all_gates_mc_pm_ac_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    else
        fprintf(fid,'\t\tback = back + EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d_ac,em_all_gates_mc_pm_ac_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    end
end

fprintf(fid,'\n\t\t%% image update\n');
fprintf(fid,'\t\tindex_image_update = (em_all_gates_mc_pm_ac_%d_%d > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map_mc_pm_ac_%d_%d > parm.EPSILON);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_ac_%d_%d(index_image_update) = em_all_gates_mc_pm_ac_%d_%d(index_image_update) .* back(index_image_update) ./ sensitivity_map_mc_pm_ac_%d_%d(index_image_update);\n',gates,matrix_size,gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_ac_%d_%d(~index_image_update) = 0.0;\n\n',gates,matrix_size);

fprintf(fid,'\t\t%% smooth image\n');
fprintf(fid,'\t\tif (parm.CONVOLUTIONINTERVAL > 0 && mod(i*s,parm.CONVOLUTIONINTERVAL) == 0)\n');
fprintf(fid,'\t\t\t em_all_gates_mc_pm_ac_%d_%d = EMrecon_IP_Convolve_ND(parm,em_all_gates_mc_pm_ac_%d_%d);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tend\n\n');

fprintf(fid,'\t\t%% reduce image to cylindrical fov\n');
fprintf(fid,'\t\tem_all_gates_mc_pm_ac_%d_%d = EMrecon_IP_CutImage_ND(parm,em_all_gates_mc_pm_ac_%d_%d);\n',gates,matrix_size,gates,matrix_size);

if (show_results == 1)
    fprintf(fid,'\n\t\t%% show current result\n');
    fprintf(fid,'\t\timagesc(squeeze(em_all_gates_mc_pm_ac_%d_%d(:,80,:))''); title(sprintf(''i%%d s%%d'',i,s)); axis xy; axis image; drawnow;\n\n',gates,matrix_size);
end

fprintf(fid,'\t\tfprintf(''i%%d s%%d done [%%f]\\n'',i,s,toc(subset_tic))\n');
fprintf(fid,'\tend\n');
fprintf(fid,'end\n');

fprintf(fid,'\n%% save results\n');
fprintf(fid,sprintf('save(''results/%s_PET_AllGates_MC_PM_AC_%d_%d'',''em_all_gates_mc_pm_ac_%d_%d'',''sensitivity_map_mc_pm_ac_%d_%d'',''parm'');\n\n',patient,gates,matrix_size,gates,matrix_size,gates,matrix_size));

fprintf(fid,'%% free memory \n');
for g=1:gates
    fprintf(fid,'clear(''motion_data_%d''); ',g);
    fprintf(fid,'clear(''data_%d''); ',g);
    fprintf(fid,'clear(''data_fp_%d_ac'');\n',g);
end

fprintf(fid,'clear(''sensitivity_map_mc_pm_ac_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''em_all_gates_mc_pm_ac_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''back'');\n');
fprintf(fid,'clear(''parm'');\n');
fclose(fid);

%% create script to reconstruct data using motion correction and attenuation correction (pm) and zero motion vectors
fid = fopen(sprintf('%s\\%s\\%s_recon_mc_pm_ac_0.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path etc.\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.EPSILON = 0.0001;\n');
fprintf(fid,'parm.CONVOLVE = 0;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = %d;\n',convolutioninterval);
fprintf(fid,'parm.MC_SCALING = 1;\n');
fprintf(fid,'parm.FIXED_SUBSET = -1;\n');

fprintf(fid,'\n%% process motion vectors\n');
for g=1:gates
    fprintf(fid,'load(''results/%s_MR_Gate_%d_%d_%d_corrected.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'u = zeros(size(u));\n');
    fprintf(fid,'v = zeros(size(v));\n');
    fprintf(fid,'w = zeros(size(w));\n');
    fprintf(fid,'motion_data_%d = EMrecon_MC_ConvertReference2Template(parm,u,v,w);\n',g);
    fprintf(fid,'clear(''parm_MC''); ');
    fprintf(fid,'clear(''u''); ');
    fprintf(fid,'clear(''v''); ');
    fprintf(fid,'clear(''w''); ');
    fprintf(fid,'clear(''MR_gate_%d_%d_%d_corrected'');\n',g,gates,matrix_size);
end

fprintf(fid,'\n%% load data and perform attenuation correction\n');
fprintf(fid,'load(''data/%s_PET_AllGates_AC_%d_%d.mat'');\n',patient,gates,matrix_size);
fprintf(fid,'clear(''allgates_data''); ');
fprintf(fid,'clear(''allgates_data_fp_ac''); ');
fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
fprintf(fid,'clear(''sensitivity_map_ac_%d'');\n',matrix_size);
for g=1:gates
    fprintf(fid,'load(''data/%s_PET_Gate_%d_%d_%d.mat'');\n',patient,g,gates,matrix_size);
    fprintf(fid,'clear(''parm_PET_PrepareDataSinglePatient''); ');
    fprintf(fid,'clear(''sensitivity_map_%d'');\n',matrix_size);
    fprintf(fid,'data_%d = data;\n',g);
    fprintf(fid,'clear(''data'');\n');
    fprintf(fid,'data_fp_%d_ac = EMrecon_MC_ForwardProjAC_ND(parm,umap_%d,data_fp,motion_data_%d);\n',g,matrix_size,g);
    fprintf(fid,'clear(''data_fp'');\n');
end

fprintf(fid,'\n%% create sensitivity_map\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'sensitivity_map_mc_pm_ac_0_%d_%d = EMrecon_MC_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d,motion_data_%d);\n',gates,matrix_size,matrix_size,g);
    else
        fprintf(fid,'sensitivity_map_mc_pm_ac_0_%d_%d = sensitivity_map_mc_pm_ac_0_%d_%d + EMrecon_MC_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d,motion_data_%d);\n',gates,matrix_size,gates,matrix_size,matrix_size,g);
    end
end

fprintf(fid,'\n%% reconstruct PET data for all gates MC AC\n');
fprintf(fid,sprintf('em_all_gates_mc_pm_ac_0_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));

fprintf(fid,'for i=1:parm.ITERATIONS\n');
fprintf(fid,'\tfor s=1:parm.SUBSETS\n');
fprintf(fid,'\t\tparm.FIXED_SUBSET = s;\n\n');
fprintf(fid,'\t\tfprintf(''i%%d s%%d\\n'',i,s)\n');
fprintf(fid,'\t\tsubset_tic = tic;\n\n');

fprintf(fid,'\t\t%% em mc step for each gate\n');
for g=1:gates
    if (g == 1)
        fprintf(fid,'\t\tback = EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d_ac,em_all_gates_mc_pm_ac_0_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    else
        fprintf(fid,'\t\tback = back + EMrecon_MC_EM_Step_PM_ND(parm,data_%d,data_fp_%d_ac,em_all_gates_mc_pm_ac_0_%d_%d,motion_data_%d);\n',g,g,gates,matrix_size,g);
    end
end

fprintf(fid,'\n\t\t%% image update\n');
fprintf(fid,'\t\tindex_image_update = (em_all_gates_mc_pm_ac_0_%d_%d > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map_mc_pm_ac_0_%d_%d > parm.EPSILON);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_ac_0_%d_%d(index_image_update) = em_all_gates_mc_pm_ac_0_%d_%d(index_image_update) .* back(index_image_update) ./ sensitivity_map_mc_pm_ac_0_%d_%d(index_image_update);\n',gates,matrix_size,gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tem_all_gates_mc_pm_ac_0_%d_%d(~index_image_update) = 0.0;\n\n',gates,matrix_size);

fprintf(fid,'\t\t%% smooth image\n');
fprintf(fid,'\t\tif (parm.CONVOLUTIONINTERVAL > 0 && mod(i*s,parm.CONVOLUTIONINTERVAL) == 0)\n');
fprintf(fid,'\t\t\t em_all_gates_mc_pm_ac_0_%d_%d = EMrecon_IP_Convolve_ND(parm,em_all_gates_mc_pm_ac_0_%d_%d);\n',gates,matrix_size,gates,matrix_size);
fprintf(fid,'\t\tend\n\n');

fprintf(fid,'\t\t%% reduce image to cylindrical fov\n');
fprintf(fid,'\t\tem_all_gates_mc_pm_ac_0_%d_%d = EMrecon_IP_CutImage_ND(parm,em_all_gates_mc_pm_ac_0_%d_%d);\n',gates,matrix_size,gates,matrix_size);

if (show_results == 1)
    fprintf(fid,'\n\t\t%% show current result\n');
    fprintf(fid,'\t\timagesc(squeeze(em_all_gates_mc_pm_ac_0_%d_%d(:,80,:))''); title(sprintf(''i%%d s%%d'',i,s)); axis xy; axis image; drawnow;\n\n',gates,matrix_size);
end

fprintf(fid,'\t\tfprintf(''i%%d s%%d done [%%f]\\n'',i,s,toc(subset_tic))\n');
fprintf(fid,'\tend\n');
fprintf(fid,'end\n');

fprintf(fid,'\n%% save results\n');
fprintf(fid,sprintf('save(''results/%s_PET_AllGates_MC_PM_AC_0_%d_%d'',''em_all_gates_mc_pm_ac_0_%d_%d'',''sensitivity_map_mc_pm_ac_0_%d_%d'',''parm'');\n\n',patient,gates,matrix_size,gates,matrix_size,gates,matrix_size));

fprintf(fid,'%% free memory \n');
for g=1:gates
    fprintf(fid,'clear(''motion_data_%d''); ',g);
    fprintf(fid,'clear(''data_%d''); ',g);
    fprintf(fid,'clear(''data_fp_%d_ac'');\n',g);
end

fprintf(fid,'clear(''sensitivity_map_mc_pm_ac_0_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''em_all_gates_mc_pm_ac_0_%d_%d'');\n',gates,matrix_size);
fprintf(fid,'clear(''back'');\n');
fprintf(fid,'clear(''parm'');\n');
fclose(fid);

end
