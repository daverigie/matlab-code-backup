function [] = MR_ResampleDataSinglePatient(base_path_win_data,base_path_win_results,matrix_size,gates,patient)

% debug
% addpath(genpath('H:\Projects\EMreconTNG\trunk\Bin'));
% gates = 5;
% base_path_linux_data = '/media/newraw/koest/Projects/MotionCorrection/Body_MotionCorrection';
% base_path_win_data = 'G:\Data\Body_MotionCorrection';
% base_path_win_results = 'M:\newraw\koest\Projects\MotionCorrection\Body_MotionCorrection';
% patient = 'Patient_B';
% matrix_size = 172;

% make new dirs for new data and results
if (exist(sprintf('%s\\%s\\data',base_path_win_results,patient),'dir') ~= 7)
    mkdir(sprintf('%s\\%s\\data',base_path_win_results,patient));
end
if (exist(sprintf('%s\\%s\\results',base_path_win_results,patient),'dir') ~= 7)
    mkdir(sprintf('%s\\%s\\results',base_path_win_results,patient));
end

% load PET dicom image to extract position of PET image for MR resampling
%[pet_volume,pet_metadata] = readdicomdir(sprintf('%s\\%s\\MoCo_WIP_%s-LM-00-OP_000_000.v-DICOM',base_path_win_data,patient,patient));
[pet_volume,pet_metadata] = readdicomdir(sprintf('%s\\%s\\%s_PET_acimage',base_path_win_data,patient,patient));

% these values have to be taken from the actual DICOM image
parm_PET.INTP_OFFSET_X = +(pet_metadata(1).ImagePositionPatient(1)+(size(pet_volume,1)-1)*pet_metadata(1).PixelSpacing(1)/2.0);
parm_PET.INTP_OFFSET_Y = -(pet_metadata(1).ImagePositionPatient(2)+(size(pet_volume,2)-1)*pet_metadata(1).PixelSpacing(2)/2.0);
parm_PET.INTP_OFFSET_Z = -(pet_metadata(end).ImagePositionPatient(3)+(size(pet_volume,3)-1)*pet_metadata(1).SliceThickness/2.0);
parm_PET.INTP_OFFSET_Y = - parm_PET.INTP_OFFSET_Y;
disp(parm_PET);

% these values are the desired matrix_size
parm_PET.SIZE_X = matrix_size;
parm_PET.SIZE_Y = matrix_size;
parm_PET.SIZE_Z = 127;

parm_PET.FOV_X = 344 * 2.08626;
parm_PET.FOV_Y = 344 * 2.08626;
parm_PET.FOV_Z = 127 * 2.03125;

parm_PET.FLIP_X = 1;
parm_PET.FLIP_Y = 1;
parm_PET.FLIP_Z = 1;

for g=1:gates
    [mr_volume,mr_metadata] = readdicomdir(sprintf('%s\\%s\\%s_MR_bin_%d_%d',base_path_win_data,patient,patient,g,gates));
    
    parm_MR.INTP_OFFSET_X = +(mr_metadata(1).ImagePositionPatient(1)+(size(mr_volume,3)-1)*mr_metadata(1).SliceThickness/2.0);
    parm_MR.INTP_OFFSET_Y = -(mr_metadata(1).ImagePositionPatient(2)+(size(mr_volume,2)-1)*mr_metadata(1).PixelSpacing(2)/2.0);
    parm_MR.INTP_OFFSET_Z = -(mr_metadata(1).ImagePositionPatient(3)-(size(mr_volume,1)-1)*mr_metadata(1).PixelSpacing(1)/2.0);
    parm_MR.INTP_OFFSET_Y = - parm_MR.INTP_OFFSET_Y;
    disp(parm_MR);
    
    % flip MR data
    mr_volume = permute(mr_volume,[3 1 2]);
    
    parm_MR.SIZE_X = size(mr_volume,1);
    parm_MR.SIZE_Y = size(mr_volume,2);
    parm_MR.SIZE_Z = size(mr_volume,3);
    
    parm_MR.FOV_X = parm_MR.SIZE_X * mr_metadata(1).SliceThickness;
    parm_MR.FOV_Y = parm_MR.SIZE_Y * mr_metadata(1).PixelSpacing(2);
    parm_MR.FOV_Z = parm_MR.SIZE_Z * mr_metadata(1).PixelSpacing(1);
    
    parm_MR.FLIP_X = 1;
    parm_MR.FLIP_Y = 1;
    parm_MR.FLIP_Z = 1;
    
    mr_volume_resampled = OF_ResampleImage(parm_MR,parm_PET,mr_volume);
    mr_volume_resampled(isnan(mr_volume_resampled)) = 0;
    
    eval(sprintf('MR_gate_%d_%d_%d = mr_volume_resampled;',g,gates,matrix_size));
    save(sprintf('%s\\%s\\data\\%s_MR_Gate_%d_%d_%d.mat',base_path_win_results,patient,patient,g,gates,matrix_size),sprintf('MR_gate_%d_%d_%d',g,gates,matrix_size));
end

end
