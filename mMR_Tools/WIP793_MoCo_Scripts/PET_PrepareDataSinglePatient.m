function [] = PET_PrepareDataSinglePatient(base_path_win_data,base_path_win_results,base_path_linux_data,matrix_size,gates,patient,only_missing_scripts)

% debug
% addpath(genpath('H:\Projects\EMreconTNG\trunk\Bin'));
% gates = 5;
% base_path_linux_data = '/media/newraw/koest/Projects/MotionCorrection/Body_MotionCorrection';
% base_path_win_data = 'G:\Data\Body_MotionCorrection';
% base_path_win_results = 'M:\newraw\koest\Projects\MotionCorrection\Body_MotionCorrection';
% patient = 'Patient_C';
% matrix_size = 172;

% make new dirs for new data and results
if (exist(sprintf('%s\\%s\\data',base_path_win_results,patient),'dir') ~= 7)
    mkdir(sprintf('%s\\%s\\data',base_path_win_results,patient));
end
if (exist(sprintf('%s\\%s\\results',base_path_win_results,patient),'dir') ~= 7)
    mkdir(sprintf('%s\\%s\\results',base_path_win_results,patient));
end

% setup parameter for EMreconTools
parm.SCANNERTYPE = 8;
parm.LISTMODEFILENAME = sprintf('%s\\%s\\%s.bf',base_path_win_data,patient,patient);
parm.SIZE_X = matrix_size;
parm.SIZE_Y = matrix_size;
parm.SIZE_Z = 127;
parm.MAXTAGS = -1;
parm.VERBOSE = 0;
parm.EMRECON_VERSION = EMrecon_Version;

% if AC data already exists, just update the recon script
if (only_missing_scripts == 0 && exist(sprintf('%s\\%s\\data\\%s_PET_AllGates_AC_%d.mat',base_path_win_results,patient,patient,matrix_size),'file') ~= 2)
    
    % check whether sensitivity_map exists for current resolution, if not, create it
    if (exist(sprintf('%s\\sensitivity_map_%d.mat',base_path_win_data,matrix_size),'file') ~= 2)
        fprintf('creating sensitivity_map for matrix_size %d...\n',matrix_size);
        eval(sprintf('sensitivity_map_%d = EMrecon_BackProj_ND(parm,ones(344*252*837,1));',matrix_size));
        save(sprintf('%s\\sensitivity_map_%d.mat',base_path_win_data,matrix_size),sprintf('sensitivity_map_%d',matrix_size));
        fprintf('creating sensitivity_map for matrix_size %d...done.\n',matrix_size);
    end
    
    % create PET gating signal based on MR RAW data
    parm.MGFFILENAME = CreateMRbasedGatingSignal(base_path_win_data,base_path_win_results,patient,gates);
    
    % display parameter for debug
    fprintf('\nEMreconParameter:\n');
    disp(parm);
    
    % rebin data into sinograms
    [gates_prompt,gates_delayed] = EMrecon_PET_Siemens_mMR_Rebin(parm);
    
    % resort data for display
    delayed_disp = squeeze(sum(reshape(gates_delayed,344,252,837,gates),3));
    prompt_disp = squeeze(sum(reshape(gates_prompt,344,252,837,gates),3));
    
    % display data
    for i=1:gates
        subplot(3,gates,i);
        p = prompt_disp(:,:,i);
        imagesc(p,[0 100000]);
        title(sprintf('Gate %d, prompt',i));
        
        subplot(3,gates,i+gates);
        d = delayed_disp(:,:,i);
        imagesc(d,[0 100000]);
        title(sprintf('Gate %d, delayed',i));
        
        subplot(3,gates,i+2*gates);
        pd = prompt_disp(:,:,i) - delayed_disp(:,:,i);
        imagesc(pd,[0 100000]);
        title(sprintf('Gate %d, p-d',i));
    end
    
    % save data preview
    saveas(gcf,sprintf('%s\\%s\\data\\%s_uncorrected_gates_%d',base_path_win_results,patient,patient,gates),'png');
    close all;
    
    % load data from pipeline for further processing
    load(sprintf('%s\\sensitivity_map_%d.mat',base_path_win_data,matrix_size));
    load(sprintf('%s\\%s\\MoCoWIPPatient%s.mat',base_path_win_data,patient,patient(9:end)));
    
    allgates_data = zeros(344*252*837,1);
    allgates_data_fp = zeros(344*252*837,1);
    
    % process datasets one after the other, i.e. gate by gate
    for i=1:gates
        parm_PET_PrepareDataSinglePatient = parm;
        
        current_prompt = reshape(gates_prompt,344,252,837,gates);
        current_prompt = squeeze(current_prompt(:,:,:,i));
        current_prompt = current_prompt(:);
        current_prompt = EMrecon_PET_Michelogram_AxialWeight(parm_data,current_prompt);
        current_prompt = EMrecon_PET_Michelogram_Normalize(parm_data,current_prompt,normalization);
        current_prompt = EMrecon_PET_Michelogram_FillGaps(parm_data,current_prompt);
        data = current_prompt;
        allgates_data = allgates_data + data;
        
        current_delayed = reshape(gates_delayed,344,252,837,gates);
        current_delayed = squeeze(current_delayed(:,:,:,i));
        current_delayed = current_delayed(:);
        current_delayed = EMrecon_PET_Michelogram_AxialWeight(parm_data,current_delayed);
        current_delayed = EMrecon_PET_Michelogram_Normalize(parm_data,current_delayed,normalization);
        current_delayed = EMrecon_PET_Michelogram_FillGaps(parm_data,current_delayed);
        data_fp = current_delayed + scatterestimate(:);
        allgates_data_fp = allgates_data_fp + data_fp;
        
        save(sprintf('%s\\%s\\data\\%s_PET_Gate_%d_%d_%d.mat',base_path_win_results,patient,patient,i,gates,matrix_size),'data','data_fp',sprintf('sensitivity_map_%d',matrix_size),'parm_PET_PrepareDataSinglePatient');
    end
    
    % free memory
    clear('gates_prompt');
    clear('gates_delayed');
    clear('current_prompt');
    clear('current_delayed');
    clear('data');
    clear('data_fp');
    
    % scale alldata to number of gates
    allgates_data = allgates_data / gates;
    allgates_data_fp = allgates_data_fp / gates;
    
    % resample umap
    umap_in_parm.SIZE_X = 344;
    umap_in_parm.SIZE_Y = 344;
    umap_in_parm.SIZE_Z = 127;
    
    umap_in_parm.FOV_X = umap_in_parm.SIZE_X * 2.08626;
    umap_in_parm.FOV_Y = umap_in_parm.SIZE_Y * 2.08626;
    umap_in_parm.FOV_Z = umap_in_parm.SIZE_Z * 2.03125;
    
    umap_in_parm.FLIP_X = 1;
    umap_in_parm.FLIP_Y = 1;
    umap_in_parm.FLIP_Z = 1;
    
    umap_in_parm.INTP_OFFSET_X = 0.0;
    umap_in_parm.INTP_OFFSET_Y = 0.0;
    umap_in_parm.INTP_OFFSET_Z = 0.0;
    
    umap_out_parm = umap_in_parm;
    
    umap_out_parm.SIZE_X = 172;
    umap_out_parm.SIZE_Y = 172;
    umap_out_parm.SIZE_Z = 127;
    
    if (matrix_size ~= 344)
        eval(sprintf('umap_%d = OF_ResampleImage(umap_in_parm,umap_out_parm,umap);',matrix_size));
    else
        umap_344 = umap;
    end
    
    % create AC sensitivity_map_ac
    fprintf('creating sensitivity_map_ac_%d...\n',matrix_size);
    eval(sprintf('sensitivity_map_ac_%d = EMrecon_BackProjAC_ND(parm,ones(344*252*837,1),umap_%d);',matrix_size,matrix_size));
    fprintf('creating sensitivity_map_ac_%d...done.\n',matrix_size);
    
    % perform attenuation correction on all_data_fp
    fprintf('creating allgates_data_fp_ac...\n');
    eval(sprintf('allgates_data_fp_ac = EMrecon_ForwardProjAC_ND(parm,umap_%d,allgates_data_fp);',matrix_size));
    fprintf('creating allgates_data_fp_ac...done.\n');
    
    % save data for AC recon using all data
    save(sprintf('%s\\%s\\data\\%s_PET_AllGates_AC_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'allgates_data','allgates_data_fp_ac', ...
        sprintf('umap_%d',matrix_size),sprintf('sensitivity_map_ac_%d',matrix_size),'parm_PET_PrepareDataSinglePatient');
    
    % save data for NAC recon using all data
    save(sprintf('%s\\%s\\data\\%s_PET_AllGates_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'allgates_data','allgates_data_fp', ...
        sprintf('sensitivity_map_%d',matrix_size),'parm_PET_PrepareDataSinglePatient');
end

%% create script to run reconstructions
fid = fopen(sprintf('%s\\%s\\%s_recon_gates.m',base_path_win_results,patient,patient),'wt');
fprintf(fid,'%% extend path\n');
fprintf(fid,'clear mex;\n');
fprintf(fid,'addpath(genpath(''%s/EMrecon''));\n\n',base_path_linux_data);

fprintf(fid,'%% setup EMrecon parameter\n');
fprintf(fid,'parm.SCANNERTYPE = 8;\n');
fprintf(fid,'parm.ITERATIONS = 3;\n');
fprintf(fid,'parm.SUBSETS = 21;\n');
fprintf(fid,'parm.FWHM = 4.0;\n');
fprintf(fid,'parm.MAX_RING_DIFF = 60;\n');
fprintf(fid,'parm.EMRECON_VERSION = EMrecon_Version();\n');
fprintf(fid,'parm.SIZE_X = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Y = %d;\n',matrix_size);
fprintf(fid,'parm.SIZE_Z = 127;\n');
fprintf(fid,'parm.CONVOLVE = 1;\n');
fprintf(fid,'parm.CONVOLUTIONINTERVAL = 63;\n\n');

recon_data = 0;
if (only_missing_scripts == 0)
    recon_data = 1;
end
if (only_missing_scripts == 1 && exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_AC_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    recon_data = 1;
end
if (recon_data == 1)
    fprintf(fid,'%% reconstruct PET data for all gates AC\n');
    fprintf(fid,sprintf('load(''data/%s_PET_AllGates_AC_%d_%d'');\n',patient,gates,matrix_size));
    fprintf(fid,sprintf('em_all_gates_ac_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));
    fprintf(fid,sprintf('em_all_gates_ac_%d_%d = EMrecon_EM_ND(parm,allgates_data,allgates_data_fp_ac,sensitivity_map_ac_%d,em_all_gates_ac_%d_%d);\n',gates,matrix_size,matrix_size,gates,matrix_size));
    fprintf(fid,sprintf('save(''results/%s_PET_AllGates_AC_%d_%d'',''em_all_gates_ac_%d_%d'',''parm'');\n',patient,gates,matrix_size,gates,matrix_size));
    fprintf(fid,sprintf('clear(''allgates_data'');\n'));
    fprintf(fid,sprintf('clear(''allgates_data_fp_ac'');\n'));
    fprintf(fid,sprintf('clear(''em_all_gates_ac_%d_%d'');\n',gates,matrix_size));
    fprintf(fid,sprintf('clear(''sensitivity_map_ac_%d'');\n',matrix_size));
    fprintf(fid,sprintf('clear(''parm_PET_PrepareDataSinglePatient'');\n'));
    fprintf(fid,sprintf('clear(''umap_%d'');\n\n',matrix_size));
end

recon_data = 0;
if (only_missing_scripts == 0)
    recon_data = 1;
end
if (only_missing_scripts == 1 && exist(sprintf('%s\\%s\\results\\%s_PET_Recon_Gate_AC_%d_%d_%d.mat',base_path_win_results,patient,patient,1,gates,matrix_size),'file') ~= 2)
    recon_data = 1;
end
if (recon_data == 1)
    fprintf(fid,'%% reconstruct PET data for reference gate AC\n');
    fprintf(fid,sprintf('load(''data/%s_PET_AllGates_AC_%d_%d.mat'');\n',patient,gates,matrix_size));
    fprintf(fid,sprintf('load(''data/%s_PET_Gate_%d_%d_%d'');\n',patient,1,gates,matrix_size));
    fprintf(fid,sprintf('data_fp_ac = EMrecon_ForwardProjAC_ND(parm,umap_%d,data_fp);\n',matrix_size));
    fprintf(fid,sprintf('em_gate_ac_%d_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',1,gates,matrix_size));
    fprintf(fid,sprintf('em_gate_ac_%d_%d_%d = EMrecon_EM_ND(parm,data,data_fp_ac,sensitivity_map_ac_%d,em_gate_ac_%d_%d_%d);\n',1,gates,matrix_size,matrix_size,1,gates,matrix_size));
    fprintf(fid,sprintf('save(''results/%s_PET_Recon_Gate_AC_%d_%d_%d'',''em_gate_ac_%d_%d_%d'',''parm'');\n',patient,1,gates,matrix_size,1,gates,matrix_size));
    fprintf(fid,sprintf('clear(''data'');\n'));
    fprintf(fid,sprintf('clear(''data_fp'');\n'));
    fprintf(fid,sprintf('clear(''data_fp_ac'');\n'));
    fprintf(fid,sprintf('clear(''allgates_data'');\n'));
    fprintf(fid,sprintf('clear(''allgates_data_fp_ac'');\n'));
    fprintf(fid,sprintf('clear(''em_gate_ac_%d_%d_%d'');\n',1,gates,matrix_size));
    fprintf(fid,sprintf('clear(''sensitivity_map_%d'');\n',matrix_size));
    fprintf(fid,sprintf('clear(''sensitivity_map_ac_%d'');\n',matrix_size));
    fprintf(fid,sprintf('clear(''parm_PET_PrepareDataSinglePatient'');\n'));
    fprintf(fid,sprintf('clear(''umap_%d'');\n\n',matrix_size));
end

recon_data = 0;
if (only_missing_scripts == 0)
    recon_data = 1;
end
if (only_missing_scripts == 1 && exist(sprintf('%s\\%s\\results\\%s_PET_AllGates_%d_%d.mat',base_path_win_results,patient,patient,gates,matrix_size),'file') ~= 2)
    recon_data = 1;
end
if (recon_data == 1)
    fprintf(fid,'%% reconstruct PET data for all gates NAC\n');
    fprintf(fid,sprintf('load(''data/%s_PET_AllGates_%d_%d.mat'');\n',patient,gates,matrix_size));
    fprintf(fid,sprintf('em_all_gates_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',gates,matrix_size));
    fprintf(fid,sprintf('em_all_gates_%d_%d = EMrecon_EM_ND(parm,allgates_data,allgates_data_fp,sensitivity_map_%d,em_all_gates_%d_%d);\n',gates,matrix_size,matrix_size,gates,matrix_size));
    fprintf(fid,sprintf('save(''results/%s_PET_AllGates_%d_%d'',''em_all_gates_%d_%d'',''parm'');\n',patient,gates,matrix_size,gates,matrix_size));
    fprintf(fid,sprintf('clear(''allgates_data'');\n'));
    fprintf(fid,sprintf('clear(''allgates_data_fp'');\n'));
    fprintf(fid,sprintf('clear(''em_all_gates_%d_%d'');\n',gates,matrix_size));
    fprintf(fid,sprintf('clear(''sensitivity_map_%d'');\n',matrix_size));
    fprintf(fid,sprintf('clear(''parm_PET_PrepareDataSinglePatient'');\n\n'));
end

for i=1:gates
    recon_data = 0;
    if (only_missing_scripts == 0)
        recon_data = 1;
    end
    if (only_missing_scripts == 1 && exist(sprintf('%s\\%s\\results\\%s_PET_Recon_Gate_%d_%d_%d.mat',base_path_win_results,patient,patient,i,gates,matrix_size),'file') ~= 2)
        recon_data = 1;
    end
    if (recon_data == 1)
        fprintf(fid,['%% reconstruct PET data for gate ' num2str(i) '\n']);
        fprintf(fid,sprintf('load(''data/%s_PET_Gate_%d_%d_%d'');\n',patient,i,gates,matrix_size));
        fprintf(fid,sprintf('em_gate_%d_%d_%d = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);\n',i,gates,matrix_size));
        fprintf(fid,sprintf('em_gate_%d_%d_%d = EMrecon_EM_ND(parm,data,data_fp,sensitivity_map_%d,em_gate_%d_%d_%d);\n',i,gates,matrix_size,matrix_size,i,gates,matrix_size));
        fprintf(fid,sprintf('save(''results/%s_PET_Recon_Gate_%d_%d_%d'',''em_gate_%d_%d_%d'',''parm'');\n',patient,i,gates,matrix_size,i,gates,matrix_size));
        fprintf(fid,sprintf('clear(''data'');\n'));
        fprintf(fid,sprintf('clear(''data_fp'');\n'));
        fprintf(fid,sprintf('clear(''em_gate_%d_%d_%d'');\n',i,gates,matrix_size));
        fprintf(fid,sprintf('clear(''sensitivity_map_%d'');\n',matrix_size));
        fprintf(fid,sprintf('clear(''parm_PET_PrepareDataSinglePatient'');\n\n'));
    end
end

fprintf(fid,'%% free memory\n');
fprintf(fid,sprintf('clear(''parm'');\n'));

end
