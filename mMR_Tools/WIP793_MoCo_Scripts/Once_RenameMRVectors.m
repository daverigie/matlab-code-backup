% Once_RenameMRVectors.m

id = 'V';

gates = 5;
base_path = 'R:\koestt01labspace\Data\PET\MotionCorrection\WIP793';

for i=2:5
    source = sprintf('%s\\Patient_%s\\Vectors\\1_%d',base_path,id,i);
    destination = sprintf('%s\\Patient_%s\\Vectors\\%d_1_%d',base_path,id,gates,i);
    
    dos(sprintf('move %s %s',source,destination));
end