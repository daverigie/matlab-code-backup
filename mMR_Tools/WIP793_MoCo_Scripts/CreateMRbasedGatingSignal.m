function [pet_gating_filename] = CreateMRbasedGatingSignal(base_path_data,base_path_results,patient,gates)

fprintf('CreateMRbasedGatingSignal...\n');

% create mr_gating_filename
mr_gating_filename = sprintf('%s\\%s\\%s_MR_GatingSignal_%d.txt',base_path_data,patient,patient,gates);
fprintf('mr_gating_filename: %s\n',mr_gating_filename);

% read gating info to variable
gating_info = load(mr_gating_filename);
gating_time = gating_info(:,1);
gating_gate = gating_info(:,2);

% create pet_listmode_filename
pet_listmode_filename = sprintf('%s\\%s\\%s.bf',base_path_data,patient,patient);
fprintf('pet_listmode_filename: %s\n',pet_listmode_filename);

% get sync time tag
pet_time_offset_in_ms = LM_GetPETMRSyncTime_MEX(pet_listmode_filename);
fprintf('pet_time_offset_in_ms: %d\n',pet_time_offset_in_ms);

% create pet_listmode_filename
pet_gating_filename = sprintf('%s\\%s\\data\\%s_PET_Gating_%d.mgf',base_path_results,patient,patient,gates);
fprintf('pet_gating_filename: %s\n',pet_gating_filename);

% create new gating info due to incompatible mr gating signal
new_gating_time = zeros(numel(gating_time)-2,1);
new_gating_gate = zeros(numel(gating_gate)-2,1);

starttime = pet_time_offset_in_ms + round((gating_time(1) + gating_time(2)) / 2.0);
stoptime = pet_time_offset_in_ms + round((gating_time(end-1) + gating_time(end)) / 2.0);

for g=1:numel(new_gating_time)
    new_gating_time(g) = round((gating_time(g+1) + gating_time(g+2)) / 2.0);
    new_gating_gate(g) = gating_gate(g+1);
end

% overwrite old gating_time and gating_gate
gating_time = new_gating_time;
gating_gate = new_gating_gate;
clear('new_gating_time');
clear('new_gating_gate');

% write new gating file to disk
% writeMGF(filename,timesignal,gatingsignal,starttime,stoptime,cardiac_gates,respiratory_gates,comments)
writeMGF(pet_gating_filename, ... % filename
    gating_time+pet_time_offset_in_ms, ... % timesignal
    gating_gate, ... % gatingsignal
    starttime, ... % starttime
    stoptime, ... % stoptime
    1, ... % cardiac gates
    gates, ... % respiratory gates
    'Gating Signal created using Robert Grimm`s CMD line tool.');

fprintf('CreateMRbasedGatingSignal...done.\n');

end
