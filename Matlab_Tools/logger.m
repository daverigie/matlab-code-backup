function callable = logger(filename)

    fclose(fopen(filename,'w+'));
    callable = @(varargin) logmsg_helper(filename, varargin{:});

end

function [] = logmsg_helper(filename, varargin)

    fid = fopen(filename,'a+');
    
    % Make sure that file closes regardless of how function exits
    c = onCleanup(@()fclose(fid));
    
    % Write message with timestamp
    timestamp = ['[',datestr(datetime),']'];
    fprintf(fid, '%s ', timestamp);
    fprintf(fid, varargin{:});    
    fprintf(fid, '\n');

end

