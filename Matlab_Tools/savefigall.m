function [] = savefigall(fighandle, filepath, extensions)

    SAVE_EXTENSIONS = {'.pdf','.png','.fig','.svg','.emf', '.jpg', '.eps'};

    if nargin < 3
        extensions = SAVE_EXTENSIONS;
    end
    
    [dirpath, basename, ext] = fileparts(filepath);
    mkdir(fullfile(dirpath, basename));
    
    buildpath = @(ext) fullfile(dirpath, basename, strcat(basename, ext));
    
    for ext = extensions
       fprintf('\nSaving %s ...', ext{1});
       savefigas(fighandle, buildpath(ext{1})); 
       fprintf('Done.');
    end
    
    fprintf('\n\n');
    
end

function savefigas(fighandle, filepath)

    [~,~,ext] = fileparts(filepath);
    
    switch upper(ext)
        case '.PDF'
            savepdfas(fighandle, filepath);
        otherwise
            saveas(fighandle, filepath)
    end

end

function savepdfas(fighandle, filepath)

    % Found this solution at:
    %   https://www.mathworks.com/matlabcentral/answers/12987-how-to-save-a-matlab-graphic-in-a-right-size-pdf

    h = fighandle;
    set(h,'Units','Inches');
    pos = get(h,'Position');
    set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(h, filepath, '-dpdf','-r0')

end