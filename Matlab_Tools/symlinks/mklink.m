function [status,result] = mklink(targetpath, linkpath)

    if ispc
        mklink_windows(targetpath, linkpath);
    else
        mklink_linux(targetpath, linkpath);
    end

end