function [status, result] = mklink_linux(targetpath, linkpath)

    command = sprintf('ln -s %s %s', targetpath, linkpath);
    [status, result] = system(command,'-echo');

end