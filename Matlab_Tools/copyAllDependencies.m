function [flist, plist] = copyAllDependencies(varargin)

    if nargin < 2
        varargin{2} = 'src';
    end
    
    if nargin < 1
        [filename, dirname] = uigetfile('*.m');
        varargin{1} = fullfile(dirname, filename);
    end
    
    mfilepath = varargin{1};
    srcpath   = varargin{2};

    fprintf('\nDetermining m-file dependencies...'); tic;
    
    [flist, plist] = matlab.codetools.requiredFilesAndProducts(mfilepath);

    toc;
        
    mkdir(srcpath);
    
    for i = 1:numel(flist)
        fprintf('\nCopying file %i of %i', i, numel(flist));
        copyfile(flist{i}, srcpath);
    end
    
    fprintf('\n\n');
    
end