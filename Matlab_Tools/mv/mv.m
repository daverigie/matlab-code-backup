function varargout = mv(varargin)

    if ispc
        movefun = @mv_win;
    else
        movefun = @mv_linux;
    end
    
    if nargout > 0
        [varargout{1:nargout}] = movefun(varargin{:});
    else
        movefun(varargin{:});
    end

end

function [status, result] = mv_win(src, dest, mode)

    if nargin < 3
        mode = '';
    end
       
    if strcmpi(mode, 'F')
        cmdstring = sprintf('%s %s %s %s', 'move', '/y', src, dest);
    else
        cmdstring = sprintf('%s %s %s', 'move', src, dest);
    end
    
    [status, result] = system(cmdstring);

end

function [status, result] = mv_linux(src, dest, mode)

    if nargin < 3
        mode = '';
    end
       
    if strcmpi(mode, 'F')
        cmdstring = sprintf('%s %s %s %s', 'mv', '-f', src, dest);
    else
        cmdstring = sprintf('%s %s %s', 'mv', src, dest);
    end
    
    [status, result] = system(cmdstring);

end