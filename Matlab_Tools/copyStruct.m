function S = copyStruct(S0)

    allFields = fields(S0);

    S = struct();
    for i = 1:1:numel(allFields)
        f = allFields{i};
        S = setfield( S, f, getfield(S0, f) );
    end
    
end