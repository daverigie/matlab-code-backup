function [subVol, mask, fig] = selectVOI(imstack)

    %{
    %% Load Dicom Images 

    if ~exist('imstack', 'var');
    %imstack = loadDicomSeries;
    imstack = loadDicomSeries('Q:\mMR_Twilite\PET_Data\Dicom\HEAD_MRAC_PET_AC_IMAGES_0011');

    %% Threshold based

    thresh_min = 20000;
    thresh_max = inf;
    mask = (thresh_min < imstack).*(imstack < thresh_max);
    mask = logical(mask);

    n_axes = ndims(imstack);
    axes = cell([1,n_axes]);
    for i=1:1:n_axes
        axes{i} = subplot(1,n_axes,i);
        imshow(squeeze(max(mask,[],i)),[]);
    end

    %linkaxes([axes{:}]);
    %}

    %% Select VOI Graphically

    [temp, rect_trans] = imcrop(squeeze(max(imstack,[],3)),[]);
    [temp,rect2] = imcrop(squeeze(max(imstack,[],1))',[]); %sag_min, trans_min, D_sag, D_trans

    sag_min = rect_trans(1); 
    cor_min = rect_trans(2); 
    D_sag = rect_trans(3); 
    D_cor = rect_trans(4);

    trans_min = rect2(2);
    D_trans = rect2(4);

    rect_cor = [sag_min, trans_min, D_sag, D_trans];
    rect_sag = [trans_min, cor_min, D_trans, D_cor];

    %% display results
    fig = figure;
    subplot(131);
    imagesc(squeeze(max(imstack,[],1))'); hold on;
    rectangle('Position',rect_cor,'EdgeColor', 'red');
    title('Coronal');

    subplot(132);
    imagesc(squeeze(max(imstack,[],2))); hold on;
    rectangle('Position',rect_sag,'EdgeColor', 'red');
    title('Sagittal');

    subplot(133);
    imagesc(squeeze(max(imstack,[],3))); hold on;
    rectangle('Position',rect_trans,'EdgeColor', 'red');
    title('Axial');
    

    %% convert to mask 
    mask = 0.0*imstack;

    row_min = round(cor_min); 
    row_max = round(row_min + D_cor);

    col_min = round(sag_min);
    col_max = round(col_min + D_sag);

    slc_min = round(trans_min);
    slc_max = round(trans_min + D_trans);

    subVol = imstack(row_min:row_max, col_min:col_max, slc_min:slc_max);
    mask(row_min:row_max, col_min:col_max, slc_min:slc_max) = 1.0;
    mask = logical(mask);

end




    
