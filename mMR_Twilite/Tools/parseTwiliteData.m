function parseTwiliteData(filepath)
	
	% This script is for parsing the text files from pmod received from the
	% Twilite blood sampling system. 
	%
	% This script will save a structure variable as a .MAT file as well as
	% create a formatted .csv file suitable for Microsoft Excel. 

	% First a .MAT file will be saved with the same base name as the original 
	% raw data file, and then the user will be asked to specify a location 
	% to save the .CSV file.
	%
	% Written by David Rigie - 7/12/2016

	if nargin == 1
		FULLPATH_INPUT = filepath	
	else
		[filepath, dirpath] = uigetfile('*.crv');
		FULLPATH_INPUT = fullfile(dirpath, filepath);
	end

	%% Read raw text file into struct
	raw_data_full = dlmread(FULLPATH_INPUT);

	twilite_data.YEAR = uint16(raw_data_full(1,1));
	twilite_data.MONTH = uint8(raw_data_full(1,2));
	twilite_data.DAY = uint8(raw_data_full(1,3));

	num_datapoints = size(raw_data_full,1);

	for i = 1:1:num_datapoints
	    datapoint.hour = uint8(raw_data_full(i,4));
	    datapoint.minute = uint8(raw_data_full(i,5));
	    datapoint.second = double(raw_data_full(i,6));
	    datapoint.coincidences  = int64(raw_data_full(i,7));
	    datapoint.singles1 = int64(raw_data_full(i,8));
	    datapoint.singles2 = int64(raw_data_full(i,9));
	    twilite_data.datapoints(i) = datapoint;
	end 

	%% Print some basic information about the data file

	fprintf('\n\nDate: %i/%i/%i\n\n', twilite_data.MONTH, twilite_data.DAY, ... 
	                              twilite_data.YEAR);

	datapoint_first = twilite_data.datapoints(1);
	datapoint_last = twilite_data.datapoints(num_datapoints);
	fprintf('Number of Data Points: %i\nStart Time: %i:%i:%f\nEnd Time: %i:%i:%f\n\n', ...
	        num_datapoints, datapoint_first.hour, datapoint_first.minute, ...
	        datapoint_first.second, datapoint_last.hour, datapoint_last.minute,...
	        datapoint_last.second);
	    
	%% Save Data as .mat file for later manipulation in MATLAB

	[pathstr, name, ext] = fileparts(FULLPATH_INPUT);
	MATFILEPATH = fullfile(pathstr, strcat(name, '.mat'));
	save(MATFILEPATH, 'twilite_data');    

	%% Create Formatted CSV File for Loading into Excel

	[pathstr, name, ext] = fileparts(FULLPATH_INPUT);
	DEFAULTPATH_OUTPUT = fullfile(pathstr,strcat(name, '_PARSED.csv'));

	[filepath, dirpath] = uiputfile(DEFAULTPATH_OUTPUT);
	FULLPATH_OUTPUT = fullfile(dirpath, filepath);

	fileID = fopen(FULLPATH_OUTPUT,'w');
	fprintf(fileID, 'Date,%i/%i/%i\n', twilite_data.MONTH, twilite_data.DAY, ...
	                                    twilite_data.YEAR);
	                                
	fprintf(fileID, 'Time,Coincidence Rate,Single Rate 1,Single Rate 2\n');

	for i=1:1:num_datapoints
	    datapoint = twilite_data.datapoints(i);
	    fprintf(fileID, '%u:%u:%f,%u,%u,%u\n', ...
	            datapoint.hour, datapoint.minute, datapoint.second,...
	            datapoint.coincidences, datapoint.singles1, datapoint.singles2);
	end

	fclose(fileID);

                                
end