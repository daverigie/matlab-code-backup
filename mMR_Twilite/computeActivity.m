% This is a script for analyzing the Twilite calibration data collected on
% August 19, 2016
% Author: Dave Rigie


%% Clear workspace
clc; clear;

%% Load image data and select VOI
[imstack, info_arr] = loadDicomSeries(uigetdir);


%% Compute some statistics
[subVol, mask, fig_voi] = selectVOI(imstack); 

%%
fig_hist = figure;
n_bins = 100;

voi_avg = mean(subVol(:)/1000);
voi_median = median(subVol(:)/1000);
voi_std = std(subVol(:)/1000);

hist(subVol(:)/1000, n_bins);
title('Activity in VOI');
ylabel('Number of Voxels');
xlabel('KBq/cc');

annotation_text = sprintf('Mean: %.3f\nMedian: %.3f\nStdev: %.3f', ...
                          voi_avg, voi_median, voi_std);
                      
h = annotation('textbox',[0.15 0.7 0.3 0.2]);
set(h,'String',annotation_text);

fig_box = figure;
boxplot(subVol(:)/1000);
ylabel('Activity in VOI (kBq/cc)');
title('Activity in VOI');



%% Save Calibration Data

results_path = uigetdir;
save(fullfile(results_path, 'results.mat'));
savefig(fig_voi, fullfile(results_path, 'voi_selection'));
savefig(fig_hist, fullfile(results_path, 'histogram'));
savefig(fig_box, fullfile(results_path, 'box_plot'));




