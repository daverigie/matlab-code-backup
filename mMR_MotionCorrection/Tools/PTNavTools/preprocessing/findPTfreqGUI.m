function [freq_min, idx] = findPTfreqGUI(kdata, guess)
% Interactive method for selecting the pilot tone frequency. Can also
% overlay the guess from the automated approach. freq_min will be used as
% the cut_off point to low/high pass filter the k-space data

    global rectPosition;
    global r;
    global fig;
    global haxes;
    global himg;
    
    global errorCounter;
    errorCounter = 0;

    N = size(kdata,1);
    
    fs = 2.0; % Using sampling frequency of 2 yields a result that is normalized according to MATLAB convention
    df = fs/N;
    
    if mod(N,1)
        fmin = -(fs-df)/2.0;
        fmax = (fs-df)/2.0;
    else
        fmin = -(fs-2*df)/2;
        fmax = (fs-df)/2;
    end
    
    ff = linspace(fmin, fmax, N);
    
    kdata = squeeze(sum(kdata, 2));
    kdata_freq_mag = abs(fftshift(fft(kdata),1));
    
    fprintf('\n\rManually bracket pilot tone and then close window...\r');
    
    [fig, haxes, himg] = imageviewer(kdata_freq_mag);
    
    if nargin > 1
       % Draw arrow pointing to guessed frequency
       [height, width, n_par] = size(kdata_freq_mag);
       [~,y] = min(abs(ff-guess));
       x1 = 0;
       x2 = width;
       y1 = y;
       y2 = y;
       hold on;
       line([x1,x2/20],[y1,y2],... 
            'Color','y', 'LineWidth',0.1);
       
       
    end
    
    set(fig, 'CloseRequestFcn', @my_closereq);
    r = imrect(haxes);
    waitfor(fig);
    
    ymin = rectPosition(2);    
        
    freq_ind = floor(ymin);
    freq_min = ff(freq_ind);
    idx = freq_ind;
    

end


function my_closereq(src,callbackdata)
% Close request function 
% to display a question dialog box 
   global errorCounter;
   global rectPosition
   global r
   global haxes
   
   try
        rectPosition = r.getPosition;
        delete(gcf);
   catch ME
       beep;
       error('Please make rectangle selection before closing!');
       errorCounter = errorCounter + 1;
       if errorCounter > 3
           delete(gcf);
       end
   end

end

