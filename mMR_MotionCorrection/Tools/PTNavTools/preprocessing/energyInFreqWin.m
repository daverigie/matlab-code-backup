function [E_Wn] = energyInFreqWin(y, dt, Wn)
% Determine what fraction of signals total energy is contained in Wn window
% ff is freq axis, and s is the fft of the signal

% Compute total energy

    yf = abs(fftshift(fft(y - mean(y))));
    tt = ((1:numel(y))-1)*dt;
    ff = getfreqaxis(tt);

    Etotal = dot(yf,yf);

    ind = logical((ff>Wn(1)).*(ff<Wn(2)));
    
    E_Wn = dot(yf(ind), yf(ind));
            

end

