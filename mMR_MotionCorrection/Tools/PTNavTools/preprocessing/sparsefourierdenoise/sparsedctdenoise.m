function X = sparsedctdenoise(X0, varargin)

    X = X0*1.0;
    for i = 1:1:size(X0,1)
       X(i,:) = sparsefourierdenoise1d(X0(i,:), varargin{:});
    end

end

function x = sparsefourierdenoise1d(x0, lambda, maxIter, acceleration)

    if nargin < 4
        acceleration = true;
    end
    
    if nargin < 3
        maxIter = 100;
    end
    
    if nargin < 2
       lambda = 0.01; 
    end
    
    lambda = lambda*norm(x0-mean(x0));
        

    % Fourier Transform Operators (Symmetrized)
    F = @(x) dct(x);
    FH = @(x) idct(x);
    
    % Other useful functions
    vnorm = @(x) norm(x(:));
    vdot = @(x,y) dot(x(:),y(:));
    
    % Initialize iteration variables
    x = x0;
    xbar = x0;
    y = x0;
    
    % Initialize Step-size variables
    theta = 1.0;
    gamma = 1.0/lambda;
    tau = 1.0;
    sigma = 1.0;

    for i = 1:maxIter
               
        % Dual Update
        y = proj(y + sigma*F(xbar));
        
        % Primal Update
        x_last = x*1.0;
        x = (lambda*x + tau*x0 - lambda*tau*FH(y))/(lambda + tau);
        
        % Acceleration
        if acceleration
           theta = 1.0/sqrt(1 + 2*gamma*tau);
           tau = tau*theta;
           sigma = sigma/theta;
        end
        
        % Extrapolation Step
        xbar = x + theta*(x - x_last);
        
        fprintf('\n[%i, cost: %E]', i, cost(x));
                
    end
    
    x = real(x);
    x = rescaleSignal2Match(x, x0);
    
    function val = cost(x)
        
        val = 0.5*vnorm(x-x0)^2 + lambda*sum(abs(F(x)));
        
    end

end

function xp = proj(x)

    xp = sign(x).*min(abs(x), 1);

end