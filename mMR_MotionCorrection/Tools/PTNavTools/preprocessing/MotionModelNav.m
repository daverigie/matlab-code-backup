classdef MotionModelNav < MotionModel
        
    properties
        T;
        Tpca;
        tResp;
        tCard;    
        respInd;
        cardInd;
        trainingParams
    end
        
    methods
        
        function obj = MotionModelNav(twix_obj_nav)
           obj = obj@MotionModel(twix_obj_nav);
        end
        
        function [obj, pcasig] = train(obj, knav, varargin)
            
            %==============================================================
            %                   PARSE INPUTS
            %--------------------------------------------------------------
            
            p = inputParser;
            p.addRequired('knav');
            p.addParameter('PCAAlgorithm', 'rsvd', @ischar);
            p.addParameter('EigCutoff',    20, @isscalar);
            p.addParameter('BSSAlgorithm', 'sobi');
            p.addParameter('fastICAParams', {'epsilon', 1e-6, ...
                                             'stabilization', 'on', ...
                                             'g', 'tanh',...
                                             'approach','symm'},...
                                             @iscell);
            p.addParameter('normalizeNavigators', false);
            
            parse(p, knav, varargin{:});
            
            % Save training params
            trainingParams = p.Results;
            obj.trainingParams = rmfield(trainingParams, 'knav');
                        
            knav = p.Results.knav;

            switch upper(p.Results.PCAAlgorithm)
                case 'RSVD'
                    pcafun = @pcaSignals_rsvd;
                case 'EIG'
                    pcafun = @pcaSignals;
                otherwise
                    error('Invalid PCA Algorithm. Use rsvd or eig.');
            end
            
            switch upper(p.Results.BSSAlgorithm)
                case 'PCAONLY'
                    bssfun = @(X) X;
                case 'FASTICA'
                    bssfun = @(X) fastica(X, p.Results.fastICAParams{:});
                case 'AMUSE'
                    bssfun = @(X) amuse(X, 1);
                case 'SOBI'
                    bssfun = @(X) sobi(X, 500);
                otherwise
                    error('Invalid BSS Algorithm');
            end
                        
            %______________________________________________________________
            
            X = obj.generate2DSignalMatrix(knav);
            
            if p.Results.normalizeNavigators
                weights = 1.0./sqrt( sum( X.*conj(X), 1 ) );
                X = bsxfun(@times, X, weights);
            end
            
            % Dimension reduction
            
            %[pcasig, V, d] = pcaSignals_rsvd(X, 'lastEig', p.Results.EigCutoff);
            % Randomly subsample matrix before PCA
            if size(X,2) > 10000
                numSamples      = 10000;
                idx             = randi([1000,size(X,2)], [1, numSamples]);
                Xs              = X(:, idx);
            else
                Xs = X;
            end

            
            lastEig = min(p.Results.EigCutoff, size(Xs,1));
            
            [~, V, d]  = pcaSignals_rsvd(Xs, 'lastEig', lastEig);
            
            T1 = V';
            obj.Tpca = T1;
            pcasig = T1*X;
            
            obj.trainFromPCA(pcasig, bssfun)
            
        end
        
        function obj = trainFromPCA(obj, pcasig, bssfun)
            % Blind Source Separation
            [S, W] = bssfun(pcasig);
            obj.T = W*obj.Tpca;
            
            % Identify Resp/Card Components
            [~, idx]  = freqRankSignals(S, obj.dt, obj.wnResp);
            %[~, idx]   = findTargetSignal(S, obj.dt, obj.wnResp);
            obj.tResp = obj.T(idx(1), :);
            obj.respInd = idx(1);
            
            
            %[~, idx]  = freqRankSignals(S, obj.dt, obj.wnCard);
            [~, idx]   = findTargetSignal(S, obj.dt, obj.wnCard);
            obj.tCard = obj.T(idx(1), :);        
            obj.cardInd = idx(1);
        end
                
        function X = generate2DSignalMatrix(obj, knav)
           
            % Reshape into 2D signal matrix
            
            if ndims(knav) == 4
                X    = permute(knav, [1,4,2,3]);
            else
                X = knav;
            end
            
            %X    = abs(fft(X));
            
            clear knav;
            
            X    = reshape(X, size(X,1)*size(X,2), []);
            
            % Expand complex numbers along vertical compension
            if ~isreal(X)
                X = cat(1, real(X), imag(X));
            end
            X = double(X);
            
            % Highpass filter and mean subtraction
            X    = double(X);
            %X    = obj.filterHi(X);
            
            % Mean subtraction
            X    = bsxfun(@minus, X, mean(X,2)); 
            
        end
    end
        
        
        
        
            
            
            
end
         
    

