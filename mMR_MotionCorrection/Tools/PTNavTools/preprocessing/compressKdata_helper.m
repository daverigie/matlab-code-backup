function [kdata] = compressKdata_helper(kdata, numComponents)

    dims = size(kdata);
    if dims(4) <= numComponents
        kdata = kdata;
        return;
    end
    
    kdata = permute(kdata,[4,1,2,3]);
    kdata = reshape(kdata,size(kdata,1),[]);
    
    [U, ~, ~, ~] = compressMatrix(kdata, numComponents);
    
    kdata = U'*kdata;
    
    kdata = reshape(kdata, numComponents,dims(1),dims(2),dims(3));
    kdata = single(permute(kdata, [2, 3, 4, 1]));

end