function [s, ind] = findTargetSignal(S, dt, targetWindow)

    ENERGY_FRACTION_THRESHOLD = 0.65;
    fnyq         = 0.5/dt;

    % Rank signals by fraction of energy in target window
    [S2, I, EE] = freqRankSignals(S, dt, targetWindow);
    S2 = S2((EE > ENERGY_FRACTION_THRESHOLD), :);
    
    % Create clusters based on power spectrum similarity
    M     = powerSpectrumSimilarityMatrix(S2);
    Cidx  = findClusters(M);
    candidates = {};
    for i = 1:1:numel(Cidx)
       candidates{i} = S2(Cidx{i},:); 
    end

    % Find cluster with highest spectral entropy
    maxEntropy = 0.0;
    iMaxEntropy = 1;
    for i = 1:1:numel(candidates)
       [candidates{i}, I, EE] = rankSignals(candidates{i}, @spectralEntropy);    
       if max(EE) > maxEntropy
           maxEntropy  = max(EE);
           iMaxEntropy = i;
       end
    end
    
    if numel(candidates) == 0
        s = S(1,:);
        ind = 1;
        return
    end
    
    % Desired signal is lowest entropy within selected cluster
    cossim = @(a,b) dot(a(:),b(:))/(norm(a(:))*norm(b(:)));
    s = candidates{iMaxEntropy}(end,:);
   
    % Figure out the index corresponding to the target signal
    clear simVec;
    for i = 1:1:size(S,1)
       simVec(i) = cossim(S(i,:), s);
    end
    [maxSim, ind] = max(simVec);
    

end