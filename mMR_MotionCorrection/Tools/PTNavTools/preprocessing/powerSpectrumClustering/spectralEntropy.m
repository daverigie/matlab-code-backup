function E = spectralEntropy(y)

    % Compute spectral density
    yf = fft(y);
    yf = conj(yf).*yf;
    
    % Normalize to convert to probability distribution
    p = yf/sum(yf);
    
    E = -sum(p.*log(p));
    


end