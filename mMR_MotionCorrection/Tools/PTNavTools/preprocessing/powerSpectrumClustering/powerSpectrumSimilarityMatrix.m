function M = powerSpectrumSimilarityMatrix(S)

    % Each row of S is a time signal

    % Compute power spectra of all signals
    F = fft(S.').';
    F = conj(F).*F;
    
    % Compute distance matrix based on cosine similarity of power spectrum
    D = squareform(pdist(F, 'cosine'));
    M = 1.0-D;

end