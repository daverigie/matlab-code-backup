N    = 1000;
Nref = 1000;

tref = linspace(0, 2999, Nref);
t    = tref(1:step:end);

yref = sin(0.1*tref);
step = Nref/N;
y    = yref(1:step:end);


plot(t,y,tref,yref)

tlag = determinesignallag(t,y,tref,yref)
%figure
%plot(t-tlag, y, tref, yref)