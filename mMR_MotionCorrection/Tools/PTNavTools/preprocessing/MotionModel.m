classdef (Abstract) MotionModel < handle
   
    properties (Abstract)
        T;
        Tpca;
        tResp;
        tCard;    
        respInd;
        cardInd;
    end
    
    properties
        % Preprocessing steps
        fcutHi       = 0.1;
        filterFun    = @butter;
        filterOrder  = 5;
        filterParams 
                
        % Freq windows for identifying resp/card components
        wnResp  = [0.10,  0.50]; % Hz
        wnCard  = [0.75,  2.00];
        
        dt
        fnyq
        miniHeader
    end
       
    methods (Abstract)
        []   = train(obj, knav);
        X    = generate2DSignalMatrix(obj, knav);
    end
    
    methods
        
        function obj = MotionModel(twix_obj)
            obj.miniHeader = createMiniHeaderFromTwix(twix_obj);
            obj.dt         = mean(diff(twix_obj.time_milliseconds*1e-3));
        end
        
        function val = get.fnyq(obj)
           % Nyquist frequency
           val = 0.5/obj.dt;            
        end
        
        function filterParams = get.filterParams(obj)
           N  = obj.filterOrder;
           Wn = obj.fcutHi/obj.fnyq;
           [b,a] = obj.filterFun(N, Wn, 'high');
           filterParams = {b, a};
        end
        
        % hipass filter signal matrix
        function X = filterHi(obj, X)
            for i = 1:1:size(X,1)
                X(i,:) = filtfilt(obj.filterParams{:}, X(i,:));
            end
        end
                
        function [Results, figPCA, figSOBI, figFinal] = estimateMotionParameters(obj, kdata)
            
            X = obj.generate2DSignalMatrix(kdata);
            
            Results.pcasig  = obj.Tpca*X;
            Results.sobisig = obj.T*X;
            resp0 = obj.tResp*X;
            card0 = obj.tCard*X;
            
            figPCA    = plotSignals(obj.dt, Results.pcasig);
            
            highlight = [obj.respInd, obj.cardInd];
            figSOBI = plotSignals(obj.dt, Results.sobisig, highlight);
            xlim([30,90]);
            
            if obj.fnyq > 5
                [b,a] = butter(5, 5/obj.fnyq, 'low');                   
                resp = filtfilt(b,a,resp0);
                card = filtfilt(b,a,card0);
            else
                resp = resp0;
                card = card0;
            end
            
            Results.resp = resp;
            Results.card = card;
            
            figFinal = figure;
            tt = ((1:length(resp))-1)*obj.dt;
            Results.time_seconds = tt;
            ax1 = subplot(211);
            overlayplot(tt, resp0, resp, 'colour', 'r','alpha', 0.25);
            title('Respiratory Component, Filtered');
            ax2 = subplot(212);
            overlayplot(tt, card0, card, 'colour', 'b','alpha', 0.25);
            title('Cardiac Component, Filtered');
            linkaxes([ax1,ax2],'x');
            xlim([30,60]);
        end
    end
    
    
end

