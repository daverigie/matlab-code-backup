function [y, cost] = sparsefourierdenoise(y0, lambda, nIter)

    if nargin < 3
        nIter = 20;
    end
    
    if nargin < 2
        lambda = 0.01;
    end

    y_original = y0;

    D = @(x) fft(x);
    DT = @(x) numel(x)*ifft(x);

    y0 = y0(:);
    
    y0 = (y0-mean(y0))/std(y0);
   
    y = y0;
    
    
    for i = 1:1:nIter
       
        fprintf('\nIter: %i', i);
        A = @(x, transp_flag) afun(x, y, lambda, D, DT);        
        z = lsqr(A, D(y0), 1.0e-6, 10);
        
        y = y0 - DT(z);
        
        cost(i) = 0.5*norm(y-y0).^2 + lambda*sum(abs(D(y)));
        
    end
    
    y = rescaleSignal2Match(y, y_original);

end

function y = afun(x, yk, lambda, D, DT)
    y = 1.0/lambda*abs(D(yk)).*x + D(DT(x));
end



