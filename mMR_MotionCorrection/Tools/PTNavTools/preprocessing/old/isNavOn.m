function flag = isNavOn(knav)
% This is just a simple function to verify if we have the rosette or
% gradients off as a sanity check since we have our sequence flag messed
% up.

% when the gradients are off, the frequency spectrum has a very sharp peak
% at the center frequency. When the gradients are on the frequency
% distribution is more spread out.

knav_freq = abs(fftshift(fft(knav(:,1,1,1)),1));
knav_freq_sorted = sort(knav_freq,'descend');

if (knav_freq_sorted(2)/knav_freq_sorted(1)) < 0.8
    flag = false;
else
    flag = true;
end


end