function [kdata_unshifted, dk] = undoFOVshift2(kdata, freq_factors, dwellTime)

    % dwellTime should be specified in nanoseconds (as in header).

    FIRST_MEAS = 122; % there appear to be 120 steady state measurements and then the MDH info is interleaved [nav, img, nav img, ... ]

    dwellTime = dwellTime*1e-9;
    % freq_factors is read directly from twixobj.image.freeParam(4,:)
    % No scaling is performed at all

    [n_read, n_part, n_views, n_coils] = size(kdata);
    
    angles = goldenAngles(0, n_views); % compute view angles
    
    kdata_unshifted = zeros(size(kdata));
        
    dHz = double(freq_factors(FIRST_MEAS:2:end));
    dHz = dHz(1:n_part:end) - 2^15; % The frequencies are encoded as unsigned integers by offsetting them by 2^15
    
    Hz_per_sample = (1.0/(n_read*dwellTime));
    dk = dHz/Hz_per_sample;
    
    dk = reshape(dk, 1, 1, n_views, 1);
     
    center = (n_read/2 + 1);
    for f=1:1:n_read
        phase_factor(f,1,:) = exp(j*2*pi*dk*(f-center)/n_read);
    end
    
    kdata_unshifted = bsxfun(@times, kdata, phase_factor);
        
end

