function [freq, idx] = findPTfreqAuto(kdata)
% Analyze the k-data to estimate the frequency of the pilot tone
% This should be done AFTER the FOV shift correction
%{
    This works by taking the derivative of the FFT-magnitude along the
    the read direction. The PT shows up as a large spike, while the part of
    the data from the patient varies more gradually. The location of the 
    PT is determined by finding the location of the biggest spike.
%}


    if ndims(kdata) > 3
        warning('Expected 3 dimensional array. Calling legacy version findPTfreqAutoOld');
        [freq, idx] = findPTfreqAutoOld(kdata);
        return;
    end

    if nargin < 2
        allowedRange = [-1,1];
    end
    
    N = size(kdata,1);
    
    fs = 2.0; % Using sampling frequency of 2 yields a result that is normalized according to MATLAB convention
    df = fs/N;
    
    if mod(N,1)
        fmin = -(fs-df)/2.0;
        fmax = (fs-df)/2.0;
    else
        fmin = -(fs-2*df)/2;
        fmax = (fs-df)/2;
    end
    
    ff = linspace(fmin, fmax, N);
    
    kdata_freq_mag = abs(fftshift(fft(kdata),1));
    
    
    n_coils = size(kdata_freq_mag, 2);
    
    freq = [];
    
    for i=1:1:n_coils
        [dx, dy] = gradient(squeeze(kdata_freq_mag(:,i,:)));
        y = sum(dy,2);
        [~, ind_min] = min(y);
        [~, ind_max] = max(y);
        freq_min = ff(ind_min);
        freq_max = ff(ind_max);
        freq_coil = 0.5*(freq_min+freq_max);
        freq = [freq;freq_coil];
        fprintf('\nCoil %i: Normalized Freq = %.10f +/- %f',i, freq_coil, df/2);
    end
           
    if numel(unique(freq)) > 1
        warning('More than one possible frequency found, using %f', mode(freq));
    end
    
    freq = mode(freq);
    idx  = findclosest(ff, freq);
    
    fprintf('\n\nMultiply by half the sampling frequency to get actual Freq\n\n');

end


function idx = findclosest(arr, val)

    [~,idx] = min(abs(arr-val));

end
