function [pcasig, icasig, resp, card, ... 
          fig_pca, fig_ica, fig_final] ... 
          = extractGatingSignals(knav, dt, varargin)


%//////////////////////////////////////////////////////////////////////////
%// ------------------ PARSE INPUT OPTIONS ------------------------------//
%//////////////////////////////////////////////////////////////////////////
    p = inputParser;
    
    p.addRequired('knav', @checkNavDims);
    p.addRequired('dt', @isscalar);
    p.addParameter('FFT', true, @islogical);
    p.addParameter('Sum', false, @islogical);
    p.addParameter('Slice', false, @islogical);
    p.addParameter('Resample', 0, @isint);
    p.addParameter('HighpassFilter', true, @islogical);
    p.addParameter('NormalizeCoils', true, @islogical);
    p.addParameter('PCAAlgorithm', 'rsvd', @ischar);
    p.addParameter('EigCutoff', 0.01, @isscalar);
    p.addParameter('RespFreq', [0.1, 0.7], @checkFreqBand);
    p.addParameter('CardFreq', [0.8, 1.7], @checkFreqBand);
    p.addParameter('fasticaParams', ...
                   {'epsilon', 1e-12, 'stabilization', 'on', 'g', 'tanh','approach','symm'},...
                   @iscell);
    p.addParameter('LowpassFilterGatingSignals', false, @islogical);
    
    complexOptions = {'concatenate', 'magnitude', 'donothing'};
    p.addParameter('HandleComplex', 'concatenate', ... 
                   @(x) any(validatestring(x,complexOptions)));
    
    parse(p, knav, dt, varargin{:});
    
    knav = p.Results.knav;
    dt = p.Results.dt;
    
    if p.Results.Resample
        resampleInterval = p.Results.Resample;
    end
    
    if p.Results.Sum && p.Results.Slice
        error('Only one of Sum/Slice can be true');
    end
    
    switch upper(p.Results.PCAAlgorithm)
        
        case 'RSVD'
            pcafun = @pcaSignals_rsvd;
        case 'EIG'
            pcafun = @pcaSignals;
        otherwise
            error('Invalid PCA Algorithm. Use rsvd or eig.');
    end
  
%//////////////////////////////////////////////////////////////////////////
%// ------------------ PREPROCESS DATA --- ------------------------------//
%//////////////////////////////////////////////////////////////////////////
    
    [n_read, n_par, n_views, n_coils] = size(knav);
    data = double(knav);
    clear knav;

    

    NAqc = n_par*n_views;
    scanTime = dt*(NAqc-1);
    tt = (0:1:(NAqc-1))*dt;
    ff = getfreqaxis(tt);

    %% Normalize coils
   
    if p.Results.NormalizeCoils
        mytic('Normalizing coils');
        for i = 1:1:n_coils
            temp = data(:,:,:,i);
            data(:,:,:,i) = data(:,:,:,i)/std(temp(:));
        end
        mytoc;
    end

    %% Create Data Matrix

    mytic('Preprocessing Data to form 2D Signal Matrix');
    
    X = permute(data, [1,4,2,3]);
    clear data;
    
    if p.Results.FFT
        X = fft(X);
    end
    
    if p.Results.Sum
        X = sum(X,1);
    elseif p.Results.Slice
        disp('slicing data');
        [~, inds] = max(sum(X,4), [], 1);
        slice_ind = mode(inds(:))
        X = X(max(1,slice_ind-5):min(slice_ind+5,end), :, :, :);
        X = sum(X,1);
    end
    
    X = reshape(X,[],NAqc);
    
    f_nyq = 0.5/dt;
    
    if ~isreal(X)
        switch p.Results.HandleComplex
            case 'concatenate'
                X = [real(X);imag(X)];
            case 'magnitude'
                X = abs(X);
            case 'donothing'
                X = X;
        end
    end

    if p.Results.HighpassFilter
        [b,a] = butter(5, 0.1/f_nyq, 'high');
        X = filtfilt(b,a,X.').';
    end
    
    
    
    if p.Results.Resample
        X = resample(X.', 1, resampleInterval).';
        tt = linspace(0,scanTime, size(X,2));
    end

    
    
    mytoc;

%//////////////////////////////////////////////////////////////////////////
%// -------------------------- PERFORM PCA ------------------------------//
%//////////////////////////////////////////////////////////////////////////
    mytic('Performing PCA');
    pcasig = pcafun(X, 'lastEig', p.Results.EigCutoff);
        
    [~, resp_ind, ~] = freqRankSignals(pcasig, dt, p.Results.RespFreq);
    [~, card_ind, ~] = freqRankSignals(pcasig, dt, p.Results.CardFreq);
    
    fig_pca = plotSignals(tt, real(pcasig), [resp_ind(1),card_ind(1)]);
    
    xlim([0,60]);
    drawnow;
    set(fig_pca, 'Name', 'LARGEST PRINCIPAL COMPONENTS (PCA ONLY)');
    mytoc;
            

%//////////////////////////////////////////////////////////////////////////
%// -------------------------- PERFORM ICA ------------------------------//
%//////////////////////////////////////////////////////////////////////////
    mytic('Performing ICA on PCA reduced signal matrix');
    
        
    [icasig] = fastica(real(pcasig), p.Results.fasticaParams{:});
    
    size(real(pcasig))
    size(icasig)
    
    [~, resp_ind, ~] = freqRankSignals(icasig, dt, p.Results.RespFreq);
    [~, card_ind, ~] = freqRankSignals(icasig, dt, p.Results.CardFreq);
    
    fig_ica = plotSignals(tt, real(icasig), [resp_ind(1),card_ind(1)]);
    xlim([0,60]);
    drawnow;
    set(fig_ica, 'Name', 'DISCOVERED INDEPENDENT COMPONENTS (ICA)');
    
    resp = icasig(resp_ind(1), :);
    card = icasig(card_ind(1), :);
    mytoc;

%//////////////////////////////////////////////////////////////////////////
%// ------------ LOW PASS FILTER RESULTING GATING SIGNALS -------------- //
%//////////////////////////////////////////////////////////////////////////
if p.Results.LowpassFilterGatingSignals
    mytic('Lowpass filtering resp/card gating signals');
    
    [b,a] = butter(5, 1.0/f_nyq, 'low');
    resp_lp = filtfilt(b,a,resp.').';
    
    [b,a] = butter(5, 2.0/f_nyq, 'low');
    card_lp = filtfilt(b,a,card.').';
    
    mytoc;
else
    resp_lp = resp;
    card_lp = card;
end

%//////////////////////////////////////////////////////////////////////////
%// -------------------------- FINAL PLOT -------------------------------//
%//////////////////////////////////////////////////////////////////////////

fig_final = figure('Position',[500,500,1500,300]);

subplot(211);

p_resp = plot(tt, resp); hold on;
set(p_resp,'Color', [1,0.75,0.75]);

p_resp_lp = plot(tt, resp_lp); 
set(p_resp_lp,'Color', [1,0,0]);

title('Respiratory Gating Signal');
xlim([100,160]);
xlabel('time (s)');

subplot(212);

p_card = plot(tt, card); hold on;
set(p_card,'Color', [0.75,0.75,1]);

p_card_lp = plot(tt, card_lp); 
set(p_card_lp,'Color', [0,0,1]);

title('Cardiac Gating Signal');
xlim([100,130]);
xlabel('time (s)');

drawnow;
resp = resp_lp;
card = card_lp;

end

%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%XXXXXXXXX ------------------ HELPER FUNCTIONS -------------------XXXXXXXXX
%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
function TF = checkNavDims(knav)

    if ~isnumeric(knav);
        error('knav is not numeric');
    end
    
    if ndims(knav) < 3
        error('Expected 4D array');
    end
    
    TF = true;
    
end

function TF = isint(x)
    TF = floor(x)==ceil(x);
end

function TF = checkFreqBand(freqBand)

    if ~all(size(freqBand) == [1,2])
        error('Frequency Band must be specifed as [flow,fhigh]');
    end
    
    if (freqBand(1) < 0) || (freqBand(2) < freqBand(1))
        error('Invalid Frequency Range');
    end

end

function tstart =  mytic(msg)

    tic;
    fprintf('\n%s ...\n',msg);
   
end

function T = mytoc(tstart)

    T = toc;
    fprintf('\nDone in %f seconds.\n', T);
    

end
