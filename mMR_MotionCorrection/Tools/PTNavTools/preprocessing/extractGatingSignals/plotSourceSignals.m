function [fig_sources] = plotSourceSignals(sourcesig, dt, varargin)

%//////////////////////////////////////////////////////////////////////////
%// ------------------ PARSE INPUT OPTIONS ------------------------------//
%//////////////////////////////////////////////////////////////////////////
    p = inputParser;
    p.addRequired('sourcesig');
    p.addRequired('dt', @isscalar);
    p.addParameter('RespFreq', [0.1, 0.7], @checkFreqBand);
    p.addParameter('CardFreq', [0.8, 1.7], @checkFreqBand);
    p.addParameter('LowpassFilterGatingSignals', true, @islogical);
       
    parse(p, sourcesig, dt, varargin{:});

%__________________________________________________________________________

        
    [~, resp_ind, ~] = freqRankSignals(sourcesig, dt, p.Results.RespFreq);
    [~, card_ind, ~] = freqRankSignals(sourcesig, dt, p.Results.CardFreq);
    
    fig_sources = plotSignals(dt, real(sourcesig), [resp_ind(1),card_ind(1)]);
    
    xlim([30,90]);
    drawnow;
    set(fig_sources, 'Name', 'Recovered Source Signals');

end

