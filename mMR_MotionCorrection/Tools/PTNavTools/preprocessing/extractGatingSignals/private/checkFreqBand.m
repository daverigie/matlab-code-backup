function TF = checkFreqBand(freqBand)

    if ~all(size(freqBand) == [1,2])
        error('Frequency Band must be specifed as [flow,fhigh]');
    end
    
    if (freqBand(1) < 0) || (freqBand(2) < freqBand(1))
        error('Invalid Frequency Range');
    end

end

