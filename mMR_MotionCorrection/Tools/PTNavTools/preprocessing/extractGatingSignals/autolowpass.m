function yf = autolowpass(y)
	
	fcut = min(0.99, 4*findfundamentalfreq(y));
	[b,a] = butter(5, fcut, 'low');
	yf = filtfilt(b,a,y);

end