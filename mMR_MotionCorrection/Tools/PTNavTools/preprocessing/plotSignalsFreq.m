function [fig, axis_list] = plotSignalsFreq(dt, S, varargin)

    tt = ((1:length(S))-1)*dt;
    ff = getfreqaxis(tt);
    
    
    Sf = fftshift(fft(S.'),1).';
    
    plotSignals(ff, real(Sf), varargin{:});
    set(gcf,'Name','Real');
    
    plotSignals(ff, imag(Sf), varargin{:});
    set(gcf,'Name','Imag');
    
    plotSignals(ff, abs(Sf), varargin{:});
    set(gcf,'Name','Magnitude');

end