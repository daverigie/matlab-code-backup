function [eigval,eigvec] = poweriteration(A, x0, nIter, verbose)
% Computes the spectral norm of operator A using power iteration.
% A and AT should be specified as function handles.

    if nargin < 3
        warning('Defaulting to 10 iterations');
        nIter = 10;
    end

    if nargin < 4
       verbose = false; 
    end
    
    x = randlike(x0)*2-1;
    x0;
    
    for i = 1:1:nIter
        x = A(x);
        x = x./norm(x(:));
        v = A(x);
        s = norm(v(:));
        if verbose
            fprintf('\nIter %i, lambda = %f',i,s);
            pause(0.0001);
        end
    end
    
    eigval = s;
    eigvec = x;
    
end



function x = randlike(x)

    if isreal(x)
        x = rand(size(x));
    else
        x = rand(size(x)) + i*rand(size(x));
    end

end
