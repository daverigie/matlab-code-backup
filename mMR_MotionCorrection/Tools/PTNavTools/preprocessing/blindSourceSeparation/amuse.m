function [S,W] = amuse(X, k)
%AMUSE blind source separation based on diagonalizing time-delayed covariance.
%   Mixed signals are arranged in 2D matrix X so that:
%   size(X) = [nMixedSignals, nTimeSamples]
%
%   Output S gives the unmixed sources, and W is the mixing matrix, such
%   that S = W*X. This only works if X is already standardized, so that
%   each row has unit variance and zero-mean. It is recommended that this
%   is done prior to calling AMUSE.
    
    [Xw, Q] = makewhite(X); % prewhiten data
    
    % Create symmetrized, time-delayed covariance matrix
    N = size(X,2);
    Ctau = Xw(:,1:(N-k))*Xw(:,(k+1):end)';
    Ctau = 0.5*(Ctau + Ctau'); % symmetrize;

    % Diagonalize time-delayed covariance matrix
    [V,D] = eig(Ctau);
    d = diag(D);
    [d, idx] = sort(d, 'descend');
    V = V(:,idx);

    % Output
    W = V'*Q; 
    S = W*X;

end



