function [] = compressKdata(kdata, numComponents, outpath)

    if ischar(kdata)
       [~,~,ext] = fileparts(kdata);
       if strcmpi(ext, '.DAT')
           twix_obj = getkdata(kdata);
       else
           twix_obj = load(kdata);
       end
       kdata = twix_obj.data;
    end

    if nargin < 2
       fprintf('\r\nUsing default %i components...', numComponents);
       numComponents = 10; 
    end
    
    if nargin < 3 && ~ischar(kdata)
        [filename, dirname] = uiputfile('*.*', 'Select location to save MAT file');
        outpath = fullfile(dirname,filename);
    end
    
    [kdata] = compressKdata_helper(kdata, numComponents)
    
    twix_obj.data = kdata;
    
    fprintf('\r\nSaving compressed kdata... '); 
    
    tic;
    save(outpath, '-struct', 'twix_obj');
    toc;

end