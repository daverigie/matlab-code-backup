function [fig, axis_list] = plotSignalsPeriodogram(dt, S, varargin)

    row = @(x) x(:)'

    Sf = []
    Fs = 1.0/dt;
    for i = 1:1:size(S,1)
       [P,f] = periodogram(S(i,:),[],[],Fs,'power');
       Sf = cat(1,Sf,row(P));
    end
    
    [fig, axis_list] = plotSignals(f, Sf, varargin{:});

end