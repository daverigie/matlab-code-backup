function [] = createShortenedKnav(knavpath, duration, outpath)

    if strcmp(outpath, knavpath)
       warning('Input path cannot be same as output path');
       [dirname, filename, ext] = fileparts(outpath);
       outpath = fullfile(dirname, sprintf([filename '_%isec' ext], duration));
    end

    MF = load(knavpath);
    hdr = MF.mini_header;
    knav = MF.knav;
    
    TR = hdr.TR*1e-6;
    [nFE, nPar, nView, nCoil] = size(knav);
    
    % Only allow duration to be a multiple of (TR*nPar)
    dt = TR*nPar;
    lastInd = ceil(duration/dt);
    knav_part = knav(:,:,1:lastInd,:);
    
    MF.knav = knav_part;
    
    save(outpath, '-struct', 'MF');

end