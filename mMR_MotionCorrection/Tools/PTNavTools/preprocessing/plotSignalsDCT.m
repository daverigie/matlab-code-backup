function [fig, axis_list] = plotSignalsDCT(dt, S, varargin)

    tt = ((1:length(S))-1)*dt;
    df = 1.0/max(tt);
    ff = (1:length(S))*df - df;
    
    
    
    Sf = dct(S.').';
    
    plotSignals(ff, Sf, varargin{:});


end