function y = div1d(x)


        N = numel(x);

        y    =  x([1,1:(N-1)]) - x(1:N); 
        y(1)    = -x(1);
        y(N)    = x(N-1);
        
        y = -y;

end