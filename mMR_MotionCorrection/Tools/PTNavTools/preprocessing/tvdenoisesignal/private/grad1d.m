function y = grad1d(x)

    N = numel(x);

    % Perform finite differencing
    y = x([2:N,N]) - x(1:N);

end