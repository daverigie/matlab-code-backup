function varargout = matchCoilConfig(varargin)
    % matchCoilConfig(hdr1, hdr2, hdr3, hdr4 ...)
    % returns indices [I1, I2, I3, I4 ...]
    % of the coil channels that are common to
    % all data, where hdr is from twix_map_obj.hdr

    % First convert hdr input to cell arrays of string identifiers for
    % each coil
    
    
    
    for iHdr = 1:1:nargin
        coilInfo{iHdr} = getCoilInfo(varargin{iHdr});
        clear S;
        for iCoil = 1:1:numel(coilInfo{iHdr})
            C = coilInfo{iHdr}(iCoil);
            S{iCoil} = strcat(C.tCoilID, C.tElement, num2str(C.lCoilCopy));
        end
        varargin{iHdr} = S;
    end
    
    % If only one scan is supplied, then keep all coils
    if nargin == 1
       varargout{1} = 1:numel(coilInfo{1});
       return
    end
    
    [varargout{1:nargout}] = findArrayIntersection(varargin{:}); 
    
    % Display coil channels common to all scans
    fprintf('\n\nCOMMON COIL CHANNELS:\n')
    for i = varargout{nargout}
        C = coilInfo{end}(i);
        fprintf('\n[CoilID] %20s [Element] %6s [lCoilCopy] %3s', ...
                C.tCoilID, C.tElement, num2str(C.lCoilCopy));
    end
    
    fprintf('\n\nDISCARDED COIL CHANNELS');
    
    % Display discarded coil channels for each scan
    for iScan = 1:1:nargin
        fprintf('\n\nScan %i', iScan);
        for iChannel = 1:1:numel(varargin{iScan})
           idx = find(varargout{iScan} == iChannel);
           if isempty(idx)
               C = coilInfo{iScan}(iChannel);
               fprintf('\n[CoilID] %20s [Element] %6s [lCoilCopy] %3s', ...
                        C.tCoilID, C.tElement, num2str(C.lCoilCopy)); 
           end
        end
    end
    
    fprintf('\n\n\n');
    
end

function varargout = findArrayIntersection(varargin)
      % findArrayIntersection(arr1, arr2, arr3, arr4 ... )
      % finds the indices [I1, I2, I3, I4 ...] of the elements that
      % are in all of the arrays/
      
      % First find total intersection
      C = findTotalIntersection(varargin{:});
      
      % Now iterate through sets to find the indices in each array
      for i = 1:1:nargin
          A = varargin{i};
          B = C;
          [~, IA] = intersect(A,B);
          varargout{i} = IA'; 
      end
      
      % Sort indices according to first array
      [I1, idx] = sort(varargout{1});
      for i = 1:1:nargin
         varargout{i} = varargout{i}(idx); 
      end
end


function C = findTotalIntersection(varargin)
    % Finds elements C that are in all sets
    
    if nargin == 2
        C = intersect(varargin{1}, varargin{2});
    else
        C = intersect(varargin{end}, findTotalIntersection(varargin{1:(end-1)}));
    end
       
end




