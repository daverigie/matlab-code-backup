function [kdata_unshifted] = undoFOVshift(kdata, offset, normalVector, ...
                                          osfactor, fov_read, constShift, angles)
    
    [n_read,n_par,n_view,n_coil] = size(kdata);
    
    if nargin < 6
        constShift = 0;
    end
                                                  
    if nargin < 7
       fprintf('\nAssuming golden angle...\n');
       angles = goldenAngles(0, n_view);
    end
        
    % convert millimeter offset to (x,y,z) pixel unit shifts
    [x0,y0,z0]   = offset2xyz(offset, normalVector);
    pixels_per_mm = n_read/(osfactor*fov_read); 
    xp = x0*pixels_per_mm;
    yp = y0*pixels_per_mm;
        
    kdata_unshifted = undoFOVshift_helper(kdata, angles, xp, yp, constShift);  
        
end




