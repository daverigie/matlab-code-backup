function image_plane = determineImagingPlane(normalVector)

    n = [normalVector.dSag, normalVector.dCor, normalVector.dTra];
    
    if all(n == [1,0,0])
        image_plane = 'sagittal';
    elseif all(n == [0,1,0])
        image_plane = 'coronal';
    elseif all(n == [0,0,1])
        image_plane = 'axial';
    else
        disp('Error, unrecognized imaging configuration');
        image_plane = -1
        
    end
                

end