function [kdata_unshifted] = undoFOVshift_helper(kdata, angles, xp, yp, constShift)

    [n_read,n_par,n_view,n_coil] = size(kdata);

    f = (1:1:n_read)';
    f0 = (n_read/2 + 1);
    angles = reshape(angles, 1, 1, n_view, 1);
    dk = xp*cosd(angles) + yp*sind(angles) + constShift; % Shift in mm    
    
    phase_factor = exp( ...
                        bsxfun( @times, ...
                                j*2*pi().*dk, ...
                               (f-f0)/n_read ) ...
                      );
    
    kdata_unshifted = bsxfun(@times, kdata, phase_factor);
        
end




