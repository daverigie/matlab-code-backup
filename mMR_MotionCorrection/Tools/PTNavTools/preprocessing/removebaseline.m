function [y3,y1,y2] = removebaseline(y0, varargin)

    % Ignore nan entries
    if any(isnan(y0))
        idx   = isnan(y0);
        [y3,y1,y2] = removebaseline(y0(~idx), varargin{:});
        y3(idx) = nan;
        y1(idx) = nan;
        y2(idx) = nan;
        return;
    end

    if nargin < 2
       yf = abs(fft(y0));
       [yfs,idx] = sort(yf,'descend');
       
       if idx(1) == 1
           maxInd = idx(2);
       else
           maxInd = idx(1);
       end
       
       T = numel(y0)/maxInd %fundamental period;
       
       varargin{1} = floor(0.75*T)
               
    end
        
    if nargin < 3
        varargin{2} = false;
    end
    
    if nargin < 4
       varargin{3} = 'peaks'; 
    end
    
    N = varargin{1};
    makePlots = true;
    
    [yupper, ylower] = envelope(y0, N, varargin{3:end});

    %y = y0-0.5*(yupper+ylower);
    
    y1 = y0./abs(yupper);
    y2 = y0./(abs(ylower));
    y3 = (y0 - 0.5*(yupper+ylower))./abs(yupper-ylower);
    
    % Filter out points that are way out of range
    [sorted_data, I] = sort(abs(y3));
    L = length(y3);
    middle_data = y3(I(floor(L/10):floor(9*L/10)));
    mu    = mean(middle_data);
    sigma = std(middle_data)
    y3(abs(y3) > 2*sigma) = nan;
        
    % handle endpoint problems
    
    iLeft  = floor(numel(y0)*0.01);
    iRight = numel(y0)-iLeft;
      
    y1([1:iLeft, iRight:end]) = nan;
    y2([1:iLeft, iRight:end]) = nan;
    y3([1:iLeft, iRight:end]) = nan;
    
    close all;
    
    
    
    

    % Display results
    
    if makePlots
    
        figure;
        ax1 = subplot(411);
        plot(y0); hold on;
        plot(yupper);
        plot(ylower);
        title('Envelope Detection');

        ax2 = subplot(412);

        plot(y1);
        title('Corrected Signal 1');   

        ax3 = subplot(413);

        plot(y2);
        title('Corrected Signal 2');   

        ax4 = subplot(414);

        plot(y3);
        title('Corrected Signal 3');   
        linkaxes([ax1,ax2,ax3,ax4],'x');
        
    end
       

end

