function [signals_ranked, ind, EE] = freqRankSignals(signals, dt, Wn)

    if nargin == 3

        [signals_ranked, ind, EE] = rankSignals(signals, ...
                                                @energyFractionInFreqWin, ...
                                                dt, Wn);
    else
       
        metricFun = @(varargin) -1*findfundamentalfreq(varargin{:});
        [signals_ranked, ind, EE] = rankSignals(signals, ...
                                                metricFun, ...
                                                dt);
        
    end

end

