function [varargout] = filterPilotTone(varargin)

    % filterPilotTone(twixobj, dataType, fcut)

    if nargin > 3
        [varargout{1:nargout}] = filterPilotTone_base(varargin{:});
        return;
    end
    
    twixobj  = varargin{1};
    dataType = varargin{2};
    fcut     = varargin{3};
    
    hdr = createMiniHeaderFromTwix(twixobj);
    
    offset       = hdr.offset;
    normalVector = hdr.normalVector;
    osfactor     = hdr.osfactor;
    fov_read     = hdr.fov.read;
    
    [varargout{1:nargout}] = filterPilotTone_base(twixobj.data, offset, normalVector, ...
                                                  osfactor, fov_read, dataType, ...
                                                  fcut);
    

end

function [kdata_clean, pt_signal, fcut] = filterPilotTone_base(kdata, offset, normalVector, ...
                                                    osfactor, fov_read, ...
                                                    dataType, fcut)
    
                                            
    disp('Converting kdata to double');
    varclass = class(kdata);
    kdata    = double(kdata);
   
    % Undo phase shifts that mess up pilot tone                                                    
    
    if strcmpi(dataType,'kdata')
        fprintf('\n\rCompensating for FOV shift correction\n');
        kdata = undoFOVshift(kdata, offset, normalVector, osfactor, fov_read);
    end
    
    if ischar(fcut)
        method = fcut;
        switch upper(method)
            case 'AUTO'
                [ptfreq, idx] = findPTfreq(kdata, 'AUTO');
                if abs(ptfreq) < 0.25
                    warning('could not find pilot tone');
                    ptfreq = 1.0;
                end
                fcut = (abs(ptfreq)) - (40/size(kdata,1));
            case 'GUI'
                fcut = findPTfreq(kdata, 'GUI')
        end
    end
    
    kdata_clean = 0.0*kdata;
    pt_signal = 0.0*kdata_clean;

    % construct filters   
    %Dwell time for navigator is 2.5us
    % Center freq is (N/2+1)
    
    fcut
    [blo,alo] = butter(5, fcut, 'low');
    [bhi,ahi] = butter(5, fcut, 'high');
    

    disp('Filtering out PT signal from k-space data ... ');
    tic;
        
    %kdata_clean = filtfilt(blo,alo,kdata);
    %pt_signal = filtfilt(bhi,ahi,kdata);
    kdata_clean = bandpassfilter(kdata, 0, fcut);
    pt_signal   = bandpassfilter(kdata, fcut, 1.0);
    
    if strcmpi(dataType,'kdata')
    
        fprintf('\n\rRestoring FOV shift correction\n');
        % reverse offset and redo phase shifts to recover original data
        offset_rev.dCor = -offset.dCor;
        offset_rev.dSag = -offset.dSag;
        offset_rev.dTra = -offset.dTra;

        kdata_clean = undoFOVshift(kdata_clean, offset_rev, normalVector, osfactor, fov_read);
    end

    kdata_clean = cast(kdata_clean, varclass);
    pt_signal   = cast(pt_signal,   varclass);
    
    toc;


end