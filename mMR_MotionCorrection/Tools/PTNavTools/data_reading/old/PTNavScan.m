classdef PTNavScan
    %UNTITLED Summary of PTNavScan class goes here
    %   Detailed explanation goes here
    
    properties
        basename
        twix_path
        
        kdata
        kdata_clean
        kdata_pt
        
        knav
        knav_compressed
        knav_clean
        knav_clean_compressed
        knav_pt
        knav_compact
        
        header
        mini_header  
    end
    
    methods
        
        function obj = PTNavScan(twix_path, loadEverything)
            
            if nargin < 2
                loadEverything = false;
            end
                       
            obj.twix_path = twix_path;
            
            [dirpath, basename, ext] = fileparts(twix_path);
            obj.basename = basename;
            
            if loadEverything
                loadfun = @obj.load;
            else
                loadfun = @obj.lazyload;
            end
        
            for fieldName = {'kdata', 'kdata_clean', 'kdata_pt', ...
                             'knav', 'knav_compressed', 'knav_clean', ...
                             'knav_clean', 'knav_clean_compressed', ...
                             'knav_pt', 'knav_compact'}
               
                         f          = fieldName{:};
                         path2field = obj.makePath(twix_path, f, '.mat');
                         obj.(f)    = loadfun(path2field);
                         
            end
            
            obj.mini_header             = obj.load(obj.makePath(twix_path, 'mini_header', '.mat'));
            obj.header                  = obj.fileread(obj.makePath(twix_path, 'header','.txt'));  
        end
    
    end
    
    methods (Static)
        
        function MF = lazyload(matpath)
            % map the data files for easy access but don't load now (uses
            % matfile function)

            if exist(matpath, 'file')
                MF = matfile(matpath, 'Writable', false);                          
            else
                MF = [];
            end

        end
        
        function data = load(matpath)
            % Loads matfiles into memory 
            
            if exist(matpath, 'file')
                data = load(matpath);                          
            else
                data = [];
            end

        end
        
        function s = fileread(textpath)
            % For reading text files
            
            if exist(textpath, 'file')
                s = fileread(textpath);                          
            else
                s = [];
            end

        end

        function newpath = makePath(twix_path, datatype, ext)

            [dirpath, basename, ~] = fileparts(twix_path);

            newpath = fullfile(strrep(dirpath, 'scanner', 'preprocessed'), ...
                               [basename '_' datatype ext]);

        end
        
    end
    
   
    
end




