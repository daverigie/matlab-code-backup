function [kdata, knav, mini_header] = extractKdataNav(varargin)
% EXTRACTKDATANAV
% extractKdataNav(twixpath, outdir)

    %======================================================================
    %                               Parse Inputs                   
    %----------------------------------------------------------------------

    p = inputParser();
    p.addOptional('twixpath', [], @(x) true);
    p.addOptional('outdir',   [], @(x) true); 
    p.addParameter('OneCoilPerFile', false, @islogical);
    
    p.parse(varargin{:});
    
    twixpath = p.Results.twixpath;
    outdir   = p.Results.outdir;
    
    if isempty(twixpath); twixpath = uigetpath(); end;
    if isempty(outdir);   outdir   = uigetdir();  end;

    % If twixpath is directory, process all .DAT files
    if isdir(twixpath)
        dirpath = twixpath;
        listing = dir(fullfile(dirpath, '*.dat'));
        for i=1:1:numel(listing)
            args = p.Results;
            args.twixpath = fullfile(dirpath, listing(i).name);
            [~,~,~] = extractKdataNav(fullfile(dirpath, listing(i).name), ... 
                                      outdir, ...
                                      'OneCoilPerFile', p.Results.OneCoilPerFile);
        end
        return;
    end

    % Create directory if it doesn't exist
    [success, message, messageid] = mkdir(outdir)
    %______________________________________________________________________
    
    %======================================================================
    %                   get useful params from header                      
    %----------------------------------------------------------------------

    mytic('Extracting header info');

    headerString = getTwixHeaderString(twixpath);
    
    fid = fopen(makeFilepath(twixpath, outdir, 'header', '.txt'), 'w+');
    fprintf(fid,'%s', headerString);
    fclose(fid);
    
    mini_header_path = makeFilepath(twixpath, outdir, 'mini_header', '.txt');
    mini_header = createMiniHeader(twixpath, mini_header_path);

    mytoc;
    %______________________________________________________________________%
    
    %======================================================================
    %                   Extract files and save                    
    %----------------------------------------------------------------------    
    [knav, hdr]  = getknav(twixpath);
    saveItems.headerString = headerString;
    saveItems.mini_header  = mini_header;
    saveItems.hdr          = hdr;
    
    knavpath     = makeFilepath(twixpath, outdir, 'knav', '.mat');    
    if p.Results.OneCoilPerFile
        savemulticoil(knavpath, knav, saveItems);
    else
        save(knavpath, 'knav', 'headerString', 'mini_header', 'hdr', '-v7.3');
    end
    
    [kdata, hdr] = getkdata(twixpath);
    kdatapath    = makeFilepath(twixpath, outdir, 'kdata' ,'.mat');
    if p.Results.OneCoilPerFile
        savemulticoil(kdatapath, kdata, saveItems);
    else
        save(kdatapath, 'kdata', 'headerString', 'mini_header', 'hdr', '-v7.3');
    end
    %______________________________________________________________________%
    
end

function [] = savemulticoil(filename, kdata, saveItems)

    [dirname,filename,ext] = fileparts(filename);
    newdir = fullfile(dirname,filename)
    mkdir(newdir);
    
    % Save each coil to separate file in a new directory
    nCoil = size(kdata,4);
    for iCoil = 1:1:nCoil
       p = fullfile(newdir, sprintf('coil%i.mat', iCoil));
       data = kdata(:,:,:,iCoil);
       save(p, 'data');
       save(p, '-struct', 'saveItems', '-append');
    end

end

function newpath = makeFilepath(twixpath, outdir, suffix, extension)

    [pathname, filename, ext] = fileparts(twixpath);
    newpath = fullfile(outdir, [filename '_' suffix extension]);

end

function tstart = mytic(msg)
    disp(sprintf('%s ...', msg));
    tic;
end

function tstop = mytoc
    disp(sprintf('Done in %f seconds.', toc));
end

