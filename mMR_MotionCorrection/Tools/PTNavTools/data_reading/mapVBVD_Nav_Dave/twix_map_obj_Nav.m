classdef twix_map_obj_Nav < twix_map_obj 
    %{ 
        Added support for handling different Eco readout lengths by
        subclassing twix_map_obj. This solution is much safer and less
        confusing than simply modifying the original twix_map_obj
    %}
    
    properties
       IMG_ECO_IDX = 1;
       NAV_ECO_IDX = 2;
    end
    
    methods
        
        %// CONSTRUCTOR //%
        function this = twix_map_obj_Nav(varargin)
            this = this@twix_map_obj(varargin{:});
        end
        
        function setDefaultFlags(this)
            
            this.arg.readType = 'IMG'; 
            setDefaultFlags@twix_map_obj(this);
          
        end
        
        function this = clean(this)

            try
                this.NCol = this.determineColLength();        
            catch ME
               warning(ME.message);
               keyboard; 
            end   

            this = clean@twix_map_obj(this);

        end

        function nCol = determineColLength(this)
           % This is added by Dave to determine the read length when we have
           % multiple echoes (e.g. navigator) that have varying read lengths

           warning( sprintf('Assuming NAV is Eco %i and IMG is Eco %i', ...
                            this.NAV_ECO_IDX, this.IMG_ECO_IDX) );

           if max(this.Eco) == 1
               nCol = this.NCol(1);
               return;
           end

           % Column length determined based on Eco Index
           switch this.arg.readType
               case 'IMG'
                   ecoIdx = this.IMG_ECO_IDX;
               case 'NAV'
                   ecoIdx = this.NAV_ECO_IDX;
               otherwise
                   ecoIdx = 1;
           end

           idx = find(this.Eco == ecoIdx);
           nCol = this.NCol(idx(1));

        end

    end
    
end % classdef
