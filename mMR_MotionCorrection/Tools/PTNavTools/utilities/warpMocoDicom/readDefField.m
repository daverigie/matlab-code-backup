function varargout = readDefField(dirpath, invFlag)

    if nargin < 2
        invFlag = false;
    end
    
    if invFlag
        basename = 'InvDefField';
    else
        basename = 'DefField';
    end
    
    counter = 1;
    for d = {'X', 'Y', 'Z'}
       filename = strcat(basename, d{:}, '.v');
       filepath = fullfile(dirpath, filename);
       varargout{counter} = readDefFieldComponent(filepath);
       counter = counter + 1;
    end

end


function [X] = readDefFieldComponent(filepath)

    fid   = fopen(filepath);
    bytes = fread(fid, 'single');
    
    X = reshape(bytes, [172, 172, 127]);
    
    fclose(fid);
    

end