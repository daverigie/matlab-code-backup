function [fulltext] = printnestedstruct(S, parent, fulltext)

    if nargin < 3
        fulltext = '';
    end
    
    if nargin < 2
        parent = '';
    end

    for f = fields(S)'
       
       field = f{1};
       val = getfield(S, field);
       
       if isstruct(val)
           fulltext = printnestedstruct(val, [parent '.' field], fulltext);
       else
           line = sprintf('%s.%s = %s', parent, field, val2str(val));
           if line(1) == '.'
               line = line(2:end);
           end
           if length(fulltext) > 0
                fulltext = sprintf('%s\n%s', fulltext, line);
           else
                fulltext = sprintf('%s%s', fulltext, line);
           end
       end
    end
    
end


function s = val2str(val)

    try 
        s = mat2str(val);
    catch ME
        s = evalc('disp(val)');
    end

end