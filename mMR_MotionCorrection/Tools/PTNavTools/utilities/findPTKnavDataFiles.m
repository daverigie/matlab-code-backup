function allpaths = findPTKnavDataFiles(basedir, searchstring)
% RECURSIVELISTFILES  list all filepaths in basedir recursively.
%   
%   allpaths = recursiveListFiles(basedir) creates a struct array of all 
%              filepaths in inside the directory specified by the string
%              basedir.
%

fprintf('\nChecking %s ...', basedir);

if nargin < 2
    searchstring = 'radvibenyunavi';
end

    containsi = @(s,x) contains(s,x,'IgnoreCase',true);

    % List files/dirs directly beneath basedir, ignore '.' and '..'
    listing = dir(basedir);
    listing = listing(3:end); 
    
    % Separate files from directories
    dirInd = [listing.isdir]; 
    dirs   = listing(dirInd);
    files  = listing(~dirInd);
        
    dirpaths = fullfile(basedir, {dirs.name})';
    ind = containsi(dirpaths,searchstring);
    dirpaths_match = dirpaths(ind);
    dirpaths_nomatch = dirpaths(~ind);
    
    filepaths = fullfile(basedir, {files.name})';
    ind = containsi(filepaths,searchstring);
    filepaths_match = filepaths(ind);
    filepaths_nomatch = filepaths(~ind);
    
    allpaths = [filepaths_match; dirpaths_match];
        
    % Add all filepaths to allpaths, passing each subdir into this
    % function
    for i = 1:1:length(dirpaths_nomatch)
       allpaths = [allpaths; findPTKnavDataFiles(dirpaths_nomatch{i}, searchstring)]; 
    end
      
end