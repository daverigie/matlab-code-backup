function X = decompressMatrix(U,S,V,Mu)

    X = bsxfun(@plus, U*S*V', Mu);

end