
function s = loadtxt2struct(filepath, outpath)

    fulltext = fileread(filepath);
    
    lines = splitLines(fulltext)';
    lines = strcat('s.', lines);
    
    for i = 1:1:numel(lines)
       line = lines{i};
       eval(line);
    end
    
    if nargin > 1
        save(outpath, '-struct', 's');
    end
    
end

function lines = splitLines(fulltext)

    lines = {};
    lines = regexp(fulltext, '[^\n=%]*=[^\n=%]*', 'match')';
    
    lines = strcat(strtrim(lines), ';');
            
end

