function path = uiputpath(varargin)

    [filename, dirname] = uiputfile(varargin{:});
    
    path = fullfile(dirname, filename);

end