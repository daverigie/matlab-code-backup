classdef VolumeWarper
    
    properties
        vectorsBasedir
        nGates
        refGate
        dirlist
    end
    
    methods
       
        function this = VolumeWarper(dirpath)
           
            
            this.vectorsBasedir = dirpath;
            this.dirlist        = regexpdir(dirpath, '\d+_\d+_\d+', 1);
            [nGates, refGate, ~] = parseVectorDir(this.dirlist(1).name);
            
            this.nGates  = nGates;
            this.refGate = refGate;           
            
        end
        
        function volWarped = warp2Ref(this, vol, gate)
            
            if gate == this.refGate
                volWarped = vol;
                return;
            end
            
            fprintf('\nWarping gate %i to ref gate %i', gate, this.refGate);

            invFlag = false;
            [U, V, W] = this.getMotionField(gate, invFlag);
            
            volWarped = warpVolume(vol, U, V, W);
            
        end
        
        function volWarped = warpRef2Gate(this, volRef, gate)
        
            if gate == this.refGate
                volWarped = volRef;
                return;
            end
            
            fprintf('\nWarping ref gate %i to gate %i', this.refGate, gate);
            
            invFlag = true;
            [U, V, W] = this.getMotionField(gate, invFlag);
         
            volWarped = warpVolume(volRef, U, V, W);
            
        end
        
        function volWarped = warp(this, vol, fromGate, toGate)
           
            volWarped = this.warp2Ref(vol, fromGate);
            volWarped = this.warpRef2Gate(volWarped, toGate);
            
        end
        
        function [U, V, W] = getMotionField(this, gate, invFlag)
           % When invFlag is false, this motion field describes
           % the deformation from gate to this.refGate
           
           dirpath   = this.constructDirpath(gate);
           [U, V, W] = readDefField(dirpath, invFlag);
            
            
        end
                
        function dirpath = constructDirpath(this, gate)
            
            dirname = sprintf('%i_%i_%i', this.nGates, this.refGate, gate);
            dirpath = fullfile(this.vectorsBasedir, dirname);
            
        end
        
    end
    
end

