function volWarped = warpVolume(vol, u, v, w)

    % It is assumed that u, v, and w are formatted as expected for PET
    % MoCo Recon Pipeline
    %
    % The image volume v(i,j,k) should be oriented so that ...
    %   increasing row (i) index moves in L>>R direction
    %   increasing col (j) index moves in A>>P direction
    %   increasing slc (k) index moves in S>>I direction

   [nRows, nCols, nSlices] = size(vol);
    
    xgrid = 1:nCols;
    ygrid = 1:nRows;
    zgrid = 1:nSlices;
    
    [a,b,c] = meshgrid(ygrid,xgrid,zgrid);
    
    x = u + b;
    y = v + a;
    z = w + c;
    
    volWarped = interp3(a,b,c,vol,y,x,z,'linear',0);
    
    
    
    
end