function [] = determineTargetGate(vectorsDirpath, newTargetGate)

    if nargin < 1
        vectorsDirpath = uigetdir();
    end
    
    if newTargetGate == targetGate
        fprintf('\n\nTarget gate already correctly specified by folder names.\n\n');
        return
    end
    
    newBasepath = [vectorsDirpath '_renamed'];        
    mkdir(newBasepath);
    
    for i = 1:1:numel(dirlist)
        
        dirpath = dirlist(i).path;
        
        [nGates, targetGate, gate] = parseVectorDir(dirlist(i).name);
        fprintf('\nCopying gate %i', gate);
        tic;        
        copyfile(dirpath, fullfile(newBasepath, sprintf('%i_%i_%i', nGates, newTargetGate, gate)));
        toc;
        
    end

end