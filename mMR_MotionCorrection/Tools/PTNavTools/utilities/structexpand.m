function c = structexpand(s)
% This is expands a parameter struct s into name value pairs. It's a
% convenience function for passing parameters around when name/value pairs
% are required

c = cell(0);

fieldlist = fields(s);

for i = 1:1:length(fieldlist)
    fieldname = fieldlist{i};
    fieldval = getfield(s, fieldname);
    c = [c,fieldname, fieldval];
end


end