
function fp = fullpath(relpath)

  if ispc
       home = [getenv('HOMEDRIVE') getenv('HOMEPATH')];
  else
       home = getenv('HOME');
  end
  
  relpath = strrep(relpath, '~', home);
  
  file = java.io.File(relpath);
  
  if file.isAbsolute()
      fp = relpath;
  else
      fp = char(file.getCanonicalPath());
  end
  

end

