classdef ImageVolumeDicom < ImageVolume
    % Encapsulates geometry information to position image voxels in space
    % 
    % This resource is excellent for summarizing DICOM geometry
    % http://nipy.org/nibabel/dicom/dicom_orientation.html
    properties
        headerArr
    end
    
    methods
        
        function obj = ImageVolumeDicom(dirpath, varargin)
                               
            [voxelData,hdrArr] = loadDicomSeries(dirpath, varargin{:});
            
            % origin
            origin = zeros([3,1]);
            origin(:) = hdrArr(1).ImagePositionPatient;
            
            % Direction cosine matrix
            DCM = zeros(3,3);
            DCM(1:6) = hdrArr(1).ImageOrientationPatient(:);
            DCM(:,3)   = cross(DCM(:,1), DCM(:,2));
            DCM(:,[1,2]) = DCM(:,[2,1]); % swap first/second column
            
            % Spacing
            voxelSpacing      = zeros([3,1]);
            voxelSpacing(1:2) = hdrArr(1).PixelSpacing;
            voxelSpacing(3)   = hdrArr(1).SliceThickness;
            
            dims = size(voxelData);
            
            obj = obj@ImageVolume(voxelData,origin,DCM,voxelSpacing,dims);
            
            obj.modality = hdrArr(1).Modality;
            obj.headerArr = hdrArr;
        
        end
        
        
   
    end
    
    
end
    


