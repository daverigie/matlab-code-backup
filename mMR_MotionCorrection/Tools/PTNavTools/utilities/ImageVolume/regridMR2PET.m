function [] = regridMR2PET(twix_obj, pet_dicom_path, outpath)

    nGate = size(twix_obj.data(:,:,:,:), 4);
    IVpet = ImageVolumeDicom(pet_dicom_path);
    voxelData_regridded = zeros([size(IVpet.voxelData) nGate]);
    
    for iGate = 1:1:nGate
       fprintf('\nProcessing gate %i', iGate);
       IVmr = ImageVolumeTwix(twix_obj.data(:,:,:,iGate), twix_obj);  
       IV_regridded = IVmr.regridvolume(IVpet);
       voxelData_regridded(:,:,:,iGate) = IV_regridded.voxelData;
    end
    
    twix_obj.data = flip(swapaxes(voxelData_regridded, 1,2),3);
    
    save(outpath, '-struct', 'twix_obj');
    
end

