% This script demonstrates the use of the inputParser class built into
% MATLAB

% use all default parameters
findArea(10);

% specify optional params using string/value pairs
findArea(10, 2, 'shape', 'square', 'units', 'cm')

% specify optional params using struct (only if StructExpand property of
% the inputParser is true. 

%NOTE: both optional inputs AND 'parameters' can be specified by the struct
% optional inputs are simply variables that don't need to be passed,
% whereas parameters must be in name/value pairs.

parm.height = 20;
parm.shape = 'square';
parm.units = 'cm';

findArea(10, parm);


