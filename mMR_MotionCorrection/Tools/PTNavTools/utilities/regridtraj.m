function xxnew = regridtraj(xx, num_points)

       
    x0 = 1:1:numel(xx);
    xnew = linspace(1,numel(xx),num_points);
    
    xxnew = interp1(x0, xx, xnew);
    
end