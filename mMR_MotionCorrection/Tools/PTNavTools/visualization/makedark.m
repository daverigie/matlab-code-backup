function [] = makedark()
    
    set(gcf, 'color', 'black');
    set(gca, 'color',  'black');
    set(gca, 'XColor', 'white');
    set(gca, 'YColor', 'white');
    set(gca, 'ColorOrder', distinguishable_colors(10, 'k'));

end