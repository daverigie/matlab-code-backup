 function [fig,haxes,himg] = imagecompare(imdata1, imdata2, scalingType)


    

    if nargin < 3
        scalingType = 'image';
    end
    
    alpha      = 0.5;  
    imdata     = imdata1*alpha + (1-alpha)*imdata2;
    sliceDims = repmat({1},[1, ndims(imdata)-2]);
    cmin       = min(imdata(:));
    cmax       = max(imdata(:));
    
    
    if nargin < 6
        clims = [cmin, cmax];
    end
    fig = figure('Visible','on', 'Position', [100,100,900,600]);
    haxes = axes('Units','pixels','Position',[150,170,800*0.65,600*0.65]);
    himg = imshow(imdata(:,:,1), 'Parent', haxes);
    set(gca, 'XTick', []); set(gca, 'YTick', []);
    updateImageWindow();
    axis(scalingType);
    imColorMap = colormap;
      
    btn_cycle = uicontrol('Style','pushbutton','String','Cycle Dim',...
                   'Position',[75 500 60 20], 'Callback', @callback_cycle);
   
    btn_transpose = uicontrol('Style','pushbutton','String','Transpose',...
                   'Position',[75 470 60 20], 'Callback', @callback_transpose);
               
    btn_fliplr = uicontrol('Style','pushbutton','String','Flip L/R',...
                   'Position',[75 440 60 20], 'Callback', @callback_fliplr);
               
    btn_flipud = uicontrol('Style','pushbutton','String','Flip U/D',...
                   'Position',[75 410 60 20], 'Callback', @callback_flipud);
               
    btn_flipZ = uicontrol('Style','pushbutton','String','Flip Z',...
                   'Position',[75 380 60 20], 'Callback', @callback_flipZ);
    
    popup  = uicontrol('Style', 'popup',...
           'String', {'gray', 'gray_r', 'fire','jet','hsv','hot','cool','parula'},...
           'Position', [75 350 60 20],...
           'Callback', @setmap); 
                     
   btn_saveImage = uicontrol('Style','pushbutton','String','Save Image',...
                   'Position',[75 200 60 20], 'Callback', @callback_saveImage);      

    %//////////////////////////////////////////////////////////////////////
    %// ----------- Create slider for slice/time ------------------------//
    %//////////////////////////////////////////////////////////////////////

    [n_rows, n_cols, sliceDims{:}] = size(imdata);
    size(imdata)
    numSliceDims = numel(sliceDims)
    sliceIdx = {ceil(sliceDims{1}/2), };
    updateImageDisplay();  
    
    sliderHorzSpacing = 50;    
    for i = 1:1:numSliceDims
        ypos = 700 + (i-1)*sliderHorzSpacing;
        sld_dim{i} = makeSlider([], [1, sliceDims{i}], ceil(sliceDims{i}/2), sliceDims{i}, ...
                          'Position', [ypos 160 20 400], ...  
                          'Parent', fig);
        
        addlistener(sld_dim{i}, 'ContinuousValueChange', @setSlice);
    end
    
    
    %//////////////////////////////////////////////////////////////////////
    %// ----------- Create contrast sliders  ----------------------------//
    %//////////////////////////////////////////////////////////////////////

    sld_low = makeSlider([], [cmin, cmax], cmin, 100, ...
                         'Position', [200 125 400 20]);

    addlistener(sld_low, 'ContinuousValueChange', @setclims);

    % Add a text uicontrol to label the slider.
    txt_low = uicontrol('Style','text',...
        'Position',[200 75 400 20],...
        'String','clow');

    sld_high = makeSlider([], [cmin, cmax], cmax, 100, ...
                          'Position', [200 75 400 20]); 

    addlistener(sld_high, 'ContinuousValueChange', @setclims);

    txt_high = uicontrol('Style','text',...
        'Position',[200 25 400 20],...
        'String','chigh');

    sld_alpha = makeSlider([], [0, 1], 1, 100, ...
                          'Position', [200 25 400 20]); 

    addlistener(sld_alpha, 'ContinuousValueChange', @setalpha);

    txt_alpha = uicontrol('Style','text',...
        'Position',[200 50 400 20],...
        'String','alpha');
    

    set( findall( fig, '-property', 'Units' ), 'Units', 'Normalized' )

    % Make figure visble after adding all components
    colorbar();
    f.Visible = 'on';
    
    
    
     function updateSliceSliders()
        [n_rows, n_cols, sliceDims{:}] = size(imdata);
        sliceIdx = {ceil(sliceDims{1}/2), 1, 1};
        himg = imshow(imdata(:,:,1,1,1), 'Parent', haxes);
        updateImageDisplay();
        updateImageWindow();
        setmap();
        for i = 1:1:numel(sliceDims)
            sld_dim{i} = makeSlider(sld_dim{i}, [1, sliceDims{i}], ...
                                    ceil(sliceDims{i}/2), sliceDims{i});
            addlistener(sld_dim{i}, 'ContinuousValueChange', @setSlice);
        end
        axis(haxes, scalingType);
     end
    
     function updateImageWindow()
        try
            caxis(haxes, clims); % set new minimum
        catch ME
            ; 
        end
     end
    
     function updateImageDisplay()
         frame1 = imdata1(:,:,sliceIdx{:});
         frame2 = imdata2(:,:,sliceIdx{:});
         frame  = frame1*alpha + frame2*(1-alpha);
         %frame = imresize(frame, 4, 'Method', 'bilinear');
         set(himg, 'CData', frame);
         title(haxes, sprintf('slice %i, gate %i, gate %i', sliceIdx{:}));
     end

     function setclims(hObject, eventdata, handles)
         clims = [sld_low.Value, sld_high.Value];
         updateImageWindow();
     end
 
     function setalpha(hObject, eventdata, handles)
        alpha = sld_alpha.Value;
        updateImageDisplay();
     end

     function setSlice(hObject, eventdata, handles)
        for iDim = 1:1:numSliceDims
            sliceIdx{iDim} = round(sld_dim{iDim}.Value);
        end
        updateImageDisplay();
     end

     function callback_cycle(src, event)
        order = 1:ndims(imdata);
        order(1:3) = circshift(order(1:3),1);
        imdata = permute(imdata, order);
        updateSliceSliders();
     end
 
     function callback_transpose(src, event)
        order = 1:ndims(imdata);
        order(1:2) = circshift(order(1:2),1);
        imdata = permute(imdata, order);
        updateSliceSliders();
     end
 
     function callback_fliplr(src, event)
        imdata = flip(imdata, 2);
        updateSliceSliders();
     end
 
    function callback_flipud(src, event)
        imdata = flip(imdata, 1);
        updateSliceSliders();
    end

    function callback_flipZ(src, event)
        imdata = flip(imdata, 3);
        updateSliceSliders();
    end

    function callback_saveImage(src, event)
        dataFrame  = imdata(:,:,sliceIdx{:});
        imageFrame = mat2gray(dataFrame, haxes.CLim);
        X   = gray2ind(imageFrame, size(imColorMap,1));
        while max(size(X)) < 512
            X = imresize(X, 2);
        end
        
        [filename, dirpath] = uiputfile('*.jpg', 'Choose Image Path');
        [dirpath, filename, ext] = fileparts(fullfile(dirpath, filename));
        imwrite(X, imColorMap, fullfile(dirpath, [filename ext]));
        save(fullfile(dirpath, [filename '.mat']), 'dataFrame');        
    end
 
    function setmap(source,event)
            val = popup.Value;
            maps = popup.String;
            newmap = maps{val};
            
            idx = strfind(newmap, '_r');
            if ~isempty(idx)
                newmap = newmap(1:(idx-1));
                newmap = flipud(eval(newmap));
            else
                newmap = eval(newmap);
            end
            
            imColorMap = newmap;
            colormap(haxes, newmap);
            drawnow;
    end
 
     function sld = makeSlider(sld, lims, val, nSteps, varargin)
        
         if nSteps == 1
             minorStep = 1;
             majorStep = 1;
         else
             minorStep = 1.0/nSteps;
             majorStep = 1.0/nSteps;
         end
         
         if isempty(sld)         
             sld = uicontrol('Style', 'slider', ...
                              'Min', lims(1), 'Max', lims(2) + eps, 'Value', val, ...
                              'SliderStep', [minorStep, majorStep], ...
                              varargin{:});
         else
            sld.Min = lims(1);
            sld.Max = lims(2) + eps;
            sld.Value = val;
            sld.SliderStep = [minorStep, majorStep];
            varargin{:};
         end
                      
         if nSteps < 2
             sld.Enable = 'off';
             sld.Visible = 'off';
         end
         
     end

 end