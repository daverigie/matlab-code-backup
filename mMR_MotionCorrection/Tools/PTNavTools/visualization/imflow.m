function varargout = imflow(I, varargin)

    im = flowImageRep(I);
    
    varargout{:} = imshow(im, varargin{:});

    colorkey;

end