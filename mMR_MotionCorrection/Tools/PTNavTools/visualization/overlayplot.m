function [fig] = overlayplot(x, y1, y2, varargin)

    %======================================================================
    %                        PARSE INPUTS
    %----------------------------------------------------------------------
    p = inputParser;

    p.addRequired('x');
    p.addRequired('y1');
    p.addRequired('y2');
    p.addParameter('colour', 'r');
    p.addParameter('alpha',  0.25);
    p.addParameter('rescale', false);
    
    p.parse(x, y1, y2, varargin{:});
    
    x = p.Results.x;
    y1 = p.Results.y1;
    y2 = p.Results.y2;
    if p.Results.rescale 
        y1 = rescaleSignal2Match(y1,y2);
    end
    colour = colorName2RGB(p.Results.colour);
    alpha = p.Results.alpha;
    
    %______________________________________________________________________

    white   = [1, 1, 1];
    colour1 = alpha*colour + (1-alpha)*white;
    colour2 = colour;
    
    fig = figure;
    
    p1 = plot(x, y1); hold on;
    set(p1,'Color', colour1);

    p2 = plot(x, y2); 
    set(p2,'Color', colour2);

end

function C = colorName2RGB(colorName)
    if ischar(colorName)
        C = bitget(find('krgybmcw'==colorName)-1,1:3);
    elseif isvector(colorName)
        C = colorName;
    end
end