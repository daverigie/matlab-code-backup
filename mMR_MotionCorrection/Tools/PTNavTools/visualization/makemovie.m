function [] = makemovie(imstack, imviewfun, filename, framerate, num_loops,compression)

    if nargin < 6
        compression = false
    end
    
    figure;

    if nargin < 3
        framerate = 5;
    end
    
    if compression
        v = VideoWriter(sprintf('%s.avi',filename));
    else
        v = VideoWriter(sprintf('%s.avi',filename),'Uncompressed AVI');
    end
    
    v.FrameRate = framerate;
    open(v);

    N = size(imstack,3);
    
    counter = 1;
    for iLoop = 1:1:num_loops
        for ind = 1:1:size(imstack,3)
            fprintf('\nWriting frame %i of %i', counter, num_loops*size(imstack,3));
            imviewfun(imstack(:,:,ind));
            %axis square;
            %axis on;
            %grid on;
            %grid minor;
            
            %set(gca,'Xcolor','white');
            %set(gca,'Ycolor','white');
            
            drawnow;
            frame = getframe;
            writeVideo(v, frame);
            counter = counter + 1;
        end
    end
    
    close(v);

end




