function varargout = plotdark(varargin)

    [varargout{1:nargout}] = plot(varargin{:});
    
    set(gcf, 'color', 'black');
    set(gca, 'color',  'black');
    set(gca, 'XColor', 'white');
    set(gca, 'YColor', 'white');
    
    % Set colors
    %{
    LineHandles = get(gca, 'Children');
    nLines      = numel(LineHandles);
    C           = distinguishable_colors(nLines, 'k');
    for iLine = 1:nLines
       set(LineHandles(iLine), 'Color', C(iLine,:));  
    end
    %}

end