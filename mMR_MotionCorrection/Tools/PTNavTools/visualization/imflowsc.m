function varargout = imflowsc(I, varargin)

    im = flowImageRep(I);
    
    varargout{:} = imagesc(im, varargin{:});

    colorkey;

end