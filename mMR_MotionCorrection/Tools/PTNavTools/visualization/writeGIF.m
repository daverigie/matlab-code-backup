function [] = writeGIF(imGenerator, fileName, frameOrder, varargin)

  %========================================================================
  %                                 Parse Inputs
  %------------------------------------------------------------------------
  
  p = inputParser;
  addRequired(p, 'imGenerator', @(x) isa(x, 'function_handle'));
  addRequired(p, 'fileName',    @ischar);
  addRequired(p, 'frameOrder',  @isvector);
  addOptional(p, 'frameDelay',  'auto', @isscalar);
  addParameter(p, 'scaleFactor', 1, @isscalar);
  
  parse(p, imGenerator, fileName, frameOrder, varargin{:});
  
  imGenerator = p.Results.imGenerator;
  [dirpath, fileName, ext] = fileparts(p.Results.fileName);
  fileName = fullfile(dirpath, [fileName '.gif']);
  frameOrder = p.Results.frameOrder;
  
  if strcmpi(p.Results.frameDelay, 'AUTO')
      frameDelay = 2.0/numel(frameOrder);
  else
      frameDelay = p.Results.frameDelay;
  end
  
  %_______________________________________________________________________%
   
   counter = 1;
   for iFrame = frameOrder(:)'
       
        [imInd, cmap] = getFrame(imGenerator, iFrame);
        [imInd, cmap] = imresize(imInd, cmap, p.Results.scaleFactor, ...
                                 'method', 'nearest');
       
        % On the first loop, create the file. In subsequent loops, append.
        if counter == 1
            imwrite(imInd, cmap, fileName,'gif', ...
                    'DelayTime',frameDelay, ...
                    'loopcount',inf);
        else
            imwrite(imInd, cmap, fileName, 'gif', ...
                    'DelayTime', frameDelay, ...
                    'writemode','append');
        end
        counter = counter + 1;
   end

end

function [imInd, cmap] = getFrame(imgen, iFrame)

        im = imgen(iFrame);
        
        if ishandle(im) % If generator is an image display function, capture the image
            axis image;
            [imInd, cmap] = frame2im(getframe(gca));
            if ndims(imInd) == 3
                [imInd, cmap] = rgb2ind(imInd, 256);
            end
        else % If generator returns a 2D/3D array convert to the appropriate format
            if ndims(im) == 2
                [imInd, cmap] = gray2ind(im, 256);
            elseif ndims(im) == 3
                [imInd, cmap] = rgb2ind(im, 256);
            end
        end

end