function [imf] = fuseim(im1, im2, varargin)
% fuseim(im1, im2, alpha, cmap1, cmap2) 
   
  %========================================================================
  %                                 Parse Inputs
  %------------------------------------------------------------------------
  
  p = inputParser;
  addRequired(p, 'im1', @ismatrix);
  addRequired(p, 'im2', @ismatrix);
  addOptional(p, 'alpha', 0.5, @isscalar);
  addOptional(p, 'cmap1', gray, @(x) true);
  addOptional(p, 'cmap2', hot, @(x) true);
  addParameter(p, 'noRescale', false);
    
  parse(p, im1, im2, varargin{:});

  im1   = p.Results.im1;
  im2   = p.Results.im2;
  alpha = p.Results.alpha;
  
  mask = (im2>0);
  
  if ischar(p.Results.cmap1)
      cmap1 = eval(p.Results.cmap1);
  else
      cmap1 = p.Results.cmap1;
  end
  
  if ischar(p.Results.cmap2)
      cmap2 = eval(p.Results.cmap2);
  else
      cmap2 = p.Results.cmap2;
  end
  
  if ~p.Results.noRescale
      im1 = mat2gray(im1);
      im2 = mat2gray(im2);
  end
  
  %_______________________________________________________________________%

  im1 = mat2rgb(im1, cmap1);
  im2 = mat2rgb(im2, cmap2);
  
  if (alpha < 0) || (alpha > 1)
      warning('Alpha must be between 0 and 1');
      alpha = mat2gray(alpha);
  end
 
  imf  = im1;
  imf(mask) = im2(mask); 
  
end

  
function imrgb = mat2rgb(X, cmap)
    n = length(cmap);
    imrgb = ind2rgb(gray2ind(X, n), cmap);
  
end

