% Load im1 and im2
load('demo_data.mat');

% Set parameters
cmap1 = 'gray';
cmap2 = 'fire';
alpha = 0.5; % blending coefficient

% Display images and fusion 
clear ax
ax(1) = subplot(131)
imshow(im1, []); title('image 1');
ax(2) = subplot(132);
imshow(im2, []); title('image 2');
ax(3) = subplot(133); 
imshow(fuseim(im1, im2, alpha, cmap1, cmap2)); title('fused image');

% Loop through different alpha values

for iLoop = 1:1:5
    for alpha = [0:.1:1, 1:-0.1:0]
        imf = fuseim(im1, im2, alpha, cmap1, cmap2);
        imshow(imf); title('fused image');
        pause(0.1);
        drawnow;
    end
end