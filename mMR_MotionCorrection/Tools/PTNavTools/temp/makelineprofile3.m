function [] = makelineprofile(matpath1, matpath2, colIdx)

    COLOR_2 = [241, 91, 78]/255;
    FIG_POSITION = [680 736 297 242];
    FONT_ARGS = {'FontSize', 11, 'FontWeight', 'bold'};

    MF1 = load(matpath1);
    
    MF2 = load(matpath2);
    
    
    fig   = figure();
    ax    = subplot(1,1,1);
    
    line1 = plot(MF1.dataFrame(:, colIdx), '--', 'LineWidth', 2.0, 'Color', 'k');
    
    hold on;
    
    line2 = plot(MF2.dataFrame(:, colIdx), 'LineWidth',       2.0, 'Color', COLOR_2);
    
    hold off;

    xlabel('S/I Position (mm)', FONT_ARGS{:});
    ylabel('SUV', FONT_ARGS{:});
    legend({'Uncorrected', 'MoCo'},'location','NorthWest');
        
    set(fig, 'Position', FIG_POSITION);


end