loadArgs = {'suv',true,'extension','.ima'};

% Load dicom stack for static reocn
staticfile     = regexpdir(pwd, '.*Static.*[^N]AC');
volStatic.path = staticfile.path;
IVStatic = ImageVolumeDicom(volStatic.path, loadArgs{:});

% Load dicom stack for moco recon
mocofile     = regexpdir(pwd, '.*Motion.*[^N]AC');
volMoco.path = mocofile.path;
IVMoco = ImageVolumeDicom(volMoco.path, loadArgs{:});


% Display both in imageviewer
vol1 = regridarray(IVStatic.voxelData, IVStatic.fov);
vol2 = regridarray(IVMoco.voxelData, IVMoco.fov);
imageviewer(cat(4, vol1, vol2));







