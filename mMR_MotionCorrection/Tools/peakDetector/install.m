function install()

%% TEMPORARY FUNCTIONS
mfilepath = fileparts(mfilename('fullpath'));
pathadd = @(p) addpath(fullfile(mfilepath, p));

%% PATHNAMES
pathadd('matlab');  % ./matlab
pathadd('mex');     % ./mex

end