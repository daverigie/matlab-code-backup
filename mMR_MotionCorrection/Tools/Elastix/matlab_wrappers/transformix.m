function [status, result] = transformix(varargin)

    % ALL IMAGES AND MASKS SHOULD BE SPECIFIED AS PATHS TO FILES
    
    %==============================================================
    %                   PARSE INPUTS
    %--------------------------------------------------------------

    varargin(:)
    p = inputParser;
    p.addOptional('outputDir', makeResultsDir(), @isdir);
    p.addOptional('transformParameterFile', [], @(x) true);    
    
    p.addParameter('inputImage', []);
    p.addParameter('deformPoints', []);
    p.addParameter('jacobianDeterminant', []);
    p.addParameter('jacobianMatrix', []);
        
    p.addParameter('processPriority', 'normal');
    p.addParameter('maxThreads', [], @(x) isequal(floor(x(1)), x(:)));
    p.addParameter('runInBackground', false, @islogical);
    p.addParameter('debug',           false, @islogical);
    
    p.parse(varargin{:});

    Results = p.Results;
    
    if isempty(Results.transformParameterFile)
        Results.transformParameterFile = uigetpath('*.txt','Select Transform Parameter File');
    end

    if isempty(Results.inputImage) && isempty(Results.deformPoints) && isempty(Results.jacobianDeterminant) ... 
    	&& isempty(Results.jacobianMatrix)

    	Results.inputImage = uigetpath('*.*', 'Select Input Image');
    	Results.deformPoints = 'all';

    end
        
    %_______________________________________________________________%
   
    stringcat = @(varargin) cat(2, varargin{:});
    
    command = 'transformix';
    command = stringcat(command, ' -out ', Results.outputDir);
    command = stringcat(command, ' -tp ', Results.transformParameterFile);
    
    if ~isempty(Results.inputImage)
        command = stringcat(command, ' -in ', Results.inputImage);
    end
    
    if ~isempty(Results.deformPoints)
        command = stringcat(command, ' -def ', Results.deformPoints);
    end
    
    if ~isempty(Results.jacobianDeterminant)
        command = stringcat(command, ' -jac ', Results.jacobianDeterminant);
    end
    
    if ~isempty(Results.jacobianMatrix)
        command = stringcat(command, ' -jacmat ', Results.jacobianMatrix);
    end
    
    if ~isempty(Results.processPriority)
        command = stringcat(command, ' -priority ', Results.processPriority);
    end
    
    if ~isempty(Results.maxThreads)
        command = stringcat(command, ' -threads ', num2str(floor(Results.maxThreads)));
    end
    
    if  Results.runInBackground
        command = stringcat(command, '&');
    end
    
    if Results.debug
        fprintf('\n\n%s\n\n', command);
        return
    end
    
    if ~exist(Results.outputDir, 'dir')
        mkdir(Results.outputDir);
    end
    
    [status, result] = system(command, '-echo');
    
    if status > 0
        error(result);
    end

end

function p = makeResultsDir()

    date_string = datestr(now, 'yyyy-mm-dd__hh-MM-ss');
    p = fullfile(pwd, ['Results_', date_string]);

end