function [U,V,W] = niftivecs2mat(nii_def_field, pet_ref_vol)
% nii_def_field should be a deformation field saved as .NII as exported by elastix/transformix
% pet_ref_vol could be a DICOM or NIFTI of one PET volume that serves as a template for aligning
% the deformation field to the correct orientation

    WORLD = 'RAS';
    DESIRED_OUTPUT_DATA_ORDERING = 'LPI';
    
    fprintf('\nUsing World Coordinate System: %s', WORLD);
    fprintf('\nOutput Data Ordering: %s', DESIRED_OUTPUT_DATA_ORDERING);

    fprintf('\nLoading PET reference volume...'); tstart = tic;    
    if exist(pet_ref_vol, 'dir')
        IVpet  = ImageVolumeDicom(pet_ref_vol);
    elseif (strcmpi(fileext(pet_ref_vol), '.NII'))
        IVpet = ImageVolumeNifti(pet_ref_vol);
    else
        error('PET image must be DICOM dir or (uncompressed) Nifti');
    end
    fprintf(' Done in %f seconds.', toc(tstart));

    IVpet.coordinateSystem = WORLD;

    fprintf('\nLoading deformation field ...'); tstart = tic;
    VF = VectorField(nii_def_field);
    VF.coordinateSystem = WORLD;
    fprintf('\nDone in %f seconds', toc(tstart));

    fprintf('\nInterpolating and reorienting deformation field for interfile ...'); 
    tstart = tic;
    % Regrid vectorfield so sampling matches up with PET volume
    VF = VF.regridvectors(IVpet);

    % Convert from physical units (mm WORLD) to unitless/indices
    [U,V,W] = VF.getIndexedVectorfield(); 

    % Make sure axis ordering is LPI as is apparently required to write the interfile
    [U, V, W] = VectorField.reorient(U, V, W, VF.dataOrdering, DESIRED_OUTPUT_DATA_ORDERING);

    % Still unsure why these sign flips are required
    U = -U;
    V = -V;
    
    fprintf('\nDone in %f seconds.\n\n\n', toc(tstart));
    
end

function ext = fileext(filepath)

    [~,~,ext] = fileparts(filepath);

end