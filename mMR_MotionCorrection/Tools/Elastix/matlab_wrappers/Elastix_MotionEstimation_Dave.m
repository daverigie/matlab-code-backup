function [] = Elastix_MotionEstimation_Dave(filelist_nii, ... 
                                            petrefpath, ...
                                            targetGate, ...
                                            targetFolder, ...
                                            parm_elastix, ...
                                            varargin)

    % This function needs to take in MR gates as nifti files and then write
    % out motion vectors'
    
    % filelist_nii should already be sorted from gate 1 to gate N
        
    if exist(petrefpath,'dir')
        IVpet = ImageVolumeDicom(petrefpath);
    elseif ~isempty(petrefpath)
        IVpet = ImageVolumeNifti(petrefpath);
    end
    
    % Initialize output array for vector data
    numGates = numel(filelist_nii);
    dims = size(IVpet.voxelData);
    vecs = zeros( cat(2,dims,3,numGates) );
    vecsInv = 0.0*vecs;
    
    % Begin loop over gates
    for iGate = 1:numGates
        
        if iGate == targetGate
            continue;
        end
        
        % Forward Motion Fields
        fixed_image_nii = filelist_nii{targetGate};
        moving_image_nii = filelist_nii{iGate};
        
        outputDir = fullfile(targetFolder, ...
                             sprintf('elastix_reg_%i_%i_%i', numGates, ...
                                                             targetGate, ...
                                                             iGate));
                             
        
        [defFieldPath] = computeMotionVectorsElastix(fixed_image_nii, ...
                                                     moving_image_nii, ...
                                                     outputDir, ...
                                                     parm_elastix, ...
                                                     varargin{:});
                                                 
        % Inverse Motion Fields
        fixed_image_nii  = filelist_nii{iGate};
        moving_image_nii = filelist_nii{targetGate};
        
        outputDir = fullfile(targetFolder, ...
                             sprintf('elastix_reg_%i_%i_%i', numGates, ...
                                                             iGate, ...
                                                             targetGate));
                             
        
        [defFieldPathInv] = computeMotionVectorsElastix(fixed_image_nii, ...
                                                     moving_image_nii, ...
                                                     outputDir, ...
                                                     parm_elastix, ...
                                                     varargin{:});     
                                                 
        if ~isempty(petrefpath)
           [U,V,W] = niftivecs2mat(defFieldPath, petrefpath);
           [Uinv, Vinv, Winv] = niftivecs2mat(defFieldPathInv, petrefpath);
           
           vecs(:,:,:,1,iGate) = U;
           vecs(:,:,:,2,iGate) = V;
           vecs(:,:,:,3,iGate) = W;
           
           vecsInv(:,:,:,1,iGate) = Uinv;
           vecsInv(:,:,:,2,iGate) = Vinv;
           vecsInv(:,:,:,3,iGate) = Winv;
        end
        
    end
    
    fprintf('\nWriting vectors to interfile');
    CreateMotionVectorHeader(targetFolder, targetGate, vecs, vecsInv);
    fprintf('\n\n\n');
    
    fprintf('\nCopying parameter file to target folder ...');
    
    if exist(parm_elastix, 'file')
        copyfile(parm_elastix, targetFolder);
    end
    
    
end
