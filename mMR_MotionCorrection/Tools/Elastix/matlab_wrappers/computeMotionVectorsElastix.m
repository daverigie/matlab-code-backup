function [defFieldPath] = computeMotionVectorsElastix(fixed_image_nii, ...
                                          moving_image_nii, ...
                                          outputDir, ...
                                          paramfile, varargin)
                                      
    %% Run elastix registration

    parm_elastix.parameterFilePath = paramfile;
    parm_elastix.outputDir = outputDir;

    elastix(fixed_image_nii, moving_image_nii, parm_elastix);

    %% Create deformation field

    transformParameterFile = getTransformParameterFile(outputDir);
    parm_transformix.inputImage = moving_image_nii;
    parm_transformix.deformPoints = 'all';

    transformix(outputDir, transformParameterFile, parm_transformix);   
    
    defFieldPath = getDefFieldPath(outputDir);
    
end

function p = getTransformParameterFile(outputDir)

    p = fullfile(outputDir, 'TransformParameters.0.txt');

end

function p = getDefFieldPath(outputDir)

    p = fullfile(outputDir, 'deformationField.nii');

end