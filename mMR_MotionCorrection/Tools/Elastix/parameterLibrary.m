function p = parameterLibrary(searchstring)

    % Use search string like 'library::default' to find parameter file
    % called "parameters_default.txt" inside the "parameter_files"
    % directory
    
    THISDIR = fileparts(mfilename('fullpath'));
    
    [tf, name] = validateSearchString(searchstring);
    
    if tf == false
        error('Invalid Parameter Specification');
    end
    
    p = fullfile(THISDIR, 'parameter_files', sprintf('%s.txt', name));
        
    if ~exist(p, 'file')
        warning('Parameter file does not exist! Using library::default');
        p = fullfile(THISDIR, 'parameter_files', sprintf('%s.txt', 'default'));
    end
    

end

function [tf, name] = validateSearchString(searchstring)

    valid_chars = '[\w-\.]';
    matches = regexpi(searchstring, ...
                     sprintf('library::(%s*)', valid_chars), 'tokens');
    
    tf = (numel(matches) == 1);
    
    if tf == true
        name = matches{1}{1};
    else
        name = [];
    end
    
end