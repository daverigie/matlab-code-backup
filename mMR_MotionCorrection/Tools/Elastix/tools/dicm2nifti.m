function [] = dicm2nifti(src, outputFolder)

    % This is a wrapper by Dave around dicm2nii that renames the files
    % according to the directory names for dicoms. It also supports a
    % filelist for multiple dirs
    
    if iscell(src)
        for i = 1:1:numel(src)
            dicm2nifti(src{i}, outputFolder);
        end
        return;
    end
    
    [~,src_filename,~] = fileparts(src);
    tempdirname = tempname;
    
    fmt          = '.nii';
    dicm2nii(src, tempdirname, fmt);
    
    % Find .nii file created in tempdirname
    filelist = regexpdir(tempdirname, '\.nii', 1)
    outfilepath = fullfile(outputFolder, [src_filename '.nii'])
    copyfile(filelist(1).path, outfilepath);
    rmdir(tempdirname, 's');

end