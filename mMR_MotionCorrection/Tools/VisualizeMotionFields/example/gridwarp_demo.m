VW = VolumeWarper(fullfile(pwd,'vectors'));
close all;

alpha = 0.05;
volTarget = data(:,:,:,1);

for g = [10]

    [U,V,W] = VW.getMotionField(g, false);

    vol = data(:,:,:,g);
    gridVol = makegrid3d(size(U),[8,1,8]);
    
    
    v = alpha*gridVol + (1-alpha)*mat2gray(vol);
    
    vWarp = warpVolume(v, U,V,W);
    
    imageviewer(vWarp);
    
    
end
