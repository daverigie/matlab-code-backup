VW = VolumeWarper(fullfile(pwd,'vectors'));
close all;
for g = [10]
    figure;
    title(sprintf('Gate %i', g));
    [U,V,W] = VW.getMotionField(g, false);

    stepSize = 4;
    slc = @(x) flipud(squeeze(x(1:stepSize:end, 96, 1:stepSize:end))');

    
    imshow(squeeze(data(:,96,:,1)));
    hold on;
    quiver(squeeze(U(:,96,:)), squeeze(V(:,96,:)));
end
