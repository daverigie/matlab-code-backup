tt = linspace(0,2,704);

kmax = 1.0;
f1 = 4.0;
f2 = 3.0;

[kx,ky,kz] = generateRosetteTraj(kmax,f1,f2,tt);

plot(kx,ky);
