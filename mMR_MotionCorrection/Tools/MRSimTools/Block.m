classdef Block < PhantomPrimitive
    
    properties
        
        id;
        
        x0;
        y0;
        z0;
        
        wx;
        wy;
        wz;
        
        A      = 1;                
    end
    
    methods
        function this = Block(varargin)
           
        %//////////////////////////////////////////////////////////////////////////
        %// ------------------ PARSE INPUT OPTIONS ------------------------------//
        %//////////////////////////////////////////////////////////////////////////
        p = inputParser;
 
        p.addParameter('id', 'None');
        
        p.addParameter('x0', 0);
        p.addParameter('y0', 0);
        p.addParameter('z0', 0);
        
        p.addParameter('wx', 1);
        p.addParameter('wy', 1);
        p.addParameter('wz', 1);
        
        p.addParameter('A', 1);
   
        parse(p, varargin{:});
        
        this.id = p.Results.id;
        
        this.x0 = p.Results.x0;
        this.y0 = p.Results.y0;
        this.z0 = p.Results.z0;
        
        this.wx = p.Results.wx;
        this.wy = p.Results.wy;
        this.wz = p.Results.wz;
        
        this.A  = p.Results.A;

        end
        
        function val = measureFT(this, kx, ky, kz)

            % blockParams is a struct with fields:
            %   x0 y0 z0 - centroid
            %   wx wy wz - width in 3 dimensions
            %   A      - constant scale factor

            val = this.A*block3dFT(kx, this.x0, this.wx, ...
                                         ky, this.y0, this.wy, ...
                                         kz, this.z0, this.wz);

        end
        
        function val = getVolume(this)
           
            val = this.wx*this.wy*this.wz;
            
        end
        
        function this = scaleVolume(this, scaleFactor)
           scaleFactor = scaleFactor^(1/3);
           this.wx = this.wx*scaleFactor;
           this.wy = this.wy*scaleFactor;
           this.wz = this.wz*scaleFactor;
        end
        
    end
    
end

