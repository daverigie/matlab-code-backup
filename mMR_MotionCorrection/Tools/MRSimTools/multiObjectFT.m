function val = multiObjectFT(ObjArr, kx, ky, kz)

    val = 0.0;
    for Obj = ObjArr
        val = val + Obj.measureFT(kx,ky,kz);
    end


end