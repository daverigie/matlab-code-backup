clear; clc;

%% Make dynamic phantom
DP = makeRespCardPhantom();
complexNoise = @(sigma, sz) complex(normrnd(0, sigma, sz), normrnd(0, sigma, sz));
NOISE_LEVEL = 1.0e-2;



%% Make stack of stars traj
nCol   = 512;
nSlice = 48;
nView  = 800;
nPar   = 37;
fovXY  = 349*2;
fovZ   = 360;

[img.kx, img.ky, img.kz] = stackOfStarsTraj(nCol, nSlice, nView, nPar, fovXY, fovZ);
img.kx = reshape(img.kx, nCol, []);
img.ky = reshape(img.ky, nCol, []);
img.kz = reshape(img.kz, nCol, []);

img.data = zeros([nCol, nPar, nView]);

%% Load Navigator trajectory
scaleFactor = 1.0e-3;
nav = load('rosetteTraj_kx_ky.mat');
nav.kz = nav.kx*0.0;
nav.data = zeros([length(nav.kz), nPar, nView]);

% Scale overall magnitude to match appropriate bandwidth
for fieldName = {'kx', 'ky', 'kz'};
   fld = fieldName{1};
   nav = setfield(nav, fld, scaleFactor*getfield(nav, fld));
end

%% Check navigator

data = multiObjectFT(DP(0), nav.kx, nav.ky, nav.kz);
plot(abs(fftshift(fft(data))));
%% Perform imaging

NAcq = size(img.kx, 2);
img.data = zeros([nCol, nPar, nView]);
TR = 12.7;
t  = 0;

for i = 1:1:NAcq
    
    fprintf('\nAcq %i of %i', i, NAcq);
    phant = DP(t);
    
    nav.data(:, i) = multiObjectFT(phant, nav.kx, nav.ky, nav.kz);
    nav.data(:, i) = nav.data(:, i) + complexNoise(NOISE_LEVEL, size(nav.data(:,i)));
    
    img.data(:, i) = multiObjectFT(phant, img.kx(:,i), img.ky(:,i), img.kz(:,i));
    img.data(:, i) = img.data(:, i) + complexNoise(NOISE_LEVEL, size(img.data(:,i)));
    
    t = t+TR;    
end


%% Analyze navigator for motion signals

X = nav.data(:,:);
X = cat(1,real(X),imag(X));

numSamples      = 5000;
idx             = randi([1000,size(X,2)], [1, numSamples]);
Xs              = X(:, idx);

[~, V, d]  = pcaSignals(Xs, 'lastEig', 100);

pcasig = V'*X;
Sb = sobi(pcasig(1:20,:),250);

