
%% Create two 3D block objets that can move at different rates
% Spatial units are millimeters

largeBox = Block('x0y0z0', [0,0,0], ...
                 'dxdydz', [200,200,200], ...
                 'A',      1);

smallBox = Block('x0y0z0', [50,100,5], ...
                 'dxdydz', [75,75,75], ...
                 'A',      1);
             
BlockArr = [smallBox, largeBox];

%% Load trajectory
MF = load('rosetteTraj_kx_ky.mat');
trajScaleFactor = 0.001;

nav.kx = trajScaleFactor*MF.kx; 
nav.ky = trajScaleFactor*MF.ky;
nav.kz = trajScaleFactor*nav.ky*0.0;

%% 3D stack of stars trajectory

rhoMax = 1;

nCol   = 512;
nSlice = 48;
nView  = 800;
nPar   = 37;

[traj3d, densCompen] = goldenAngleStackOfStars(nCol, nSlice, nView, nPar);
traj3d = traj3d*0.5;

img.kx = traj3d(:,1);
img.ky = traj3d(:,2);
img.kz = traj3d(:,3)/8;

%% Simulate motion

load physio_templates.mat;
respFun = resp;
cardFun = card;

TR       = 12.7e-3; % seconds


numTimesteps = 29600;

nav.data = single(zeros([704, nPar, nView, 1]));
img.data = single(zeros([nCol, nPar, nView, 1]));


for i = 1:1:numTimesteps   
    
    idx = i;
    fprintf('\ntimestep %i of %i', i, numTimesteps);
        
    largeBox.x0y0z0 = [10,14,7]*respFun(idx);
    largeBox.dxdydz = [10,14,7]*respFun(idx) + [200,200,200];
                   
    smallBox.x0y0z0 = [4,8,4]*cardFun(idx) + [50, 150, 5];
    smallBox.dxdydz = [4,8,4]*cardFun(idx) + [75,75,75];
    
    complexrnd = @(varargin) complex(normrnd(varargin{:}), normrnd(varargin{:}));
    
    nav.data(:,i) = multiBlockFT(nav.kx,nav.ky,nav.kz,[smallBox, largeBox]) + ...
                0.0*complexrnd(0, 1000, [length(nav.kx),1]);
            
    line_idx = (1:512) + (i-1)*512;
    line.kx = img.kx(line_idx);
    line.ky = img.ky(line_idx);
    line.kz = img.kz(line_idx);
    
    img.data(:,i) = multiBlockFT(line.kx,line.ky,line.kz,[smallBox, largeBox]) + ...
                0.0*complexrnd(0, 1000, [length(line.kx),1]);
    
end

img.data = img.data/mean(abs(img.data(:)));
D = nav.data(:,:);
pcasig = pcaSignals_rsvd(cat(1,real(D),imag(D)),'lastEig',100);
Sb = sobi(pcasig(1:12,:), 250);


