function Obj = transformObject(initObj, motionPath, growthFrac, t)

    % Returns a PhantomPrimitive at time t

    % Motion path is relative to starting point
    
    Obj = initObj;
    
    Obj.x0 = Obj.x0 + motionPath.x0(t);
    Obj.y0 = Obj.y0 + motionPath.y0(t);
    Obj.z0 = Obj.z0 + motionPath.z0(t);
    
    Obj = Obj.scaleVolume(growthFrac(t));
    
       

end