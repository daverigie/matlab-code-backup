classdef (Abstract) PhantomPrimitive < matlab.mixin.Heterogeneous
    %UNTITLED7 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Abstract)
        id
        x0
        y0
        z0
        A
    end
    
    methods (Abstract)
        val = measureFT(this, kx, ky, kz);
        val = getVolume(this);
        val = scaleVolume(this, scaleFactor);
    end
    
end

