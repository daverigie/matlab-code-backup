
function val = block3dFT(kx, x0, wx, ky, y0, wy, kz, z0, wz)

    val = rectFT(kx, x0, wx).*rectFT(ky, y0, wy).*rectFT(kz, z0, wz);
    
end

function val = rectFT(v, x0, w)

    val =  exp(-i()*2*pi*v.*x0).*abs(w).*sinc(w.*v);

end