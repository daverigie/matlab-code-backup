FOV = 700;
N   = 128;
dx  = FOV/N;
xx  = linspace(-0.5*FOV, 0.5*FOV, N);
kk  = getfreqaxis(xx);

[kx, ky, kz] = meshgrid(kk);

DP = makeRespCardPhantom();

Nt = 50;

I3D = zeros([N,N,N,Nt]);

tt = linspace(30000, 40000, Nt);

for i = 1:1:length(tt)
    t = tt(i);
    {i, t}
    val = multiObjectFT(DP(t), kx, ky, kz);
    I   = abs(fftshift(ifftn(ifftshift(val))));
    I3D(:,:,:,i) = I;
end

%% Make GIF

II = mat2gray(flipud(squeeze(sum(I3D,2))));
framegen = @(i) II(:,:,i);
writeGIF(framegen, 'phantom3.gif', 1:size(I3D,4), 'frameDelay', 0.2);

%% Make GIF Slice
II = mat2gray(flipud(squeeze(I3D(:,:,end/2,:))));
framegen = @(i) II(:,:,i);
writeGIF(framegen, 'phantom2.gif', 1:size(I3D,4), 'frameDelay', 0.2);