#include <iostream>

using namespace std;

#include "main/optical_flow.h"
#include "tools/image.h"
#include "io/parameter_list.h"

int main()
{
    parameter_list ParameterList;
    ParameterList.add_parameter_double("OF_ALGO",4);
    ParameterList.add_parameter_double("SIZE_X",172);
    ParameterList.add_parameter_double("SIZE_Y",172);
    ParameterList.add_parameter_double("SIZE_Z",127);

    image Ia(ParameterList);
    image Ib(ParameterList);

    optical_flow OpticalFlow(ParameterList);
    OpticalFlow.calculate_flow(ParameterList,Ia,Ib);

    cout << "Hello World!" << endl;

    return 0;
}
