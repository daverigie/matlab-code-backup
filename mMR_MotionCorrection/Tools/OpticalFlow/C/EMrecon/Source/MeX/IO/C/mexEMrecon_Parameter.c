/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "mexEMrecon_Parameter.h"


void ReadFromMatlab(const mxArray *prhs[])
{
    const char **fnames;
    mxArray *mxParam;
    
    const mwSize *parm_size;
    mwSize parm_dimensions;
    
    int ifield, nfields;
    double *tmpDouble;
    char tmpChar[255];
    
    /* input must be a structure */
    if(!mxIsStruct(prhs))
    {
        mexErrMsgIdAndTxt("MATLAB:ReadFromMatlab:inputNotStruct","Input must be a structure.");
    }
    
    /* initialize parameter to load default values for all scanner */
    InitializeParameter();
    
    /* get number of structure elements */
    nfields = mxGetNumberOfFields(prhs);
    
    /* get names of structure elements */
    fnames = mxCalloc(nfields, sizeof(*fnames));
    for (ifield=0;ifield<nfields;ifield++){
        fnames[ifield] = mxGetFieldNameByNumber(prhs,ifield);
    }
    
    /* copy structure elements into C parameter structure */
    for(ifield=0;ifield<nfields;ifield++)
    {
        mxParam = mxGetFieldByNumber(prhs,0,ifield);
        
        if (mxIsChar(mxParam))
        {
            SetParameter(fnames[ifield],mxArrayToString(mxParam));
        }
        else
        {
            parm_size = mxGetDimensions(mxParam);
            parm_dimensions = mxGetNumberOfDimensions(mxParam);
            
            /* the value returned by mxGetNumberOfDimensions is always 2 or greater */
            if (parm_dimensions == 2 && parm_size[0] == 1 && parm_size[1] == 1)
            {
                tmpDouble = mxGetPr(mxParam);
                sprintf(tmpChar,"%f",tmpDouble[0]);
                SetParameter(fnames[ifield],tmpChar);
            }
            else
            {
                if (parm_dimensions == 2 && parm_size[1] == 1)
                {
                    SetParameterVector(fnames[ifield],mxGetPr(mxParam),parm_size[0]);
                }
                else
                {
                    mexPrintf("MATLAB:ReadFromMatlab input type not supported [%s]\n",fnames[ifield]);
                }
            }
        }
    }
    mxFree(fnames);
    
    /* scannertype has to be set before running anything within EMrecon */
    Parameter.scannertype = UpdateParameterInt("SCANNERTYPE",Parameter.scannertype);
    
    /* log output is disabled (besides 0/1) by default */
    Parameter.verbose = UpdateParameterInt("VERBOSE",Parameter.verbose);
    
    /* protect scanner information (allow no log or output) by using a fixed verbose level */
#ifdef PROTECT_SCANNER_INFO
    Parameter.verbose = 0;
#endif
}
