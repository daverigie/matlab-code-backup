/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#ifndef TOOLS_H
#define TOOLS_H


/* includes */
#ifndef WINDOWS_MATLAB
#include <omp.h>
#endif
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>


/* emrecon includes */
#include "../IO/Parameter.h"


/* define macros */
#ifndef WINDOWS_MATLAB
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#else
#define M_PI 3.141592653589793
#endif


/* struct definitions */
typedef struct {
    int coord;
    double length;
} path_element;

typedef struct {
    double x1, y1, z1, x2, y2, z2;
    int valid;
} event;




/* function declarations */
void convolve(double *in, double *out);
void cut_image(double *f);
void *safe_calloc(long num, int size);
path_element *safe_calloc_path();
int safe_omp_get_num_threads();
int safe_omp_get_thread_num();

void tv(double *in, double *out, double eps);
void tv_2d(double *in, double *out, double eps);
double tv_2d_u(double *in, int x, int y, double eps);
void tv_3d(double *in, double *out, double eps);
double tv_3d_u(double *in, int x, int y, int z, double eps);


#endif
