%% clear and close
close all; clear; clc;


%% define compile targets
compile_mexOF_PartialDerivatives_2D = 0;
compile_mexOF_PartialDerivatives_3D = 0;
compile_mexOF_Laplacian_2D = 0;
compile_mexOF_Laplacian_3D = 0;
compile_mexOF_OpticalFlow_2D = 1;
compile_mexOF_OpticalFlow_3D = 1;


%% add scripts
add_OF_ApplyMotionCorrection_2D = 1;
add_OF_ApplyMotionCorrection_3D = 1;
add_OF_ConcatMotionVectors_2D = 1;
add_OF_ConcatMotionVectors_3D = 1;
add_OF_OpticalFlowMultiLevel_2D = 1; % requires compile_mexOF_OpticalFlow_2D = 1
add_OF_OpticalFlowMultiLevel_3D = 1; % requires compile_mexOF_OpticalFlow_3D = 1
add_OF_ResampleImage = 1;


%% set path to corresponding source code and compiler flags
% set location of source code (has to be defined once)
os_source_windows = 'G:\OpticalFlow\trunk\C\MoCo\Source\MeX';

% set cflags
cflags_windows = '-DWINDOWS_MATLAB COPTIMFLAGS=-O3 -DNDEBUG';
cflags_linux = 'CFLAGS=''$CFLAGS -w -fopenmp'' -lgomp COP/TIMFLAGS=-O3 -DNDEBUG';


%% detect OS
if (ispc == 0 && isunix == 1)
    os = 'linux';
elseif (ispc == 1 && isunix == 0)
    os = 'windows';
else
    error('cannot detect os');
end


%% set os_source and mex compiler options with regard to the os
if (strcmp(os,'linux') == 1)
    fprintf('compiling mexOpticalFlow for linux...\n');
    os_source = os_source_linux;
    cflags = cflags_linux;
else
    fprintf('compiling mexOpticalFlow for windows...\n');
    os_source = os_source_windows;
    cflags = cflags_windows;
end
compile_tic = tic;

% cd to source directory
cd(os_source);


%% compile mex functions
% matlab mex interface id
mex_id = 0;

% add main EMrecon components
mex_cmd_main = ' ../../../EMrecon/Source/MeX/IO/C/mexEMrecon_Parameter.c';
mex_cmd_main = [mex_cmd_main ' ../../../EMrecon/Source/IO/Parameter.c'];
mex_cmd_main = [mex_cmd_main ' ../../../EMrecon/Source/IO/IO.c'];
mex_cmd_main = [mex_cmd_main ' ../../../EMrecon/Source/Main/Tools.c'];

% enable / disable debug output
if (0)
    mex_cmd_main = [mex_cmd_main ' -DEMRECON_DEBUG'];
end

% setup outdir
mex_cmd_main = [mex_cmd_main ' -outdir ../../Bin'];


%% mexOF_PartialDerivatives_2D
if (compile_mexOF_PartialDerivatives_2D == 1)
    fprintf('compiling mexOF_PartialDerivatives_2D...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' mexOF_PartialDerivatives_2D.c'];
    mex_cmd = [mex_cmd ' ../Main/PartialDerivatives.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_PartialDerivatives_2D';
    
    fprintf('compiling mexOF_PartialDerivatives_2D...done in %2.5f secs.\n',toc);
end


%% mexOF_PartialDerivatives_3D
if (compile_mexOF_PartialDerivatives_3D == 1)
    fprintf('compiling mexOF_PartialDerivatives_3D...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' mexOF_PartialDerivatives_3D.c'];
    mex_cmd = [mex_cmd ' ../Main/PartialDerivatives.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_PartialDerivatives_3D';
    
    fprintf('compiling mexOF_PartialDerivatives_3D...done in %2.5f secs.\n',toc);
end


%% mexOF_OpticalFlow_2D
if (compile_mexOF_OpticalFlow_2D == 1)
    fprintf('compiling mexOF_OpticalFlow_2D...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' mexOF_OpticalFlow_2D.c'];
    mex_cmd = [mex_cmd ' ../Main/OpticalFlow.c'];
    mex_cmd = [mex_cmd ' ../Main/OpticalFlow_B.c'];
    mex_cmd = [mex_cmd ' ../Main/OpticalFlow_BNQ.c'];
    mex_cmd = [mex_cmd ' ../Main/OpticalFlow_HS.c'];
    %     mex_cmd = [mex_cmd ' ../Main/OpticalFlow_HS_V2.c'];
    %     mex_cmd = [mex_cmd ' ../Main/GradientNormSquare.c'];
    mex_cmd = [mex_cmd ' ../Main/PartialDerivatives.c'];
    mex_cmd = [mex_cmd ' ../Main/Laplacian.c'];
    mex_cmd = [mex_cmd ' ../Main/WeightedAverage.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_OpticalFlow_2D';
    
    fprintf('compiling mexOF_OpticalFlow_2D...done in %2.5f secs.\n',toc);
end


%% mexOF_OpticalFlow_3D
if (compile_mexOF_OpticalFlow_3D == 1)
    fprintf('compiling mexOF_OpticalFlow_3D...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' mexOF_OpticalFlow_3D.c'];
    mex_cmd = [mex_cmd ' ../Main/OpticalFlow.c'];
    mex_cmd = [mex_cmd ' ../Main/OpticalFlow_B.c'];
    mex_cmd = [mex_cmd ' ../Main/OpticalFlow_BNQ.c'];
    mex_cmd = [mex_cmd ' ../Main/OpticalFlow_HS.c'];
    %     mex_cmd = [mex_cmd ' ../Main/OpticalFlow_HS_V2.c'];
    %     mex_cmd = [mex_cmd ' ../Main/GradientNormSquare.c'];
    mex_cmd = [mex_cmd ' ../Main/PartialDerivatives.c'];
    mex_cmd = [mex_cmd ' ../Main/Laplacian.c'];
    mex_cmd = [mex_cmd ' ../Main/WeightedAverage.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_OpticalFlow_3D';
    
    fprintf('compiling mexOF_OpticalFlow_3D...done in %2.5f secs.\n',toc);
end


%% mexOF_Laplacian_2D
if (compile_mexOF_Laplacian_2D == 1)
    fprintf('compiling mexOF_Laplacian_2D...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' mexOF_Laplacian_2D.c'];
    mex_cmd = [mex_cmd ' ../Main/Laplacian.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_Laplacian_2D';
    
    fprintf('compiling mexOF_Laplacian_2D...done in %2.5f secs.\n',toc);
end


%% mexOF_Laplacian_3D
if (compile_mexOF_Laplacian_3D == 1)
    fprintf('compiling mexOF_Laplacian_3D...\n');
    tic;
    
    mex_cmd = ['mex ' cflags];
    mex_cmd = [mex_cmd ' mexOF_Laplacian_3D.c'];
    mex_cmd = [mex_cmd ' ../Main/Laplacian.c'];
    mex_cmd = [mex_cmd mex_cmd_main];
    
    disp(mex_cmd);
    eval(mex_cmd);
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_Laplacian_3D';
    
    fprintf('compiling mexOF_Laplacian_3D...done in %2.5f secs.\n',toc);
end


%% add_OF_ApplyMotionCorrection_2D
if (add_OF_ApplyMotionCorrection_2D == 1)
    fprintf('adding script OF_ApplyMotionCorrection_2D...\n');
    tic;
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_ApplyMotionCorrection_2D';
    
    fprintf('adding script OF_ApplyMotionCorrection_2D...done in %2.5f secs.\n',toc);
end


%% add_OF_ApplyMotionCorrection_3D
if (add_OF_ApplyMotionCorrection_3D == 1)
    fprintf('adding script OF_ApplyMotionCorrection_3D...\n');
    tic;
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_ApplyMotionCorrection_3D';
    
    fprintf('adding script OF_ApplyMotionCorrection_3D...done in %2.5f secs.\n',toc);
end


%% add_OF_ConcatMotionVectors_2D
if (add_OF_ConcatMotionVectors_2D == 1)
    fprintf('adding script OF_ConcatMotionVectors_2D...\n');
    tic;
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_ConcatMotionVectors_2D';
    
    fprintf('adding script OF_ConcatMotionVectors_2D...done in %2.5f secs.\n',toc);
end


%% add_OF_ConcatMotionVectors_3D
if (add_OF_ConcatMotionVectors_3D == 1)
    fprintf('adding script OF_ConcatMotionVectors_3D...\n');
    tic;
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_ConcatMotionVectors_3D';
    
    fprintf('adding script OF_ConcatMotionVectors_3D...done in %2.5f secs.\n',toc);
end


%% add_OF_OpticalFlowMultiLevel_2D
if (add_OF_OpticalFlowMultiLevel_2D == 1)
    fprintf('adding script OF_OpticalFlowMultiLevel_2D...\n');
    tic;
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_OpticalFlowMultiLevel_2D';
    
    fprintf('adding script OF_OpticalFlowMultiLevel_2D...done in %2.5f secs.\n',toc);
end


%% add_OF_OpticalFlowMultiLevel_3D
if (add_OF_OpticalFlowMultiLevel_3D == 1)
    fprintf('adding script OF_OpticalFlowMultiLevel_3D...\n');
    tic;
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_OpticalFlowMultiLevel_3D';
    
    fprintf('adding script OF_OpticalFlowMultiLevel_3D...done in %2.5f secs.\n',toc);
end

% add_OF_ResampleImage
if (add_OF_ResampleImage == 1)
    fprintf('adding script OF_ResampleImage...\n');
    tic;
    
    mex_id = mex_id + 1;
    OFMatlabTools(mex_id).name = 'OF_ResampleImage';
    
    fprintf('adding script OF_ResampleImage...done in %2.5f secs.\n',toc);
end


%% protect corresponding Matlab scripts
if (exist('OFMatlabTools','var') == 1)
    % cd to Matlab directory
    cd('Matlab');
    
    % protect scripts
    for i=1:numel(OFMatlabTools)
        fprintf('pcode %s.m\n',OFMatlabTools(i).name);
        pcode(sprintf('%s.m',OFMatlabTools(i).name));
    end
    
    % move scripts
    for i=1:numel(OFMatlabTools)
        fprintf('mv %s.p ../../../Bin\n',OFMatlabTools(i).name);
        dos(sprintf('mv %s.p ../../../Bin',OFMatlabTools(i).name));
    end
    
    % return to source directory
    cd(os_source);
end

%% plot time used for compiling
if (strcmp(os,'linux') == 1)
    fprintf('compiling mexOpticalFlow for linux...done in %2.5f secs.\n',toc(compile_tic));
else
    fprintf('compiling mexOpticalFlow for windows...done in %2.5f secs.\n',toc(compile_tic));
end
