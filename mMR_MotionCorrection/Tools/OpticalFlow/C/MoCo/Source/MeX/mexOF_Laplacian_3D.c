/* global includes */
#include "mex.h"
#include "string.h"
#ifndef WINDOWS_MATLAB
#include <omp.h>
#endif


/* OF includes */
#include "mexOF_Version.h"


/* EMrecon includes */
#include "../../../EMrecon/Source/MeX/Parameter_MEX.h"


/*  the gateway routine. */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int valid = 1;
    const mwSize *image_size;
    double *output_dimensions;
    double *u,*v,*w,*u_Laplacian,*v_Laplacian,*w_Laplacian;
    
    /* check for valid input/output parameter */
    if (nlhs != 4)
    {
        mexPrintf("mexOF_Laplacian_3D [%s]\n",OF_VERSION);
        mexPrintf("using EMrecon [%s]\n",EMRECON_VERSION);
    }
    else
    {
        /* initialize default parameter */
        SetParameter("SIZE_X","0");
        SetParameter("SIZE_Y","0");
        SetParameter("SIZE_Z","0");
        SetParameter("FOV_X","0.0");
        SetParameter("FOV_Y","0.0");
        SetParameter("FOV_Z","0.0");
        
        
        /* transfer parameter from Matlab structure to EMrecon parameter structure */
        ReadFromMatlab(prhs[0]);
        
        
        /* update current parameter list */
        UpdateParameterList();
        
        
        /* validate x dimension */
        if (Parameter.size_x <= 0 || Parameter.fov_x <= 0)
        {
            mexPrintf("mexOF_Laplacian_3D: x Dimension error, size: [%d], fov: [%f]\n",Parameter.size_x,Parameter.fov_x);
            valid = 0;
        }
        
        /* validate y dimension */
        if (Parameter.size_y <= 0 || Parameter.fov_y <= 0)
        {
            mexPrintf("mexOF_Laplacian_3D: y Dimension error, size: [%d], fov: [%f]\n",Parameter.size_y,Parameter.fov_y);
            valid = 0;
        }
        
        /* validate z dimension */
        if (Parameter.size_z <= 0 || Parameter.fov_z <= 0)
        {
            mexPrintf("mexOF_Laplacian_3D: z Dimension error, size: [%d], fov: [%f]\n",Parameter.size_z,Parameter.fov_z);
            valid = 0;
        }
        
        /* validate image size 1 */
        image_size = mxGetDimensions(prhs[1]);
        if (image_size[0] != Parameter.imagesize)
        {
            mexPrintf("mexOF_Laplacian_3D: Dimensions do not match [%d] != [%d] (u)\n",Parameter.imagesize,image_size[0]);
            valid = 0;
        }
        
        /* validate image size 2 */
        image_size = mxGetDimensions(prhs[2]);
        if (image_size[0] != Parameter.imagesize)
        {
            mexPrintf("mexOF_Laplacian_3D: Dimensions do not match [%d] != [%d] (v)\n",Parameter.imagesize,image_size[0]);
            valid = 0;
        }
        
        /* validate image size 3 */
        image_size = mxGetDimensions(prhs[3]);
        if (image_size[0] != Parameter.imagesize)
        {
            mexPrintf("mexOF_Laplacian_3D: Dimensions do not match [%d] != [%d] (w)\n",Parameter.imagesize,image_size[0]);
            valid = 0;
        }
        
        /* if input data is invalid */
        if (valid == 0)
        {
            plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            u_Laplacian = mxGetPr(plhs[0]);
            u_Laplacian[0] = -1.0;
            
            plhs[1] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            v_Laplacian = mxGetPr(plhs[1]);
            v_Laplacian[0] = -1.0;
            
            plhs[2] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            w_Laplacian = mxGetPr(plhs[2]);
            w_Laplacian[0] = -1.0;
            
            plhs[3] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[3]);
            output_dimensions[0] = -1.0;
        }
        else
        {
            mexPrintf("mexOF_Laplacian_3D...\n");
            
            /* get pointer to input data */
            u = mxGetData(prhs[1]);
            v = mxGetData(prhs[2]);
            w = mxGetData(prhs[3]);
            
            /* output */
            plhs[0] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            u_Laplacian = mxGetPr(plhs[0]);
            
            plhs[1] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            v_Laplacian = mxGetPr(plhs[1]);
            
            plhs[2] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            w_Laplacian = mxGetPr(plhs[2]);
            
            /* dimensions of the output (might be used to reshape the 1d output vector) */
            plhs[3] = mxCreateNumericMatrix(3,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[3]);
            output_dimensions[0] = (double)Parameter.size_x;
            output_dimensions[1] = (double)Parameter.size_y;
            output_dimensions[2] = (double)Parameter.size_z;
            
            /* derive laplacian in 3D */
            Laplacian_3D(u,v,w,u_Laplacian,v_Laplacian,w_Laplacian);
            
            mexPrintf("mexOF_Laplacian_3D...done.\n");
        }
    }
}
