/* global includes */
#include "mex.h"
#include "string.h"
#ifndef WINDOWS_MATLAB
#include <omp.h>
#endif


/* OF includes */
#include "mexOF_Version.h"


/* EMrecon includes */
#include "../../../EMrecon/Source/MeX/Parameter_MEX.h"


/*  the gateway routine. */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int valid = 1;
    const mwSize *image_size;
    double *output_dimensions;
    double *Ix,*Iy,*Iz,*It,*Ia,*Ib;
    
    /* check for valid input/output parameter */
    if (nlhs != 5)
    {
        mexPrintf("mexOF_PartialDerivatives_3D [%s]\n",OF_VERSION);
        mexPrintf("using EMrecon [%s]\n",EMRECON_VERSION);
    }
    else
    {
        /* initialize default parameter */
        SetParameter("SIZE_X","0");
        SetParameter("SIZE_Y","0");
        SetParameter("SIZE_Z","0");
        SetParameter("FOV_X","0.0");
        SetParameter("FOV_Y","0.0");
        SetParameter("FOV_Z","0.0");
        
        
        /* transfer parameter from Matlab structure to EMrecon parameter structure */
        ReadFromMatlab(prhs[0]);
        
        
        /* update current parameter list */
        UpdateParameterList();
        
        
        /* validate x dimension */
        if (Parameter.size_x <= 0 || Parameter.fov_x <= 0)
        {
            mexPrintf("mexOF_PartialDerivatives_3D: x Dimension error, size: [%d], fov: [%f]\n",Parameter.size_x,Parameter.fov_x);
            valid = 0;
        }
        
        /* validate y dimension */
        if (Parameter.size_y <= 0 || Parameter.fov_y <= 0)
        {
            mexPrintf("mexOF_PartialDerivatives_3D: y Dimension error, size: [%d], fov: [%f]\n",Parameter.size_y,Parameter.fov_y);
            valid = 0;
        }
        
        /* validate z dimension */
        if (Parameter.size_z <= 0 || Parameter.fov_z <= 0)
        {
            mexPrintf("mexOF_PartialDerivatives_3D: z Dimension error, size: [%d], fov: [%f]\n",Parameter.size_z,Parameter.fov_z);
            valid = 0;
        }
        
        /* validate image size 1 */
        image_size = mxGetDimensions(prhs[1]);
        if (image_size[0] != Parameter.imagesize)
        {
            mexPrintf("mexOF_PartialDerivatives_3D: Dimensions do not match [%d] != [%d] (Ia)\n",Parameter.imagesize,image_size[0]);
            valid = 0;
        }
        
        /* validate image size 2 */
        image_size = mxGetDimensions(prhs[2]);
        if (image_size[0] != Parameter.imagesize)
        {
            mexPrintf("mexOF_PartialDerivatives_3D: Dimensions do not match [%d] != [%d] (Ib)\n",Parameter.imagesize,image_size[0]);
            valid = 0;
        }
        
        /* if input data is invalid */
        if (valid == 0)
        {
            plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            Ix = mxGetPr(plhs[0]);
            Ix[0] = -1.0;
            
            plhs[1] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            Iy = mxGetPr(plhs[1]);
            Iy[0] = -1.0;
            
            plhs[2] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            Iz = mxGetPr(plhs[2]);
            Iz[0] = -1.0;
            
            plhs[3] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            It = mxGetPr(plhs[3]);
            It[0] = -1.0;
            
            plhs[4] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[4]);
            output_dimensions[0] = -1.0;
        }
        else
        {
            mexPrintf("mexOF_PartialDerivatives_3D...\n");
            
            /* get pointer to input data */
            Ia = mxGetData(prhs[1]);
            Ib = mxGetData(prhs[2]);
            
            /* output */
            plhs[0] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            Ix = mxGetPr(plhs[0]);
            
            plhs[1] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            Iy = mxGetPr(plhs[1]);
            
            plhs[2] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            Iz = mxGetPr(plhs[2]);
            
            plhs[3] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            It = mxGetPr(plhs[3]);
            
            /* dimensions of the output (might be used to reshape the 1d output vector) */
            plhs[4] = mxCreateNumericMatrix(3,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[4]);
            output_dimensions[0] = (double)Parameter.size_x;
            output_dimensions[1] = (double)Parameter.size_y;
            output_dimensions[2] = (double)Parameter.size_z;
            
            /* derive partial derivatives in 3D */
            PartialDerivativesTemporal_3D(Ia,Ib,Ix,Iy,Iz,It);
            
            mexPrintf("mexOF_PartialDerivatives_3D...done.\n");
        }
    }
}
