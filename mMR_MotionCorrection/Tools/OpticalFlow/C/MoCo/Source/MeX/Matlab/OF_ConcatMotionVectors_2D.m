function [concat_u,concat_v] = OF_ConcatMotionVectors_2D(parm,u1,v1,u2,v2)

% OF_ConcatMotionVectors_2D(parm,u1,v1,u2,v2)

if (nargin ~= 5 || nargout ~= 2)
    fprintf('\nOF_ConcatMotionVectors_2D\n');
    fprintf('Usage: [concat_u,concat_v] = OF_ConcatMotionVectors_2D(parm,u1,v1,u2,v2)\n\n');
    fprintf('\n');
    concat_u = -1;
    concat_v = -1;
else
    fprintf('OF_ConcatMotionVectors_2D...\n');
    tic;
    
    dx = parm.FOV_X / parm.SIZE_X;
    dy = parm.FOV_Y / parm.SIZE_Y;
    
    if (mod(parm.SIZE_X,2) == 0)
        xgrid = -(parm.SIZE_X/2.0-0.5)*dx:dx:(parm.SIZE_X/2.0-0.5)*dx;
    else
        xgrid = -((parm.SIZE_X-1)/2.0)*dx:dx:((parm.SIZE_X-1)/2.0)*dx;
    end
    
    if (mod(parm.SIZE_Y,2) == 0)
        ygrid = -(parm.SIZE_Y/2.0-0.5)*dy:dy:(parm.SIZE_Y/2.0-0.5)*dy;
    else
        ygrid = -((parm.SIZE_Y-1)/2.0)*dy:dy:((parm.SIZE_Y-1)/2.0)*dy;
    end
    
    [a,b] = meshgrid(ygrid,xgrid);
    x = u2 + b;
    y = v2 + a;
    
    % x direction
    concat_u = interp2(a,b,u1+b,y,x,'linear');
    concat_u(isnan(concat_u)) = 0;
    concat_u = concat_u - b;
    
    % y direction
    concat_v = interp2(a,b,v1+a,y,x,'linear');
    concat_v(isnan(concat_v)) = 0;
    concat_v = concat_v - a;
    
    fprintf('OF_ConcatMotionVectors_2D...done. [%f sec]\n',toc);
end

end