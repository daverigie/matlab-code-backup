function [u_Avg,v_Avg] = OF_Laplacian_2D(parm,u,v)

% OF_Laplacian_2D(parm,u,v)

if (nargin ~= 3 || nargout ~= 2)
    fprintf('\nOF_Laplacian_2D\n');
    fprintf('Usage: [u_Avg,v_Avg] = OF_Laplacian_2D(parm,u,v)\n\n');
    mexOF_Laplacian_2D;
    fprintf('\n');
    u_Avg = -1;
    v_Avg = -1;
else
    fprintf('OF_Laplacian_2D...\n');
    tic;
    u = reshape(u,numel(u),1);
    v = reshape(v,numel(v),1);
    [u_Avg,v_Avg,dims] = mexOF_Laplacian_2D(parm,u,v);
    u_Avg = reshape(u_Avg,dims(1),dims(2),dims(3));
    v_Avg = reshape(v_Avg,dims(1),dims(2),dims(3));
    fprintf('OF_Laplacian_2D...done. [%f sec]\n',toc);
end

end