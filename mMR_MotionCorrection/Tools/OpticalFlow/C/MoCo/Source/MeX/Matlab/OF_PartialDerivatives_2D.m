function [Ix,Iy,It] = OF_PartialDerivatives_2D(parm,Ia,Ib)

% OF_PartialDerivatives_2D(parm,Ia,Ib)

if (nargin ~= 3 || nargout ~= 3)
    fprintf('\nOF_PartialDerivatives_2D\n');
    fprintf('Usage: [Ix,Iy,It] = OF_PartialDerivatives_2D(parm,Ia,Ib)\n\n');
    mexOF_PartialDerivatives_2D;
    fprintf('\n');
    Ix = -1;
    Iy = -1;
    It = -1;
else
    fprintf('OF_PartialDerivatives_2D...\n');
    tic;
    Ia = reshape(Ia,numel(Ia),1);
    Ib = reshape(Ib,numel(Ib),1);
    [Ix,Iy,It,dims] = mexOF_PartialDerivatives_2D(parm,Ia,Ib);
    Ix = reshape(Ix,dims(1),dims(2),dims(3));
    Iy = reshape(Iy,dims(1),dims(2),dims(3));
    It = reshape(It,dims(1),dims(2),dims(3));
    fprintf('OF_PartialDerivatives_2D...done. [%f sec]\n',toc);
end

end