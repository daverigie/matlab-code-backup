function [concat_u,concat_v,concat_w] = OF_ConcatMotionVectors_3D(parm,u1,v1,w1,u2,v2,w2)

% OF_ConcatMotionVectors_3D(parm,u1,v1,w1,u2,v2,w2)

if (nargin ~= 7 || nargout ~= 3)
    fprintf('\nOF_ConcatMotionVectors_3D\n');
    fprintf('Usage: [concat_u,concat_v,concat_w] = OF_ConcatMotionVectors_3D(parm,u1,v1,w1,u2,v2,w2)\n\n');
    fprintf('\n');
    concat_u = -1;
    concat_v = -1;
    concat_w = -1;
else
    fprintf('OF_ConcatMotionVectors_3D...\n');
    tic;
    
    dx = parm.FOV_X / parm.SIZE_X;
    dy = parm.FOV_Y / parm.SIZE_Y;
    dz = parm.FOV_Z / parm.SIZE_Z;
    
    if (mod(parm.SIZE_X,2) == 0)
        xgrid = -(parm.SIZE_X/2.0-0.5)*dx:dx:(parm.SIZE_X/2.0-0.5)*dx;
    else
        xgrid = -((parm.SIZE_X-1)/2.0)*dx:dx:((parm.SIZE_X-1)/2.0)*dx;
    end
    
    if (mod(parm.SIZE_Y,2) == 0)
        ygrid = -(parm.SIZE_Y/2.0-0.5)*dy:dy:(parm.SIZE_Y/2.0-0.5)*dy;
    else
        ygrid = -((parm.SIZE_Y-1)/2.0)*dy:dy:((parm.SIZE_Y-1)/2.0)*dy;
    end
    
    if (mod(parm.SIZE_Z,2) == 0)
        zgrid = -(parm.SIZE_Z/2.0-0.5)*dz:dz:(parm.SIZE_Z/2.0-0.5)*dz;
    else
        zgrid = -((parm.SIZE_Z-1)/2.0)*dz:dz:((parm.SIZE_Z-1)/2.0)*dz;
    end
    
    [a,b,c] = meshgrid(ygrid,xgrid,zgrid);
    x = u2 + b;
    y = v2 + a;
    z = w2 + c;
    
    % x direction
    concat_u = interp3(a,b,c,u1+b,y,x,z,'linear');
    concat_u(isnan(concat_u)) = 0;
    concat_u = concat_u - b;
    
    % y direction
    concat_v = interp3(a,b,c,v1+a,y,x,z,'linear');
    concat_v(isnan(concat_v)) = 0;
    concat_v = concat_v - a;
    
    % z direction
    concat_w = interp3(a,b,c,w1+c,y,x,z,'linear');
    concat_w(isnan(concat_w)) = 0;
    concat_w = concat_w - c;
    
    fprintf('OF_ConcatMotionVectors_3D...done. [%f sec]\n',toc);
end

end