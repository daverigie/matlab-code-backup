#ifndef OPTICALFLOW_BNQ_H
#define OPTICALFLOW_BNQ_H


/* Optical Flow includes */
#include "GradientNormSquare.h"
#include "Laplacian.h"
#include "PartialDerivatives.h"
#include "WeightedAverage.h"


/* EMrecon includes */
#include "../../../EMrecon/Source/IO/Parameter.h"


void OpticalFlow_BNQ_2D(double *Ia, double *Ib, double *u, double *v);
void OpticalFlow_BNQ_3D(double *Ia, double *Ib, double *u, double *v, double *w);

void Update_Psi1_2D(double *Psi1, double *Ia, double *Ib, double *Ix, double *Iy, double *u, double *v);
void Update_Psi1_3D(double *Psi1, double *Ia, double *Ib, double *Ix, double *Iy, double *Iz, double *u, double *v, double *w);

void Update_Psi2_2D(double *Psi2, double *u, double *v);
void Update_Psi2_3D(double *Psi2, double *u, double *v, double *w);


#endif
