#ifndef WEIGHTEDAVERAGE_H
#define WEIGHTEDAVERAGE_H


/* EMrecon includes */
#include "../../../EMrecon/Source/IO/Parameter.h"


void WeightedAverage_2D(double *u, double *v, double *u_Avg, double *v_Avg);
void WeightedAverage_3D(double *u, double *v, double *w, double *u_Avg, double *v_Avg, double *w_Avg);

void WeightedAveragePsi_2D(double *Psi1, double *Psi2, double *u, double *v, double *u_Avg, double *v_Avg);
void WeightedAveragePsi_3D(double *Psi1, double *Psi2, double *u, double *v, double *w, double *u_Avg, double *v_Avg, double *w_Avg);

#endif
