#ifndef OPTICALFLOW_B_H
#define OPTICALFLOW_B_H


/* Optical Flow includes */
#include "Laplacian.h"
#include "PartialDerivatives.h"
#include "WeightedAverage.h"


/* EMrecon includes */
#include "../../../EMrecon/Source/MeX/IO/C/mexEMrecon_Parameter.h"


void OpticalFlow_B_2D(double *Ia, double *Ib, double *u, double *v);
void OpticalFlow_B_3D(double *Ia, double *Ib, double *u, double *v, double *w);


#endif
