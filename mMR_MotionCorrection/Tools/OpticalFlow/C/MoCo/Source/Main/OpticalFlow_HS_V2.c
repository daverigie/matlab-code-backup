#include "OpticalFlow_HS_V2.h"


void OpticalFlow_HS_V2_2D(double *Ia, double *Ib, double *u, double *v)
{
    int iteration,y;
    double *u_Laplacian,*v_Laplacian,*Ix,*Iy,*It;
    double alpha = GetParameterDouble("ALPHA");
    double gradient_threshold = GetParameterDouble("GRADIENT_THRESHOLD");

    u_Laplacian = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    v_Laplacian = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Ix = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    It = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    
    /* calculate partial derivatives wrt x,y,t */
    /*PartialDerivativesTemporal_2D(Ia,Ib,Ix,Iy,It);*/
    /*PartialDerivatives_Spatial*/

    convolve(Ix,Ix);
    convolve(Iy,Iy);
    convolve(It,It);

    /* iteration loop */
    for (iteration=0;iteration<Parameter.iterations;iteration++)
    {
        /* calculate laplacian */
        Laplacian_2D(u,v,u_Laplacian,v_Laplacian);

        /* update u and v */
#pragma omp parallel for
        for (y=0;y<Parameter.size_y;y++)
        {
            int i,x;
            double u_tmp, v_tmp;

            for (x=0;x<Parameter.size_x;x++)
            {
                i = x + y * Parameter.size_x;
                /* images should be scaled to [0,1000], i.e. 10 as min(Ix[i]) seems to be reasonable */
                if (fabs(Ix[i]) > gradient_threshold)
                {
                    u_tmp = (u_Laplacian[i] - 1.0 / alpha * Ix[i] * ((Iy[i]*v[i]) + It[i])) / (1.0 / alpha * Ix[i]*Ix[i]);
                }
                else
                {
                    u_tmp = 0.0;
                }
                /* images should be scaled to [0,1000], i.e. 10 as min(Iy[i]) seems to be reasonable */
                if (fabs(Iy[i]) > gradient_threshold)
                {
                    v_tmp = (v_Laplacian[i] - 1.0 / alpha * Iy[i] * ((Ix[i]*u[i]) + It[i])) / (1.0 / alpha * Iy[i]*Iy[i]);
                }
                else
                {
                    v_tmp = 0.0;
                }

                u[i] = u_tmp;
                v[i] = v_tmp;
            }
        }
    }
    
    free(u_Laplacian);
    free(v_Laplacian);
    free(Ix);
    free(Iy);
    free(It);
}


void OpticalFlow_HS_V2_3D(double *Ia, double *Ib, double *u, double *v, double *w)
{
    int iteration,z;
    double *u_Laplacian,*v_Laplacian,*w_Laplacian,*Ix,*Iy,*Iz,*It;
    double alpha = GetParameterDouble("ALPHA");
    double gradient_threshold = GetParameterDouble("GRADIENT_THRESHOLD");
    
    u_Laplacian = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    v_Laplacian = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    w_Laplacian = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Ix = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iz = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    It = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    
    /* calculate partial derivatives wrt x,y,z,t */
    PartialDerivativesTemporal_3D(Ia,Ib,Ix,Iy,Iz,It);

    /* iteration loop */
    for (iteration=0;iteration<Parameter.iterations;iteration++)
    {
        /* calculate laplacian */
        Laplacian_3D(u,v,w,u_Laplacian,v_Laplacian,w_Laplacian);

        /* update u, v and w */
#pragma omp parallel for
        for (z=0;z<Parameter.size_z;z++)
        {
            int i,x,y;
            double u_tmp, v_tmp, w_tmp;

            for (y=0;y<Parameter.size_y;y++)
            {
                for (x=0;x<Parameter.size_x;x++)
                {
                    i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                    /* images should be scaled to [0,1000], i.e. 10 as min(Ix[i]) seems to be reasonable */
                    if (fabs(Ix[i]) > gradient_threshold)
                    {
                        u_tmp = (u_Laplacian[i] - 1.0 / alpha * Ix[i] * ((Iy[i]*v[i]) + (Iz[i]*w[i]) + It[i])) / (1.0 / alpha * Ix[i]*Ix[i]);
                    }
                    else
                    {
                        u_tmp = 0.0;
                    }
                    /* images should be scaled to [0,1000], i.e. 10 as min(Iy[i]) seems to be reasonable */
                    if (fabs(Iy[i]) > gradient_threshold)
                    {
                        v_tmp = (v_Laplacian[i] - 1.0 / alpha * Iy[i] * ((Ix[i]*u[i]) + (Iz[i]*w[i]) + It[i])) / (1.0 / alpha * Iy[i]*Iy[i]);
                    }
                    else
                    {
                        v_tmp = 0.0;
                    }
                    /* images should be scaled to [0,1000], i.e. 10 as min(Iz[i]) seems to be reasonable */
                    if (fabs(Iz[i]) > gradient_threshold)
                    {
                        w_tmp = (w_Laplacian[i] - 1.0 / alpha * Iz[i] * ((Ix[i]*u[i]) + (Iy[i]*v[i]) + It[i])) / (1.0 / alpha * Iz[i]*Iz[i]);
                    }
                    else
                    {
                        w_tmp = 0.0;
                    }

                    u[i] = u_tmp;
                    v[i] = v_tmp;
                    w[i] = w_tmp;
                }
            }
        }
    }
    
    free(u_Laplacian);
    free(v_Laplacian);
    free(w_Laplacian);
    free(Ix);
    free(Iy);
    free(Iz);
    free(It);
}
