#include "GradientNormSquare.h"


void GradientNormSquare_2D(double *u, double *v, double *GNS)
{
    int y;
    double *ux,*uy,*vx,*vy;

    ux = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    uy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    vx = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    vy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    PartialDerivatives_2D(u,ux,uy);
    PartialDerivatives_2D(v,vx,vy);

#pragma omp parallel for
    for (y=0;y<Parameter.size_y;y++)
    {
        int i,x;

        for (x=0;x<Parameter.size_x;x++)
        {
            i = x + y * Parameter.size_x;
            GNS[i] = ux[i]*ux[i] + uy[i]*uy[i] + vx[i]*vx[i] + vy[i]*vy[i];
        }
    }

    free(ux);
    free(uy);
    free(vx);
    free(vy);
}


void GradientNormSquare_3D(double *u, double *v, double *w, double *GNS)
{
    int z;
    double *ux,*uy,*uz,*vx,*vy,*vz,*wx,*wy,*wz;

    ux = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    uy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    uz = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    vx = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    vy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    vz = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    wx = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    wy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    wz = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    PartialDerivatives_3D(u,ux,uy,uz);
    PartialDerivatives_3D(v,vx,vy,vz);
    PartialDerivatives_3D(w,wx,wy,wz);

#pragma omp parallel for
    for (z=0;z<Parameter.size_z;z++)
    {
        int i,x,y;

        for (y=0;y<Parameter.size_y;y++)
        {
            for (x=0;x<Parameter.size_x;x++)
            {
                i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_z;
                GNS[i] = ux[i]*ux[i] + uy[i]*uy[i] + uz[i]*uz[i] + vx[i]*vx[i] + vy[i]*vy[i] + vz[i]*vz[i] + wx[i]*wx[i] + wy[i]*wy[i] + wz[i]*wz[i];
            }
        }
    }

    free(ux);
    free(uy);
    free(uz);
    free(vx);
    free(vy);
    free(vz);
    free(wx);
    free(wy);
    free(wz);
}
