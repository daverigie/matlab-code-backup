#include "OpticalFlow_BNQ.h"


void OpticalFlow_BNQ_2D(double *Ia, double *Ib, double *u, double *v)
{
    int oiteration,iteration,y;
    double c,dx,dy;
    double *u_Avg,*v_Avg,*Ix,*Iy,*Psi1,*Psi2;
    double alpha = GetParameterDouble("ALPHA");
    int outer_iterations = GetParameterDouble("OUTER_ITERATIONS");

    /* allocate memory for temporary images */
    Ix = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    u_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    v_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Psi1 = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Psi2 = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    /* pixel size for scaling */
    dx = Parameter.fov_x/(double)Parameter.size_x;
    dy = Parameter.fov_y/(double)Parameter.size_y;

    /* calculate partial derivatives wrt x,y */
    PartialDerivatives_Spatial_2D(Ia,Ix,Iy);

    /* outer iteration loop */
    for (oiteration=0;oiteration<outer_iterations;oiteration++)
    {
        /* update Psi1 and Psi2 */
        Update_Psi1_2D(Psi1,Ia,Ib,Ix,Iy,u,v);
        Update_Psi2_2D(Psi2,u,v);

        /* iteration loop */
        for (iteration=0;iteration<Parameter.iterations;iteration++)
        {
            /* calculate average in neighborhood */
            WeightedAveragePsi_2D(Psi1,Psi2,u,v,u_Avg,v_Avg);

            /* update u and v */
#pragma omp parallel for
            for (y=1;y<Parameter.size_y-1;y++)
            {
                int i,i1,i2,i3,i4,x;
                double denominator;

                for (x=1;x<Parameter.size_x-1;x++)
                {
                    i = x + y * Parameter.size_x;
                    i1 = (x-1) + y * Parameter.size_x;
                    i2 = (x+1) + y * Parameter.size_x;
                    i3 = x + (y-1) * Parameter.size_x;
                    i4 = x + (y+1) * Parameter.size_x;

                    /* scaling factor */
                    c = ((2.0*Psi2[i]+Psi2[i1]+Psi2[i2])/(2.0*dx)
                         + (2.0*Psi2[i]+Psi2[i3]+Psi2[i4])/(2.0*dy)) / Psi1[i];

                    denominator = c * (c * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i]);

                    /* update of for x and y direction */
                    u[i] = u_Avg[i] - (Ix[i] * (c * Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + c * (Ib[i]-Ia[i]))
                                       - (1-c) * (c * alpha + Iy[i]*Iy[i]) * u_Avg[i]) / denominator;

                    v[i] = v_Avg[i] - (Iy[i] * (Ix[i] * u_Avg[i] + c * Iy[i] * v_Avg[i] + c * (Ib[i]-Ia[i]))
                                       - (1-c) * (c * alpha + Ix[i]*Ix[i]) * v_Avg[i]) / denominator;
                }
            }
        }
    }

    /* free memory of temporary images */
    free(u_Avg);
    free(v_Avg);
    free(Ix);
    free(Iy);
    free(Psi1);
    free(Psi2);
}


void OpticalFlow_BNQ_3D(double *Ia, double *Ib, double *u, double *v, double *w)
{
    int oiteration,iteration,z;
    double c,dx,dy,dz;
    double *u_Avg,*v_Avg,*w_Avg,*Ix,*Iy,*Iz,*Psi1,*Psi2;
    double alpha = GetParameterDouble("ALPHA");
    int outer_iterations = GetParameterDouble("OUTER_ITERATIONS");

    /* allocate memory for temporary images */
    Ix = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iz = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    u_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    v_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    w_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Psi1 = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Psi2 = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    /* voxel size for scaling */
    dx = Parameter.fov_x/(double)Parameter.size_x;
    dy = Parameter.fov_y/(double)Parameter.size_y;
    dz = Parameter.fov_z/(double)Parameter.size_z;

    /* calculate partial derivatives wrt x,y and z */
    PartialDerivatives_Spatial_3D(Ia,Ix,Iy,Iz);

    /* outer iteration loop */
    for (oiteration=0;oiteration<outer_iterations;oiteration++)
    {
        /* update Psi1 and Psi2 */
        Update_Psi1_3D(Psi1,Ia,Ib,Ix,Iy,Iz,u,v,w);
        Update_Psi2_3D(Psi2,u,v,w);

        /* iteration loop */
        for (iteration=0;iteration<Parameter.iterations;iteration++)
        {
            /* calculate average in neighborhood */
            WeightedAveragePsi_3D(Psi1,Psi2,u,v,w,u_Avg,v_Avg,w_Avg);

            /* update u, v and w */
#pragma omp parallel for
            for (z=1;z<Parameter.size_z-1;z++)
            {
                int i,i1,i2,i3,i4,i5,i6,x,y;
                double denominator;

                for (y=1;y<Parameter.size_y-1;y++)
                {
                    for (x=1;x<Parameter.size_x-1;x++)
                    {
                        i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                        i1 = (x-1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                        i2 = (x+1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                        i3 = x + (y-1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                        i4 = x + (y+1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                        i5 = x + y * Parameter.size_x + (z-1) * Parameter.size_x * Parameter.size_y;
                        i6 = x + y * Parameter.size_x + (z+1) * Parameter.size_x * Parameter.size_y;

                        /* scaling factor */
                        c = ((2.0*Psi2[i]+Psi2[i1]+Psi2[i2])/(2.0*dx)
                             + (2.0*Psi2[i]+Psi2[i3]+Psi2[i4])/(2.0*dy)
                             + (2.0*Psi2[i]+Psi2[i5]+Psi2[i6])/(2.0*dz)) / Psi1[i];

                        denominator = c * (c * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i] + Iz[i]*Iz[i]);

                        /* update of for x,y and z direction */
                        u[i] = u_Avg[i] - (Ix[i] * (c * Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + Iz[i] * w_Avg[i] + c * (Ib[i]-Ia[i]))
                                           - (1-c) * (c * alpha + Iy[i]*Iy[i] + Iz[i]*Iz[i]) * u_Avg[i]) / denominator;

                        v[i] = v_Avg[i] - (Iy[i] * (Ix[i] * u_Avg[i] + c * Iy[i] * v_Avg[i] + Iz[i] * w_Avg[i] + c * (Ib[i]-Ia[i]))
                                           - (1-c) * (c * alpha + Ix[i]*Ix[i] + Iz[i]*Iz[i]) * v_Avg[i]) / denominator;

                        w[i] = w_Avg[i] - (Iz[i] * (Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + c * Iz[i] * w_Avg[i] + c * (Ib[i]-Ia[i]))
                                           - (1-c) * (c * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i]) * w_Avg[i]) / denominator;
                    }
                }
            }
        }
    }

    /* free memory of temporary images */
    free(u_Avg);
    free(v_Avg);
    free(w_Avg);
    free(Ix);
    free(Iy);
    free(Iz);
    free(Psi1);
    free(Psi2);
}


void Update_Psi1_2D(double *Psi1, double *Ia, double *Ib, double *Ix, double *Iy, double *u, double *v)
{
    int y;
    double beta1 = GetParameterDouble("BETA1");

    /* update psi */
#pragma omp parallel for
    for (y=1;y<Parameter.size_y-1;y++)
    {
        int i,x;
        double tmp;

        for (x=1;x<Parameter.size_x-1;x++)
        {
            i = x + y * Parameter.size_x;

            tmp = Ix[i]*u[i]+Iy[i]*v[i]+Ib[i]-Ia[i];

            Psi1[i] = 1.0 / sqrt(1.0 + (tmp*tmp)/beta1);
        }
    }
}


void Update_Psi1_3D(double *Psi1, double *Ia, double *Ib, double *Ix, double *Iy, double *Iz, double *u, double *v, double *w)
{
    int z;
    double beta1 = GetParameterDouble("BETA1");

    /* update psi */
#pragma omp parallel for
    for (z=1;z<Parameter.size_z-1;z++)
    {
        int i,x,y;
        double tmp;

        for (y=1;y<Parameter.size_y-1;y++)
        {
            for (x=1;x<Parameter.size_x-1;x++)
            {
                i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                tmp = Ix[i]*u[i]+Iy[i]*v[i]+Iz[i]*w[i]+Ib[i]-Ia[i];

                Psi1[i] = 1.0 / sqrt(1.0 + (tmp*tmp)/beta1);
            }
        }
    }
}


void Update_Psi2_2D(double *Psi2, double *u, double *v)
{
    int y;
    double dx,dy;
    double beta2 = GetParameterDouble("BETA2");

    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;

#pragma omp parallel for
    for (y=1;y<Parameter.size_y-1;y++)
    {
        int x;
        int i, i1, i2;
        double ux, uy, vx, vy;

        for (x=1;x<Parameter.size_x-1;x++)
        {
            i = x + y * Parameter.size_x;

            i1 = (x-1) + y * Parameter.size_x;
            i2 = (x+1) + y * Parameter.size_x;
            ux = (u[i2] - u[i1]) / (2.0 * dx);
            vx = (v[i2] - v[i1]) / (2.0 * dx);

            i1 = x + (y-1) * Parameter.size_x;
            i2 = x + (y+1) * Parameter.size_x;
            uy = (u[i2] - u[i1]) / (2.0 * dy);
            vy = (v[i2] - v[i1]) / (2.0 * dy);

            Psi2[i] = 1.0 / sqrt(1.0 + (ux*ux+uy*uy+vx*vx+vy*vy)/beta2);
        }
    }
}


void Update_Psi2_3D(double *Psi2, double *u, double *v, double *w)
{
    int z;
    double dx, dy, dz;
    double beta2 = GetParameterDouble("BETA2");

    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;
    dz = Parameter.fov_z / (double)Parameter.size_z;

#pragma omp parallel for
    for (z=1;z<Parameter.size_z-1;z++)
    {
        int x, y;
        int i, i1, i2;
        double ux, uy, uz, vx, vy, vz, wx, wy, wz;

        for (y=1;y<Parameter.size_y-1;y++)
        {
            for (x=1;x<Parameter.size_x-1;x++)
            {
                i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;

                i1 = (x-1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i2 = (x+1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                ux = (u[i2] - u[i1]) / (2.0 * dx);
                vx = (v[i2] - v[i1]) / (2.0 * dx);
                wx = (w[i2] - w[i1]) / (2.0 * dx);

                i1 = x + (y-1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i2 = x + (y+1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                uy = (u[i2] - u[i1]) / (2.0 * dy);
                vy = (v[i2] - v[i1]) / (2.0 * dy);
                wy = (w[i2] - w[i1]) / (2.0 * dy);

                i1 = x + y * Parameter.size_x + (z-1) * Parameter.size_x * Parameter.size_y;
                i2 = x + y * Parameter.size_x + (z+1) * Parameter.size_x * Parameter.size_y;
                uz = (u[i2] - u[i1]) / (2.0 * dz);
                vz = (v[i2] - v[i1]) / (2.0 * dz);
                wz = (w[i2] - w[i1]) / (2.0 * dz);

                Psi2[i] = 1.0 / sqrt(1.0 + (ux*ux+uy*uy+uz*uz+vx*vx+vy*vy+vz*vz+wx*wx+wy*wy+wz*wz)/beta2);
            }
        }
    }
}
