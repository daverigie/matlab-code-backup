#include "Laplacian.h"


void Laplacian_2D(double *u, double *v, double *u_Laplacian, double *v_Laplacian)
{
    int y;
    double dx,dy;
    
    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;

#pragma omp parallel for
    for (y=1;y<Parameter.size_y-1;y++)
    {
        int x;
        int i, i1, i2, i3, i4;

        for (x=1;x<Parameter.size_x-1;x++)
        {
            i = x + y * Parameter.size_x;

            i1 = (x-1) + y * Parameter.size_x;
            i2 = (x+1) + y * Parameter.size_x;
            i3 = x + (y-1) * Parameter.size_x;
            i4 = x + (y+1) * Parameter.size_x;

            u_Laplacian[i] = (-2.0*u[i] + (u[i1] + u[i2])) / dx + (-2.0*u[i] + (u[i3] + u[i4])) / dy;
            v_Laplacian[i] = (-2.0*v[i] + (v[i1] + v[i2])) / dx + (-2.0*v[i] + (v[i3] + v[i4])) / dy;
        }
    }
}


void Laplacian_3D(double *u, double *v, double *w, double *u_Laplacian, double *v_Laplacian, double *w_Laplacian)
{
    int z;
    double dx,dy,dz;
    double scale_x, scale_y, scale_z, scale_sum;
    
    dx = Parameter.fov_x / (double)Parameter.size_x;
    dy = Parameter.fov_y / (double)Parameter.size_y;
    dz = Parameter.fov_z / (double)Parameter.size_z;
    
    scale_x = 1.0 / (6.0 * dx);
    scale_y = 1.0 / (6.0 * dy);
    scale_z = 1.0 / (6.0 * dz);

    /* make sure sum of weights is equal to 1 */
    scale_sum = 2.0 * scale_x + 2.0 * scale_y + 2.0 * scale_z;
    scale_x = scale_x / scale_sum;
    scale_y = scale_y / scale_sum;
    scale_z = scale_z / scale_sum;
    
#pragma omp parallel for
    for (z=1;z<Parameter.size_z-1;z++)
    {
        int x,y;
        int i, i1, i2, i3, i4, i5, i6;

        for (y=1;y<Parameter.size_y-1;y++)
        {
            for (x=1;x<Parameter.size_x-1;x++)
            {
                i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;

                i1 = (x-1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i2 = (x+1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                
                i3 = x + (y-1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                i4 = x + (y+1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
                
                i5 = x + y * Parameter.size_x + (z-1) * Parameter.size_x * Parameter.size_y;
                i6 = x + y * Parameter.size_x + (z+1) * Parameter.size_x * Parameter.size_y;
                
                u_Laplacian[i] = - u[i] + scale_x * (u[i1] + u[i2]) + scale_y * (u[3] + u[4]) + scale_z * (u[i5] + u[i6]);
                v_Laplacian[i] = - v[i] + scale_x * (v[i1] + v[i2]) + scale_y * (v[3] + v[4]) + scale_z * (v[i5] + v[i6]);
                w_Laplacian[i] = - w[i] + scale_x * (w[i1] + w[i2]) + scale_y * (w[3] + w[4]) + scale_z * (w[i5] + w[i6]);
            }
        }
    }
}
