#include "ResampleImage.h"


void ResampleImage_1D(double *input_image, double *output_image)
{


    % interp test
    function [new] = interptest(old,old_x,old_dx,new_x,new_dx)

    new = zeros(new_x,1);

    % loop over all new bins
    for i=0:new_x-1
        % calculate bins for old resolution
        min_old_x = floor((i * new_dx) / old_dx);
        max_old_x = ceil(((i+1) * new_dx) / old_dx); % ceil?
        min_old_x_temp = floor(((i+1) * new_dx) / old_dx);

        % catch unwanted double use of bins
        if (max_old_x >= min_old_x_temp)
            max_old_x = max_old_x - 1;
        end

        % restrict values to valid bins
        min_old_x = max(min_old_x,0);
        max_old_x = min(max_old_x,old_x-1);

        % loop over old bins and add them weighted
        for j=min_old_x:max_old_x
            % calculate weight for each bin
            weight_calculated = 0;
            xl_old = j * old_dx;
            xr_old = (j+1) * old_dx;
            xl_new = i * new_dx;
            xr_new = (i+1) * new_dx;

            % new bins wider than old bins
            if (new_x <= old_x && xl_new <= xl_old && xr_new >= xr_old)
                weight = old_dx;
                weight_calculated = 1;
            end

            % new bins smaller than old bins
            if (weight_calculated == 0 && new_x > old_x && xl_new >= xl_old && xr_new <= xr_old)
                weight = new_dx;
                weight_calculated = 1;
            end

            % overlap to the left
            if (weight_calculated == 0 && xl_new <= xl_old && xr_new < xr_old)
                weight = xr_new - xl_old;
                weight_calculated = 1;
            end

            % overlap to the right
            if (weight_calculated == 0 && xl_new > xl_old && xr_new >= xr_old)
                weight = xr_old - xl_new;
            end

            new(i+1) = new(i+1) + weight / new_dx * old(j+1);
        end
    end
    end


}


void ResampleImage_2D(double *input_image, double *output_image)
{

}


//void PartialDerivatives_Spatial_2D(double *Ia, double *Ix, double *Iy)
//{
//    int y;
//    double dx,dy;

//    dx = Parameter.fov_x / (double)Parameter.size_x;
//    dy = Parameter.fov_y / (double)Parameter.size_y;

//    for (y=0;y<Parameter.imagesize;y++)
//    {
//        Ix[y] = 0.0;
//        Iy[y] = 0.0;
//    }

//#pragma omp parallel for
//    for (y=1;y<Parameter.size_y-1;y++)
//    {
//        int x;
//        int i, i1, i2;

//        for (x=1;x<Parameter.size_x-1;x++)
//        {
//            i = x + y * Parameter.size_x;

//            i1 = (x-1) + y * Parameter.size_x;
//            i2 = (x+1) + y * Parameter.size_x;
//            Ix[i] = (Ia[i2] - Ia[i1]) / (2.0 * dx);

//            i1 = x + (y-1) * Parameter.size_x;
//            i2 = x + (y+1) * Parameter.size_x;
//            Iy[i] = (Ia[i2] - Ia[i1]) / (2.0 * dy);
//        }
//    }
//}


//void PartialDerivatives_Spatial_3D(double *Ia, double *Ix, double *Iy, double *Iz)
//{
//    int z;
//    double dx,dy,dz;

//    dx = Parameter.fov_x / (double)Parameter.size_x;
//    dy = Parameter.fov_y / (double)Parameter.size_y;
//    dz = Parameter.fov_z / (double)Parameter.size_z;

//    for (z=0;z<Parameter.imagesize;z++)
//    {
//        Ix[z] = 0.0;
//        Iy[z] = 0.0;
//        Iz[z] = 0.0;
//    }

//#pragma omp parallel for
//    for (z=1;z<Parameter.size_z-1;z++)
//    {
//        int x,y;
//        int i, i1, i2;

//        for (y=1;y<Parameter.size_y-1;y++)
//        {
//            for (x=1;x<Parameter.size_x-1;x++)
//            {
//                i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;

//                i1 = (x-1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
//                i2 = (x+1) + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
//                Ix[i] = (Ia[i2] - Ia[i1]) / (2.0 * dx);

//                i1 = x + (y-1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
//                i2 = x + (y+1) * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;
//                Iy[i] = (Ia[i2] - Ia[i1]) / (2.0 * dy);

//                i1 = x + y * Parameter.size_x + (z-1) * Parameter.size_x * Parameter.size_y;
//                i2 = x + y * Parameter.size_x + (z+1) * Parameter.size_x * Parameter.size_y;
//                Iz[i] = (Ia[i2] - Ia[i1]) / (2.0 * dz);
//            }
//        }
//    }
//}
