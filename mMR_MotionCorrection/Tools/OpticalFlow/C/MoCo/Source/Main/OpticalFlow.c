#include "OpticalFlow.h"


int main(int argc, char *argv[])
{
    return 0;
}


void OpticalFlow_2D(double *Ia, double *Ib, double *u, double *v)
{
    int i, algorithm;

    /* ensure u and v are zero */
    for (i=0;i<Parameter.imagesize;i++)
    {
        u[i] = 0.0;
        v[i] = 0.0;
    }

    /* select algorithm according to OF_ALGO */
    algorithm = GetParameterInt("OF_ALGO");

    /* Version 1:
     * Original Horn Schunk
     * see Horn Schunk 1981 */
    if (algorithm == 1)
    {
        OpticalFlow_HS_2D(Ia,Ib,u,v);
    }
    
    /* Version 2:
     * A Global Optical Flow Algorithm
     * see 2) in Dawood et al. IEEE 2008 */
    if (algorithm == 2)
    {
        /* OpticalFlow_HS_V2_2D(Ia,Ib,u,v); */
    }

    /* Version 3:
     * Combined Local Global Optical Flow Algorithm
     * see 3) in Dawood et al. IEEE 2008 */
    if (algorithm == 3)
    {
        /* OpticalFlow_B_2D(Ia,Ib,u,v); */
    }

    /* Version 4:
     * Non-Quadratic Combined Local Global Optical Flow Algorithm
     * see 4) in Dawood et al. IEEE 2008 */
    if (algorithm == 4)
    {
        OpticalFlow_BNQ_2D(Ia,Ib,u,v);
    }
}


void OpticalFlow_3D(double *Ia, double *Ib, double *u, double *v, double *w)
{
    int i, algorithm;

    /* ensure u, v and w are zero */
    for (i=0;i<Parameter.imagesize;i++)
    {
        u[i] = 0.0;
        v[i] = 0.0;
        w[i] = 0.0;
    }

    /* select algorithm according to OF_ALGO */
    algorithm = GetParameterInt("OF_ALGO");

    /* Version 1:
     * Original Horn Schunk
     * see Horn Schunk 1981 */
    if (algorithm == 1)
    {
        OpticalFlow_HS_3D(Ia,Ib,u,v,w);
    }

    /* Version 2:
     * A Global Optical Flow Algorithm
     * see 2) in Dawood et al. IEEE 2008 */
    if (algorithm == 2)
    {
        /* OpticalFlow_HS_V2_3D(Ia,Ib,u,v,w); */
    }

    /* Version 3:
     * Combined Local Global Optical Flow Algorithm
     * see 3) in Dawood et al. IEEE 2008 */
    if (algorithm == 3)
    {
        /* OpticalFlow_B_3D(Ia,Ib,u,v,w); */
    }

    /* Version 4:
     * Non-Quadratic Combined Local Global Optical Flow Algorithm
     * see 4) in Dawood et al. IEEE 2008 */
    if (algorithm == 4)
    {
        OpticalFlow_BNQ_3D(Ia,Ib,u,v,w);
    }
}
