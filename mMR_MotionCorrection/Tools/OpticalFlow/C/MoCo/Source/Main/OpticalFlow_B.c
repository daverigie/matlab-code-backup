#include "OpticalFlow_B.h"


void OpticalFlow_B_2D(double *Ia, double *Ib, double *u, double *v)
{
    int i,x,y,kx,ky,ksizex,ksizey,iteration;
    double dx, dy, resscale, sigma, ksum;
    double *u_Avg,*v_Avg,*Ix,*Iy,*kernel;
    double alpha = GetParameterDouble("ALPHA");

    /* allocate memory for temporary images */
    Ix = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    u_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    v_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    /* scaling factor to account for pixel size */
    dx = Parameter.fov_x/(double)Parameter.size_x;
    dy = Parameter.fov_y/(double)Parameter.size_y;
    resscale = 2.0/dx + 2.0/dy;

    /* initialize kernel */
    ksum = 0.0;
    sigma = Parameter.fwhm/2.3548;
    ksizex = (int)(6.0*sigma/dx)+1;
    ksizey = (int)(6.0*sigma/dy)+1;
    if (ksizex%2 == 0) ksizex++;
    if (ksizey%2 == 0) ksizey++;
    kernel = (double*)(safe_calloc(ksizex*ksizey, sizeof(double)));

    for (ky=-(ksizey-1)/2;ky<=(ksizey-1)/2;ky++)
    {
        for (kx=-(ksizex-1)/2;kx<=(ksizex-1)/2;kx++)
        {
            kernel[kx+(ksizex-1)/2+(ky+(ksizey-1)/2)*Parameter.size_x] = exp(-(kx*kx*dx*dx+ky*ky*dy*dy)/(2*sigma*sigma));
            ksum += exp(-(kx*kx*dx*dx+ky*ky*dy*dy)/(2*sigma*sigma));
        }
    }

    for (ky=-(ksizey-1)/2;ky<=(ksizey-1)/2;ky++)
    {
        for (kx=-(ksizex-1)/2;kx<=(ksizex-1)/2;kx++)
        {
            kernel[kx+(ksizex-1)/2+(ky+(ksizey-1)/2)*Parameter.size_x] /= ksum;
        }
    }

    /* calculate partial derivatives wrt x,y */
    PartialDerivatives_Spatial_2D(Ia,Ix,Iy);

    /* iteration loop */
    for (iteration=0;iteration<Parameter.iterations;iteration++)
    {
        /* calculate average in neighborhood */
        WeightedAverage_2D(u,v,u_Avg,v_Avg);

        /* update u and v */
#pragma omp parallel for private (i,x,kx,ky)
        for (y=(ksizey-1)/2;y<Parameter.size_y-(ksizey-1)/2;y++)
        {
            double denominator;

            for (x=(ksizex-1)/2;x<Parameter.size_x-(ksizex-1)/2;x++)
            {
                for (ky=-(ksizey-1)/2;ky<=(ksizey-1)/2;ky++)
                {
                    for (kx=-(ksizex-1)/2;kx<=(ksizex-1)/2;kx++)
                    {
                        i = (x+kx) + (y+ky) * Parameter.size_x;

                        denominator = resscale * (resscale * alpha * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i]);

                        /* update of for x and y direction */
                        u[i] = u_Avg[i] - (Ix[i] * (resscale * Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + resscale * (Ib[i]-Ia[i]))
                                           - (1-resscale) * (resscale * alpha * alpha + Iy[i]*Iy[i]) * u_Avg[i]) / denominator;

                        v[i] = v_Avg[i] - (Iy[i] * (Ix[i] * u_Avg[i] + resscale * Iy[i] * v_Avg[i] + resscale * (Ib[i]-Ia[i]))
                                           - (1-resscale) * (resscale * alpha * alpha + Ix[i]*Ix[i]) * v_Avg[i]) / denominator;
                    }
                }
            }
        }
    }

    /* free memory of temporary images and weighting kernel */
    free(u_Avg);
    free(v_Avg);
    free(Ix);
    free(Iy);
    free(kernel);
}


void OpticalFlow_B_3D(double *Ia, double *Ib, double *u, double *v, double *w)
{
    int iteration,z;
    double resscale;
    double *u_Avg,*v_Avg,*w_Avg,*Ix,*Iy,*Iz;
    double alpha = GetParameterDouble("ALPHA");

    /* allocate memory for temporary images */
    Ix = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    Iz = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    u_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    v_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));
    w_Avg = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

    /* scaling factor to account for pixel size */
    resscale = 2.0/(Parameter.fov_x/(double)Parameter.size_x) + 2.0/(Parameter.fov_y/(double)Parameter.size_y) + 2.0/(Parameter.fov_z/(double)Parameter.size_z);

    /* calculate partial derivatives wrt x,y and z */
    PartialDerivatives_Spatial_3D(Ia,Ix,Iy,Iz);

    /* iteration loop */
    for (iteration=0;iteration<Parameter.iterations;iteration++)
    {
        /* calculate average in neighborhood */
        WeightedAverage_3D(u,v,w,u_Avg,v_Avg,w_Avg);

        /* update u, v and w*/
#pragma omp parallel for
        for (z=0;z<Parameter.size_z;z++)
        {
            int i,x,y;
            double denominator;

            for (y=0;y<Parameter.size_y;y++)
            {
                for (x=0;x<Parameter.size_x;x++)
                {
                    i = x + y * Parameter.size_x + z * Parameter.size_x * Parameter.size_y;

                    denominator = resscale * (resscale * alpha * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i] + Iz[i]*Iz[i]);

                    /* update of for x,y and z direction */
                    u[i] = u_Avg[i] - (Ix[i] * (resscale * Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + Iz[i] * w_Avg[i] + resscale * (Ib[i]-Ia[i]))
                                       - (1-resscale) * (resscale * alpha * alpha + Iy[i]*Iy[i] + Iz[i]*Iz[i]) * u_Avg[i]) / denominator;

                    v[i] = v_Avg[i] - (Iy[i] * (Ix[i] * u_Avg[i] + resscale * Iy[i] * v_Avg[i] + Iz[i] * w_Avg[i] + resscale * (Ib[i]-Ia[i]))
                                       - (1-resscale) * (resscale * alpha * alpha + Ix[i]*Ix[i] + Iz[i]*Iz[i]) * v_Avg[i]) / denominator;

                    w[i] = w_Avg[i] - (Iz[i] * (Ix[i] * u_Avg[i] + Iy[i] * v_Avg[i] + resscale * Iz[i] * w_Avg[i] + resscale * (Ib[i]-Ia[i]))
                                       - (1-resscale) * (resscale * alpha * alpha + Ix[i]*Ix[i] + Iy[i]*Iy[i]) * w_Avg[i]) / denominator;
                }
            }
        }
    }

    /* free memory of temporary images */
    free(u_Avg);
    free(v_Avg);
    free(w_Avg);
    free(Ix);
    free(Iy);
    free(Iz);
}
