#ifndef GRADIENTNORMSQUARE_H
#define GRADIENTNORMSQUARE_H


/* EMrecon includes */
#include "../../../EMrecon/Source/IO/Parameter.h"


void GradientNormSquare_2D(double *u, double *v, double *GNS);
void GradientNormSquare_3D(double *u, double *v, double *w, double *GNS);


#endif
