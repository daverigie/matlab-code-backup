#ifndef RESAMPLEIMAGE_H
#define RESAMPLEIMAGE_H


/* EMrecon includes */
#include "../../../EMrecon/Source/IO/Parameter.h"


void ResampleImage_1D(double *input_image, double *output_image);
void ResampleImage_2D(double *input_image, double *output_image);
void ResampleImage_3D(double *input_image, double *output_image);


#endif
