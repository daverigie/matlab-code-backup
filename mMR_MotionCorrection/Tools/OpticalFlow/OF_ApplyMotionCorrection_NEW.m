function [new_I] = OF_ApplyMotionCorrection_NEW(parm,I,u,v,w)

% - missing is check of vector / image size

% OF_ApplyMotionCorrection_NEW(parm,I,u,v,[w])

if ((nargin ~= 4 && nargin ~= 5) || nargout ~= 1)
    fprintf('\nOF_ApplyMotionCorrection_NEW\n');
    fprintf('Usage: [new_I] = OF_ApplyMotionCorrection_NEW(parm,I,u,v,[w])\n\n');
    fprintf('\n');
    new_I = -1;
else
    fprintf('OF_ApplyMotionCorrection_NEW...\n');
    tic;
    
    % 2D case
    if (nargin == 4)
        new_I = mex_OF_ApplyMotionCorrection(parm,I(:),u(:),v(:));
    end
    
    % 3D case
    if (nargin == 5)
        new_I = mex_OF_ApplyMotionCorrection(parm,I(:),u(:),v(:),w(:));
    end
    
    % reshape image to old size
    new_I = reshape(new_I,size(I));
    
    fprintf('OF_ApplyMotionCorrection_NEW...done. [%f sec]\n',toc);
end

end
