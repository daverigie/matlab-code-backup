#ifndef OPTICAL_FLOW_FACTORY_H
#define OPTICAL_FLOW_FACTORY_H


#include "../io/parameter_list.h"
#include "optical_flow_algo.h"
#include "optical_flow_algo_bnq.h"


class optical_flow_factory
{
private:
    optical_flow_algo_bnq OPTICAL_FLOW_ALGO_BNQ;

public:
    optical_flow_factory();

    optical_flow_algo* create_algorithm(parameter_list);
};


#endif // OPTICAL_FLOW_FACTORY_H
