#include "optical_flow_algo_bnq.h"


void optical_flow_algo_bnq::setup(parameter_list ParameterList)
{
    //cout << "optical_flow_algo_bnq::setup" << endl;

    /* initialize required parameters for algorithm */
    alpha = ParameterList.get_parameter_double("ALPHA",1.0);
    beta1 = ParameterList.get_parameter_double("BETA1",1.0);
    beta2 = ParameterList.get_parameter_double("BETA2",1.0);
    i_iterations = ParameterList.get_parameter_int("I_ITERATIONS",1);
    o_iterations = ParameterList.get_parameter_int("O_ITERATIONS",1);

    //cout << "optical_flow_algo_bnq::setup END" << endl;
}


optical_flow_algo_bnq::optical_flow_algo_bnq()
{
}


void optical_flow_algo_bnq::calculate_flow()
{
    //cout << "optical_flow_algo_bnq::calculate_flow" << endl;

    double d_x = Ia.get_d_x();
    double d_y = Ia.get_d_y();
    double d_z = Ia.get_d_z();
    int size_x = Ia.get_size_x();
    int size_y = Ia.get_size_y();
    int size_z = Ia.get_size_z();

    /* initialize required data */
    Psi1.initialize(Ia);
    Psi2.initialize(Ia);

    /* catch 2D case */
    if (size_z == 1)
    {
        /* calculate partial derivatives */
        calculate_partial_derivatives();

        /* outer iteration loop */
        for (int o_loop=0;o_loop<o_iterations;o_loop++)
        {
            /* calculate psi */
            calculate_psi();

            /* inner iteration loop */
            for (int i_loop=0;i_loop<i_iterations;i_loop++)
            {
                /* calculate weighted averages */
                calculate_weighted_averages_psi();

                /* update u and v */
#pragma omp parallel for
                for (int y=1;y<size_y-1;y++)
                {
                    int i,i1,i2,i3,i4;
                    double c,value,denominator;

                    for (int x=1;x<size_x-1;x++)
                    {
                        i = x + y * size_x;
                        i1 = (x-1) + y * size_x;
                        i2 = (x+1) + y * size_x;
                        i3 = x + (y-1) * size_x;
                        i4 = x + (y+1) * size_x;

                        /* scaling factor */
                        c = ((2.0*Psi2.get_value(i)+Psi2.get_value(i1)+Psi2.get_value(i2))/(2.0*d_x)
                             + (2.0*Psi2.get_value(i)+Psi2.get_value(i3)+Psi2.get_value(i4))/(2.0*d_y)) / Psi1.get_value(i);

                        denominator = c * (c * alpha + Ix.get_value(i)*Ix.get_value(i) + Iy.get_value(i)*Iy.get_value(i));

                        /* update of for x and y direction */
                        value = u_Avg.get_value(i) - (Ix.get_value(i) * (c * Ix.get_value(i) * u_Avg.get_value(i) + Iy.get_value(i) * v_Avg.get_value(i) + c * It.get_value(i))
                                                      - (1-c) * (c * alpha + Iy.get_value(i)*Iy.get_value(i)) * u_Avg.get_value(i)) / denominator;
                        u.set_value(i,value);

                        value = v_Avg.get_value(i) - (Iy.get_value(i) * (Ix.get_value(i) * u_Avg.get_value(i) + c * Iy.get_value(i) * v_Avg.get_value(i) + c * It.get_value(i))
                                                      - (1-c) * (c * alpha + Ix.get_value(i)*Ix.get_value(i)) * v_Avg.get_value(i)) / denominator;
                        v.set_value(i,value);
                    }
                }
            }
        }
    }

    /* catch 3D case */
    if (size_z > 1)
    {
        /* calculate partial derivatives */
        calculate_partial_derivatives();

        /* outer iteration loop */
        for (int o_loop=0;o_loop<o_iterations;o_loop++)
        {
            /* calculate psi */
            calculate_psi();

            /* inner iteration loop */
            for (int i_loop=0;i_loop<i_iterations;i_loop++)
            {
                /* calculate weighted averages */
                calculate_weighted_averages_psi();

                /* update u,v and w */
#pragma omp parallel for
                for (int z=1;z<size_z-1;z++)
                {
                    int i,i1,i2,i3,i4,i5,i6;
                    double c,value,denominator;

                    for (int y=1;y<size_y-1;y++)
                    {
                        for (int x=1;x<size_x-1;x++)
                        {
                            i = x + y * size_x + z * size_x * size_y;
                            i1 = (x-1) + y * size_x + z * size_x * size_y;
                            i2 = (x+1) + y * size_x + z * size_x * size_y;
                            i3 = x + (y-1) * size_x + z * size_x * size_y;
                            i4 = x + (y+1) * size_x + z * size_x * size_y;
                            i5 = x + y * size_x + (z-1) * size_x * size_y;
                            i6 = x + y * size_x + (z+1) * size_x * size_y;

                            /* scaling factor */
                            c = ((2.0*Psi2.get_value(i)+Psi2.get_value(i1)+Psi2.get_value(i2))/(2.0*d_x)
                                 + (2.0*Psi2.get_value(i)+Psi2.get_value(i3)+Psi2.get_value(i4))/(2.0*d_y)
                                 + (2.0*Psi2.get_value(i)+Psi2.get_value(i5)+Psi2.get_value(i6))/(2.0*d_z)) / Psi1.get_value(i);

                            denominator = c * (c * alpha + Ix.get_value(i)*Ix.get_value(i) + Iy.get_value(i)*Iy.get_value(i) + Iz.get_value(i)*Iz.get_value(i));

                            /* update of for x, y and z direction */
                            value = u_Avg.get_value(i) - (Ix.get_value(i) * (c * Ix.get_value(i) * u_Avg.get_value(i) + Iy.get_value(i) * v_Avg.get_value(i) + Iz.get_value(i) * w_Avg.get_value(i) + c * It.get_value(i))
                                                          - (1-c) * (c * alpha + Iy.get_value(i)*Iy.get_value(i) + Iz.get_value(i)*Iz.get_value(i)) * u_Avg.get_value(i)) / denominator;
                            u.set_value(i,value);

                            value = v_Avg.get_value(i) - (Iy.get_value(i) * (Ix.get_value(i) * u_Avg.get_value(i) + c * Iy.get_value(i) * v_Avg.get_value(i) + Iz.get_value(i) * w_Avg.get_value(i) + c * It.get_value(i))
                                                          - (1-c) * (c * alpha + Ix.get_value(i)*Ix.get_value(i) + Iz.get_value(i)*Iz.get_value(i)) * v_Avg.get_value(i)) / denominator;
                            v.set_value(i,value);

                            value = w_Avg.get_value(i) - (Iz.get_value(i) * (Ix.get_value(i) * u_Avg.get_value(i) + Iy.get_value(i) * v_Avg.get_value(i) + c * Iz.get_value(i) * w_Avg.get_value(i) + c * It.get_value(i))
                                                          - (1-c) * (c * alpha + Ix.get_value(i)*Ix.get_value(i) + Iy.get_value(i)*Iy.get_value(i)) * w_Avg.get_value(i)) / denominator;
                            w.set_value(i,value);
                        }
                    }
                }
            }
        }
    }

    //cout << "optical_flow_algo_bnq::calculate_flow END" << endl;
}


/**
 * @brief optical_flow_algo_bnq::calculate_psi
 */
void optical_flow_algo_bnq::calculate_psi()
{
    //cout << "optical_flow_algo_bnq::calculate_psi" << endl;

    double d_x = Ia.get_d_x();
    double d_y = Ia.get_d_y();
    double d_z = Ia.get_d_z();
    int size_x = Ia.get_size_x();
    int size_y = Ia.get_size_y();
    int size_z = Ia.get_size_z();

    Psi1.set_zero();
    Psi2.set_zero();

    /* catch 2D case */
    if (size_z == 1)
    {
        /* calculate Psi1 */
#pragma omp parallel for
        for (int y=1;y<size_y-1;y++)
        {
            int i;
            double tmp;

            for (int x=1;x<size_x-1;x++)
            {
                i = x + y * size_x;

                tmp =  Ix.get_value(i)*u.get_value(i);
                tmp += Iy.get_value(i)*v.get_value(i);
                tmp += It.get_value(i);

                Psi1.set_value(i,1.0/sqrt(1.0+(tmp*tmp)/beta1));
            }
        }

        /* calculate Psi2 */
#pragma omp parallel for
        for (int y=1;y<size_y-1;y++)
        {
            int i, i1, i2;
            double ux, uy, vx, vy;

            for (int x=1;x<size_x-1;x++)
            {
                i = x + y * size_x;

                i1 = (x-1) + y * size_x;
                i2 = (x+1) + y * size_x;
                ux = (u.get_value(i2) - u.get_value(i1)) / (2.0 * d_x);
                vx = (v.get_value(i2) - v.get_value(i1)) / (2.0 * d_x);

                i1 = x + (y-1) * size_x;
                i2 = x + (y+1) * size_x;
                uy = (u.get_value(i2) - u.get_value(i1)) / (2.0 * d_y);
                vy = (v.get_value(i2) - v.get_value(i1)) / (2.0 * d_y);

                Psi2.set_value(i,1.0/sqrt(1.0+(ux*ux+uy*uy+vx*vx+vy*vy)/beta2));
            }
        }
    }

    /* catch 3D case */
    if (size_z > 1)
    {
        /* calculate Psi1 */
#pragma omp parallel for
        for (int z=1;z<size_z-1;z++)
        {
            for (int y=1;y<size_y-1;y++)
            {
                int i;
                double tmp;

                for (int x=1;x<size_x-1;x++)
                {
                    i = x + y * size_x + z * size_x * size_y;

                    tmp =  Ix.get_value(i)*u.get_value(i);
                    tmp += Iy.get_value(i)*v.get_value(i);
                    tmp += Iz.get_value(i)*w.get_value(i);
                    tmp += It.get_value(i);

                    Psi1.set_value(i,1.0/sqrt(1.0+(tmp*tmp)/beta1));
                }
            }
        }

        /* calculate Psi2 */
#pragma omp parallel for
        for (int z=1;z<size_z-1;z++)
        {
            int i, i1, i2;
            double ux, uy, uz, vx, vy, vz, wx, wy, wz;

            for (int y=1;y<size_y-1;y++)
            {
                for (int x=1;x<size_x-1;x++)
                {
                    i = x + y * size_x + z * size_x * size_y;

                    i1 = (x-1) + y * size_x + z * size_x * size_y;
                    i2 = (x+1) + y * size_x + z * size_x * size_y;
                    ux = (u.get_value(i2) - u.get_value(i1)) / (2.0 * d_x);
                    vx = (v.get_value(i2) - v.get_value(i1)) / (2.0 * d_x);
                    wx = (w.get_value(i2) - w.get_value(i1)) / (2.0 * d_x);

                    i1 = x + (y-1) * size_x + z * size_x * size_y;
                    i2 = x + (y+1) * size_x + z * size_x * size_y;
                    uy = (u.get_value(i2) - u.get_value(i1)) / (2.0 * d_y);
                    vy = (v.get_value(i2) - v.get_value(i1)) / (2.0 * d_y);
                    wy = (w.get_value(i2) - w.get_value(i1)) / (2.0 * d_y);

                    i1 = x + y * size_x + (z-1) * size_x * size_y;
                    i2 = x + y * size_x + (z+1) * size_x * size_y;
                    uz = (u.get_value(i2) - u.get_value(i1)) / (2.0 * d_z);
                    vz = (v.get_value(i2) - v.get_value(i1)) / (2.0 * d_z);
                    wz = (w.get_value(i2) - w.get_value(i1)) / (2.0 * d_z);

                    Psi2.set_value(i,1.0/sqrt(1.0+(ux*ux+uy*uy+uz*uz+vx*vx+vy*vy+vz*vz+wx*wx+wy*wy+wz*wz)/beta2));
                }
            }
        }
    }

    //cout << "optical_flow_algo_bnq::calculate_psi END" << endl;
}


void optical_flow_algo_bnq::calculate_weighted_averages_psi()
{
    //cout << "optical_flow_algo_bnq::calculate_weighted_averages_psi" << endl;

    double d_x = Ia.get_d_x();
    double d_y = Ia.get_d_y();
    double d_z = Ia.get_d_z();
    int size_x = Ia.get_size_x();
    int size_y = Ia.get_size_y();
    int size_z = Ia.get_size_z();

    /* weighted averages in 2D */
    if (size_z == 1)
    {
        u_Avg.set_zero();
        v_Avg.set_zero();

#pragma omp parallel for
        for (int y=1;y<size_y-1;y++)
        {
            double value;
            int i, i1, i2, i3, i4;

            for (int x=1;x<size_x-1;x++)
            {
                i = x + y * size_x;

                i1 = (x-1) + y * size_x;
                i2 = (x+1) + y * size_x;
                i3 = x + (y-1) * size_x;
                i4 = x + (y+1) * size_x;

                value =  ((Psi2.get_value(i)+Psi2.get_value(i1))*u.get_value(i1) + (Psi2.get_value(i)+Psi2.get_value(i2))*u.get_value(i2)) / (2.0*d_x);
                value += ((Psi2.get_value(i)+Psi2.get_value(i3))*u.get_value(i3) + (Psi2.get_value(i)+Psi2.get_value(i4))*u.get_value(i4)) / (2.0*d_y);
                value /= Psi1.get_value(i);
                u_Avg.set_value(i,value);

                value =  ((Psi2.get_value(i)+Psi2.get_value(i1))*v.get_value(i1) + (Psi2.get_value(i)+Psi2.get_value(i2))*v.get_value(i2)) / (2.0*d_x);
                value += ((Psi2.get_value(i)+Psi2.get_value(i3))*v.get_value(i3) + (Psi2.get_value(i)+Psi2.get_value(i4))*v.get_value(i4)) / (2.0*d_y);
                value /= Psi1.get_value(i);
                v_Avg.set_value(i,value);
            }
        }
    }

    /* weighted averages in 3D */
    if (size_z > 1)
    {
        u_Avg.set_zero();
        v_Avg.set_zero();
        w_Avg.set_zero();

#pragma omp parallel for
        for (int z=1;z<size_z-1;z++)
        {
            for (int y=1;y<size_y-1;y++)
            {
                double value;
                int i, i1, i2, i3, i4, i5 ,i6;

                for (int x=1;x<size_x-1;x++)
                {
                    i = x + y * size_x + z * size_x * size_y;

                    i1 = (x-1) + y * size_x + z * size_x * size_y;
                    i2 = (x+1) + y * size_x + z * size_x * size_y;
                    i3 = x + (y-1) * size_x + z * size_x * size_y;
                    i4 = x + (y+1) * size_x + z * size_x * size_y;
                    i5 = x + y * size_x + (z-1) * size_x * size_y;
                    i6 = x + y * size_x + (z+1) * size_x * size_y;

                    value =  ((Psi2.get_value(i)+Psi2.get_value(i1))*u.get_value(i1) + (Psi2.get_value(i)+Psi2.get_value(i2))*u.get_value(i2)) / (2.0*d_x);
                    value += ((Psi2.get_value(i)+Psi2.get_value(i3))*u.get_value(i3) + (Psi2.get_value(i)+Psi2.get_value(i4))*u.get_value(i4)) / (2.0*d_y);
                    value += ((Psi2.get_value(i)+Psi2.get_value(i5))*u.get_value(i5) + (Psi2.get_value(i)+Psi2.get_value(i6))*u.get_value(i6)) / (2.0*d_z);
                    value /= Psi1.get_value(i);
                    u_Avg.set_value(i,value);

                    value =  ((Psi2.get_value(i)+Psi2.get_value(i1))*v.get_value(i1) + (Psi2.get_value(i)+Psi2.get_value(i2))*v.get_value(i2)) / (2.0*d_x);
                    value += ((Psi2.get_value(i)+Psi2.get_value(i3))*v.get_value(i3) + (Psi2.get_value(i)+Psi2.get_value(i4))*v.get_value(i4)) / (2.0*d_y);
                    value += ((Psi2.get_value(i)+Psi2.get_value(i5))*v.get_value(i5) + (Psi2.get_value(i)+Psi2.get_value(i6))*v.get_value(i6)) / (2.0*d_z);
                    value /= Psi1.get_value(i);
                    v_Avg.set_value(i,value);

                    value =  ((Psi2.get_value(i)+Psi2.get_value(i1))*w.get_value(i1) + (Psi2.get_value(i)+Psi2.get_value(i2))*w.get_value(i2)) / (2.0*d_x);
                    value += ((Psi2.get_value(i)+Psi2.get_value(i3))*w.get_value(i3) + (Psi2.get_value(i)+Psi2.get_value(i4))*w.get_value(i4)) / (2.0*d_y);
                    value += ((Psi2.get_value(i)+Psi2.get_value(i5))*w.get_value(i5) + (Psi2.get_value(i)+Psi2.get_value(i6))*w.get_value(i6)) / (2.0*d_z);
                    value /= Psi1.get_value(i);
                    w_Avg.set_value(i,value);
                }
            }
        }
    }

    //cout << "optical_flow_algo_bnq::calculate_weighted_averages_psi END" << endl;
}
