#include "optical_flow.h"


/**
 * @brief optical_flow::optical_flow
 */
optical_flow::optical_flow()
{
}


/**
 * @brief optical_flow::optical_flow
 * @param ParameterList
 */
optical_flow::optical_flow(parameter_list ParameterList)
{
    OpticalFlow = Optical_Flow_Factory.create_algorithm(ParameterList);
}


/**
 * @brief optical_flow::calculate_flow
 * @param ParameterList
 * @param Ia
 * @param Ib
 */
void optical_flow::calculate_flow(parameter_list ParameterList, image Ia, image Ib)
{
    int level = ParameterList.get_parameter_int("LEVEL",1);

    /* setup algorithm */
    /* - local parameter for selected algorithm */
    OpticalFlow->setup(ParameterList);

    /* - single level optical flow */
    if (level == 1)
    {
        /* initialize algorithm */
        OpticalFlow->initialize(Ia,Ib);

        /* run algorithm */
        OpticalFlow->calculate_flow();

        /* store results */
        u = OpticalFlow->get_u();
        v = OpticalFlow->get_v();
        if (Ia.get_size_z() > 1)
        {
            w = OpticalFlow->get_w();
        }
    }

    /* - multi level optical flow */
    if (level > 1)
    {
        double level_ratio = ParameterList.get_parameter_double("LEVEL_RATIO",0.9);

        double scale_factor;
        image Ia_local, Ib_local;
        image u_local, v_local, w_local;

        /* 2D optical flow */
        if (Ia.get_size_z() == 1)
        {
            /* loop over all level */
            for (int i=level;i>0;i--)
            {
                /* setup local copies of the original images */
                Ia_local = Ia;
                Ib_local = Ib;
                if (i != level)
                {
                    Ib_local.apply_motion_fields(u,v);
                }

                /* downsampling of images */
                scale_factor = pow(level_ratio,(i-1));
                Ia_local.resample(scale_factor);
                Ib_local.resample(scale_factor);

                /* initialize algorithm */
                OpticalFlow->initialize(Ia_local,Ib_local);

                /* run algorithm */
                OpticalFlow->calculate_flow();

                /* get results, upsample and scale vectors */
                u_local = OpticalFlow->get_u();
                v_local = OpticalFlow->get_v();

                /* here are two different scale factors used */
                /* - corresponds to Matlab implementation */
                /* - needs to be updated */
                u_local.resample(Ia.get_size_x(),Ia.get_size_y());
                v_local.resample(Ia.get_size_x(),Ia.get_size_y());

                u_local.scale(1.0/scale_factor);
                v_local.scale(1.0/scale_factor);

                /* concat motion fields */
                if (i == level)
                {
                    u = u_local;
                    v = v_local;
                }
                else
                {
                    u.concat_motion_fields_x(u_local,v_local);
                    v.concat_motion_fields_y(u_local,v_local);
                }
            }
        }

        /* 3D optical flow */
        if (Ia.get_size_z() > 1)
        {
            /* loop over all level */
            for (int i=level;i>0;i--)
            {
                cout << "optical_flow::calculate_flow " << i << " / " << level << endl;

                /* setup local copies of the original images */
                Ia_local = Ia;
                Ib_local = Ib;
                if (i != level)
                {
                    Ib_local.apply_motion_fields(u,v,w);
                }

                /* downsampling of images */
                scale_factor = pow(level_ratio,(i-1));
                Ia_local.resample(scale_factor);
                Ib_local.resample(scale_factor);

                /* initialize algorithm */
                OpticalFlow->initialize(Ia_local,Ib_local);

                /* run algorithm */
                OpticalFlow->calculate_flow();

                /* get results, upsample and scale vectors */
                u_local = OpticalFlow->get_u();
                v_local = OpticalFlow->get_v();
                w_local = OpticalFlow->get_w();

                /* here are two different scale factors used */
                /* - corresponds to Matlab implementation */
                /* - needs to be updated */
                u_local.resample(Ia.get_size_x(),Ia.get_size_y(),Ia.get_size_z());
                v_local.resample(Ia.get_size_x(),Ia.get_size_y(),Ia.get_size_z());
                w_local.resample(Ia.get_size_x(),Ia.get_size_y(),Ia.get_size_z());

                u_local.scale(1.0/scale_factor);
                v_local.scale(1.0/scale_factor);
                w_local.scale(1.0/scale_factor);

                /* concat motion fields */
                if (i == level)
                {
                    u = u_local;
                    v = v_local;
                    w = w_local;
                }
                else
                {
                    u.concat_motion_fields_x(u_local,v_local,w_local);
                    v.concat_motion_fields_y(u_local,v_local,w_local);
                    w.concat_motion_fields_z(u_local,v_local,w_local);
                }

                cout << "optical_flow::calculate_flow " << i << " / " << level << " end" << endl;
            }
        }
    }
}
