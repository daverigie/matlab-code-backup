#ifndef OPTICAL_FLOW_ALGO_BNQ_H
#define OPTICAL_FLOW_ALGO_BNQ_H


#include "optical_flow_algo.h"


class optical_flow_algo_bnq : public optical_flow_algo
{
private:
    image Psi1,Psi2;
    double alpha,beta1,beta2;
    int i_iterations,o_iterations;

public:
    optical_flow_algo_bnq();

    void calculate_psi();
    void calculate_weighted_averages_psi();

    void setup(parameter_list);
    void calculate_flow();
};


#endif // OPTICAL_FLOW_ALGO_BNQ_H
