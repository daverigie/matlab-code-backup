#include "parameter.h"


/**
 * @brief parameter::parameter
 */
parameter::parameter()
{

}


/**
 * @brief parameter::parameter
 * @param value
 */
parameter::parameter(double value)
{
    double_vector_value.push_back(value);
}


/**
 * @brief parameter::parameter
 * @param value
 */
parameter::parameter(vector<double> value)
{
    vector<double>::iterator i;

    for (i=value.begin();i!=value.end();i++)
    {
        int position = distance(value.begin(),i);
        double_vector_value.push_back(value.at(position));
    }
}


/**
 * @brief parameter::get_parameter_double
 * @return
 */
double parameter::get_parameter_double()
{
    return double_vector_value.at(0);
}


/**
 * @brief parameter::get_parameter_double_vector
 * @return
 */
vector<double> parameter::get_parameter_double_vector()
{
    return double_vector_value;
}
