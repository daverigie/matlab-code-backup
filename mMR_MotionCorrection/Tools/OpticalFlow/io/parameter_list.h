#ifndef PARAMETER_LIST_H
#define PARAMETER_LIST_H


#include "parameter.h"


class parameter_list
{
private:
    vector<string> vector_name;
    vector<parameter> vector_value;

public:
    parameter_list();

    void add_parameter_double(string, double);
    void add_parameter_double_vector(string, vector<double>);

    double get_parameter_double(string);
    double get_parameter_double(string, double);

    vector<double> get_parameter_double_vector(string);
    vector<double> get_parameter_double_vector(string, double, int);

    int get_parameter_int(string);
    int get_parameter_int(string, int);
};


#endif // PARAMETER_LIST_H
