#ifndef PARAMETER_H
#define PARAMETER_H

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class parameter
{
protected:
    vector<double> double_vector_value;
    string string_value;

public:
    parameter();
    parameter(double);
    parameter(vector<double> value);

    double get_parameter_double();
    vector<double> get_parameter_double_vector();
};

#endif // PARAMETER_H
