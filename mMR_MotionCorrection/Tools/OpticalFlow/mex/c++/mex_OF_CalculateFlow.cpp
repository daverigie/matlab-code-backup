#include "mex_parse_image.h"
#include "mex_parse_parameter.h"
#include "../../main/optical_flow.h"
#include "../../main/optical_flow_algo_bnq.h"


using namespace std;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if ((nlhs == 2 || nlhs == 3) && nrhs == 3)
    {
        /* parse input */
        parameter_list ParameterList = mex_parse_parameter(prhs,0);
        image Ia = mex_parse_image(ParameterList,prhs,1);
        image Ib = mex_parse_image(ParameterList,prhs,2);
        
        optical_flow OpticalFlow(ParameterList);
        OpticalFlow.calculate_flow(ParameterList,Ia,Ib);
                
        /* create output */
        mex_copy_image(OpticalFlow.get_u(),plhs,0);
        mex_copy_image(OpticalFlow.get_v(),plhs,1);
        
        if (nlhs == 3)
        {
            mex_copy_image(OpticalFlow.get_w(),plhs,2);
        }
    }
    else
    {
        mexPrintf("usage:\n[u,v,[w]] = mex_OF_CalculateFlow(parm,Ia,Ib);\n");
    }
}
