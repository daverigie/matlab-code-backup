#include "mex_parse_image.h"
#include "mex_parse_parameter.h"


using namespace std;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if (nlhs == 1 && (nrhs == 4 || nrhs == 5))
    {
        /* parse input */
        parameter_list ParameterList = mex_parse_parameter(prhs,0);
        image Image = mex_parse_image(ParameterList,prhs,1);
        image u = mex_parse_image(ParameterList,prhs,2);
        image v = mex_parse_image(ParameterList,prhs,3);
        
        /* apply motion vectors */
        if (nrhs == 4)
        {
            Image.apply_motion_fields(u,v);
        }
        else
        {
            image w = mex_parse_image(ParameterList,prhs,4);
            Image.apply_motion_fields(u,v,w);
        }
        
        /* create output */
        mex_copy_image(Image,plhs,0);
    }
    else
    {
        mexPrintf("usage:\n[new_image] = mex_OF_ApplyMotionCorrection(parm,old_image,u,v,[w]);\n");
    }
}
