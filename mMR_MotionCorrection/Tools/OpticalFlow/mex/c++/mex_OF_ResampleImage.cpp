#include "mex_parse_image.h"
#include "mex_parse_parameter.h"


using namespace std;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if (nlhs == 1 && nrhs == 3)
    {
        /* parse input */
        parameter_list ParameterList1 = mex_parse_parameter(prhs,0);
        parameter_list ParameterList2 = mex_parse_parameter(prhs,1);
        
        /* resample image */
        image Image = mex_parse_image(ParameterList1,prhs,2);
        double factor_x = ParameterList2.get_parameter_double("SIZE_X",1.0) / ParameterList1.get_parameter_double("SIZE_X",1.0);
        double factor_y = ParameterList2.get_parameter_double("SIZE_Y",1.0) / ParameterList1.get_parameter_double("SIZE_Y",1.0);
        if (Image.get_size_z() == 1)
        {
            Image.resample(factor_x,factor_y);
        }
        if (Image.get_size_z() > 1)
        {
            double factor_z = ParameterList2.get_parameter_double("SIZE_Z",1.0) / ParameterList1.get_parameter_double("SIZE_Z",1.0);
            Image.resample(factor_x,factor_y,factor_z);
        }
        
        /* create output */
        mex_copy_image(Image,plhs,0);
    }
    else
    {
        mexPrintf("usage:\n[new_image] = mex_OF_ResampleImage(parm1,parm2,old_image);\n");
    }
}
