clear mex; clear; clc;

%mex COPTIMFLAGS=-O3 -DNDEBUG c++\mex_OF_ConcatMotionVectors.cpp c++\mex_parse_image.cpp c++\mex_parse_parameter.cpp ..\io\parameter_list.cpp ..\io\parameter.cpp ..\tools\image.cpp
%mex COPTIMFLAGS=-O3 -DNDEBUG c++\mex_OF_ResampleImage.cpp c++\mex_parse_image.cpp c++\mex_parse_parameter.cpp ..\io\parameter_list.cpp ..\io\parameter.cpp ..\tools\image.cpp
%mex COPTIMFLAGS=-O3 -DNDEBUG c++\mex_OF_SmoothImage.cpp c++\mex_parse_image.cpp c++\mex_parse_parameter.cpp ..\io\parameter_list.cpp ..\io\parameter.cpp ..\tools\image.cpp

mex -outdir bin COPTIMFLAGS=-O3 -DNDEBUG c++\mex_OF_ApplyMotionCorrection.cpp c++\mex_parse_image.cpp c++\mex_parse_parameter.cpp ..\io\parameter_list.cpp ..\io\parameter.cpp ..\tools\image.cpp
copyfile('matlab/OF_ApplyMotionCorrection_NEW.m','bin/OF_ApplyMotionCorrection_NEW.m');

mex -outdir bin COPTIMFLAGS=-O3 -DNDEBUG c++\mex_OF_CalculateFlow.cpp c++\mex_parse_image.cpp c++\mex_parse_parameter.cpp ..\io\parameter_list.cpp ..\io\parameter.cpp ..\tools\image.cpp ...
    ..\main\optical_flow.cpp ..\main\optical_flow_algo.cpp ..\main\optical_flow_algo_bnq.cpp ..\main\optical_flow_factory.cpp
copyfile('matlab/OF_CalculateFlow_NEW.m','bin/OF_CalculateFlow_NEW.m');

copyfile('matlab/CreateMotionVectorHeader.m','bin/CreateMotionVectorHeader.m');
copyfile('matlab/CreateMotionVectorHeader_SingleVector.m','bin/CreateMotionVectorHeader_SingleVector.m');
copyfile('matlab/OF_MotionEstimation_Demo.m','bin/OF_MotionEstimation_Demo.m');
