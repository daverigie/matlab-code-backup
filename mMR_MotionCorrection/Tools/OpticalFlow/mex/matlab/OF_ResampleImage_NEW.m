function [new_I] = OF_ResampleImage_NEW(parmA,parmB,I)

% OF_ResampleImage_NEW(parmA,parmB,I)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nOF_ResampleImage_NEW\n');
    fprintf('Usage: [new_I] = OF_ResampleImage_NEW(parmA,parmB,I)\n\n');
    fprintf('\n');
    new_I = -1;
else
    fprintf('OF_ResampleImage_NEW...\n');
    tic;
    
    % resample image
    new_I = mex_OF_ResampleImage(parmA,parmB,I(:));
    
    % reshape image to new size
    new_I = reshape(new_I,parmB.SIZE_X,parmB.SIZE_Y,parmB.SIZE_Z);
    
    fprintf('OF_ResampleImage_NEW...done. [%f sec]\n',toc);
end

end
