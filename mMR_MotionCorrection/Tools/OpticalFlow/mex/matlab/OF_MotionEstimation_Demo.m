close all; clear; clear mex; clc;

% extend path
addpath(genpath('../OpticalFlow'));

% set filename
filename = 'Patient3_MID02763_mr_recon_regridded_Moco.mat';
target_gate = 1;

I_ITERATIONS = [15 15];
O_ITERATIONS = [1 3];
LEVEL        = [20 20];
LEVEL_RATIO  = [0.9 0.9];
ALPHA        = 40.0;
BETA1        = 2.0;
BETA2        = 1.0;
SMOOTH_BEFORE_DOWNSAMPLING = 1;
SMOOTH_AFTER_UPSAMPLING = 0;

%% no modifications below this line %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=1:numel(I_ITERATIONS)
    targetfolder = sprintf('%s/%d/setup_%d',pwd,target_gate,k);
    
    % load data
    load(filename);
    
    % - we need double
    recon_cs = double(abs(data(:,:,:,:)));
    
    % algo parameter
    parm_OF.SIZE_X = size(recon_cs,1);
    parm_OF.SIZE_Y = size(recon_cs,2);
    parm_OF.SIZE_Z = size(recon_cs,3);
    parm_OF.D_X = 4.17252;
    parm_OF.D_Y = 4.17252;
    parm_OF.D_Z = 2.03125;
    
    parm_OF.O_ITERATIONS = O_ITERATIONS(min(end,k));
    parm_OF.I_ITERATIONS = I_ITERATIONS(min(end,k));
    parm_OF.ALPHA        = ALPHA(min(end,k));
    parm_OF.LEVEL        = LEVEL(min(end,k));
    parm_OF.LEVEL_RATIO  = LEVEL_RATIO(min(end,k));
    parm_OF.BETA1        = BETA1(min(end,k));
    parm_OF.BETA2        = BETA2(min(end,k));
    parm_OF.SMOOTH_BEFORE_DOWNSAMPLING = SMOOTH_BEFORE_DOWNSAMPLING(min(end,k));
    parm_OF.SMOOTH_AFTER_UPSAMPLING    = SMOOTH_AFTER_UPSAMPLING(min(end,k));
    
    % no modification below this line #########################################
    scale = 100.0;
    gates = size(recon_cs,4);
    
    corr_before = ones(gates,1);
    corr_after = ones(gates,1);
    
    Ia = recon_cs(:,:,:,target_gate);
    Ia = Ia - min(Ia(:));
    Ia = Ia / max(Ia(:)) * scale;
    
    vecs = zeros(parm_OF.SIZE_X,parm_OF.SIZE_Y,parm_OF.SIZE_Z,3,gates);
    vecsInv = zeros(parm_OF.SIZE_X,parm_OF.SIZE_Y,parm_OF.SIZE_Z,3,gates);
    
    for i=1:gates
        fprintf('estimating motion for gate [%d]...\n',i);
        gate_tic = tic;
        if (i ~= target_gate)
            Ib = recon_cs(:,:,:,i);
            Ib = Ib - min(Ib(:));
            Ib = Ib / max(Ib(:)) * scale;
            
            [u,v,w] = OF_CalculateFlow_NEW(parm_OF,Ia,Ib);
            Ib_corrected = OF_ApplyMotionCorrection_NEW(parm_OF,Ib,u,v,w);
            corr_before(i) = corr(Ia(:),Ib(:));
            corr_after(i) = corr(Ia(:),Ib_corrected(:));
            fprintf('before correction: %f\n',corr_before(i));
            fprintf(' after correction: %f\n',corr_after(i));
            vecs(:,:,:,1,i) = u/parm_OF.D_X;
            vecs(:,:,:,2,i) = v/parm_OF.D_Y;
            vecs(:,:,:,3,i) = w/parm_OF.D_Z;
            
            [u,v,w] = OF_CalculateFlow_NEW(parm_OF,Ib,Ia);
            vecsInv(:,:,:,1,i) = u/parm_OF.D_X;
            vecsInv(:,:,:,2,i) = v/parm_OF.D_Y;
            vecsInv(:,:,:,3,i) = w/parm_OF.D_Z;
        end
        fprintf('estimating motion for gate [%d]...done. [%f]\n',i,toc(gate_tic));
    end
    
    CreateMotionVectorHeader(targetfolder,vecs,vecsInv);
    
    figure(1);
    y_slice = 78;
    subplot(2,3,1);
    imagesc(flip(rot90(squeeze(Ia(:,y_slice,:))),1),[0 50]);
    axis image;
    subplot(2,3,2);
    imagesc(flip(rot90(squeeze(Ib_corrected(:,y_slice,:))),1),[0 50]);
    axis image;
    subplot(2,3,3);
    imagesc(flip(rot90(squeeze(Ib(:,y_slice,:))),1),[0 50]);
    axis image;
    subplot(2,3,3);
    plot(corr_before,'blue'); hold on;
    plot(corr_after,'red'); hold off;
    axis([1 gates 0.8 1]);
    subplot(2,3,5);
    imagesc(flip(rot90(squeeze(Ia(:,y_slice,:))),1)-flip(rot90(squeeze(Ib_corrected(:,y_slice,:))),1),[-5 5]);
    axis image;
    subplot(2,3,6);
    imagesc(flip(rot90(squeeze(Ia(:,y_slice,:))),1)-flip(rot90(squeeze(Ib(:,y_slice,:))),1),[-5 5]);
    axis image;
    saveas(gcf,sprintf('%s/setup_%d.png',targetfolder,k),'png');
    close(1);
    
    save(sprintf('%s/setup_%d.mat',targetfolder,k),'parm_OF','y_slice','corr_before','corr_after');
end


