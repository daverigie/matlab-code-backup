%% clear and close
close all; clear mex; clear; clc;


%% set path to EMrecon
cpwd = pwd;
run('../../../Source/MeX/SetupMeXEMrecon.m');
addpath(genpath(sprintf('%s/../../Bin',path_to_emrecon_mex_source)));


%% display info of installed EMrecon version
EMrecon_Version;


%% setup parameter
% setup scanner type
parm.SCANNERTYPE = 8;
% setup reconstruction volume
parm.FOV_X = 717.6734;
parm.FOV_Y = parm.FOV_X;
parm.FOV_Z = 257.9688;
parm.OFFSET_X = -parm.FOV_X/2.0;
parm.OFFSET_Y = -parm.FOV_Y/2.0;
parm.OFFSET_Z = -parm.FOV_Z/2.0;
parm.SIZE_X = 172;
parm.SIZE_Y = parm.SIZE_X;
parm.SIZE_Z = 127;
% reconstruction parameter
parm.ITERATIONS = 1;
parm.SUBSETS = 21;
parm.EPSILON = 0.0001;
parm.FIXED_SUBSET = -1;
parm.MAX_RING_DIFF = 5; %set to 60 in order to use all data;


%% prepare data
% EMrecon_ForwardProj_ND(parm,image)
fprintf('prepare data...\n');
% if data.mat from normalization example exists, use this data; otherwise phantom.
if (exist('../Normalization/data.mat','file') == 2)
    load('../Normalization/data.mat');
    % use only direct sinograms in case of MAX_RING_DIFF = 5
    if (parm.MAX_RING_DIFF == 5)
        prompt = prompt(1:344*252*127);
        delayed = delayed(1:344*252*127);
    end
else
    phantom_size = 128;
    offset_x = (parm.SIZE_X-phantom_size)/2;
    offset_y = (parm.SIZE_Y-phantom_size)/2;
    reference = zeros(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);
    for z=1:parm.SIZE_Z
        reference(1+offset_x:end-offset_x,1+offset_y:end-offset_y,z) = phantom(phantom_size);
    end
    reference = reference - min(reference(:)); % EM requires images >= 0
    prompt = EMrecon_ForwardProj_ND(parm,reference);
    delayed = zeros(size(prompt));
end
fprintf('prepare data...done.\n\n');


%% Create sensitivity_map for EM reconstruction
% EMrecon_BackProj_ND(parm,data)
fprintf('Create sensitivity_map...\n');
parm.FIXED_SUBSET = -1;
sensitivity_map = EMrecon_BackProj_ND(parm,ones(size(prompt)));
fprintf('Create sensitivity_map...done.\n\n');


%% EM reconstruction (loop within MeX)
% EMrecon_EM_ND(parm,data,data_noise,sensitivity_map,startimage)
fprintf('EM reconstruction (loop within MeX)...\n');
em_1 = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);
em_1(sensitivity_map <= 0.0) = 0.0;
em_1 = EMrecon_EM_ND(parm,prompt,delayed,sensitivity_map,em_1);
fprintf('EM reconstruction (loop within MeX)...done.\n\n');


%% EM reconstruction (loop within Matlab)
% EMrecon_EM_ND(parm,data,data_noise,sensitivity_map,startimage)
fprintf('EM reconstruction (loop within Matlab)...\n');
em_2 = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);
em_2(sensitivity_map <= 0.0) = 0.0;
iterations = parm.ITERATIONS;
parm.ITERATIONS = 1;
for i=1:iterations
    em_2 = EMrecon_EM_ND(parm,prompt,delayed,sensitivity_map,em_2);
end
parm.ITERATIONS = iterations;
fprintf('EM reconstruction (loop within Matlab)...done.\n\n');


%% EM reconstruction using forward and backward operator
fprintf('EM reconstruction (using FP and BP operator)...\n');
em_3 = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);
em_3(sensitivity_map <= 0.0) = 0.0;
for i=1:parm.ITERATIONS
    for s=1:parm.SUBSETS
        parm.FIXED_SUBSET = s;
        % fwp project
        fwdp = EMrecon_ForwardProj_ND(parm,em_3) + delayed;
        % compare
        c = zeros(size(fwdp)); c(fwdp>parm.EPSILON) = parm.SUBSETS * prompt(fwdp>parm.EPSILON) ./ fwdp(fwdp>parm.EPSILON);
        % back project
        back = EMrecon_BackProj_ND(parm,c);
        % image update
        index_image_update = (em_3 > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map > parm.EPSILON);
        em_3(index_image_update) = em_3(index_image_update) .* back(index_image_update) ./ sensitivity_map(index_image_update);
        em_3(~index_image_update) = 0.0;
    end
end
fprintf('EM reconstruction (using FP and BP operator)...done.\n\n');


%% show results
if (exist('../Normalization/data.mat','file') ~= 2)
    subplot(2,2,1);
    imagesc(flipdim(reference(:,:,round(parm.SIZE_Z/2.0)),1), [0 0.1*max(em_1(:))]); axis xy; axis image;
    title('reference image');
end

subplot(2,2,2);
imagesc(flipdim(em_1(:,:,round(parm.SIZE_Z/2.0)),1), [0 0.1*max(em_1(:))]); axis xy; axis image;
title('em reconstruction (loop within MeX)');

subplot(2,2,3);
imagesc(flipdim(em_2(:,:,round(parm.SIZE_Z/2.0)),1), [0 0.1*max(em_1(:))]); axis xy; axis image;
title('em reconstruction (loop within Matlab)');

subplot(2,2,4);
imagesc(flipdim(em_3(:,:,round(parm.SIZE_Z/2.0)),1), [0 0.1*max(em_1(:))]); axis xy; axis image;
title('em reconstruction (using FP and BP operator)');


%% display EMrecon version
EMrecon_Version;

%% l2 dist
l2_e1_e2 = norm(em_1(:)-em_2(:),2);
l2_e1_e3 = norm(em_1(:)-em_3(:),2);
l2_e2_e3 = norm(em_2(:)-em_3(:),2);

disp(l2_e1_e2);
disp(l2_e1_e3);
disp(l2_e2_e3);
