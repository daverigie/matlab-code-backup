%% clear and close
close all; clear mex; clear; clc;


%% set path to EMrecon
cpwd = pwd;
run('../../../Source/MeX/SetupMeXEMrecon.m');
addpath(genpath(sprintf('%s/../../Bin',path_to_emrecon_mex_source)));


%% display info of installed EMrecon version
EMrecon_Version;


%% choose whether to reconstruct only with direct sinograms (for testing)
use_only_direct_sinograms = 1;


%% choose whether to reconstruct just a single slice (for testing)
use_only_single_slice = 1;


%% setup parameter
% setup scanner type
parm.SCANNERTYPE = 4;
% setup reconstruction volume
parm.SIZE_X = 128;
parm.SIZE_Y = 128;
parm.SIZE_Z = 159;
parm.FOV_X = parm.SIZE_X * 0.776383;
parm.FOV_Y = parm.SIZE_Y * 0.776383;
parm.FOV_Z = parm.SIZE_Z * 0.796;
parm.OFFSET_X = -parm.FOV_X/2.0;
parm.OFFSET_Y = -parm.FOV_Y/2.0;
parm.OFFSET_Z = -parm.FOV_Z/2.0;
parm.OFFSET_C = 48.0;
% reconstruction parameter
parm.ITERATIONS = 3;
parm.SUBSETS = 16;
% set verbosity level (disable log with 0. more info with increased level)
parm.VERBOSE = 1;
% set amount of data to be used for reconstruction
if (use_only_direct_sinograms == 1)
    parm.MAX_RING_DIFF = 3;
else
    parm.MAX_RING_DIFF = 79;
end
% modify parm.SIZE_Z if only one big slice should be reconstructed
if (use_only_single_slice == 1)
    parm.SIZE_Z = 1;
end


%% prepare data
fprintf('prepare data...\n');
tic_data = tic;
% if data.mat from rebin example exists, use this data; otherwise phantom.
if (exist('../Rebin/data.mat','file') == 2)
    load('../Rebin/data.mat');
else
    reference = zeros(parm.SIZE_X,parm.SIZE_X,parm.SIZE_Z);
    pcenter = round(parm.SIZE_X/2.0);
    for z=1:parm.SIZE_Z
        reference(pcenter-10:pcenter+10,pcenter-10:pcenter+10,z) = 1.0;
    end
    prompt = EMrecon_ForwardProj_ND(parm,reference);
    delayed = zeros(size(prompt));
end
if (use_only_direct_sinograms == 1)
    prompt = prompt(1:160*128*159);
    delayed = delayed(1:160*128*159);
end
fprintf('prepare data...done. (%f sec)\n\n',toc(tic_data));


%% Create sensitivity_map for EM reconstruction
% - geometric normalization is calculated by backprojection of 1
% EMrecon_BackProj_ND(parm,data)
fprintf('Create sensitivity_map...\n');
tic_sens = tic;
sensitivity_map = EMrecon_BackProj_ND(parm,ones(size(prompt)));
fprintf('Create sensitivity_map...done. (%f sec)\n\n',toc(tic_sens));


%% EM reconstruction (loop within MeX)
% EMrecon_EM_ND(parm,data,data_noise,sensitivity_map,startimage)
fprintf('EM reconstruction (loop within MeX)...\n');
em_1 = ones(parm.SIZE_X,parm.SIZE_Y,parm.SIZE_Z);
em_1 = EMrecon_EM_ND(parm,prompt,delayed,sensitivity_map,em_1);
fprintf('EM reconstruction (loop within MeX)...done.\n\n');


%% show results
subplot(2,3,1);
imagesc(squeeze(sensitivity_map(:,:,round(parm.SIZE_Z/2.0)))); axis xy; axis image;
title('sensitivity\_map (x,y)');

subplot(2,3,2);
imagesc(squeeze(sensitivity_map(:,round(parm.SIZE_Y/2.0),:))'); axis xy; axis image;
title('sensitivity\_map (x,z)');

subplot(2,3,3);
imagesc(squeeze(sensitivity_map(round(parm.SIZE_X/2.0),:,:))'); axis xy; axis image;
title('sensitivity\_map (y,z)');

subplot(2,3,4);
imagesc(squeeze(sum(em_1,3))); axis xy; axis image;
title('em (x,y)');

subplot(2,3,5);
imagesc(flipdim(squeeze(sum(em_1,2)),1)'); axis xy; axis image;
title('em (x,z)');

subplot(2,3,6);
imagesc(squeeze(sum(em_1,1))'); axis xy; axis image;
title('em (y,z)');
