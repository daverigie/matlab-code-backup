%% clear and close
close all; clear mex; clear; clc;


%% set path to EMrecon
cpwd = pwd;
run('../../../Source/MeX/SetupMeXEMrecon.m');
addpath(genpath(sprintf('%s/../../Bin',path_to_emrecon_mex_source)));


%% display info of installed EMrecon version
EMrecon_Version;


%% setup parameter
% setup scanner type
parm.SCANNERTYPE = 1;
% define scanner specific parameter
parm.PROJECTIONS = 128;
parm.ANGLES = 128;
parm.BIN_WIDTH = 1.0; % mm
parm.RADIUS = 100.0; % mm
% setup reconstruction volume
parm.SIZE_X = 128;
parm.SIZE_Y = parm.SIZE_X;
parm.SIZE_Z = 1;
parm.FOV_X = 128.0; % mm
parm.FOV_Y = parm.FOV_X;
parm.FOV_Z = 1.0;
parm.OFFSET_X = -parm.FOV_X/2.0;
parm.OFFSET_Y = -parm.FOV_Y/2.0;
parm.OFFSET_Z = -parm.FOV_Z/2.0;
parm.OFFSET_C = 64.0; % mm (everything outside a ring of 64mm is set to 0)
% reconstruction parameter
parm.ITERATIONS = 3;
parm.SUBSETS = 16;
% set verbosity level (disable log with 0. more info with increased level)
parm.VERBOSE = 1;
% used for the image update (see below for example)
parm.EPSILON = 0.0001;
parm.FIXED_SUBSET = -1;


%% prepare data
% - create demo data using forward projection
% EMrecon_ForwardProj_ND(parm,image)
fprintf('prepare data...\n');
% check whether image processing toolbox is available
v = ver;
if (any(strcmp('Image Processing Toolbox', {v.Name})) == 1)
    reference = phantom(parm.SIZE_X);
    reference = reference - min(reference(:)); % EM requires images >= 0
else
    reference = zeros(parm.SIZE_X,parm.SIZE_X);
    pcenter = round(parm.SIZE_X/2.0);
    reference(pcenter-10:pcenter+10,pcenter-10:pcenter+10) = 1.0;
end
newdata = EMrecon_ForwardProj_ND(parm,reference);
fprintf('prepare data...done.\n\n');


%% Create sensitivity_map for EM reconstruction
% - geometric normalization is calculated by backprojection of 1
% EMrecon_BackProj_ND(parm,data)
fprintf('Create sensitivity_map...\n');
sensitivity_map = EMrecon_BackProj_ND(parm,ones(size(newdata)));
fprintf('Create sensitivity_map...done.\n\n');


%% EM reconstruction (loop within MeX)
% EMrecon_EM_ND(parm,data,data_noise,sensitivity_map,startimage)
fprintf('EM reconstruction (loop within MeX)...\n');
em_1 = ones(parm.SIZE_X,parm.SIZE_Y);
em_1(sensitivity_map <= 0.0) = 0.0;
em_1 = EMrecon_EM_ND(parm,newdata,zeros(size(newdata)),sensitivity_map,em_1);
fprintf('EM reconstruction (loop within MeX)...done.\n\n');


%% EM reconstruction (loop within Matlab)
% EMrecon_EM_ND(parm,data,data_noise,sensitivity_map,startimage)
fprintf('EM reconstruction (loop within Matlab)...\n');
em_2 = ones(parm.SIZE_X,parm.SIZE_Y);
em_2(sensitivity_map <= 0.0) = 0.0;
iterations = parm.ITERATIONS;
parm.ITERATIONS = 1;
for i=1:iterations
    em_2 = EMrecon_EM_ND(parm,newdata,zeros(size(newdata)),sensitivity_map,em_2);
end
parm.ITERATIONS = iterations;
fprintf('EM reconstruction (loop within Matlab)...done.\n\n');


%% EM reconstruction using forward and backward operator
fprintf('EM reconstruction (using FP and BP operator)...\n');
em_3 = ones(parm.SIZE_X,parm.SIZE_Y);
em_3(sensitivity_map <= 0.0) = 0.0;
for i=1:parm.ITERATIONS
    for s=1:parm.SUBSETS
        parm.FIXED_SUBSET = s;
        % fwp project
        fwdp = EMrecon_ForwardProj_ND(parm,em_3);
        % compare
        c = zeros(size(fwdp)); c(fwdp>parm.EPSILON) = parm.SUBSETS * newdata(fwdp>parm.EPSILON) ./ fwdp(fwdp>parm.EPSILON);
        % back project
        back = EMrecon_BackProj_ND(parm,c);
        % image update
        index_image_update = (em_3 > parm.EPSILON) & (back > parm.EPSILON) & (sensitivity_map > parm.EPSILON);
        em_3(index_image_update) = em_3(index_image_update) .* back(index_image_update) ./ sensitivity_map(index_image_update);
        em_3(~index_image_update) = 0.0;
    end
end
fprintf('EM reconstruction (using FP and BP operator)...done.\n\n');


%% show results
subplot(2,3,1);
imagesc(flipdim(reference,1), [0 1]); axis xy; axis image;
title('reference image');

subplot(2,3,2);
imagesc(reshape(newdata,parm.PROJECTIONS,parm.ANGLES)'); axis xy; axis image;
xlabel('projection'); ylabel('angle'); title('forward projection (data)');

subplot(2,3,4);
imagesc(flipdim(em_1,1)); axis xy; axis image;
title('em reconstruction (loop within Matlab)');

subplot(2,3,5);
imagesc(flipdim(em_2,1)); axis xy; axis image;
title('em reconstruction (loop within MeX)');

subplot(2,3,6);
imagesc(flipdim(em_3,1)); axis xy; axis image;
title('em reconstruction (using FP and BP operator)');


%% calculate 2-norm to estimate numerical error
% - when using a single core, the reconstruction results are identical,
%   independent of where the actual image update is done. hence one can
%   easily create new algorithms using e.g. fp and bp only and compare the
%   results to the reference (OS)EM fully implemented in C.
% - when using multicore, the error should still be negligible.
norm_ref_em1 = norm(reference(:)-em_1(:),2);
norm_ref_em2 = norm(reference(:)-em_2(:),2);
norm_ref_em3 = norm(reference(:)-em_3(:),2);
fprintf('2-norm(ref-em_1) = %f\n',norm_ref_em1);
fprintf('2-norm(ref-em_2) = %f\n',norm_ref_em2);
fprintf('2-norm(ref-em_3) = %f\n',norm_ref_em3);

norm_em1_em2 = norm(em_1(:)-em_2(:),2);
norm_em1_em3 = norm(em_1(:)-em_3(:),2);
norm_em2_em3 = norm(em_2(:)-em_3(:),2);
fprintf('2-norm(em_1-em_2) = %f\n',norm_em1_em2);
fprintf('2-norm(em_1-em_3) = %f\n',norm_em1_em3);
fprintf('2-norm(em_2-em_3) = %f\n',norm_em2_em3);
