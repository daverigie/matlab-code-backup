/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "ForwardProjection.h"


/* ############################################################################################# */
/* ### ForwardProjection ####################################################################### */
/* ############################################################################################# */
/**
 * @brief ForwardProjection
 * @param input_image
 * @param output_data
 */
void ForwardProjection(double *input_image, double *output_data)
{
    long i;

    double *local_copy;
    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file,sizeof(event)));
    read_events(events_in_file,Eventvector);

    local_copy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        local_copy[i] = input_image[i];
    }

    cut_image(local_copy);

    /* convolution with a gaussian kernel */
    if (Parameter.convolve == 1)
    {
        convolve(local_copy,local_copy);
    }

    /* assure that output data is zero */
#pragma omp parallel for
    for (i=0;i<events_in_file;i++)
    {
        output_data[i] = 0.0;
    }

    /* event loop */
#pragma omp parallel for firstprivate(Path)
    for (i=0;i<events_in_file;i++)
    {
        if (Eventvector[i].valid == 1)
        {
            int j;

            if (Path == 0)
            {
                Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
            }

            compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

            for (j=0;Path[j].coord!=-1;j++)
            {
                output_data[i] += local_copy[Path[j].coord]*Path[j].length;
            }
        }
    }
    /* event loop end */

    free(Eventvector);
    free(local_copy);
    free(PathOMP);
}


/* ############################################################################################# */
/* ### ForwardProjectionAC ##################################################################### */
/* ############################################################################################# */
/**
 * @brief ForwardProjectionAC
 * @param input_image
 * @param input_data
 * @param output_data
 */
void ForwardProjectionAC(double *input_image, double *input_data, double *output_data)
{
    long i;

    double *local_copy;
    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file,sizeof(event)));
    read_events(events_in_file,Eventvector);

    local_copy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        local_copy[i] = input_image[i];
    }

    cut_image(local_copy);

    /* convolution with a gaussian kernel */
    if (Parameter.convolve == 1)
    {
        convolve(local_copy,local_copy);
    }

    /* assure that output data is zero */
#pragma omp parallel for
    for (i=0;i<events_in_file;i++)
    {
        output_data[i] = 0.0;
    }

    /* event loop */
#pragma omp parallel for firstprivate(Path)
    for (i=0;i<events_in_file;i++)
    {
        if (Eventvector[i].valid == 1)
        {
            int j;
            double attenuation = 0.0;

            if (Path == 0)
            {
                Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
            }

            compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

            for (j=0;Path[j].coord!=-1;j++)
            {
                attenuation += local_copy[Path[j].coord]*Path[j].length;
            }

            output_data[i] = input_data[i]*exp(attenuation/10.0);
        }
    }
    /* event loop end */

    free(Eventvector);
    free(local_copy);
    free(PathOMP);
}


/* ############################################################################################# */
/* ### ForwardProjectionMask ################################################################### */
/* ############################################################################################# */
/**
 * @brief ForwardProjectionMask
 * @param input_image
 * @param input_mask
 * @param output_data
 */
void ForwardProjectionMask(double *input_image, double *input_mask, double *output_data)
{
    long i;

    double *local_copy;
    event *Eventvector;
    path_element *Path = 0;
    path_element *PathOMP = safe_calloc_path();

    long events_in_file = get_number_of_events_in_file();
    setup_siddon();

    Eventvector = (event*)(safe_calloc(events_in_file,sizeof(event)));
    read_events(events_in_file,Eventvector);

    local_copy = (double*)(safe_calloc(Parameter.imagesize, sizeof(double)));

#pragma omp parallel for
    for (i=0;i<Parameter.imagesize;i++)
    {
        local_copy[i] = input_image[i];
    }

    cut_image(local_copy);

    /* convolution with a gaussian kernel */
    if (Parameter.convolve == 1)
    {
        convolve(local_copy,local_copy);
    }

    /* assure that output data is zero */
#pragma omp parallel for
    for (i=0;i<events_in_file;i++)
    {
        output_data[i] = 0.0;
    }

    /* event loop */
#pragma omp parallel for firstprivate(Path)
    for (i=0;i<events_in_file;i++)
    {
        if (Eventvector[i].valid == 1 && input_mask[i] > 0.0)
        {
            int j;

            if (Path == 0)
            {
                Path = PathOMP + safe_omp_get_thread_num() * (Parameter.size_x+Parameter.size_y+Parameter.size_z);
            }

            compute_path(Eventvector[i].x1, Eventvector[i].y1, Eventvector[i].z1, Eventvector[i].x2, Eventvector[i].y2, Eventvector[i].z2, Path);

            for (j=0;Path[j].coord!=-1;j++)
            {
                output_data[i] += local_copy[Path[j].coord]*input_mask[i]*Path[j].length;
            }
        }
    }
    /* event loop end */

    free(Eventvector);
    free(local_copy);
    free(PathOMP);
}
