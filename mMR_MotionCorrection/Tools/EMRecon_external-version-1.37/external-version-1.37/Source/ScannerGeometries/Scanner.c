/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "Scanner.h"


/* ############################################################################################# */
/* ### get_number_of_events_in_file ############################################################ */
/* ############################################################################################# */
/**
 * @brief get_number_of_events_in_file
 * @return
 */
long get_number_of_events_in_file()
{
    long number_of_events_in_file = -1;

    switch (Parameter.scannertype)
    {
#ifdef PET_2D_DEMO
    case 1:
        number_of_events_in_file = pet_2d_demo_get_number_of_events_in_file();
        break;
#endif

#ifdef PET_SIEMENS_INVEON
    case 4:
        number_of_events_in_file = pet_michelogram_get_number_of_events_in_file();
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        number_of_events_in_file = pet_michelogram_get_number_of_events_in_file();
        break;
#endif

#ifdef PET_GE_DISCOVERY_ST
    case 10:
        number_of_events_in_file = pet_michelogram_get_number_of_events_in_file();
        break;
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    case 21:
        number_of_events_in_file = ct_siemens_definition_as_get_number_of_events_in_file();
        break;
#endif
    }
    return number_of_events_in_file;
}


/* ############################################################################################# */
/* ### read_events ############################################################################# */
/* ############################################################################################# */
/**
 * @brief read_events
 * @param events_to_read
 * @param Eventvector
 */
void read_events(long events_to_read, event *Eventvector)
{
    switch (Parameter.scannertype)
    {
#ifdef PET_2D_DEMO
    case 1:
        pet_2d_demo_read_events(events_to_read,Eventvector);
        break;
#endif

#ifdef PET_SIEMENS_INVEON
    case 4:
        pet_michelogram_read_events(events_to_read,Eventvector);
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        pet_michelogram_read_events(events_to_read,Eventvector);
        break;
#endif

#ifdef PET_GE_DISCOVERY_ST
    case 10:
        pet_michelogram_read_events(events_to_read,Eventvector);
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    case 21:
        ct_siemens_definition_as_read_events(events_to_read,Eventvector);
        break;
#endif
    }
}


/* ############################################################################################# */
/* ### release_memory ########################################################################## */
/* ############################################################################################# */
/**
 * @brief release_memory
 */
void release_memory()
{
    switch (Parameter.scannertype)
    {
#ifdef PET_SIEMENS_INVEON
    case 4:
        pet_michelogram_release_memory();
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        pet_michelogram_release_memory();
        break;
#endif

#ifdef PET_GE_DISCOVERY_ST
    case 10:
        pet_michelogram_release_memory();
#endif
    }
}


/* ############################################################################################# */
/* ### scanner_info ############################################################################ */
/* ############################################################################################# */
/**
 * @brief scanner_info
 * @return
 */
char* scanner_info()
{
    char *PET_2D_DEMO_INFO;
    char *PET_GE_DISCOVERY_ST_INFO;
    char *PET_SIEMENS_INVEON_INFO;
    char *PET_SIEMENS_MMR_INFO;
    char *CT_SIEMENS_DEFINITION_AS_INFO;

    char *scanner_description = (char*)calloc(1024,sizeof(char));

#ifdef PET_2D_DEMO
    /* 1 */
    PET_2D_DEMO_INFO = pet_2d_demo_info();
    strcat(scanner_description,PET_2D_DEMO_INFO);
    free(PET_2D_DEMO_INFO);
#endif

#ifdef PET_SIEMENS_INVEON
    /* 4 */
    PET_SIEMENS_INVEON_INFO = pet_siemens_inveon_info();
    strcat(scanner_description,PET_SIEMENS_INVEON_INFO);
    free(PET_SIEMENS_INVEON_INFO);
#endif

#ifdef PET_SIEMENS_MMR
    /* 8 */
    PET_SIEMENS_MMR_INFO = pet_siemens_mmr_info();
    strcat(scanner_description,PET_SIEMENS_MMR_INFO);
    free(PET_SIEMENS_MMR_INFO);
#endif

#ifdef PET_GE_DISCOVERY_ST
    /* 10 */
    PET_GE_DISCOVERY_ST_INFO = pet_ge_discovery_st_info();
    strcat(scanner_description,PET_GE_DISCOVERY_ST_INFO);
    free(PET_GE_DISCOVERY_ST_INFO);
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    /* 21 */
    CT_SIEMENS_DEFINITION_AS_INFO = ct_siemens_definition_as_info();
    strcat(scanner_description,CT_SIEMENS_DEFINITION_AS_INFO);
    free(CT_SIEMENS_DEFINITION_AS_INFO);
#endif

    return scanner_description;
}


/* ############################################################################################# */
/* ### setup_scanner ########################################################################### */
/* ############################################################################################# */
/**
 * @brief setup_scanner
 */
void setup_scanner()
{
    /* initialize parameter */
    InitializeParameter();

    /* load scanner setup */
    switch (Parameter.scannertype)
    {
#ifdef PET_2D_DEMO
    case 1:
        pet_2d_demo_setup();
        break;
#endif

#ifdef PET_SIEMENS_INVEON
    case 4:
        pet_siemens_inveon_setup();
        break;
#endif

#ifdef PET_SIEMENS_MMR
    case 8:
        pet_siemens_mmr_setup();
        break;
#endif

#ifdef PET_GE_DISCOVERY_ST
    case 10:
        pet_ge_discovery_st_setup();
        break;
#endif

#ifdef CT_SIEMENS_DEFINITION_AS
    case 21:
        ct_siemens_definition_as_setup();
        break;
#endif

    default:
#ifdef PET_2D_DEMO
        pet_2d_demo_setup();
        Parameter.scannertype = 1;
#else
        message(0,"selected scannertype (%d) does not exist\n",Parameter.scannertype);
        exit(EXIT_FAILURE);
#endif
    }

    /* update parameter */
    UpdateParameterList();
}
