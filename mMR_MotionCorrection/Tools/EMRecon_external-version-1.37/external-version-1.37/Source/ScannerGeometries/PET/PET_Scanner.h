/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#ifndef PET_SCANNER_H
#define PET_SCANNER_H


/* emrecon includes */
#include "../../IO/Parameter.h"
#include "../../Main/Siddon.h"


/* struct definitions */
typedef struct
{
    double A_x,A_y,A_z,
    B_x,B_y,B_z,
    C_x,C_y,C_z,
    D_x,D_y,D_z,
    E_x,E_y,E_z,
    F_x,F_y,F_z,
    G_x,G_y,G_z,
    H_x,H_y,H_z,
    P_x,P_y,P_z,
    Q_x,Q_y,Q_z;
    float efficiency;
    int virtual_crystal;
} pet_crystal;

typedef struct
{
    int axial_blocks,
    transversal_blocks,
    axial_crystals_per_block,
    transversal_crystals_per_block,
    rings,
    crystal_offset_a,
    crystal_offset_b,
    crystals_per_ring,
    flip_x_axis,
    flip_y_axis,
    flip_z_axis,
    spherical,
    use_virtual_crystals,
    v_crystals_per_ring,
    v_crystals_between_blocks,
    v_rings_between_blocks,
    v_rings;
    double crystal_width_x,
    crystal_width_y,
    crystal_width_z,
    crystal_gap_y,
    crystal_gap_z,
    block_offset,
    doi,
    rotation_offset,
    spherical_angle,
    radius,
    v_crystal_width_y,
    v_crystal_width_z;
    float *norm_axial_effects,
    *norm_crystal_interference,
    *norm_geometry_effects;
} pet_scanner;

typedef struct
{
    int angles,
    projections;
} sinogram;


/* global variables */
pet_crystal *PET_Crystal_Array;
pet_scanner PET_Scanner;
sinogram Sinogram;


/* function declarations */
void pet_scanner_calculate_crystal_positions();
void pet_scanner_convert_d1d2_to_rphi(int D1, int D2, int* r, int *phi);
void pet_scanner_convert_rphi_to_d1d2(int r, int phi, int *D1, int *D2);
void pet_scanner_derive_subset_vector(int current_subset, int *subsetvector);
void pet_scanner_disable_virtual_crystals();
void pet_scanner_draw_crystal_geometry(double *output_image);
void pet_scanner_release_memory();
void pet_scanner_setup();
void pet_scanner_update_parameter();


#endif
