/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "PET_2D_Demo.h"


/* ############################################################################################# */
/* ### pet_2d_demo_get_number_of_events_in_file ################################################ */
/* ############################################################################################# */
/**
 * @brief pet_2d_demo_get_number_of_events_in_file
 * @return
 */
long pet_2d_demo_get_number_of_events_in_file()
{
    long events_in_file;
    events_in_file = Sinogram.projections*Sinogram.angles;
    return events_in_file;
}


/* ############################################################################################# */
/* ### pet_2d_demo_info ######################################################################## */
/* ############################################################################################# */
/**
 * @brief pet_2d_demo_info
 * @return
 */
char* pet_2d_demo_info()
{
    char *scanner_info = (char*)calloc(50,sizeof(char));
    sprintf(scanner_info,"\t\t\t  1) [PET] 2D Demo\n");
    return scanner_info;
}


/* ############################################################################################# */
/* ### pet_2d_demo_read_events ################################################################# */
/* ############################################################################################# */
/**
 * @brief pet_2d_demo_read_events
 * @param events_to_read
 * @param Eventvector
 */
void pet_2d_demo_read_events(long events_to_read, event *Eventvector)
{
    int i,k,phi,r;
    double n1,n2;
    int *subsetvector;

    /* subset calculation */
    if (Parameter.fixed_subset > 0)
    {
        subsetvector = (int*)safe_calloc(Sinogram.angles/Parameter.subsets,sizeof(int));
        pet_scanner_derive_subset_vector(Parameter.fixed_subset-1,subsetvector);
    }

    for (i=0;i<events_to_read;i++)
    {
        phi = i / Sinogram.projections;
        r = i - phi * Sinogram.projections;

        n1 = -cos(-M_PI+M_PI/Sinogram.angles*phi);
        n2 = +sin(-M_PI+M_PI/Sinogram.angles*phi);

        Eventvector[i].x1 = + PET_2D_demo.radius * n1 + (-(Sinogram.projections-1)*PET_2D_demo.bin_width/2 + r*PET_2D_demo.bin_width) * n2;
        Eventvector[i].y1 = + PET_2D_demo.radius * n2 - (-(Sinogram.projections-1)*PET_2D_demo.bin_width/2 + r*PET_2D_demo.bin_width) * n1;
        Eventvector[i].z1 = 0.0;
        Eventvector[i].x2 = - PET_2D_demo.radius * n1 + (-(Sinogram.projections-1)*PET_2D_demo.bin_width/2 + r*PET_2D_demo.bin_width) * n2;
        Eventvector[i].y2 = - PET_2D_demo.radius * n2 - (-(Sinogram.projections-1)*PET_2D_demo.bin_width/2 + r*PET_2D_demo.bin_width) * n1;
        Eventvector[i].z2 = 0.0;
        Eventvector[i].valid = 1;

        /* subset choice, i.e. all events the are not in the particular fixed_subset are declared invalid */
        if (Parameter.fixed_subset > 0)
        {
            Eventvector[i].valid = 0;

            for(k=0;k<Sinogram.angles/Parameter.subsets;k++)
            {
                if (phi == subsetvector[k])
                {
                    k = Sinogram.angles;
                    Eventvector[i].valid = 1;
                }
            }
        }
    }

    /* release subsetvector */
    if (Parameter.fixed_subset > 0)
    {
        free(subsetvector);
    }
}


/* ############################################################################################# */
/* ### pet_2d_demo_setup ####################################################################### */
/* ############################################################################################# */
/**
 * @brief pet_2d_demo_setup
 */
void pet_2d_demo_setup()
{
    message(5,"\nsetup 2d demo...\n");

    /* image related parameter */
    Parameter.size_x = 128;
    Parameter.size_y = 128;
    Parameter.size_z = 1;
    Parameter.fov_x = 128.0;
    Parameter.fov_y = 128.0;
    Parameter.fov_z = 1.0;
    Parameter.offset_x = -Parameter.fov_x/2.0;
    Parameter.offset_y = -Parameter.fov_y/2.0;
    Parameter.offset_z = -Parameter.fov_z/2.0;
    Parameter.offset_c = 0.0;

    /* data / reconstruction related parameter */
    Parameter.iterations = 1;
    Parameter.subsets = 1;

    /* define basic scanner geometry */
    PET_2D_demo.radius = 500.0;
    PET_2D_demo.bin_width = 1.0;

    /* sinogram properties */
    Sinogram.projections = 128;
    Sinogram.angles = 128;

    /* update parameter only used for this scanner type*/
    PET_2D_demo.radius = UpdateParameterDouble("RADIUS",PET_2D_demo.radius);
    PET_2D_demo.bin_width = UpdateParameterDouble("BIN_WIDTH",PET_2D_demo.bin_width);

    /* update sinogram properties */
    Sinogram.angles = UpdateParameterInt("ANGLES",Sinogram.angles);
    Sinogram.projections = UpdateParameterInt("PROJECTIONS",Sinogram.projections);

    message(5,"setup 2d demo...done.\n");
}
