/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "PET_Siemens_Inveon.h"


/* ############################################################################################# */
/* ### pet_siemens_inveon_info ################################################################# */
/* ############################################################################################# */
/**
 * @brief pet_siemens_inveon_info
 * @return
 */
char* pet_siemens_inveon_info()
{
    char *scanner_info = (char*)calloc(50,sizeof(char));
    sprintf(scanner_info,"\t\t\t  4) [PET] Siemens Inveon\n");
    return scanner_info;
}


/* ############################################################################################# */
/* ### pet_siemens_inveon_rebin_48bit_data_span3 ############################################### */
/* ############################################################################################# */
/**
 * @brief pet_siemens_inveon_rebin_48bit_data_span3
 *
 * Listmode handling for Siemens Inveon is based on the following publications:
 *
 * 1) Bindseil, 2013, Approaches Toward Combining Positron Emission Tomography with Magnetic
 *    Resonance Imaging, University of Western Ontario - Electronic Thesis and Dissertation
 *    Repository. Paper 1419. (http://ir.lib.uwo.ca/etd/1419)
 *
 * 2) Newport et al., QuickSilver: A Flexible, Extensible, and High-Speed Architecture for
 *    Multi-Modality Imaging, 2006, IEEE NSS Conference Record, M08-1, pp.2333-2334
 *
 * @param input_listmode
 * @param output_prompt
 * @param output_delayed
 */
void pet_siemens_inveon_rebin_48bit_data_span3(char *input_listmode, double *output_prompt, double *output_delayed)
{
    int i,j,cr1,cr2,ring1,ring2,D1,D2,gates,current_gate;
    int ringtmp,Dtmp,D1tmp,D2tmp,valid_event;
    int r,phi,s;
    int bit[48];
    FILE *listmodefile;
    unsigned char n;
    double *msec;
    long current_timemark = -1;
    /* seems like the scanner does not start with t=0 */
    long timemark_offset = -1;
    long starttime = -1;
    long stoptime = -1;

#ifdef WINDOWS_MATLAB
    errno_t err;
    __int64 filesize, tag, tags, maxtags;

    /* existence of listmodefile was checked before */
    err = fopen_s(&listmodefile,input_listmode,"rb");
    _fseeki64(listmodefile,0,SEEK_END);
    filesize = _ftelli64(listmodefile);
    tags = filesize/4;
    _fseeki64(listmodefile,0,SEEK_SET);

    message(4,"tags: %lld\n",tags);
    message(4,"filesize: %lld\n",filesize);
#else
    long filesize, tag, tags, maxtags;

    /* existence of listmodefile was checked before */
    listmodefile = fopen(input_listmode,"rb");
    fseek(listmodefile,0,SEEK_END);
    filesize = ftell(listmodefile);
    tags = filesize/6;
    fseek(listmodefile,0,SEEK_SET);
#endif

    /* calculate number of gates */
    gates = mgf_number_of_respiratory_gates * mgf_number_of_cardiac_gates;

    /* set number of tags to process */
    maxtags = tags;
    if (GetParameterInt("MAXTAGS") > 0)
    {
        maxtags = min(maxtags,GetParameterInt("MAXTAGS"));
    }

    /* calloc space for scaling */
    msec = safe_calloc(gates,sizeof(double));

    /* initialize vector */
    for(i=0;i<gates;i++)
    {
        for(j=0;j<Michelogram.sensitivityevents;j++)
        {
            output_prompt[j+i*Michelogram.sensitivityevents] = 0.0;
            output_delayed[j+i*Michelogram.sensitivityevents] = 0.0;
        }
    }

    /* skip loop over all tags in case of invalid mgf file */
    if (mgf_number_of_entries == -1)
    {
        maxtags = 0;
    }

    /* loop over tags, start- stoptime need to be added */
    for (tag=0;tag<maxtags;tag++)
    {
        /* analyze bit by bit - very slow, but debugging is way easier */
        for(j=0;j<6;j++)
        {
            fread(&n,1,1,listmodefile);

            /* extract bits */
            for(i=0;i<8;i++)
            {
                bit[i+j*8] = n%2;
                n /= 2;
            }
        }

        /* Time Tag */
        if (bit[43] == 1 && bit[42] == 0 && bit[41] == 1 && bit[40] == 0 && bit[39] == 0 && bit[38] == 0 && bit[37] == 0 && bit[36] == 0)
        {
            current_timemark = 0;
            for (i=0;i<36;i++)
            {
                current_timemark += bit[i]*(long)pow(2,i);
            }

            /* safe first timemark and use it as offset */
            if (timemark_offset < 0)
            {
                timemark_offset = current_timemark;
            }

            /* adjust current_timemark by offset */
            current_timemark -= timemark_offset;
        }

        /* only use data with valid timestamp */
        if (current_timemark > -1)
        {
            /* per default gate = 1 */
            current_gate = 1;

            /* check for valid starttime */
            starttime = mgf_starttime;
            if (current_timemark < mgf_starttime)
            {
                current_gate = 0;
            }

            /* check for valid stoptime */
            if (mgf_stoptime > 0)
            {
                if (current_timemark <= mgf_stoptime)
                {
                    stoptime = current_timemark;
                }
                if (current_timemark >= mgf_stoptime)
                {
                    current_gate = 0;
                    tag = maxtags;
                }
            }
            else
            {
                stoptime = current_timemark;
            }

            /* - in case of just 1 valid entry, only use mgf_starttime and mgf_stoptime */
            /* - in case of more than 1 valid entry, use mgf_timesignal and mgf_gatingsignal */
            if (mgf_number_of_entries > 1)
            {
                if (current_gate > 0)
                {
                    /* update mgf_dataindex according to current time */
                    if (current_timemark > mgf_timesignal[mgf_dataindex])
                    {
                        mgf_dataindex++;
                    }

                    /* update current gate */
                    current_gate = mgf_gatingsignal[mgf_dataindex];
                }
            }

            /* use only counts with valid current_gate */
            if (current_gate >= 1)
            {
                /* save time for scaling */
                if (bit[43] == 1 && bit[42] == 0 && bit[41] == 1 && bit[40] == 0 && bit[39] == 0 && bit[38] == 0 && bit[37] == 0 && bit[36] == 0)
                {
                    msec[current_gate-1]++;
                }

                /* Event Tag */
                if (bit[43] == 0)
                {
                    /* calculate crystal and rings - again very slow */
                    cr1 = 0;
                    cr2 = 0;
                    for (i=0;i<17;i++)
                    {
                        cr1 += bit[i]*(int)pow(2,i);
                        cr2 += bit[i+19]*(int)pow(2,i);
                    }
                    ring1 = cr1 / 320;
                    ring2 = cr2 / 320;
                    D1 = cr1 - ring1*320;
                    D2 = cr2 - ring2*320;

                    /* check event order and reorder if required */
                    valid_event = 0;
                    pet_scanner_convert_d1d2_to_rphi(D1,D2,&r,&phi);
                    pet_scanner_convert_rphi_to_d1d2(r,phi,&D1tmp,&D2tmp);

                    if (D1 != D1tmp || D2 != D2tmp)
                    {
                        Dtmp = D1;
                        D1 = D2;
                        D2 = Dtmp;

                        ringtmp = ring1;
                        ring1 = ring2;
                        ring2 = ringtmp;

                        pet_scanner_convert_d1d2_to_rphi(D1,D2,&r,&phi);
                        pet_scanner_convert_rphi_to_d1d2(r,phi,&D1tmp,&D2tmp);
                        if (D1 == D1tmp && D2 == D2tmp)
                        {
                            valid_event = 1;
                        }
                    }
                    else
                    {
                        valid_event = 1;
                    }

                    if (valid_event == 1)
                    {
                        s = Michelogram.sinogram_number[ring1+PET_Scanner.rings*ring2];

                        if (bit[42] == 0)
                        {
                            output_delayed[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles]++;
                        }
                        if (bit[42] == 1)
                        {
                            output_prompt[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles]++;
                        }
                    }
                }
            }
        }
    }

    /* scale data to per h */
    for(i=0;i<gates;i++)
    {
        if (msec[i] > 0.0)
        {
            for(j=0;j<Michelogram.sensitivityevents;j++)
            {
                output_prompt[j+i*Michelogram.sensitivityevents] = output_prompt[j+i*Michelogram.sensitivityevents] / msec[i] * 1000.0 * 3600.0;
                output_delayed[j+i*Michelogram.sensitivityevents] = output_delayed[j+i*Michelogram.sensitivityevents] / msec[i] * 1000.0 * 3600.0;
            }
        }
    }

    /* plot start and stoptime */
    message(4,"used starttime:\t%ld ms\n",starttime);
    message(4,"mgf_starttime:\t%ld ms\n",mgf_starttime);
    message(4,"used stoptime:\t%ld ms\n",stoptime);
    message(4,"mgf_stoptime:\t%ld ms\n",mgf_stoptime);

    /* free memory */
    free(msec);

    /* after rebinning close listmode file */
    fclose(listmodefile);
}


/* ############################################################################################# */
/* ### pet_siemens_inveon_setup ################################################################ */
/* ############################################################################################# */
/**
 * @brief pet_siemens_inveon_setup
 *
 * Geometry of Siemens Inveon is based on the following publications:
 *
 * 1) Kemp et al., NEMA NU 2-2007 performance measurements of the Siemens Inveon
 *    preclinical small animal PET system, 2009, PMB 54, pp. 2359-2376
 *
 * 2) Constantinescu et al., Performance evaluation of an Inveon PET preclinical scanner, 2009,
 *    PMB 54, pp. 2885-2899
 */
void pet_siemens_inveon_setup()
{
    message(5,"\nsetup siemens inveon...\n");

    /* initialize */
    pet_michelogram_setup();

    /* image related parameter */
    Parameter.size_x = 128;
    Parameter.size_y = 128;
    Parameter.size_z = 159;
    Parameter.fov_x = 99.328;
    Parameter.fov_y = 99.328;
    Parameter.fov_z = 126.564;
    Parameter.offset_x = -Parameter.fov_x/2.0;
    Parameter.offset_y = -Parameter.fov_y/2.0;
    Parameter.offset_z = -Parameter.fov_z/2.0;
    Parameter.offset_c = 48.0;

    /* data / reconstruction related parameter */
    Parameter.datatype = 1;
    Parameter.iterations = 3;
    Parameter.subsets = 16;

    /* scanner geometry */
    PET_Scanner.axial_blocks = 4;
    PET_Scanner.transversal_blocks = 16;

    PET_Scanner.axial_crystals_per_block = 20;
    PET_Scanner.transversal_crystals_per_block = 20;

    PET_Scanner.crystal_width_x = 10.0;
    PET_Scanner.crystal_width_y = 1.51;
    PET_Scanner.crystal_width_z = 1.51;
    PET_Scanner.crystal_gap_y = 0.08; /* please note that the crystal pitch for y and z is inverted wrt the publications. */
    PET_Scanner.crystal_gap_z = 0.12;

    PET_Scanner.block_offset = 0.08;
    PET_Scanner.crystal_offset_b = 10;
    PET_Scanner.radius = 80.5;
    PET_Scanner.rotation_offset = 0.0;

    PET_Scanner.doi = 4.58;

    /* sinogram properties */
    Sinogram.projections = 128;
    Sinogram.angles = 160;

    /* michelogram properties */
    Michelogram.span = 3;
    Michelogram.max_ring_diff = 79;

    /* update parameter and create crystal geometry */
    pet_michelogram_update_parameter();
    pet_scanner_calculate_crystal_positions();
    pet_michelogram_initialize();
    pet_michelogram_disable_virtual_crystals();

    message(5,"setup siemens inveon...done.\n");
}
