/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "PET_Michelogram.h"


/* ############################################################################################# */
/* ### pet_michelogram_axial_weight ############################################################ */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_axial_weight
 * @param input_michelogram
 * @param output_michelogram
 */
void pet_michelogram_axial_weight(double *input_michelogram, double *output_michelogram)
{
    int r,phi,s;

    for (s=0;s<Michelogram.sinograms;s++)
    {
        for (phi=0;phi<Sinogram.angles;phi++)
        {
            for (r=0;r<Sinogram.projections;r++)
            {
                if (Michelogram.sinogram_weight[s] > 0)
                {
                    output_michelogram[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] = input_michelogram[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] / (double)(Michelogram.sinogram_weight[s]);
                }
            }
        }
    }
}


/* ############################################################################################# */
/* ### pet_michelogram_compress_sinogram_stack ################################################# */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_compress_sinogram_stack
 * @param input_sinogram_stack
 * @param output_michelogram
 */
void pet_michelogram_compress_sinogram_stack(double *input_sinogram_stack, double *output_michelogram)
{
    int n,ringA,ringB,r,phi,s;

    for(n=0;n<Sinogram.angles*Sinogram.projections*Michelogram.sinograms_in_stack;n++) {
        s = n / (Sinogram.angles*Sinogram.projections);
        phi = (n - s*Sinogram.angles*Sinogram.projections) / Sinogram.projections;
        r = n - s*Sinogram.angles*Sinogram.projections - phi*Sinogram.projections;

        ringA = Michelogram.sinogram_rings_lookup[2*s];
        ringB = Michelogram.sinogram_rings_lookup[2*s+1];

        s = Michelogram.sinogram_number[ringA+(PET_Scanner.rings+PET_Scanner.v_rings)*ringB];
        output_michelogram[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] += input_sinogram_stack[n];
    }
}


/* ############################################################################################# */
/* ### pet_michelogram_disable_virtual_crystals ################################################ */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_disable_virtual_crystals
 */
void pet_michelogram_disable_virtual_crystals()
{
    Michelogram.sinogram_mask = (float*)safe_calloc(Michelogram.sensitivityevents,sizeof(float));

    if (PET_Scanner.use_virtual_crystals == 1)
    {
        int i;
        for (i=0;i<Michelogram.sensitivityevents;i++)
        {
            Michelogram.sinogram_mask[i] = 1.0;
        }
    }
    else
    {
        int r,phi,s,D1,D2,ringA,ringB;

        pet_scanner_disable_virtual_crystals();

        for (ringA=0;ringA<PET_Scanner.rings+PET_Scanner.v_rings;ringA++)
        {
            for (ringB=0;ringB<PET_Scanner.rings+PET_Scanner.v_rings;ringB++)
            {
                for (phi=0;phi<Sinogram.angles;phi++)
                {
                    for (r=0;r<Sinogram.projections;r++)
                    {
                        pet_scanner_convert_rphi_to_d1d2(r,phi,&D1,&D2);
                        s = Michelogram.sinogram_number[ringA+(PET_Scanner.rings+PET_Scanner.v_rings)*ringB];
                        if (s >= 0 && PET_Crystal_Array[D1+ringA*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)].virtual_crystal == 0 && PET_Crystal_Array[D2+ringB*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)].virtual_crystal == 0)
                        {
                            Michelogram.sinogram_mask[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] += 1.0;
                        }
                    }
                }
            }
        }

        for (s=0;s<Michelogram.sinograms;s++)
        {
            for (phi=0;phi<Sinogram.angles;phi++)
            {
                for (r=0;r<Sinogram.projections;r++)
                {
                    if (Michelogram.sinogram_mask[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] < 1.0)
                    {
                        Michelogram.sinogram_mask[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] = -1.0;
                    }
                    else
                    {
                        Michelogram.sinogram_mask[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] = (float)Michelogram.sinogram_weight[s]/Michelogram.sinogram_mask[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles];
                    }
                }
            }
        }
    }
}


/* ############################################################################################# */
/* ### pet_michelogram_get_number_of_events_in_file ############################################ */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_get_number_of_events_in_file
 * @return
 */
long pet_michelogram_get_number_of_events_in_file()
{
    long number_of_events_in_file;
    number_of_events_in_file = (long)Michelogram.sensitivityevents;
    return number_of_events_in_file;
}


/* ############################################################################################# */
/* ### pet_michelogram_initialize ############################################################## */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_initialize
 */
void pet_michelogram_initialize()
{
    int *rcache;
    short *dead_ring;

    int c,s,ringA,ringB,segment;
    int total_segments;
    int max_segment;

    int *slice_offset;
    int *slice_index;

    message(5,"initializing michelogram...\n");

    rcache = safe_calloc(PET_Scanner.rings+PET_Scanner.v_rings,sizeof(int));
    dead_ring = (short*)safe_calloc(PET_Scanner.rings+PET_Scanner.v_rings,sizeof(int));

    total_segments = (2*Michelogram.max_ring_diff+1)/Michelogram.span;
    max_segment = (total_segments-1)/2;

    slice_offset = safe_calloc(max_segment*2+1,sizeof(int));
    slice_index = safe_calloc(3*(max_segment*2+1),sizeof(int));
    Michelogram.segments = safe_calloc(total_segments,sizeof(int));

    /* - create slice_index array to calculate number of sinograms and slice_offset */
    for (segment=0;segment<=max_segment;segment++)
    {
        if (segment == 0)
        {
            slice_index[segment*3+2] = 2*(PET_Scanner.rings+PET_Scanner.v_rings-1)+1;
            slice_index[segment*3+1] = slice_index[segment*3+2]-1;
            slice_index[segment*3]= 0;
        }
        else
        {
            slice_index[(2*segment-1)*3+2] = 2*(PET_Scanner.rings+PET_Scanner.v_rings-1)-(2*segment-1)*Michelogram.span;
            slice_index[(2*segment-1)*3+1] = slice_index[(2*segment-2)*3+1]+slice_index[(2*segment-1)*3+2];
            slice_index[(2*segment-1)*3]= slice_index[(2*segment-2)*3+1]+1;

            slice_index[2*segment*3+2] = slice_index[(2*segment-1)*3+2];
            slice_index[2*segment*3+1] = slice_index[(2*segment-1)*3+1]+slice_index[2*segment*3+2];
            slice_index[2*segment*3] = slice_index[(2*segment-1)*3+1]+1;
        }
    }
    Michelogram.sinograms = slice_index[2*max_segment*3+1]+1;
    Michelogram.sensitivityevents = Michelogram.sinograms*Sinogram.projections*Sinogram.angles;

    /* create slice_offset to calculate sinogram_number and sinogram_slice */
    for (segment=0;segment<total_segments;segment++)
    {
        if (segment == 0)
        {
            slice_offset[segment] = 0;
        }
        else
        {
            slice_offset[segment] = slice_index[(segment-1)*3+1];
        }
    }
    free(slice_index);

    /* allocate lookup tables */
    Michelogram.sinogram_number = (short*)safe_calloc((PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.rings+PET_Scanner.v_rings),sizeof(short));
    Michelogram.sinogram_rings_lookup = (short*)safe_calloc(2*(PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.rings+PET_Scanner.v_rings),sizeof(short));
    Michelogram.sinogram_slice = (short*)safe_calloc(Michelogram.sinograms,sizeof(short));
    Michelogram.sinogram_slice_z_position = (double*)safe_calloc(Michelogram.sinograms*2,sizeof(double));
    Michelogram.sinogram_weight = (short*)safe_calloc(Michelogram.sinograms,sizeof(short));

    /* initalize sinogram_number */
    for (s=0;s<(PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.rings+PET_Scanner.v_rings);s++)
    {
        Michelogram.sinogram_number[s] = -1;
    }

    /* needed to catch combinations of dead crystals (gaps) */
    for (s=0;s<PET_Scanner.rings+PET_Scanner.v_rings;s++)
    {
        for (c=0;c<PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring;c++)
        {
            dead_ring[s] = (short)min(dead_ring[s],PET_Crystal_Array[c+s*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)].virtual_crystal);
        }
    }

    /* needed for sinogram_rings_lookup */
    rcache[1] = PET_Scanner.rings+PET_Scanner.v_rings;
    for (s=2;s<PET_Scanner.rings+PET_Scanner.v_rings;s++)
    {
        rcache[s] = rcache[s-1] + 2*(PET_Scanner.rings+PET_Scanner.v_rings-s+1);
    }

    /* catch spherical geometry */
    if (PET_Scanner.spherical == 1)
    {
        Michelogram.sinogram_slice_x_position = (double*)safe_calloc(PET_Scanner.crystals_per_ring*Michelogram.sinograms*2,sizeof(double));
        Michelogram.sinogram_slice_y_position = (double*)safe_calloc(PET_Scanner.crystals_per_ring*Michelogram.sinograms*2,sizeof(double));
    }

    /* calculate sinogram_number,sinogram_rings_lookup,sinogram_slice,[sinogram_slice_x_position,sinogram_slice_y_position],sinogram_slice_z_position,sinogram_weight */
    for (ringA=0;ringA<PET_Scanner.rings+PET_Scanner.v_rings;ringA++)
    {
        for (ringB=0;ringB<PET_Scanner.rings+PET_Scanner.v_rings;ringB++)
        {
            if (ringA == ringB)
            {
                s = ringA;
            }

            if (ringA > ringB)
            {
                s = rcache[ringA-ringB] + ringB;
            }

            if (ringA < ringB)
            {
                s = rcache[ringB-ringA] + PET_Scanner.rings+PET_Scanner.v_rings - ringB + 2*ringA;
            }

            Michelogram.sinogram_rings_lookup[2*s] = (short)ringA;
            Michelogram.sinogram_rings_lookup[2*s+1] = (short)ringB;

            for (segment=0;segment<=max_segment;segment++)
            {
                if (segment == 0)
                {
                    if (abs(ringA-ringB) <= (Michelogram.span-1)/2)
                    {
                        s = ringA + ringB;
                        Michelogram.sinogram_number[ringA+(PET_Scanner.rings+PET_Scanner.v_rings)*ringB] = (short)s;
                        Michelogram.sinogram_slice[s] = (short)(ringA+ringB);

                        /* catch spherical geometry */
                        if (PET_Scanner.spherical == 1)
                        {
                            for (c=0;c<PET_Scanner.crystals_per_ring;c++)
                            {
                                Michelogram.sinogram_slice_x_position[2*(c+s*PET_Scanner.crystals_per_ring)] += PET_Crystal_Array[c+ringA*PET_Scanner.crystals_per_ring].P_x;
                                Michelogram.sinogram_slice_y_position[2*(c+s*PET_Scanner.crystals_per_ring)] += PET_Crystal_Array[c+ringA*PET_Scanner.crystals_per_ring].P_y;
                                Michelogram.sinogram_slice_x_position[2*(c+s*PET_Scanner.crystals_per_ring)+1] += PET_Crystal_Array[c+ringB*PET_Scanner.crystals_per_ring].P_x;
                                Michelogram.sinogram_slice_y_position[2*(c+s*PET_Scanner.crystals_per_ring)+1] += PET_Crystal_Array[c+ringB*PET_Scanner.crystals_per_ring].P_y;
                            }
                        }

                        /* account for dead crystal rings (axial gaps), watch out to be in an existing block... */
                        if (max(dead_ring[ringA],dead_ring[ringB]) == 0)
                        {
                            Michelogram.sinogram_slice_z_position[2*s] += PET_Crystal_Array[ringA*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)].P_z;
                            Michelogram.sinogram_slice_z_position[2*s+1] += PET_Crystal_Array[ringB*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)].P_z;
                            Michelogram.sinogram_weight[s]++;
                        }
                    }
                }

                if (segment > 0) {
                    if (abs(ringA-ringB) > ((2*segment-1)*Michelogram.span-1)/2 && abs(ringA-ringB) <= ((2*segment+1)*Michelogram.span-1)/2)
                    {
                        if (ringA > ringB)
                        {
                            s = slice_offset[2*segment-1] + ringA + ringB -(Michelogram.span-1)/2 - (segment-1)*Michelogram.span;
                        }
                        else
                        {
                            s = slice_offset[2*segment] + ringA + ringB - (Michelogram.span-1)/2 - (segment-1)*Michelogram.span;
                        }
                        Michelogram.sinogram_number[ringA+(PET_Scanner.rings+PET_Scanner.v_rings)*ringB] = (short)s;
                        Michelogram.sinogram_slice[s] = (short)(ringA+ringB);

                        /* catch spherical geometry */
                        if (PET_Scanner.spherical == 1)
                        {
                            for (c=0;c<PET_Scanner.crystals_per_ring;c++)
                            {
                                Michelogram.sinogram_slice_x_position[2*(c+s*PET_Scanner.crystals_per_ring)] += PET_Crystal_Array[c+ringA*PET_Scanner.crystals_per_ring].P_x;
                                Michelogram.sinogram_slice_y_position[2*(c+s*PET_Scanner.crystals_per_ring)] += PET_Crystal_Array[c+ringA*PET_Scanner.crystals_per_ring].P_y;
                                Michelogram.sinogram_slice_x_position[2*(c+s*PET_Scanner.crystals_per_ring)+1] += PET_Crystal_Array[c+ringB*PET_Scanner.crystals_per_ring].P_x;
                                Michelogram.sinogram_slice_y_position[2*(c+s*PET_Scanner.crystals_per_ring)+1] += PET_Crystal_Array[c+ringB*PET_Scanner.crystals_per_ring].P_y;
                            }
                        }

                        /* account for dead crystal rings (axial gaps), watch out to be in an existing block... */
                        if (max(dead_ring[ringA],dead_ring[ringB]) == 0)
                        {
                            Michelogram.sinogram_slice_z_position[2*s] += PET_Crystal_Array[ringA*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)].P_z;
                            Michelogram.sinogram_slice_z_position[2*s+1] += PET_Crystal_Array[ringB*(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)].P_z;
                            Michelogram.sinogram_weight[s]++;
                        }
                    }
                }
            }
        }
    }
    free(rcache);
    free(dead_ring);
    free(slice_offset);


    /* account for axial compression */
    for (s=0;s<Michelogram.sinograms;s++)
    {
        if (Michelogram.sinogram_weight[s] > 0)
        {
            Michelogram.sinogram_slice_z_position[2*s] /= (double)(Michelogram.sinogram_weight[s]);
            Michelogram.sinogram_slice_z_position[2*s+1] /= (double)(Michelogram.sinogram_weight[s]);
        }
    }

    /* catch spherical geometry */
    if (PET_Scanner.spherical == 1)
    {
        for (s=0;s<Michelogram.sinograms;s++)
        {
            for (c=0;c<PET_Scanner.crystals_per_ring;c++)
            {
                Michelogram.sinogram_slice_x_position[2*(c+s*PET_Scanner.crystals_per_ring)] /= Michelogram.sinogram_weight[s];
                Michelogram.sinogram_slice_y_position[2*(c+s*PET_Scanner.crystals_per_ring)] /= Michelogram.sinogram_weight[s];
                Michelogram.sinogram_slice_x_position[2*(c+s*PET_Scanner.crystals_per_ring)+1] /= Michelogram.sinogram_weight[s];
                Michelogram.sinogram_slice_y_position[2*(c+s*PET_Scanner.crystals_per_ring)+1] /= Michelogram.sinogram_weight[s];
            }
        }
    }

    /* count number of sinograms in stack */
    Michelogram.sinograms_in_stack = 0;
    for (s=0;s<(PET_Scanner.rings+PET_Scanner.v_rings)*(PET_Scanner.rings+PET_Scanner.v_rings);s++)
    {
        if (Michelogram.sinogram_number[s] > -1)
        {
            Michelogram.sinograms_in_stack++;
        }
    }

    message(5,"projections: \t \t %d\n",Sinogram.projections);
    message(5,"angles: \t \t \t %d\n",Sinogram.angles);
    message(5,"span: \t \t \t \t %d\n",Michelogram.span);
    message(5,"max_ring_diff: \t \t %d\n",Michelogram.max_ring_diff);
    message(5,"sinograms: \t \t \t %d\n",Michelogram.sinograms);

    message(5,"initializing michelogram...done.\n");
}


/* ############################################################################################# */
/* ### pet_michelogram_issrb ################################################################### */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_issrb
 * @param input_sinogram
 * @param output_michelogram
 */
void pet_michelogram_issrb(double *input_sinogram, double *output_michelogram)
{
    int s,phi,r;

    for (s=0;s<(2*(PET_Scanner.rings+PET_Scanner.v_rings)-1);s++)
    {
        for (phi=0;phi<Sinogram.angles;phi++)
        {
            for (r=0;r<Sinogram.projections;r++)
            {
                output_michelogram[r+phi*Sinogram.projections+s*Sinogram.angles*Sinogram.projections] = input_sinogram[r+phi*Sinogram.projections+s*Sinogram.angles*Sinogram.projections];
            }
        }
    }

    for (s=(2*(PET_Scanner.rings+PET_Scanner.v_rings)-1);s<Michelogram.sinograms;s++)
    {
        for (phi=0;phi<Sinogram.angles;phi++)
        {
            for (r=0;r<Sinogram.projections;r++)
            {
                output_michelogram[r+phi*Sinogram.projections+s*Sinogram.angles*Sinogram.projections] = input_sinogram[r+phi*Sinogram.projections+Michelogram.sinogram_slice[s]*Sinogram.angles*Sinogram.projections];
            }
        }
    }
}


/* ############################################################################################# */
/* ### pet_michelogram_normalize ############################################################### */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_normalize
 *
 * The normalization procedure is based on the following publications:
 *
 * 1) B. Bendriem, D.W. Townsend, The Theory and Practice of 3D PET, 1998
 *
 * @param input_michelogram
 * @param input_norm
 * @param output_michelogram
 */
void pet_michelogram_normalize(double *input_michelogram, double *input_norm, double *output_michelogram)
{
    int i,index,r,s,phi,ringA,ringB,D1,D2;
    float *normalization;

    /* calloc memory (temporary reorder input_norm) */
    PET_Scanner.norm_axial_effects = (float*)safe_calloc(Michelogram.sinograms,sizeof(float));
    PET_Scanner.norm_crystal_interference = (float*)safe_calloc((PET_Scanner.transversal_crystals_per_block+PET_Scanner.v_crystals_between_blocks)*Sinogram.projections,sizeof(float));
    PET_Scanner.norm_geometry_effects = (float*)safe_calloc(Sinogram.projections*(2*(PET_Scanner.rings+PET_Scanner.v_rings)-1),sizeof(float));

    /* initialize with default values */
    for (i=0;i<Michelogram.sinograms;i++)
    {
        PET_Scanner.norm_axial_effects[i] = 1.0;
    }

    for (i=0;i<(PET_Scanner.transversal_crystals_per_block+PET_Scanner.v_crystals_between_blocks)*Sinogram.projections;i++)
    {
        PET_Scanner.norm_crystal_interference[i] = 1.0;
    }

    for (i=0;i<Sinogram.projections*(2*(PET_Scanner.rings+PET_Scanner.v_rings)-1);i++)
    {
        PET_Scanner.norm_geometry_effects[i] = 1.0;
    }

    /* initialize output */
    for (i=0;i<Michelogram.sensitivityevents;i++)
    {
        output_michelogram[i] = input_michelogram[i];
    }

    /* load actual norm data */
    index = 0;
    /* reading geometric effects */
    for (i=0;i<Sinogram.projections*(2*(PET_Scanner.rings+PET_Scanner.v_rings)-1);i++)
    {
        PET_Scanner.norm_geometry_effects[i] = input_norm[index];
        if (PET_Scanner.norm_geometry_effects[i] < Parameter.epsilon)
        {
            PET_Scanner.norm_geometry_effects[i] = 1.0;
        }
        index++;
    }

    /* reading crystal interference */
    for (i=0;i<(PET_Scanner.transversal_crystals_per_block+PET_Scanner.v_crystals_between_blocks)*Sinogram.projections;i++)
    {
        PET_Scanner.norm_crystal_interference[i] = input_norm[index];
        if (PET_Scanner.norm_crystal_interference[i] < Parameter.epsilon)
        {
            PET_Scanner.norm_crystal_interference[i] = 1.0;
        }
        index++;
    }

    /* reading crystal efficiencies */
    for (i=0;i<(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)*(PET_Scanner.rings+PET_Scanner.v_rings);i++)
    {
        PET_Crystal_Array[i].efficiency = input_norm[index];
        if (PET_Crystal_Array[i].efficiency < Parameter.epsilon)
        {
            PET_Crystal_Array[i].efficiency = 1.0;
        }
        index++;
    }

    /* reading axial effects */
    for (i=0;i<Michelogram.sinograms;i++)
    {
        PET_Scanner.norm_axial_effects[i] = input_norm[index];
        if (PET_Scanner.norm_axial_effects[i] < Parameter.epsilon)
        {
            PET_Scanner.norm_axial_effects[i] = 1.0;
        }
        index++;
    }

    /* correcting geometric effects */
    for (s=0;s<Michelogram.sinograms;s++)
    {
        if (Michelogram.sinogram_weight[s] > 0)
        {
            for (phi=0;phi<Sinogram.angles;phi++)
            {
                for (r=0;r<Sinogram.projections;r++)
                {
                    output_michelogram[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] /= PET_Scanner.norm_geometry_effects[r+Michelogram.sinogram_slice[s]*Sinogram.projections];
                }
            }
        }
    }

    /* correcting crystal interference */
    for (s=0;s<Michelogram.sinograms;s++)
    {
        if (Michelogram.sinogram_weight[s] > 0)
        {
            for (phi=0;phi<Sinogram.angles;phi++)
            {
                for (r=0;r<Sinogram.projections;r++)
                {
                    output_michelogram[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] /= PET_Scanner.norm_crystal_interference[(phi%(PET_Scanner.transversal_crystals_per_block+PET_Scanner.v_crystals_between_blocks))+r*(PET_Scanner.transversal_crystals_per_block+PET_Scanner.v_crystals_between_blocks)];
                }
            }
        }
    }

    /* correcting crystal efficiencies */
    normalization = safe_calloc(Michelogram.sensitivityevents,sizeof(float));

    for (ringA=0;ringA<(PET_Scanner.rings+PET_Scanner.v_rings);ringA++)
    {
        for (ringB=0;ringB<(PET_Scanner.rings+PET_Scanner.v_rings);ringB++)
        {
            for (phi=0;phi<Sinogram.angles;phi++)
            {
                for (r=0;r<Sinogram.projections;r++)
                {
                    pet_scanner_convert_rphi_to_d1d2(r,phi,&D1,&D2);
                    s = Michelogram.sinogram_number[ringA+(PET_Scanner.rings+PET_Scanner.v_rings)*ringB];

                    if (s >= 0)
                    {
                        normalization[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] += (float)(PET_Crystal_Array[D1+(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)*ringA].efficiency*PET_Crystal_Array[D2+(PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)*ringB].efficiency/Michelogram.sinogram_weight[s]);
                    }
                }
            }
        }
    }

    for (s=0;s<Michelogram.sinograms;s++)
    {
        if (Michelogram.sinogram_weight[s] > 0)
        {
            for (phi=0;phi<Sinogram.angles;phi++)
            {
                for (r=0;r<Sinogram.projections;r++)
                {
                    output_michelogram[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] /= normalization[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles];
                }
            }
        }
    }
    free(normalization);

    /* correcting axial effects */
    for (s=0;s<Michelogram.sinograms;s++)
    {
        if (Michelogram.sinogram_weight[s] > 0)
        {
            for (phi=0;phi<Sinogram.angles;phi++)
            {
                for (r=0;r<Sinogram.projections;r++)
                {
                    output_michelogram[r+phi*Sinogram.projections+s*Sinogram.projections*Sinogram.angles] *= PET_Scanner.norm_axial_effects[s];
                }
            }
        }
    }
}


/* ############################################################################################# */
/* ### pet_michelogram_read_events ############################################################# */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_read_events
 * @param events_to_read
 * @param Eventvector
 */
void pet_michelogram_read_events(int events_to_read, event *Eventvector)
{
    int i,k,s,phi,r,D1,D2,valid_phi;
    int *subsetvector;

    /* subset choice */
    if (Parameter.fixed_subset > 0)
    {
        subsetvector = (int*)safe_calloc(Sinogram.angles/Parameter.subsets,sizeof(int));
        pet_scanner_derive_subset_vector(Parameter.fixed_subset-1,subsetvector);
    }

    /* loop over all events and calculate LOR position */
    for (i=0;i<events_to_read;i++)
    {
        /* calculate detectors D1, D2 for LOR lookup */
        s = i / (Sinogram.angles*Sinogram.projections);
        phi = (i - s*Sinogram.angles*Sinogram.projections) / Sinogram.projections;
        r = i - s*Sinogram.angles*Sinogram.projections - phi*Sinogram.projections;
        pet_scanner_convert_rphi_to_d1d2(r,phi,&D1,&D2);

        /* assign default values for current event */
        Eventvector[i].x1 = PET_Crystal_Array[D1].P_x;
        Eventvector[i].y1 = PET_Crystal_Array[D1].P_y;
        Eventvector[i].x2 = PET_Crystal_Array[D2].P_x;
        Eventvector[i].y2 = PET_Crystal_Array[D2].P_y;
        Eventvector[i].z1 = Michelogram.sinogram_slice_z_position[2*s];
        Eventvector[i].z2 = Michelogram.sinogram_slice_z_position[2*s+1];
        Eventvector[i].weightA = -1.0;
        Eventvector[i].weightB = 0.0;
        Eventvector[i].valid = 1;

        /* catch spherical geometry */
        if (PET_Scanner.spherical == 1)
        {
            Eventvector[i].x1 = Michelogram.sinogram_slice_x_position[2*(D1+s*PET_Scanner.crystals_per_ring)];
            Eventvector[i].y1 = Michelogram.sinogram_slice_y_position[2*(D1+s*PET_Scanner.crystals_per_ring)];
            Eventvector[i].x2 = Michelogram.sinogram_slice_x_position[2*(D2+s*PET_Scanner.crystals_per_ring)+1];
            Eventvector[i].y2 = Michelogram.sinogram_slice_y_position[2*(D2+s*PET_Scanner.crystals_per_ring)+1];
        }

        /* check whether corresponding sinogram is valid or not, e.g. in case of virtual crystals */
        if (Michelogram.sinogram_mask[i] < 0)
        {
            Eventvector[i].valid = 0;
        }

        /* subset choice */
        if (Parameter.fixed_subset > 0)
        {
            valid_phi = 0;
            Eventvector[i].valid = 0;

            for(k=0;k<Sinogram.angles/Parameter.subsets;k++)
            {
                if (phi == subsetvector[k])
                {
                    valid_phi = 1;
                    k = Sinogram.angles;
                }
            }

            if (valid_phi == 1)
            {
                Eventvector[i].valid = 1;
            }
        }
    }

    /* subset choice */
    if (Parameter.fixed_subset > 0)
    {
        free(subsetvector);
    }
}


/* ############################################################################################# */
/* ### pet_michelogram_release_memory ########################################################## */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_release_memory
 */
void pet_michelogram_release_memory()
{
    pet_scanner_release_memory();

    if (Michelogram.segments != 0)
    {
        free(Michelogram.segments);
    }

    if (Michelogram.sinogram_mask != 0)
    {
        free(Michelogram.sinogram_mask);
    }

    if (Michelogram.sinogram_number != 0)
    {
        free(Michelogram.sinogram_number);
    }

    if (Michelogram.sinogram_rings_lookup != 0)
    {
        free(Michelogram.sinogram_rings_lookup);
    }

    if (Michelogram.sinogram_slice != 0)
    {
        free(Michelogram.sinogram_slice);
    }

    if (Michelogram.sinogram_slice_x_position != 0)
    {
        free(Michelogram.sinogram_slice_x_position);
    }

    if (Michelogram.sinogram_slice_y_position != 0)
    {
        free(Michelogram.sinogram_slice_y_position);
    }

    if (Michelogram.sinogram_slice_z_position != 0)
    {
        free(Michelogram.sinogram_slice_z_position);
    }

    if (Michelogram.sinogram_weight != 0)
    {
        free(Michelogram.sinogram_weight);
    }
}


/* ############################################################################################# */
/* ### pet_michelogram_setup ################################################################### */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_setup
 */
void pet_michelogram_setup()
{
    pet_scanner_setup();
}


/* ############################################################################################# */
/* ### pet_michelogram_update_parameter ######################################################## */
/* ############################################################################################# */
/**
 * @brief pet_michelogram_update_parameter
 */
void pet_michelogram_update_parameter()
{
    pet_scanner_update_parameter();

    Michelogram.max_ring_diff = UpdateParameterInt("MAX_RING_DIFF",Michelogram.max_ring_diff);
}
