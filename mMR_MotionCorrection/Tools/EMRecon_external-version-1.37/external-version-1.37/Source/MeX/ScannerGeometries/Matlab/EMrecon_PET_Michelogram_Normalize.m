function [normalized_data] = EMrecon_PET_Michelogram_Normalize(parm,data,norm)

% EMrecon_PET_Michelogram_Normalize(parm,data,norm)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_PET_Michelogram_Normalize\n');
    fprintf('Usage: normalized_data = EMrecon_PET_Michelogram_Normalize(parm,data,norm);\n\n');
    mexEMrecon_PET_Michelogram_Normalize;
    fprintf('\n');
    normalized_data = -1;
else
    vfprintf(1,parm,'EMrecon_PET_Michelogram_Normalize...\n');
    tic;
    normalized_data = mexEMrecon_PET_Michelogram_Normalize(parm,data,norm);
    vfprintf(1,parm,'EMrecon_PET_Michelogram_Normalize...done. [%f sec]\n',toc);
end

end