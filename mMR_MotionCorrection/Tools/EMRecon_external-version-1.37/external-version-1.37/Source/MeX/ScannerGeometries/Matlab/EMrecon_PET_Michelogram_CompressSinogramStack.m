function [michelogram] = EMrecon_PET_Michelogram_CompressSinogramStack(parm,sinogram_stack)

% EMrecon_PET_Michelogram_CompressSinogramStack(parm,sinogram_stack)

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_PET_Michelogram_CompressSinogramStack\n');
    fprintf('Usage: michelogram = EMrecon_PET_Michelogram_CompressSinogramStack(parm,sinogram_stack);\n\n');
    mexEMrecon_PET_Michelogram_CompressSinogramStack;
    fprintf('\n');
    michelogram = -1;
else
    vfprintf(1,parm,'EMrecon_PET_Michelogram_CompressSinogramStack...\n');
    tic;
    michelogram = mexEMrecon_PET_Michelogram_CompressSinogramStack(parm,sinogram_stack);
    vfprintf(1,parm,'EMrecon_PET_Michelogram_CompressSinogramStack...done. [%f sec]\n',toc);
end

end