function [crystal_geometry] = EMrecon_PET_DrawCrystalGeometry_1D(parm)

% EMrecon_PET_DrawCrystalGeometry_1D(parm)

if (nargin ~= 1 || nargout ~= 1)
    fprintf('\nEMrecon_PET_DrawCrystalGeometry_1D\n');
    fprintf('Usage: crystal_geometry = EMrecon_PET_DrawCrystalGeometry_1D(parm);\n\n');
    mexEMrecon_PET_DrawCrystalGeometry;
    fprintf('\n');
    crystal_geometry = -1;
else
    vfprintf(1,parm,'EMrecon_PET_DrawCrystalGeometry_1D...\n');
    tic;
    crystal_geometry = mexEMrecon_PET_DrawCrystalGeometry(parm);
    vfprintf(1,parm,'EMrecon_PET_DrawCrystalGeometry_1D...done. [%f sec]\n',toc);
end

end