/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "mexEMrecon_PET_Michelogram_Normalize.h"


/*  the gateway routine. */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    long number_of_required_events,required_norm_size;
    const mwSize *number_of_events_in_file, *norm_size;
    double *input_data,*input_norm,*output_data;
    
    /* check for valid input/output parameter */
    if (nlhs < 1 || nlhs > 2)
    {
        message(0,"mexEMrecon_PET_Siemens_NormalizeData [%s]\n\n",EMRECON_VERSION);
        message(0,"supported scanner: \n%s\n",scanner_info());
    }
    else
    {
        /* transfer parameter from Matlab structure to EMrecon parameter structure */
        ReadFromMatlab(prhs[0]);

        message(2,"mexEMrecon_PET_Michelogram_Normalize...\n");
        
        /* load basic scanner setup */
        setup_scanner();
        
        /* compare number of events with matrix size to check data validity */
        /* expected number of events in file */
        number_of_required_events = get_number_of_events_in_file();
        /* actual number of events in file */
        number_of_events_in_file = mxGetDimensions(prhs[1]);
        /* required size of norm file */
        /* 1) geometric effects */
        required_norm_size = Sinogram.projections*(2*(PET_Scanner.rings+PET_Scanner.v_rings)-1);
        /* 2) crystal interference */
        required_norm_size = required_norm_size + (PET_Scanner.transversal_crystals_per_block+PET_Scanner.v_crystals_between_blocks)*Sinogram.projections;
        /* 3) crystal efficiencies */
        required_norm_size = required_norm_size + (PET_Scanner.crystals_per_ring+PET_Scanner.v_crystals_per_ring)*(PET_Scanner.rings+PET_Scanner.v_rings);
        /* 4) axial effects */
        required_norm_size = required_norm_size + Michelogram.sinograms;
        /* actual norm size */
        norm_size = mxGetDimensions(prhs[2]);
        
        if (number_of_required_events != number_of_events_in_file[0] || required_norm_size != norm_size[0])
        {
            if (number_of_required_events != number_of_events_in_file[0])
            {
                message(0,"mexEMrecon_PET_Michelogram_Normalize: Data dimensions do not match [%d] != [%d]\n",number_of_required_events,number_of_events_in_file[0]);
            }
            if (required_norm_size != norm_size[0])
            {
                message(0,"mexEMrecon_PET_Michelogram_Normalize: Norm dimensions do not match [%d] != [%d]\n",required_norm_size,norm_size[0]);
            }
            
            plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_data = mxGetPr(plhs[0]);
            output_data[0] = -1.0;
        }
        else
        {
            /* get pointer to input data */
            input_data = mxGetData(prhs[1]);
            input_norm = mxGetData(prhs[2]);
            
            /* output */
            plhs[0] = mxCreateNumericMatrix(number_of_required_events,1,mxDOUBLE_CLASS,mxREAL);
            output_data = mxGetPr(plhs[0]);
            
            /* normalize michelogram */
            pet_michelogram_normalize(input_data,input_norm,output_data);
        }
        
        /* release memory */
        release_memory();

        message(2,"mexEMrecon_PET_Michelogram_Normalize...done.\n");
    }
}
