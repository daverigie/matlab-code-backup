/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "mexEMrecon_PET_DrawCrystalGeometry.h"


/*  the gateway routine. */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *output_image,*output_dimensions;
    
    /* check for valid input/output parameter */
    if (nlhs < 1 || nlhs > 2)
    {
        message(0,"mexEMrecon_PET_DrawCrystalGeometry [%s]\n\n",EMRECON_VERSION);
        message(0,"supported scanner: \n%s\n",scanner_info());
    }
    else
    {
        /* transfer parameter from Matlab structure to EMrecon parameter structure */
        ReadFromMatlab(prhs[0]);

        message(2,"mexEMrecon_PET_DrawCrystalGeometry...\n");

        /* initialize pet scanner setup for validity check later on */
        pet_scanner_setup();

        /* load basic scanner setup */
        setup_scanner();

        /* check for valid crystal geometry */
        if (PET_Scanner.axial_blocks == 0)
        {
            message(0,"mexEMrecon_PET_DrawCrystalGeometry: invalid geometry. PET_Scanner.axial_blocks = [%d]\n",PET_Scanner.axial_blocks);
            plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_image = mxGetPr(plhs[0]);
            output_image[0] = -1.0;

            plhs[1] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[1]);
            output_dimensions[0] = -1.0;
        }
        else
        {
            /* output */
            plhs[0] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            output_image = mxGetPr(plhs[0]);

            /* dimensions of the output (might be used to reshape the 1d output vector) */
            plhs[1] = mxCreateNumericMatrix(3,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[1]);
            output_dimensions[0] = (double)Parameter.size_x;
            output_dimensions[1] = (double)Parameter.size_y;
            output_dimensions[2] = (double)Parameter.size_z;

            /* perform DrawCrystalGeometry */
            pet_scanner_draw_crystal_geometry(output_image);
        }

        /* release memory */
        release_memory();

        message(2,"mexEMrecon_PET_DrawCrystalGeometry...done.\n");
    }
}
