function [image] = EMrecon_EM_Step_1D(parm,data,data_fp,startimage)

% EMrecon_EM_Step_1D(parm,data,data_fp,startimage)

if (nargin ~= 4 || nargout ~= 1)
    fprintf('\nEMrecon_EM_Step_1D\n');
    fprintf('Usage: image = EMrecon_EM_Step_1D(parm,data,data_fp,startimage);\n\n');
    mexEMrecon_EM_Step;
    fprintf('\n');
    image = -1;
else
    vfprintf(1,parm,'EMrecon_EM_Step_1D...\n');
    tic;
    image = mexEMrecon_EM_Step(parm,data,data_fp,startimage);
    vfprintf(1,parm,'EMrecon_EM_Step_1D...done. [%f sec]\n',toc);
end

end