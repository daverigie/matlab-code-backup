function [image] = EMrecon_EM_ResoReco_ND(parm,data,data_fp,sensitivity_map,startimage)

% EMrecon_EM_ResoReco_ND(parm,data,data_fp,sensitivity_map,startimage)

if (nargin ~= 5 || nargout ~= 1)
    fprintf('\nEMrecon_EM_ResoReco_ND\n');
    fprintf('Usage: image = EMrecon_EM_ResoReco_ND(parm,data,data_fp,sensitivity_map,startimage);\n\n');
    mexEMrecon_EM_ResoReco;
    fprintf('\n');
    image = -1;
else
    vfprintf(1,parm,'EMrecon_EM_ResoReco_ND...\n');
    tic;
    [image,dims] = mexEMrecon_EM_ResoReco(parm,data,data_fp,sensitivity_map(:),startimage(:));
    if (numel(dims) == 3)
        image = reshape(image,dims(1),dims(2),dims(3));
    end
    vfprintf(1,parm,'EMrecon_EM_ResoReco_ND...done. [%f sec]\n',toc);
end

end