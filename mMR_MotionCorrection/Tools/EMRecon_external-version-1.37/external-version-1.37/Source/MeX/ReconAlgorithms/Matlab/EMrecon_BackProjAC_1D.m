function [bp_image] = EMrecon_BackProjAC_1D(parm,data,image)

% EMrecon_BackProj_1D(parm,data,image)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_BackProjAC_1D\n');
    fprintf('Usage: bp_image = EMrecon_BackProjAC_1D(parm,data,image);\n\n');
    mexEMrecon_BackProjAC;
    fprintf('\n');
    bp_image = -1;
else
    vfprintf(1,parm,'EMrecon_BackProjAC_1D...\n');
    tic;
    bp_image = mexEMrecon_BackProjAC(parm,data,image);
    vfprintf(1,parm,'EMrecon_BackProjAC_1D...done. [%f sec]\n',toc);
end

end