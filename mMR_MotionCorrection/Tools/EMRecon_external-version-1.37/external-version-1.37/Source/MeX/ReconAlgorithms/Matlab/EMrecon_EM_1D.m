function [image] = EMrecon_EM_1D(parm,data,data_fp,sensitivity_map,startimage)

% EMrecon_EM_1D(parm,data,data_fp,sensitivity_map,startimage)

if (nargin ~= 5 || nargout ~= 1)
    fprintf('\nEMrecon_EM_1D\n');
    fprintf('Usage: image = EMrecon_EM_1D(parm,data,data_fp,sensitivity_map,startimage);\n\n');
    mexEMrecon_EM;
    fprintf('\n');
    image = -1;
else
    vfprintf(1,parm,'EMrecon_EM_1D...\n');
    tic;
    image = mexEMrecon_EM(parm,data,data_fp,sensitivity_map,startimage);
    vfprintf(1,parm,'EMrecon_EM_1D...done. [%f sec]\n',toc);
end

end