/*
 * This file is part of the EMrecon software package.
 *
 * The EMrecon software package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * Copyright (C) Thomas Koesters, 2009-2015
 * - emrecon.support@wwu.de
 * - http://emrecon.uni-muenster.de/
 *
 *
 * EMrecon is developed and supported by ...
 *
 * The Collaborative Research Centre 656 Molecular Cardiovascular Imaging - SFB 656 MoBil
 * University of Muenster, Muenster, Germany
 * http://www.sfbmobil.de/
 * SFB 656 MoBil is funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation)
 *
 * Center for Advanced Imaging Innovation and Research - CAI2R
 * New York University, New York, USA
 * http://www.cai2r.net/
 * CAI2R is a National Biomedical Technology Resource Center supported by
 * the National Institute of Biomedical Imaging and Bioengineering (NIBIB)
 *
 * European Institute for Molecular Imaging - EIMI
 * University of Muenster, Muenster, Germany
 * http://www.uni-muenster.de/EIMI/
 *
 * Department of Mathematics and Computer Science
 * University of Muenster, Muenster, Germany
 * http://wwwmath.uni-muenster.de/
 *
 * Permission to use this code for scientific, non-commercial work is granted provided appropriate
 * credit is given. Please use [Koesters et al., "EMrecon: An Expectation Maximization Based Image
 * Reconstruction Framework for Emission Tomography Data", NSS/MIC Conference Record, IEEE, 2011,
 * pp. 4365-4368] for citations and http://emrecon.uni-muenster.de/ for reference and further
 * license information.
 */
#include "mexEMrecon_IP_ComputePath.h"


/*  the gateway routine. */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    long coord;
    const mwSize *coordA_size, *coordB_size;
    double *coordA,*coordB;
    double *output_image,*output_dimensions;
    path_element *Path;
    
    /* check for valid input/output parameter */
    if (nlhs < 1 || nlhs > 2)
    {
        message(0,"mexEMrecon_IP_ComputePath [%s]\n\n",EMRECON_VERSION);
        message(0,"supported scanner: \n%s\n",scanner_info());
    }
    else
    {
        /* transfer parameter from Matlab structure to EMrecon parameter structure */
        ReadFromMatlab(prhs[0]);
        
        message(2,"mexEMrecon_IP_ComputePath...\n");
        
        /* load basic scanner setup */
        setup_scanner();
        
        /* coodinate size */
        coordA_size = mxGetDimensions(prhs[1]);
        coordB_size = mxGetDimensions(prhs[2]);
        
        if (coordA_size[0] != 3 || coordB_size[0] != 3)
        {
            if (coordA_size[0] != 3)
            {
                message(0,"mexEMrecon_IP_ComputePath: Dimensions do not match [%d] != [%d]\n",3,coordA_size[0]);
            }
            if (coordB_size[0] != 3)
            {
                message(0,"mexEMrecon_IP_ComputePath: Dimensions do not match [%d] != [%d]\n",3,coordB_size[0]);
            }
            plhs[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_image = mxGetPr(plhs[0]);
            output_image[0] = -1.0;
            
            plhs[1] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[1]);
            output_dimensions[0] = -1.0;
        }
        else
        {
            /* get pointer to input data */
            coordA = mxGetData(prhs[1]);
            coordB = mxGetData(prhs[2]);
            
            /* output */
            plhs[0] = mxCreateNumericMatrix(Parameter.imagesize,1,mxDOUBLE_CLASS,mxREAL);
            output_image = mxGetPr(plhs[0]);
            
            /* dimensions of the output (might be used to reshape the 1d output vector) */
            plhs[1] = mxCreateNumericMatrix(3,1,mxDOUBLE_CLASS,mxREAL);
            output_dimensions = mxGetPr(plhs[1]);
            output_dimensions[0] = (double)Parameter.size_x;
            output_dimensions[1] = (double)Parameter.size_y;
            output_dimensions[2] = (double)Parameter.size_z;
            
            /* setup Path and initialize siddon */
            setup_siddon();
            Path = (path_element*)(safe_calloc(Parameter.size_x+Parameter.size_y+Parameter.size_z, sizeof(path_element)));

            /* compute path */
            compute_path(coordA[0],coordA[1],coordA[2],coordB[0],coordB[1],coordB[2],Path);

            /* update image */
            for (coord=0;Path[coord].coord!=-1;coord++)
            {
                output_image[Path[coord].coord] += Path[coord].length;
            }

            /* free path */
            free(Path);
        }
        
        /* release memory */
        release_memory();
        
        message(2,"mexEMrecon_IP_ComputePath...done.\n");
    }
}
