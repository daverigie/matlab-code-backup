function [path] = EMrecon_IP_ComputePath_1D(parm,coordA,coordB)

% EMrecon_IP_ComputePath_1D(parm,coordA,coordB)

if (nargin ~= 3 || nargout ~= 1)
    fprintf('\nEMrecon_IP_ComputePath_ND\n');
    fprintf('Usage: path = EMrecon_IP_ComputePath_ND(parm,coordA,coordB);\n\n');
    mexEMrecon_IP_ComputePath;
    fprintf('\n');
    path = -1;
else
    vfprintf(1,parm,'EMrecon_IP_ComputePath_ND...\n');
    tic;
    path = mexEMrecon_IP_ComputePath(parm,coordA(:),coordB(:));
    vfprintf(1,parm,'EMrecon_IP_ComputePath_ND...done. [%f sec]\n',toc);
end

end