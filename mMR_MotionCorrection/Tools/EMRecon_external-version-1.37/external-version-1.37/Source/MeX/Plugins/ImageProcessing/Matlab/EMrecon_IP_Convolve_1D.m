function [new_image] = EMrecon_IP_Convolve_1D(parm,image)

% EMrecon_IP_Convolve_1D(parm,image)

if (nargin ~= 2 || nargout ~= 1)
    fprintf('\nEMrecon_IP_Convolve_1D\n');
    fprintf('Usage: new_image = EMrecon_IP_Convolve_1D(parm,image);\n\n');
    mexEMrecon_IP_Convolve;
    fprintf('\n');
    new_image = -1;
else
    vfprintf(1,parm,'EMrecon_IP_Convolve_1D...\n');
    tic;
    new_image = mexEMrecon_IP_Convolve(parm,image);
    vfprintf(1,parm,'EMrecon_IP_Convolve_1D...done. [%f sec]\n',toc);
end

end