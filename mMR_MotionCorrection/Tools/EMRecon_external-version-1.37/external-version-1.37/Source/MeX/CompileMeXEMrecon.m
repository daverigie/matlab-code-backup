%% clear and close
close all; clear mex; clear; clc;


%% define compile targets
%% scanner setup
% PET
add_scanner_PET_Siemens_Inveon = 1;
add_scanner_PET_Siemens_mMR = 1;

%% recon + tools setup
% Main
compile_mexEMrecon_Version = 1;

% ReconAlgorithms
compile_mexEMrecon_EM = 1;
compile_mexEMrecon_EM_Step = 1;
compile_mexEMrecon_EM_ResoReco = 1;
compile_mexEMrecon_BackProj = 1;
compile_mexEMrecon_BackProjAC = 1;
compile_mexEMrecon_BackProjMask = 1;
compile_mexEMrecon_ForwardProj = 1;
compile_mexEMrecon_ForwardProjAC = 1;
compile_mexEMrecon_ForwardProjMask = 1;

% ScannerGeometries
compile_mexEMrecon_PET_Michelogram_CompressSinogramStack = 1;
compile_mexEMrecon_PET_Michelogram_GetMask = 1;
compile_mexEMrecon_PET_Michelogram_ISSRB = 1;
compile_mexEMrecon_PET_Michelogram_Normalize = 1;
compile_mexEMrecon_PET_Michelogram_AxialWeight = 1;
compile_mexEMrecon_PET_DrawCrystalGeometry = 1;

% ScannerTools
compile_mexEMrecon_PET_Siemens_Inveon_Rebin = 1; % requires add_scanner_PET_Siemens_Inveon = 1
compile_mexEMrecon_PET_Siemens_mMR_Rebin = 1;    % requires add_scanner_PET_Siemens_mMR = 1

% Plugins
compile_ImageProcessingPlugin = 1;

% Plugins: ImageProcessing
if (compile_ImageProcessingPlugin == 1)
    compile_mexEMrecon_IP_ComputePath = 1;
    compile_mexEMrecon_IP_Convolve = 1;
    compile_mexEMrecon_IP_CutImage = 1;
end

% protect (pure) Matlab functions
protect_MatlabEMreconTools = 1;

% if set to 1, all output / log is disabled
protect_scanner_info = 1;


%% set path to corresponding source code and compiler flags
% run SetupMexEMrecon.m to setup path to the source code
run('SetupMeXEMrecon.m');


%% compile mex version of EMrecon using setup from above
run('CompileMeXEMrecon_Main.m');
